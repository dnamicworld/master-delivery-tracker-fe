# Master Delivery Report FE

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Run Project Locally
___

### Requirements
- Node v14 ([nvm recommended](https://github.com/nvm-sh/nvm))
- Yarn `npm i -g`
- Okta access (contact Okta team if needed)

### Steps
1. Create `.env` and copy content from `.env-example`: `cp .env-example .env`
2. Fill `.env` with the required values.
3. Add `.npmrc` with the required values check example
4. Install all dependencies `yarn install`
5. Start project `yarn start`
   
Open http://localhost:3000 to view it in the browser.

## Storybook
___

This uses [Storybook](https://storybook.js.org/) to keep tracked all components available in the application.

### Usage
1. Make sure that you have installed all dependencies with `yarn install`
2. Run `yarn storybook`

Open http://localhost:6006 to view it in the browser.


# Deployments
___

This project uses Amazon S3, Cloudfront and Route 53. You'll find the deployment scripts inside the folder
`./scripts` which contains to main files, `distribution.py` and `invalidate.py`.

- Distribution: Create or store the built project into an S3 bucket. If Cloudfront distribution exists it
  will be used if not will be created.
- Invalidate: It is used to invalidate the Cloudfront cache after each deployment.

Deployment scripts are triggered using [Github Actions](https://github.com/features/actions) workflows. You can see the workflows here:
`.github/workflows`.

### Run / Test Github Actions locally
If you need to run any workflow locally you can use [act](https://github.com/nektos/act) (follow link for further information). 
- Linux / Mac OS ([Homebrew](https://brew.sh)): `brew install act`
- Windows ([Chocolatey](https://chocolatey.org/)): `choco install act-cli`

Visit https://github.com/nektos/act for more installation options.

# Other Scripts

---

* Linting: `yarn lint`
* Autofix linting issues: `yarn lint:fix`
* Test: `yarn test`
* Build: `yarn build`


# Project Libraries

---

* [okta-react](https://github.com/okta/okta-react):This project uses Okta for SSO authentication purposes.
* [Bootstrap 3](https://getbootstrap.com/docs/3.3/): Design system has been designed based on Boostrap 3.
* [React Bootstrap v0.33.1](https://react-bootstrap-v3.netlify.app/): Useful Bootstrap 3 pre-built components.
* [AG Grid](https://www.ag-grid.com/react-data-grid/getting-started/): Used to build the tables.


# Folder Structure

### Top level folders
    .
    ├── build                   # Compiled files
    ├── public                  # Static files
    ├── rules                   # React configuration
    ├── scripts                 # Python deployment scripts
    ├── src                     # Source files
    └── venv                    # Python virtual env

> These are the most relevant top level directories

### Source files
    .
    ├── ...
    ├── src                     # Source files
    │   ├── api                 # Axios configuration
    │   ├── assets              # All assets including images, fonts and sass
    │   ├── components          # Shared custom components specifically for MDR
    │   ├── config              # Configuration files
    │   ├── context             # React context providers
    │   ├── DS                  # Design system folder, here goes all the reusable components between apps.
    │   ├── hooks               # React custom hooks
    │   ├── models              # Models including which comes from the API and 
    │   ├── pages               # Pages components
    │   ├── stories             # Storybook configuration
    │   ├── types               # Typescript modules
    │   └── utils               # Utilitites
    └── ...
### Component folder convention
    .
    ├── ...
    ├── src                                                 # Source files
    │   └── DS                                              # Design system folder, here goes all the reusable components between apps.
    │       └── components                                  # Components 
    │           ├── bootstrpap                              # Source files
    │           ├── custom                                  # Source files
    │           │   ├── MyComponent                         # Use Pascal Case for component's folder name
    │           │   │   ├── MyComponent.controller.ts       # Controllers used by the component
    │           │   │   ├── MyComponent.model.ts            # Models used by the component
    │           │   │   ├── MyComponent.module.scss         # Scoped sass styles
    │           │   │   ├── MyComponent.tsx                 # React component
    │           │   │   └── index.ts                        # Export component and any other variable here
    │           │   └── index.ts                            # Export all components here
    │           └── models                                  # Design system's models
    └── ...
> Notice that not all the files are required, it will depend on the complexity of the component.
