import logging
import os
from utils import cloudfront, s3

logging.getLogger().setLevel(logging.INFO)

bucket_name = os.getenv('AWS_S3_BUCKET_NAME')
region = os.getenv('AWS_REGION')
oai = os.getenv('AWS_OAI')

bucket_created_or_exists = s3.create_bucket(bucket_name, region)
block_public_access = s3.block_public_access(bucket_name)
website_created = s3.set_s3_website(bucket_name)

if bucket_created_or_exists and block_public_access and website_created:
  created_policy= s3.set_bucket_policy(bucket_name, oai)
  if created_policy:
    dist_id = cloudfront.get_distribution_id(bucket_name)
    if dist_id is None:
      created_distribution = cloudfront.create_distribution(bucket_name, oai)
      logging.info('created_distribution: ' + str(created_distribution))
