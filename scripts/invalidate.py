import logging
import os
from utils import cloudfront

logging.getLogger().setLevel(logging.INFO)

bucket_name = os.getenv('AWS_S3_BUCKET_NAME')

cloudfront.create_invalidation(bucket_name)
