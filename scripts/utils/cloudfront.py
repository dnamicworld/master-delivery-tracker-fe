import logging
import boto3
import time
import random
import string
from botocore.exceptions import ClientError

cloudfront_client = boto3.client('cloudfront')

def create_distribution(bucket_name, oai):
  try:
    distribution_config = {
      'Comment': 'Master Delivery Report: ' + bucket_name,
      'Enabled': True,
      'CallerReference': str(time.time()),
      'DefaultRootObject': 'index.html',
      'HttpVersion': 'http2',
      'IsIPV6Enabled': True,
      'Origins': {
        'Quantity': 1,
        'Items': [
          {
            'Id': '1',
            'DomainName': bucket_name + '.s3.amazonaws.com',
            'S3OriginConfig': {
              'OriginAccessIdentity': 'origin-access-identity/cloudfront/'  + oai
            }
          }
        ],
      },
      'DefaultCacheBehavior': {
        'TargetOriginId': '1',
        'ViewerProtocolPolicy': 'redirect-to-https',
        'TrustedSigners': {
          'Quantity': 0,
          'Enabled': False
        },
        'ForwardedValues': {
          'Cookies': {'Forward':'all'},
          'Headers': { 'Quantity': 0 },
          'QueryString': False,
          'QueryStringCacheKeys': { 'Quantity': 0 },
        },
        'MinTTL': 1000,
      },
      'CustomErrorResponses': {
        'Quantity': 2,
        'Items': [
          {
            'ErrorCode': 403,
            'ResponsePagePath': '/index.html',
            'ResponseCode': '200',
            'ErrorCachingMinTTL': 123
          },
          {
            'ErrorCode': 404,
            'ResponsePagePath': '/index.html',
            'ResponseCode': '200',
            'ErrorCachingMinTTL': 123
          },
        ]
      },
    }
    response = cloudfront_client.create_distribution(DistributionConfig = distribution_config)
    print('create_distribution response', response)

  except ClientError as e:
    logging.error(e)
    return False

  return True

def create_invalidation(bucket_name):
  try:
    dist_id = get_distribution_id(bucket_name)
    caller_reference = ''.join(random.choice(string.ascii_lowercase) for i in range(10))
    response = cloudfront_client.create_invalidation(
      DistributionId=dist_id,
      InvalidationBatch={
        'Paths': {
          'Quantity': 1,
          'Items': ['/*']
        },
        'CallerReference': caller_reference
      }
    )
    print('create_invalidation response', response)
  except ClientError as e:
    logging.error(e)

def get_distributions():
  try:
    response = cloudfront_client.list_distributions()
    distributions_list = response['DistributionList']
    items = distributions_list['Items'] if 'Items' in distributions_list else []
    return items
  except ClientError as e:
    logging.error(e)
    return e


def get_distribution_id(bucket_name):
  distribution_id = None
  distribution_items = get_distributions()

  for dist in distribution_items:
    if distribution_id is None:
      for item in dist['Origins']['Items']:
        if item['DomainName'] == bucket_name + '.s3.amazonaws.com':
          print('Cloudfront dist', dist)
          distribution_id = dist['Id']
          break
    else:
      break

  return distribution_id
