import logging
import boto3
import json
from botocore.exceptions import ClientError

s3_client = boto3.client('s3')

def create_bucket(bucket_name, region):
  try:
    if bucket_exists(bucket_name):
      logging.info('Bucket already exists')
    else:
      if region is None or region == 'us-east-1':
        response = s3_client.create_bucket(
          Bucket=bucket_name
        )
      else:
        response = s3_client.create_bucket(
          Bucket=bucket_name,
          CreateBucketConfiguration={'LocationConstraint': region}
        )
      logging.info('create_bucket response: ' + str(response))
  except ClientError as e:
    logging.error('create_bucket error: ' + str(e))
    return False
  return True

def bucket_exists(bucket_name):
  s3 = boto3.resource('s3')
  return s3.Bucket(bucket_name) in s3.buckets.all()

def set_bucket_policy(bucket_name, oai):
  try:
    bucket_policy = {
      "Version": "2008-10-17",
      "Statement": [
        {
          "Effect": "Allow",
          "Principal": {
            "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity " + oai
          },
          "Action": "s3:GetObject",
          "Resource": "arn:aws:s3:::" + bucket_name + "/*"
        }
      ]
    }
    bucket_policy = json.dumps(bucket_policy)
    response = s3_client.put_bucket_policy(Bucket=bucket_name, Policy=bucket_policy)
    logging.info('set_bucket_policy response: ' + str(response))
  except ClientError as e:
    logging.error('set_bucket_policy error:' + str(e))
    return False
  return True

def block_public_access(bucket_name):
  try:
    public_access_block_configuration = {
      'BlockPublicAcls': True,
      'IgnorePublicAcls': True,
      'BlockPublicPolicy': True,
      'RestrictPublicBuckets': True
    }
    response = s3_client.put_public_access_block(
      Bucket=bucket_name,
      PublicAccessBlockConfiguration=public_access_block_configuration
    )
    logging.info('block_public_access response: ' + str(response))
  except ClientError as e:
    logging.error('block_public_access error: ' + str(e))
    return False
  return True

def set_s3_website(bucket_name):
  try:
    website_configuration = {
      'ErrorDocument': {
        'Key': 'index.html'
      },
      'IndexDocument': {
        'Suffix': 'index.html'
      }
    }
    response = s3_client.put_bucket_website(
      Bucket=bucket_name,
      WebsiteConfiguration=website_configuration
    )
    logging.info('configure_bucket_as_a_website response: ' + str(response))
  except ClientError as e:
    logging.error('block_public_access error: ' + str(e))
    return False
  return True
