import 'jest-canvas-mock'
import '@testing-library/jest-dom'
import {Action} from 'history'
import $ from 'jquery'

// eslint-disable-next-line react/jsx-props-no-spreading
const MockResponsiveContainer = (props: any) => <div {...props} />

global.window.$ = $
global.window.$.fn.collapse = jest.fn(() => $())

jest.setTimeout(5000)

jest.mock('recharts', () => ({
  // @ts-ignore
  ...jest.requireActual('recharts'),
  ResponsiveContainer: MockResponsiveContainer,
}))

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    replace: jest.fn(),
    length: 0,
    location: {
      pathname: '',
      search: '',
      state: '',
      hash: '',
    },
    action: 'REPLACE' as Action,
    push: jest.fn(),
    go: jest.fn(),
    goBack: jest.fn(),
    goForward: jest.fn(),
    block: jest.fn(),
    listen: jest.fn(),
    createHref: jest.fn(),
  }),
}))

jest.mock('@okta/okta-react', () => ({
  useOktaAuth: () => ({
    authState: {},
    authService: {},
    oktaAuth: {
      signInWithRedirect: () => new Promise(() => {}),
      getUser: () =>
        new Promise(() => ({given_name: 'Test', family_name: 'User'})),
    },
  }),
}))
