import {GridApi} from 'ag-grid-community'
import {
  createContext,
  Dispatch,
  useContext,
  useState,
  SetStateAction,
} from 'react'

export type TableValue = {
  gridApi: GridApi | null
  setGridApi: Dispatch<SetStateAction<GridApi | null>>
}

export const TableContext = createContext<TableValue | undefined>(undefined)

export const TableContextProvider = ({children}: {children: JSX.Element}) => {
  const [gridApi, setGridApi] = useState<GridApi | null>(null)
  return (
    <TableContext.Provider
      value={{
        gridApi,
        setGridApi,
      }}
    >
      {children}
    </TableContext.Provider>
  )
}

export default function useTable() {
  const context = useContext(TableContext)
  if (context === undefined) {
    throw new Error('useTable must be used within a TableContext')
  }
  return context
}
