import {Search} from 'history'

import {
  createContext,
  Dispatch,
  SetStateAction,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'

import {useHistory, useParams} from 'react-router-dom'

import {mutate} from 'swr'

import {useAlertDisplay} from '../context/alert'

import {useModalDisplay} from '../context/modal'
import {AssetType, DeliveryStatusActions, Product, Report} from '../models'
import {AssetOption} from '../models/AssetOption'
import {
  cancelEditing,
  DetailsRouteParams,
  ProductDetailState,
  ProductPageInitialState,
} from '../pages/shared/ProductDetails'
import {buildProductRoute} from '../pages/shared/ProductDetails/ProductDetails.controller'
import {
  ALBUM_COVER_PLACEHOLDER,
  API_ROUTES,
  MODALS,
  NEW_PRODUCT,
  ROUTES,
} from '../utils/constants'
import {
  cloneDeep,
  FIELD_FE_SEPARATOR,
  getQueryParam,
  isEmpty,
  isEqual,
  isNumber,
  onFormOperationProductSave,
} from '../utils/helpers'

import useAllOptions from './data/useAllOptions'
import useAssetTypes from './data/useAssetTypes'
import useProducts from './data/useProducts'
import useReports from './data/useReports'

export type ProductDetailsValue = {
  assetTypes: AssetType[]
  isNewProduct: boolean
  getProcessedAssets: () => {
    customAssets: AssetType[]
    filteredAssets: AssetType[]
    optionalAssets: AssetType[]
  }
  handleEditTable: (tracks?: Product['tracks']) => void
  handleEditForm: () => void
  noChanges: boolean
  onFieldEditing: (name: string) => void
  onFormCancel: () => void
  onFormSave: (product?: Product, continueEditing?: boolean) => void
  productType?: string
  productDetailState: ProductDetailState
  report?: Report
  setProductDetailState: Dispatch<SetStateAction<ProductDetailState>>
  updateProduct: (name: string, value: any) => void
  updateState: (name: string, value: any) => void
  removeCoverArtLink: () => void
  albumCover: {
    src: string
    setImg: Dispatch<SetStateAction<string>>
  }
  isLoading: boolean
  fromSave: boolean
  toInitial: () => void
  options: AssetOption[]
}

export const ProductDetailsContext = createContext<
  ProductDetailsValue | undefined
>(undefined)

const getReportId = (search: Search) => {
  const reportIdParam = getQueryParam(search, 'reportId')

  return reportIdParam &&
    !Array.isArray(reportIdParam) &&
    isNumber(reportIdParam)
    ? Number(reportIdParam)
    : undefined
}

const getImageSrc = (upc?: string | null) => {
  let src = ALBUM_COVER_PLACEHOLDER

  if (upc) {
    const upcParts = upc.split(FIELD_FE_SEPARATOR)
    if (upcParts.length) {
      src = `/albums/${upcParts[0]}.png`
    }
  }
  return src
}

export const ProductDetailsContextProvider = ({
  children,
}: {
  children: JSX.Element
}) => {
  const history = useHistory()
  const {productId} = useParams<DetailsRouteParams>()
  const {
    location: {search},
  } = history
  const {displayModal} = useModalDisplay()
  const [report, setReport] = useState<Report | undefined>(undefined)
  const [albumCoverSrc, setImg] = useState<string>('')
  const [stateAssetTypes, setStateAssetTypes] = useState<AssetType[]>([])
  const [fromSave, setFromSave] = useState(false)

  const productType = report?.listViewType
  const isNewProduct = isEmpty(productId)
  const reportId = getReportId(search)
  const editingURLParam = getQueryParam(search, 'editing')
  const {products, isLoading: isLoadingProducts} = useProducts(
    {id: String(productId)},
    () => productId !== undefined,
  )
  const {options = []} = useAllOptions()

  const {assetTypes: assetTypesResponse, isLoading: isLoadingAssetTypes} =
    useAssetTypes()

  const {reports, isLoading: isLoadingReports} = useReports(
    {
      id: String(isNewProduct ? reportId : (products as Product)?.reportId),
    },
    () => (isNewProduct && reportId !== undefined) || !isEmpty(products),
  )

  const [productDetailState, setProductDetailState] =
    useState<ProductDetailState>({
      invalid: false,
      editing: Boolean(editingURLParam),
      ...ProductPageInitialState,
    })

  const {product, initial} = productDetailState
  const {display} = useAlertDisplay()

  useEffect(() => {
    const reportToSet = Array.isArray(reports) ? undefined : reports
    setReport(reportToSet)
  }, [reports])

  useEffect(() => {
    const assetTypes = Array.isArray(assetTypesResponse)
      ? assetTypesResponse
      : []
    setStateAssetTypes(assetTypes)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(assetTypesResponse)])

  useEffect(() => {
    setImg(getImageSrc(product?.upc))
  }, [product?.upc])
  const defaultAssetType = stateAssetTypes.filter(
    (asset) => asset.optional !== true,
  )
  const productAssetTracks = defaultAssetType.map((asset) => {
    const defaultOption = asset.assetOptions.find(
      (item) => item.display === asset.defaultValue,
    )
    return {
      assetTypeId: asset.id,
      deliveryStatus:
        defaultOption?.deliveryStatus ?? DeliveryStatusActions.NOT_REQUIRED,
      sampleRate: null,
      bitDepth: null,
      selection: asset.defaultValue,
      assetType: asset.shortName,
    }
  })
  const productInitialValue = useMemo(
    () =>
      isNewProduct && productType
        ? {
            ...NEW_PRODUCT(productAssetTracks),
            productType,
            reportId: Number(reportId),
          }
        : undefined,
    [isNewProduct, productType, productAssetTracks, reportId],
  )

  useEffect(() => {
    if (!Array.isArray(products)) {
      setProductDetailState((prevAlbumState) => ({
        ...cloneDeep(prevAlbumState),
        initial: products ?? productInitialValue,
        product: products ?? productInitialValue,
        editing: Boolean(editingURLParam),
        invalid: false,
      }))
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(products), JSON.stringify(productInitialValue)])

  useEffect(() => {
    history.push({
      search,
    })
  }, [history, search])

  const noChanges = useMemo(() => isEqual(product, initial), [product, initial])

  const onFormCancelSuccess = () => {
    setProductDetailState((prevProduct) => ({
      ...prevProduct,
      product: prevProduct.initial,
      ...cancelEditing,
      invalid: false,
    }))
  }

  const showConfirmation = () => {
    displayModal(MODALS.DISCARD_MODAL, MODALS.TITLES.WARNING, {
      actionName: MODALS.ACTIONS.DISCARD,
      onActionClick: onFormCancelSuccess,
    })
  }

  const updateState = (name: string, value: any) => {
    setProductDetailState((prevState) => ({
      ...cloneDeep(prevState),
      [name]: value,
    }))
  }

  const removeCoverArtLink = () => {
    setProductDetailState((prevState) => ({
      ...cloneDeep(prevState),
      product: {
        ...(cloneDeep(prevState.product ?? {}) as Product),
        coverArtLink: undefined,
      },
      initial: {
        ...(cloneDeep(prevState.initial ?? {}) as Product),
        coverArtLink: undefined,
      },
    }))
  }

  const updateProduct = useCallback(
    (name: string, value: any) => {
      updateState('product', {
        ...cloneDeep(product),
        [name]: value,
      })
    },
    [product],
  )

  const onFormSave = useCallback(
    async (productToSave?: Product, continueEditing?: boolean) => {
      const title = isNewProduct ? 'Product Saved' : 'Product Updated'
      setFromSave(true)
      const productIdPath = isNewProduct ? '' : `/${productToSave?.id}`
      const URL = `${API_ROUTES.PRODUCTS}${productIdPath}`

      const validateProduct = (toValidate?: Product): boolean => {
        const validTracks = !toValidate?.tracks?.find((track) => !track.title)
        return !!toValidate?.title && !!toValidate?.artist && validTracks
      }

      const productValid = validateProduct(productToSave)
      try {
        if (productToSave && productValid) {
          const response = await onFormOperationProductSave(
            URL,
            productToSave,
            isNewProduct,
          )

          if (response && response.id) {
            const productPath = buildProductRoute(
              response.productType,
              response.id,
            )
            if (isNewProduct) {
              history.push({pathname: productPath})
            } else {
              await mutate(`/${URL}`, response)
              if (response?.productType !== initial?.productType) {
                history.push({
                  pathname: productPath,
                })
              } else {
                const editingValues = continueEditing
                  ? {
                      editing: productDetailState.editing,
                      editingOnlyTable: productDetailState.editingOnlyTable,
                    }
                  : cancelEditing
                setProductDetailState((prevProduct) => ({
                  ...prevProduct,
                  product: response,
                  initial: response,
                  editingField: prevProduct.editingField
                    ? ''
                    : prevProduct.editingField,
                  invalid: false,
                  ...editingValues,
                }))
              }
            }
            setFromSave(false)
            display(title)
          }
        } else {
          updateState('invalid', !productValid)
        }
      } catch (err) {
        // eslint-disable-next-line no-console
        console.log('ERROR UPDATING...', productToSave)
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [history, initial, isNewProduct],
  )

  const onFieldEditing = (name: string) => {
    updateState('editingField', name)
  }

  const getProcessedAssets = useCallback(() => {
    const defaultAssets = {
      customAssets: [],
      filteredAssets: [],
      optionalAssets: [],
    }
    return (
      stateAssetTypes?.reduce(
        (
          results: {
            customAssets: AssetType[]
            filteredAssets: AssetType[]
            optionalAssets: AssetType[]
          },
          asset: AssetType,
        ) => {
          const result = cloneDeep(results)
          const key = asset.excludeFromTotal ? 'customAssets' : 'filteredAssets'
          result[key].push(asset)
          if (asset.optional) {
            result.optionalAssets.push(asset)
          }
          return result
        },
        defaultAssets,
      ) ?? defaultAssets
    )
  }, [stateAssetTypes])

  const onFormCancel = () => {
    const discardHandler = () => {
      if (noChanges) {
        setProductDetailState((prevProductDetail) => ({
          ...cloneDeep(prevProductDetail),
          ...cancelEditing,
          invalid: false,
        }))
      } else {
        showConfirmation()
      }
    }

    if (isNewProduct) {
      const hasNoRoutes = history.action !== 'PUSH'
      if (hasNoRoutes) {
        history.push(ROUTES.HOME)
      } else {
        history.go(-1)
      }
    } else {
      discardHandler()
    }
  }

  const handleEdit = (editField: string, tracks?: Product['tracks']) => {
    setProductDetailState((prevProductDetail) => ({
      ...cloneDeep(prevProductDetail),
      product: {
        ...(cloneDeep(prevProductDetail.product ?? {}) as Product),
        tracks: tracks ?? prevProductDetail.product?.tracks,
      },
      [editField]: true,
      invalid: false,
    }))
  }
  const toInitial = () => {
    setProductDetailState((prevProduct) => ({
      ...prevProduct,
      product: prevProduct.initial,
      initial: prevProduct.initial,
    }))
  }
  const handleEditTable = (tracks?: Product['tracks']) =>
    handleEdit('editingOnlyTable', tracks)

  const handleEditForm = () => handleEdit('editing')

  return (
    <ProductDetailsContext.Provider
      value={{
        assetTypes: stateAssetTypes,
        getProcessedAssets,
        handleEditTable,
        handleEditForm,
        isNewProduct,
        noChanges,
        onFieldEditing,
        onFormCancel,
        onFormSave,
        productDetailState,
        productType,
        report,
        setProductDetailState,
        updateProduct,
        updateState,
        removeCoverArtLink,
        albumCover: {
          src: albumCoverSrc,
          setImg,
        },
        isLoading: isLoadingProducts && isLoadingReports && isLoadingAssetTypes,
        fromSave,
        toInitial,
        options,
      }}
    >
      {children}
    </ProductDetailsContext.Provider>
  )
}

export default function useProductDetails() {
  const context = useContext(ProductDetailsContext)
  if (context === undefined) {
    throw new Error(
      'useProductDetails must be used within a ProductDetailsContext',
    )
  }
  return context
}
