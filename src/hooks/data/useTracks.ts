import useSWR from 'swr'

import {AssetTrack} from '../../models'
import {API_ROUTES} from '../../utils/constants'
import {getApiURL, getEndpointURL, QueryString} from '../../utils/helpers'

import {HookResponse, PaginationResponse, QueryHook} from './models'

interface AssetTracksQueryParams extends QueryString {
  years?: string[]
  labels?: string[]
  assetsTypes?: string[]
  artists?: string[]
  marketingLabels?: string[]
  selections?: string[]
  sort?: string
}
interface AssetTrackResponse extends HookResponse, PaginationResponse {
  assetTracks: AssetTrack[]
}

const useAssetTracks: QueryHook<AssetTrackResponse> = (
  queryParams: AssetTracksQueryParams = {},
  shouldFetch?: () => boolean,
) => {
  const {data, error, mutate} = useSWR(
    getApiURL(
      getEndpointURL(API_ROUTES.ASSET_TRACKS, queryParams),
      shouldFetch,
    ),
  )

  return {
    assetTracks: data?.data,
    count: data?.count,
    pages: data?.pages,
    pageSize: data?.pageSize,
    pageNumber: data?.pageNumber,
    isLoading: !error,
    isError: error,
    mutate,
  }
}

export default useAssetTracks
