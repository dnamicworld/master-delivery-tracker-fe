import useSWR from 'swr'

import {AssetType} from '../../models'
import {API_ROUTES} from '../../utils/constants'

import {
  changeColorToLower,
  cloneDeep,
  getApiURL,
  getEndpointURL,
  QueryString,
} from '../../utils/helpers'

import {HookResponse} from './models'

interface AssetTypesResponse extends HookResponse {
  assetTypes?: AssetType | AssetType[]
}

interface AssetTypesParams {
  id?: number
  filterDefaultAssets?: boolean
}

const useAssetTypes = (
  queryParams: AssetTypesParams = {},
  shouldFetch?: () => boolean,
): AssetTypesResponse => {
  const {filterDefaultAssets, ...remainingParams} = queryParams
  const {data, error, mutate} = useSWR(
    getApiURL(
      getEndpointURL(API_ROUTES.ASSET_TYPES, remainingParams as QueryString),
      shouldFetch,
    ),
  )

  let assetTypes: AssetType[] | AssetType

  if (Array.isArray(data)) {
    const dataFiltered = filterDefaultAssets
      ? data?.filter((asset: AssetType) => !asset.excludeFromTotal)
      : data

    assetTypes = dataFiltered.map((assetType: AssetType) => {
      const assetTypeCopy = cloneDeep(assetType)
      assetTypeCopy.assetOptions =
        assetTypeCopy.assetOptions.map(changeColorToLower)
      return assetTypeCopy
    })
  } else {
    assetTypes = data?.assetOptions?.map(changeColorToLower)
  }

  return {
    assetTypes,
    isLoading: !error && !assetTypes,
    isError: error,
    mutate,
  }
}

export default useAssetTypes
