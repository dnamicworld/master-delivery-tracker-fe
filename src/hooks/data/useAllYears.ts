import useSWR from 'swr'

import {appFetcher} from '../../config/axios'

import {API_ROUTES} from '../../utils/constants'

import {getApiURL, getEndpointURL, QueryString} from '../../utils/helpers'

import {HookResponse, QueryHook} from './models'

interface YearRangeResponse extends HookResponse {
  allYears?: number[]
}

interface YearRangeParams extends QueryString {
  labelId?: string
}

const useAllYears: QueryHook<YearRangeResponse> = (
  queryParams: YearRangeParams = {},
  shouldFetch?: () => boolean,
) => {
  const {data, error, mutate} = useSWR(
    getApiURL(getEndpointURL(API_ROUTES.ALL_YEARS, queryParams), shouldFetch),
    appFetcher,
  )

  return {
    allYears: data,
    isLoading: !error && !data,
    isError: error,
    mutate,
  }
}

export default useAllYears
