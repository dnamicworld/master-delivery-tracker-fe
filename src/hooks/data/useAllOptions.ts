import useSWR from 'swr'

import {AssetOption} from '../../models'
import {API_ROUTES} from '../../utils/constants'

import {
  changeColorToLower,
  getApiURL,
  getEndpointURL,
  QueryString,
} from '../../utils/helpers'

import {HookResponse} from './models'

interface AllOptionsResponse extends HookResponse {
  options?: AssetOption[]
}

const useAllOptions = (shouldFetch?: () => boolean): AllOptionsResponse => {
  const {data, error, mutate} = useSWR(
    getApiURL(
      getEndpointURL(API_ROUTES.ALL_OPTIONS, {} as QueryString),
      shouldFetch,
    ),
  )

  const options = data?.map(changeColorToLower)

  return {
    options,
    isLoading: !error && !data,
    isError: error,
    mutate,
  }
}

export default useAllOptions
