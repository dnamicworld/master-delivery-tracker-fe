import useSWR from 'swr'

import {API_ROUTES} from '../../utils/constants'
import {
  isEmpty,
  getApiURL,
  QueryString,
  getEndpointURL,
} from '../../utils/helpers'

import {QueryHook} from './models'

import {addAdditionalPropsToProduct, ProductsResponse} from './useProducts'

interface SearchProductsQueryParams extends QueryString {
  admin?: string
  artist?: string
  exact?: string
  labels?: string[]
  projectTitle?: string
  phrase?: string
  trackTitle?: string
  id?: string
}
const useSearchProducts: QueryHook<ProductsResponse> = (
  queryParams: SearchProductsQueryParams = {},
  shouldFetch?: () => boolean,
) => {
  const {data, error, mutate} = useSWR(
    getApiURL(
      getEndpointURL(API_ROUTES.PRODUCTS_SEARCH, queryParams),
      shouldFetch,
    ),
  )

  const products: ProductsResponse['products'] = data
    ? addAdditionalPropsToProduct(data)
    : data

  return {
    products,
    isLoading: !error && isEmpty(products),
    isError: error,
    mutate,
  }
}

export default useSearchProducts
