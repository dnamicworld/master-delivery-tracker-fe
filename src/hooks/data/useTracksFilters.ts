import useSWR from 'swr'

import {API_ROUTES} from '../../utils/constants'
import {getApiURL} from '../../utils/helpers'

import {HookResponse} from './models'

export interface AssetTracksFiltersResponse extends HookResponse {
  marketingLabels: string[]
  artists: string[]
  selections: string[]
}

const useAssetTracksFilters = (
  shouldFetch?: () => boolean,
): AssetTracksFiltersResponse => {
  const {data, error, mutate} = useSWR(
    getApiURL(API_ROUTES.ASSET_TRACKS_FILTERS, shouldFetch),
  )
  return {
    ...data,
    isLoading: !error,
    isError: error,
    mutate,
  }
}

export default useAssetTracksFilters
