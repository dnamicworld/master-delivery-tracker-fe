import useSWR from 'swr'

import {AssetTrackData} from '../../pages/AssetReport'

import {API_ROUTES} from '../../utils/constants'
import {getApiURL, getEndpointURL, QueryString} from '../../utils/helpers'

import {HookResponse} from './models'

interface AssetTracksDataQueryParams extends QueryString {
  years?: string[]
  labels?: string[]
  assetsTypes?: string[]
  artists?: string[]
  marketingLabels?: string[]
  selections?: string[]
}

interface AssetTrackDataResponse extends HookResponse {
  assetTracksData: AssetTrackData[]
}

const useAssetTracksData = (
  queryParams: AssetTracksDataQueryParams = {},
  shouldFetch?: () => boolean,
): AssetTrackDataResponse => {
  const {data, error, mutate} = useSWR(
    getApiURL(
      getEndpointURL(API_ROUTES.ASSET_TRACKS_DATA, queryParams),
      shouldFetch,
    ),
  )
  const assetTracksData: AssetTrackDataResponse['assetTracksData'] = data
  return {
    assetTracksData,
    isLoading: !error,
    isError: error,
    mutate,
  }
}

export default useAssetTracksData
