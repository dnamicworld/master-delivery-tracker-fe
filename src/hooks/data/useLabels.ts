import useSWR from 'swr'

import {Label} from '../../models'

import {API_ROUTES} from '../../utils/constants'

import {getApiURL, getEndpointURL, QueryString} from '../../utils/helpers'

import {HookResponse, QueryHook} from './models'

interface LabelsResponse extends HookResponse {
  labels?: Label | Label[]
}

interface LabelQueryParams extends QueryString {
  id?: number
}

const useLabels: QueryHook<LabelsResponse> = (
  queryParams: LabelQueryParams = {},
  shouldFetch?: () => boolean,
) => {
  const {data, error, mutate} = useSWR(
    getApiURL(getEndpointURL(API_ROUTES.LABELS, queryParams), shouldFetch),
  )

  return {
    labels: data,
    isLoading: !error && !data,
    isError: error,
    mutate,
  }
}

export default useLabels
