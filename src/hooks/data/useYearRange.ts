import useSWR from 'swr'

import {API_ROUTES} from '../../utils/constants'

import {getApiURL, getEndpointURL, QueryString} from '../../utils/helpers'

import {HookResponse, QueryHook} from './models'

interface YearRangeResponse extends HookResponse {
  max?: number
  min?: number
}

interface YearRangeParams extends QueryString {
  id?: number
}
// {{baseUrl}}/v1/reports/years
const useYearRange: QueryHook<YearRangeResponse> = (
  queryParams: YearRangeParams = {},
  shouldFetch?: () => boolean,
) => {
  const {id} = queryParams
  const route = id
    ? getEndpointURL(API_ROUTES.PRODUCTS, queryParams)
    : `/${API_ROUTES.REPORTS}`
  const {data, error, mutate} = useSWR(
    getApiURL(`${route}/${API_ROUTES.YEARS_RANGE}`, shouldFetch),
  )

  return {
    ...(data ?? {}),
    isLoading: !error && !data,
    isError: error,
    mutate,
  }
}

export default useYearRange
