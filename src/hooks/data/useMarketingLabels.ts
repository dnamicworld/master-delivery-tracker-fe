import useSWR from 'swr'

import {API_ROUTES} from '../../utils/constants'

import {getApiURL, getEndpointURL, QueryString} from '../../utils/helpers'

import {HookResponse, QueryHook} from './models'

interface LabelsResponse extends HookResponse {
  marketingLabels?: string[]
}

interface LabelQueryParams extends QueryString {
  id?: number
}

const useMarketingLabels: QueryHook<LabelsResponse> = (
  queryParams: LabelQueryParams = {},
  shouldFetch?: () => boolean,
) => {
  const {data, error, mutate} = useSWR(
    getApiURL(
      `${getEndpointURL(API_ROUTES.REPORTS, queryParams)}/${
        API_ROUTES.MARKETING_LABELS
      }`,
      shouldFetch,
    ),
  )

  return {
    marketingLabels: data,
    isLoading: !error && !data,
    isError: error,
    mutate,
  }
}

export default useMarketingLabels
