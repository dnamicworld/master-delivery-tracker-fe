import {KeyedMutator} from 'swr'

import {QueryString} from '../../../utils/helpers'

export interface HookResponse {
  isLoading: boolean
  isError: boolean
  mutate: KeyedMutator<any>
}

export type QueryHook<T extends HookResponse> = (
  queryParams?: QueryString,
  shouldFetch?: () => boolean,
) => T
