export interface PaginationParams {
  page?: number
  size?: number
}

export interface PaginationResponse {
  count?: number
  pages?: number
  pageSize?: number
  pageNumber?: number
}
