import useSWR from 'swr'

import {Product} from '../../models'

import {API_ROUTES} from '../../utils/constants'
import {
  replaceSeparators,
  cloneDeep,
  getApiURL,
  getEndpointURL,
  QueryString,
} from '../../utils/helpers'

import {HookResponse, QueryHook} from './models'

export interface ProductsResponse extends HookResponse {
  products?: Product[] | Product
}

export const getProductMarketingLabel = (products: Product[]): string[] =>
  products.reduce((prev: string[], {marketingLabel}) => {
    const prevCopy = [...cloneDeep(prev)]
    if (marketingLabel && !prevCopy.includes(marketingLabel)) {
      prevCopy.push(marketingLabel)
    }
    return prevCopy
  }, [])

interface ProductQueryParams extends QueryString {
  id?: string
  reportId?: string
}

const buildAdditionalProduct = (product: Product) => {
  const productCopy = cloneDeep(product)
  return {
    ...productCopy,
    upc: replaceSeparators(productCopy?.upc ?? ''),
    selectionNumber: replaceSeparators(productCopy?.selectionNumber ?? ''),
  }
}

export const addAdditionalPropsToProduct = (productsRaw: Product[] | Product) =>
  Array.isArray(productsRaw)
    ? (productsRaw.map((product) =>
        buildAdditionalProduct(product),
      ) as Product[])
    : buildAdditionalProduct(productsRaw)

const useProducts: QueryHook<ProductsResponse> = (
  queryParams: ProductQueryParams = {},
  shouldFetch?: () => boolean,
) => {
  const {data, error, mutate} = useSWR(
    getApiURL(getEndpointURL(API_ROUTES.PRODUCTS, queryParams), shouldFetch),
  )

  const products: ProductsResponse['products'] = data
    ? addAdditionalPropsToProduct(data)
    : data

  return {
    products,
    isLoading: !!shouldFetch?.() && !error && products === undefined,
    isError: error,
    mutate,
  }
}

export default useProducts
