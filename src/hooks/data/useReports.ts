import useSWR from 'swr'

import {Report} from '../../models'
import {API_ROUTES} from '../../utils/constants'

import {getApiURL, getEndpointURL, QueryString} from '../../utils/helpers'

import {HookResponse, PaginationResponse, QueryHook} from './models'

interface ReportsResponse extends HookResponse, PaginationResponse {
  reports?: Report[] | Report
}

export interface ReportQueryParams extends QueryString {
  releaseTypes?: string
  labelIds?: string[]
  years?: string[]
}

const useReports: QueryHook<ReportsResponse> = (
  queryParams: ReportQueryParams = {},
  shouldFetch?: () => boolean,
) => {
  const {id} = queryParams
  const {data, error, mutate} = useSWR(
    getApiURL(getEndpointURL(API_ROUTES.REPORTS, queryParams), shouldFetch),
  )

  return {
    ...(id
      ? {reports: data}
      : {
          reports: data?.data,
          count: data?.count,
          pages: data?.pages,
          pageSize: data?.pageSize,
          pageNumber: data?.pageNumber,
        }),
    isLoading: !error && !data,
    isError: error,
    mutate,
  }
}

export default useReports
