import {useEffect, useState} from 'react'
import {useHistory} from 'react-router-dom'

import {NoResults, PaginationOptions} from '../../DS/components'
import {
  DB_SOURCE,
  SEARCH_SOURCE,
} from '../../components/Search/SearchComponent.controller'
import {appFetcher} from '../../config/axios'
import {usePageFilterContext} from '../../context/Filters/pageFilters'
import {useAppContext} from '../../context/coreProvider'
import useAssetTracks from '../../hooks/data/useTracks'
import useAssetTracksData from '../../hooks/data/useTracksData'
import useAssetTracksFilters from '../../hooks/data/useTracksFilters'
import {AssetTrack} from '../../models'
import {API_ROUTES, BREADCRUMBS, productPage} from '../../utils/constants'
import {
  cloneDeep,
  getEndpointURL,
  getQueryParam,
  isEmpty,
  isUndefined,
  normalizeFilters,
  omit,
  parseQuery,
} from '../../utils/helpers'
import {toAssetTracksData} from '../../utils/helpers/assets'
import ResultCount from '../shared/ResultsCount'

import {AssetsCards, AssetsFilters, AssetsTable} from './components'
import {AssetTrackData} from './components/AssetCards/AssetsCards.model'

export interface AssetTrackFilters {
  artist: string[]
  resolution: string[]
  year: string[]
  subLabel: string[]
  assetType: string[]
  label: string[]
  artistSearch: string[]
  projectSearch: string[]
  titleSearch: string[]
  productIdSearch: number[]
  dataSource: string
}

const filterInitialValue: AssetTrackFilters = {
  year: [],
  assetType: [],
  artist: [],
  label: [],
  resolution: [],
  subLabel: [],
  artistSearch: [],
  projectSearch: [],
  titleSearch: [],
  productIdSearch: [],
  dataSource: DB_SOURCE,
}

interface AssetState {
  result: string[]
}

const AssetReportPage = () => {
  const {
    filters,
    handleFilterChange,
    handleCheckOnEnter,
    setFiltersState,
    setFiltersBarContent,
    handleResetAllFilters,
    resetFilters,
  } = usePageFilterContext()
  const [stateAssets, setAssets] = useState<AssetTrack[]>([])
  const [sort, setSort] = useState<string>()

  const [paginationOptions, setPaginationOptions] = useState<PaginationOptions>(
    {page: 1, pages: 0, count: 0, size: productPage},
  )

  const artistOption = filters.artist?.length
    ? filters.artist
    : filters.artistSearch

  const assetReportQueryParams = {
    ...omit(paginationOptions, ['pages', 'count']),
    years: filters.year,
    labels: filters.label,
    marketingLabels: filters.subLabel,
    selections: filters.resolution,
    assetsTypes: filters.assetType,
    artists: artistOption,
    projects: filters.projectSearch,
    titles: filters.titleSearch,
    productIds: filters.productIdSearch,
    sort,
  }
  const history = useHistory<AssetState>()
  const {
    location: {search, state},
  } = history
  const dataSource = getQueryParam(search, 'dataSource')
  const requestData = dataSource !== SEARCH_SOURCE
  const [assetTracksDataHistory, setAssetTrackDataHistory] = useState<
    AssetTrackData[]
  >([])
  const {artists, selections, marketingLabels} = useAssetTracksFilters()
  const shouldQueryAssets = !isEmpty(filters) && requestData
  const {assetTracks, count, pages, pageNumber} = useAssetTracks(
    assetReportQueryParams,
    () => shouldQueryAssets,
  )
  // eslint-disable-next-line consistent-return
  const downloadAllAssetTracks = async () => {
    const params = {...assetReportQueryParams, size: count || productPage}
    const URL = getEndpointURL(API_ROUTES.ASSET_TRACKS, params)
    try {
      const {data} = await appFetcher(URL)
      return data
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('Error')
    }
  }

  const downloadData = shouldQueryAssets
    ? downloadAllAssetTracks
    : () => Promise.resolve(stateAssets)

  const {assetTracksData} = useAssetTracksData(
    {
      years: filters.year,
      labels: filters.label,
      marketingLabels: filters.subLabel,
      selections: filters.resolution,
      assetsTypes: filters.assetType,
      artists: artistOption,
      projects: filters.projectSearch,
      titles: filters.titleSearch,
      productIds: filters.productIdSearch,
    },
    () => !isEmpty(filters) && requestData,
  )

  useEffect(() => {
    if (assetTracksData) {
      setAssetTrackDataHistory(assetTracksData)
    }
  }, [assetTracksData])

  useEffect(() => {
    if (assetTracks) {
      const assetsToSet = Array.isArray(assetTracks)
        ? cloneDeep(assetTracks)
        : []
      setAssets(assetsToSet)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(assetTracks)])

  useEffect(() => {
    window.scrollTo(0, 0)
    const filtersToSet = normalizeFilters(search, filterInitialValue)
    setFiltersState<AssetTrackFilters>(filtersToSet)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (!isEmpty(parseQuery(search))) {
      const filtersToSet = normalizeFilters(search, filterInitialValue)
      setFiltersState<AssetTrackFilters>(filtersToSet)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [search])

  useEffect(() => {
    if (!requestData && state) {
      const {result} = state
      const historyStateAssets: AssetTrack[] = result.map((item: string) =>
        JSON.parse(item),
      )
      setAssets(historyStateAssets)
      const assetTrackDataConverted = toAssetTracksData(historyStateAssets)
      setAssetTrackDataHistory(assetTrackDataConverted)
    }
  }, [requestData, state])

  useEffect(() => {
    if (
      !isUndefined(count) &&
      !isUndefined(pages) &&
      !isUndefined(pageNumber)
    ) {
      setPaginationOptions((prevState) => ({
        ...prevState,
        count: count / 2,
        page: pageNumber,
        pages,
      }))
    }
  }, [count, pages, pageNumber])

  useEffect(() => {
    setPaginationOptions((prevState) => ({
      ...prevState,
      page: 1,
    }))
  }, [filters])

  useEffect(() => {
    setFiltersBarContent(
      <AssetsFilters
        artists={artists}
        resolutions={selections}
        subLabels={marketingLabels}
        filters={filters}
        handleFilterChange={handleFilterChange}
        resetFilters={resetFilters}
        handleCheckOnEnter={handleCheckOnEnter}
        setFiltersState={setFiltersState}
      />,
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    filters,
    artists,
    selections,
    marketingLabels,
    setFiltersBarContent,
    handleFilterChange,
    resetFilters,
  ])

  const {setTitle, setBreadcrumbs} = useAppContext()

  useEffect(() => {
    setTitle(<>Assets Delivered Report</>)
    setBreadcrumbs([
      {
        ...BREADCRUMBS.HOME,
      },
      {
        ...BREADCRUMBS.ASSET_REPORT,
        active: true,
      },
    ])
  }, [setBreadcrumbs, setTitle])

  const onClear = () => {
    const filtersToSet = normalizeFilters('', filterInitialValue)
    setFiltersState<AssetTrackFilters>(filtersToSet)
  }

  const searchOpen = filters.artistSearch?.concat(
    filters.projectSearch,
    filters.titleSearch,
    filters.productIdSearch,
  )?.length

  return (
    <>
      {searchOpen ? (
        <ResultCount
          display={stateAssets.length}
          count={count}
          linkText='Clear'
          displayReset
          handleReset={onClear}
        />
      ) : null}
      <AssetsCards assetsValues={assetTracksDataHistory} />
      {!isEmpty(stateAssets) ? (
        <AssetsTable
          assetsValues={stateAssets}
          paginationOptions={paginationOptions}
          setPaginationOptions={setPaginationOptions}
          setSort={setSort}
          allAssetsToDownload={downloadData}
        />
      ) : (
        <NoResults onClick={handleResetAllFilters} />
      )}
    </>
  )
}

export default AssetReportPage
