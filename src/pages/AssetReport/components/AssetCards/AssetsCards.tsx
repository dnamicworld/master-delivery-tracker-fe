import {uniquely} from 'src/utils/helpers'

import {Card, Col, Row} from '../../../../DS/components'

import {AssetsCardsProps} from './AssetsCards.model'

const AssetsCards = ({assetsValues}: AssetsCardsProps) => (
  <Row className='d-flex flex-wrap' key={JSON.stringify(assetsValues)}>
    {assetsValues?.map((element) => (
      <Col key={uniquely()} sm={2} className='mb-20'>
        <Card className='py-20 px-40' radius>
          <div className='align-items-center'>
            <p className='text-center mb-10'>{element.title}</p>
            <h2 className='text-center m-0'>{element.value}</h2>
          </div>
        </Card>
      </Col>
    ))}
  </Row>
)

export default AssetsCards
