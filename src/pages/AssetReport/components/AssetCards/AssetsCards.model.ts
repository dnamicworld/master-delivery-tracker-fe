export interface AssetTrackData {
  title: string
  value: number
}
export interface AssetsCardsProps {
  assetsValues?: Array<AssetTrackData>
}
