import {
  ArtistFilter,
  AssetTypesFilter,
  LabelsFilter,
  ResolutionFilter,
  SelectOption,
  SubLabelsFilter,
  YearFilter,
} from '../../../../components'
import {FilterWrapperProps} from '../../../../context/Filters/productsFilters'

interface AssetsFiltersProps extends FilterWrapperProps {
  artists?: string[]
  resolutions?: string[]
  subLabels?: string[]
  handleCheckOnEnter: (name: string, filteredOptions: SelectOption[]) => void
  setFiltersState: <T>(filters: T, initial?: T) => void
}

const AssetsFilters = ({
  filters,
  handleFilterChange,
  handleCheckOnEnter,
  artists,
  resolutions,
  subLabels,
  resetFilters,
  setFiltersState,
}: AssetsFiltersProps) => (
  <div className='filters d-flex album_filters'>
    <ArtistFilter
      value={filters?.artist ?? ''}
      name='artist'
      options={artists ?? []}
      onChange={handleFilterChange}
      resetFilters={resetFilters}
      handleCheckOnEnter={handleCheckOnEnter}
    />
    <AssetTypesFilter
      name='assetType'
      value={filters?.assetType ?? ''}
      multiple
      onChange={handleFilterChange}
      handleCheckOnEnter={handleCheckOnEnter}
      includeAllAssets
      resetFilters={resetFilters}
    />
    <ResolutionFilter
      value={filters?.resolution ?? ''}
      name='resolution'
      options={resolutions ?? []}
      onChange={handleFilterChange}
      resetFilters={resetFilters}
      handleCheckOnEnter={handleCheckOnEnter}
    />
    <YearFilter
      value={filters?.year ?? ''}
      name='year'
      onChange={handleFilterChange}
      handleCheckOnEnter={handleCheckOnEnter}
      multiple
      resetFilters={resetFilters}
    />
    <LabelsFilter
      name='label'
      value={filters?.label ?? ''}
      onChange={handleFilterChange}
      resetFilters={resetFilters}
      handleCheckOnEnter={handleCheckOnEnter}
    />
    <SubLabelsFilter
      filters={filters}
      value={filters?.subLabel ?? ''}
      name='subLabel'
      subLabels={subLabels ?? []}
      label='All sub Labels'
      resetFilters={resetFilters}
      handleCheckOnEnter={handleCheckOnEnter}
      setFiltersState={setFiltersState}
    />
  </div>
)

export default AssetsFilters
