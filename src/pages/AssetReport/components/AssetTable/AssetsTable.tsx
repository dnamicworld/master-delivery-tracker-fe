import {ColDef} from 'ag-grid-community'
import {useCallback, useRef} from 'react'

import {DynamicPagination} from '../../../../DS/components'
import {AgGridTable, SelectOption} from '../../../../components'
import {LinkCellRenderer} from '../../../../components/AgGrid/CellRenderers'

import {AssetTrack} from '../../../../models'
import {uniquely} from '../../../../utils/helpers'

import {
  buildAssetTracksFileName,
  getAssetsTableHeaders,
} from './AssetsTable.controller'
import {TableAssetsTypesProps} from './AssetsTable.model'
import {ExportAssetTracksHandler} from './exports/ExportAssetTracksHandler'

const AssetsTable = ({
  assetsValues = [],
  paginationOptions,
  setPaginationOptions,
  setSort,
  allAssetsToDownload,
}: TableAssetsTypesProps) => {
  const gridRef = useRef<any>()

  const postSort = useCallback(() => {
    const columnOptions = gridRef.current?.columnApi.getColumnState()
    if (columnOptions) {
      const sortOption = columnOptions.find((item: ColDef) => item.sort)
      if (sortOption) {
        setSort(`${sortOption.colId},${sortOption.sort}`)
      } else {
        setSort(undefined)
      }
    }
  }, [setSort, gridRef])

  const columnDefs = getAssetsTableHeaders()

  const kabobOptions: SelectOption[] = [
    ...[
      {
        name: 'Export as Excel',
        value: 'Export as Excel',
        onClick: async () => {
          const data = await allAssetsToDownload()
          const excelHandler = new ExportAssetTracksHandler(data)
          excelHandler.download(buildAssetTracksFileName('MDR ASSET INDEX'))
        },
      },
    ],
  ]

  return (
    <DynamicPagination
      paginationSize={paginationOptions.pages}
      paginationOptions={paginationOptions}
      setPaginationOptions={setPaginationOptions}
    >
      <AgGridTable
        gridRef={gridRef}
        columnDefs={columnDefs}
        kabob={{options: kabobOptions, multiple: true}}
        fullWidth
        rowData={assetsValues}
        stopEditingWhenCellsLoseFocus
        postSort={postSort}
        getRowNodeId={(data: AssetTrack) =>
          `${data.id}${data.assetTypeId}` || uniquely()
        }
        frameworkComponents={{
          linkCellRender: LinkCellRenderer,
        }}
      />
    </DynamicPagination>
  )
}
export default AssetsTable
