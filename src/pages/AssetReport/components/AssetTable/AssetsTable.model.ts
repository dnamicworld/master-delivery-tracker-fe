import {Dispatch, SetStateAction} from 'react'

import {PaginationOptions} from '../../../../DS/components'
import {AssetTrack} from '../../../../models'

export interface TableAssetsTypesProps {
  assetsValues?: Array<AssetTrack>
  paginationOptions: PaginationOptions
  setPaginationOptions: Dispatch<SetStateAction<PaginationOptions>>
  setSort: Dispatch<SetStateAction<string | undefined>>
  allAssetsToDownload: () => Promise<AssetTrack[]>
}
