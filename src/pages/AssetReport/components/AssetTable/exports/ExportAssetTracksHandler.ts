import {AssetTrack} from '../../../../../models'
import {
  boldRow,
  CellValue,
  Downloader,
  downloadExcelFile,
  trimValue,
} from '../../../../../utils/helpers'

export class ExportAssetTracksHandler implements Downloader<AssetTrack> {
  private readonly tracks: AssetTrack[] = []

  constructor(tracks: AssetTrack[]) {
    this.tracks = tracks
  }

  getHeaders = (): CellValue[] =>
    boldRow(
      [
        'Artist',
        'Track Title',
        'Project Title',
        'Asset Type',
        'Resolution',
        'Year',
        'Label',
        'Sub Label',
      ],
      true,
    )

  getInfo = (tracks: AssetTrack[]) =>
    tracks.map((track) => [
      trimValue(track.artist),
      trimValue(track.title),
      trimValue(track.project),
      trimValue(track.assetType),
      track.selection,
      track.year.toString(),
      trimValue(track.name),
      trimValue(track.marketingLabel),
    ])

  transformData = (): CellValue[][] => [
    this.getHeaders(),
    ...this.getInfo(this.tracks),
  ]

  download = (fileName: string) => {
    downloadExcelFile(fileName, this.transformData(), undefined, true)
  }
}
