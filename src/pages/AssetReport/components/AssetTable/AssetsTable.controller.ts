import {ColDef, ValueGetterParams} from 'ag-grid-community'

import {AssetTrack} from '../../../../models/AssetTrack'
import {TABLE} from '../../../../utils/constants'
import {ROUTES} from '../../../../utils/constants/routes'
import {
  formatBlankValues,
  getDateString,
  isAlbum,
} from '../../../../utils/helpers'
import {getDetailPathname} from '../../../shared/IndexPages'

const disableDefaultSort = () => 0

export const getAssetsTableHeaders = (): ColDef[] => [
  {
    headerName: 'Artist',
    field: 'artist',
    sortable: true,
    unSortIcon: true,
    cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
    cellRendererParams: {
      showValueOnTooltip: true,
    },
    comparator: disableDefaultSort,
  },
  {
    headerName: 'Track Title',
    field: 'title',
    sortable: true,
    unSortIcon: true,
    cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
    cellRendererParams: {
      showValueOnTooltip: true,
    },
    comparator: disableDefaultSort,
  },
  {
    headerName: 'Project',
    field: 'project',
    sortable: true,
    unSortIcon: true,
    cellRenderer: 'linkCellRender',
    cellRendererParams: {
      path: (data: AssetTrack) =>
        isAlbum(data.productType)
          ? getDetailPathname(ROUTES.ALBUM_DETAIL, data.productId)
          : getDetailPathname(ROUTES.SINGLE_DETAIL, data.productId),
    },
    comparator: disableDefaultSort,
  },
  {
    headerName: 'Asset Type',
    field: 'assetType',
    sortable: true,
    unSortIcon: true,
    comparator: disableDefaultSort,
  },
  {
    headerName: 'Resolution',
    field: 'resolution',
    valueGetter: (params: ValueGetterParams) => {
      const {selection} = params.data
      return selection
    },
    sortable: true,
    unSortIcon: true,
    comparator: disableDefaultSort,
  },
  {
    headerName: 'Year',
    field: 'year',
    sortable: true,
    unSortIcon: true,
    comparator: disableDefaultSort,
  },
  {
    headerName: 'Label',
    field: 'name',
    sortable: true,
    unSortIcon: true,
    cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
    cellRendererParams: {
      showValueOnTooltip: true,
    },
    comparator: disableDefaultSort,
  },
  {
    headerName: 'Sub Label',
    field: 'marketingLabel',
    valueFormatter: formatBlankValues,
    sortable: true,
    unSortIcon: true,
    cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
    cellRendererParams: {
      showValueOnTooltip: true,
    },
    comparator: disableDefaultSort,
  },
]
export const buildAssetTracksFileName = (filename: string) =>
  `${filename}_${getDateString({
    date: new Date(),
  })}`
