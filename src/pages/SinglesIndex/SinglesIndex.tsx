import {GridApi} from 'ag-grid-community'
import {useEffect, useMemo, useState} from 'react'
import {useHistory} from 'react-router-dom'

import {NoResults, Pagination} from '../../DS/components'
import {LoaderWithText} from '../../DS/components/custom/LoaderWithText'
import {usePageFilterContext} from '../../context/Filters/pageFilters'
import {useProductFilterContext} from '../../context/Filters/productsFilters'
import {useAppContext} from '../../context/coreProvider'
import {Product, ProductFilters, Report} from '../../models'
import {
  LOADING_RESULTS,
  ROUTES,
  INDEX_PAGES_INITIAL_FILTERS,
} from '../../utils/constants'
import {
  cloneDeep,
  isEmpty,
  isEqual,
  normalizeData,
  parseQuery,
  QueryString,
} from '../../utils/helpers'
import {
  getPageData,
  IndexPageData,
  getIndexBreadcrumbs,
  IndexTableActions,
  IndexPagesFilters,
  getPageLabelName,
  IndexPagesTable,
} from '../shared/IndexPages'
import {updateProductAction} from '../shared/IndexPages/IndexPages.controller'
import {
  IndexCardPlaceholder,
  IndexDashboardCard,
  IndexPageSwitchers,
} from '../shared/IndexPages/components'
import ResultCount from '../shared/ResultsCount'

const {SINGLE_DETAIL} = ROUTES

const SinglesIndex = () => {
  const {setTitle, setBreadcrumbs} = useAppContext()
  const {
    report,
    filteredProducts,
    products,
    filters,
    showPlaceholder,
    setFiltersBarContent,
    setFilteredProducts,
    setNoChanges,
  } = useProductFilterContext()

  const history = useHistory()
  const {
    filtersChanged,
    handleResetAllFilters,
    handleCheckOnEnter,
    setFiltersState,
    setWithTags,
    setIgnoreOnTags,
    resetFilters,
  } = usePageFilterContext()

  const [gridApi, setGridApi] = useState<GridApi | null>(null)
  const [productsDisplay, setProductsDisplay] = useState<number>(0)

  const {
    location: {search, pathname},
  } = history

  const hasReport = !isEmpty(report)

  const PAGE_DATA: IndexPageData = useMemo(() => {
    const label = hasReport ? getPageLabelName(report?.label) : ''
    const title = hasReport ? (
      <IndexPageSwitchers
        filters={filters}
        label={label}
        report={report as Report}
        setFiltersState={setFiltersState}
        resetFilters={resetFilters}
      />
    ) : (
      <>No Report found for year {Number(filters.year)}</>
    )
    return {
      ...getPageData(pathname),
      RELEASE_TYPE: report?.listViewType ?? '',
      LABEL: label,
      TITLE: title,
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hasReport, filters, resetFilters, pathname, report])

  useEffect(() => {
    const title = showPlaceholder ? (
      <LoaderWithText loadingMessage={LOADING_RESULTS} />
    ) : (
      PAGE_DATA.TITLE
    )
    setTitle(title)
    const breadCrumbs = getIndexBreadcrumbs(PAGE_DATA.BREAD_CRUMB)
    setBreadcrumbs(breadCrumbs)
  }, [
    PAGE_DATA.TITLE,
    PAGE_DATA.BREAD_CRUMB,
    setTitle,
    setBreadcrumbs,
    showPlaceholder,
  ])

  useEffect(() => {
    window.scrollTo(0, 0)
    const queryFilters = parseQuery(search)
    const filtersToSet = normalizeData<ProductFilters, QueryString>(
      {
        ...cloneDeep(INDEX_PAGES_INITIAL_FILTERS),
        releaseType: PAGE_DATA.RELEASE_TYPE,
      },
      queryFilters,
    )
    setFiltersState<ProductFilters>(filtersToSet)
    setIgnoreOnTags(['releaseType', 'year'])
    setWithTags(true)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    setFiltersBarContent(
      <IndexPagesFilters
        filters={filters}
        handleFilterChange={() => {}}
        report={report}
        products={products}
        resetFilters={resetFilters}
        handleCheckOnEnter={handleCheckOnEnter}
        setFiltersState={setFiltersState}
      />,
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters, resetFilters])

  const productCount = filteredProducts.length

  return showPlaceholder ? (
    <IndexCardPlaceholder />
  ) : (
    <div>
      {hasReport && !isEmpty(filteredProducts) ? (
        <>
          <ResultCount
            display={productsDisplay}
            count={productCount}
            displayReset={filtersChanged}
            handleReset={handleResetAllFilters}
          />
          <IndexDashboardCard
            products={filteredProducts}
            releaseType={report?.listViewType ?? ''}
          />
          <IndexTableActions report={report} />
          <Pagination
            items={filteredProducts}
            onPreviousPage={gridApi?.paginationGoToPreviousPage.bind(gridApi)}
            onNextPage={gridApi?.paginationGoToNextPage.bind(gridApi)}
            onGoToPage={gridApi?.paginationGoToPage.bind(gridApi)}
          >
            {({page, count, size}) => {
              const productsOnPages = page * size
              const productsNumber =
                productsOnPages > count ? count : productsOnPages
              setProductsDisplay(productsNumber)
              return (
                <IndexPagesTable
                  pagination
                  products={filteredProducts}
                  getGridReadyEvent={(params) => {
                    setGridApi(params.api)
                  }}
                  updateProduct={(
                    productToUpdate,
                    action,
                    isEditing,
                    column,
                  ) => {
                    const newProducts: Product[] = updateProductAction(
                      filteredProducts,
                      productToUpdate,
                      action,
                    )
                    if (!isEditing || column?.getColId() === 'stats') {
                      setFilteredProducts(newProducts)
                    }
                    const noChanges = isEqual(newProducts, products)
                    setNoChanges(noChanges)
                  }}
                  detailRoute={SINGLE_DETAIL}
                />
              )
            }}
          </Pagination>
        </>
      ) : (
        <NoResults onClick={handleResetAllFilters} />
      )}
    </div>
  )
}

export default SinglesIndex
