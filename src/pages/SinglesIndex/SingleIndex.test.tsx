import {act, render} from '@testing-library/react'

import 'jest-canvas-mock'

import ReactRouterDom, {BrowserRouter} from 'react-router-dom'

import {withProductFilters} from '../../components/AppRouter'

import {SinglesIndex} from '.'

describe('Single Index Page', () => {
  test('renders Single index page', async () => {
    const promise = Promise.resolve()
    const SingleIndexPage = withProductFilters(SinglesIndex)
    jest.spyOn(ReactRouterDom, 'useParams').mockReturnValue({labelId: '1'})
    await act(() => promise)
    const renderSingleIndex = render(
      <BrowserRouter>
        <SingleIndexPage />
      </BrowserRouter>,
    )
    expect(renderSingleIndex).toMatchSnapshot()
  })
})
