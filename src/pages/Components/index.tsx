import {useState} from 'react'
import {Grid} from 'react-bootstrap'

import {
  Col,
  Row,
  Search,
  Table,
  TextField,
  Footer,
  Dropdown,
  Chip,
} from '../../DS/components'

import {FA_ICONS} from '../../utils/constants'

import {handleInputChange} from '../../utils/helpers'

import {
  Checkboxes,
  Charts,
  ComponentContainer,
  Loader,
  ProgressBar,
  Typography,
  Navbar,
  DatePicker,
  DatePickerIcon,
  RangeDatePicker,
  AgGridTableExample,
  Selects,
  Breadcrumbs,
  DashboardCards,
  Lists,
  Panels,
  Modals,
} from './components'

const TABLE_HEADERS = [
  'IDs',
  'Title & Artist',
  'Track Duration',
  'Clip Duration',
  'Additional Clips',
  'Dates',
  'Comments',
]

const TABLE_BODY = [
  {
    id: '1',
    title: 'title 1',
    track_duration: 'track duration 1',
    clip_duration: 'clip duration 1',
    additional: 'additional 1',
    dates: 'dates 1',
    comments: 'comments 1',
  },
  {
    id: '2',
    title: 'title 2',
    track_duration: 'track duration 2',
    clip_duration: 'clip duration 2',
    additional: 'additional 2',
    dates: 'dates 2',
    comments: 'comments 2',
  },
]

const Components = () => {
  const [fields, setFields] = useState({
    search: '',
    textfield: '',
    timeMask: '',
  })

  const setTextFields = (name: string, value: string) => {
    setFields({
      ...fields,
      [name]: value,
    })
  }

  const handleFieldsChange = handleInputChange(setTextFields)

  return (
    <>
      <Navbar />
      <Grid>
        <Row className='justify-content-md-center align-items-center'>
          <Col xs={12}>
            <h1>Components Page</h1>
          </Col>
        </Row>
        <ComponentContainer name='Typography'>
          <Typography />
        </ComponentContainer>
        <ComponentContainer name='Table'>
          <Table headers={TABLE_HEADERS}>
            {TABLE_BODY.map((data) => (
              <tr key={data.id}>
                <td>{data.id}</td>
                <td>{data.title}</td>
                <td>{data.track_duration}</td>
                <td>{data.clip_duration}</td>
                <td>{data.additional}</td>
                <td>{data.dates}</td>
                <td>{data.comments}</td>
              </tr>
            ))}
          </Table>
        </ComponentContainer>
        <ComponentContainer name='Footer'>
          <Footer />
        </ComponentContainer>
        <Charts />
        <Checkboxes />
        <ComponentContainer name='Search'>
          <Search
            value={fields.search}
            name='search'
            placeholder='Search by Artist or Title or UPC or ISRC'
            onChange={handleFieldsChange}
          />
        </ComponentContainer>
        <ComponentContainer name='Progress Bars'>
          <ProgressBar />
        </ComponentContainer>
        <ComponentContainer name='Loader'>
          <Loader />
        </ComponentContainer>
        <ComponentContainer name='Text Input'>
          <TextField
            type='text'
            name='textfield'
            value={fields.textfield}
            onChange={handleFieldsChange}
          />
        </ComponentContainer>
        <ComponentContainer name='Time Input Mask'>
          <TextField
            type='time'
            name='timeMask'
            value={fields.timeMask}
            onChange={handleFieldsChange}
          />
        </ComponentContainer>
        <ComponentContainer name='Dropdown'>
          <Dropdown
            trigger={
              <button
                className='btn btn-default dropdown-toggle'
                type='button'
                id='dropdownMenu1'
              >
                Dropdown
              </button>
            }
          >
            <p>content</p>
          </Dropdown>
        </ComponentContainer>
        <ComponentContainer name='DatePicker'>
          <DatePicker />
        </ComponentContainer>
        <ComponentContainer name='DatePicker with Icon'>
          <DatePickerIcon />
        </ComponentContainer>
        <ComponentContainer name='RangeDatePicker'>
          <RangeDatePicker />
        </ComponentContainer>
        <ComponentContainer name='AgGridTable'>
          <AgGridTableExample />
        </ComponentContainer>
        <ComponentContainer name='Chip'>
          <Chip
            content='Canvasback'
            iconName={FA_ICONS.close}
            iconClick={() =>
              // eslint-disable-next-line no-alert
              alert('Clicked')
            }
          />
        </ComponentContainer>
        <ComponentContainer name='Select'>
          <Selects />
        </ComponentContainer>
        <ComponentContainer name='Breadcrumb'>
          <Breadcrumbs />
        </ComponentContainer>
        <ComponentContainer name='DashboardCard'>
          <DashboardCards />
        </ComponentContainer>
        <ComponentContainer name='List'>
          <Lists />
        </ComponentContainer>
        <ComponentContainer name='Panel'>
          <Panels />
        </ComponentContainer>
        <ComponentContainer name='Modals'>
          <Modals />
        </ComponentContainer>
      </Grid>
      <br />
      <br />
    </>
  )
}

export default Components
