import {Col, Row} from '../../../DS/components/bootstrap'

const componentTypography = () => (
  <Row>
    <Col sm={6}>
      <div className='page-header no-local-icon'>
        <h1>
          Page Header<small>Optional page-header subtext</small>
        </h1>
      </div>
      <div>
        <h1>Heading 1: 32px</h1>
        <h2>Heading 2: 26px</h2>
        <h3>Heading 3: 20px</h3>
        <h4>Heading 4: 16px</h4>
        <h5>Heading 5: 13px</h5>
        <h6>Heading 6: 13px</h6>
      </div>
      <div>
        <p className='lead'>
          Lead: Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor
          auctor.
        </p>
      </div>
    </Col>
    <Col sm={6}>
      <div className='page-header no-local-icon'>
        <h1>
          Page Header w Article Class<small>Optional page-header subtext</small>
        </h1>
      </div>
      <div className='bs-example'>
        <h1>Heading 1: 38px</h1>
        <h2>Heading 2: 32px</h2>
        <h3>Heading 3: 26px</h3>
        <h4>Heading 4: 16px</h4>
        <h5>Heading 5: 13px</h5>
        <h6>Heading 6: 13px</h6>
      </div>
      <Col sm={12}>
        <div className='bs-example'>
          <p className='lead'>
            Lead: Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor
            auctor.
          </p>
        </div>
      </Col>
    </Col>
    <Col sm={12}>
      <div className='row'>
        <h2>Proxima Nova Extended Family</h2>
        <Col lg={4}>
          <div>
            <p className='font-weight-light'>Proxima Nova Light</p>
            <p className='font-weight-light'>
              <em>Proxima Nova Light Italic</em>
            </p>
          </div>
        </Col>
        <Col lg={4}>
          <div>
            <p>Proxima Nova Regular</p>
            <p>
              <em>Proxima Nova regular Italic</em>
            </p>
          </div>
        </Col>
        <Col lg={4}>
          <div>
            <p>
              <strong>Proxima Nova bold</strong>
            </p>
            <p>
              <strong>
                <em>Proxima Nova Bold Italic</em>
              </strong>
            </p>
          </div>
        </Col>
        <Col lg={4}>
          <div>
            <p>
              <strong className='font-weight-sbold'>Proxima Nova SBold</strong>
            </p>
            <p>
              <strong className='font-weight-sbold'>
                <em>Proxima Nova SBold Italic</em>
              </strong>
            </p>
          </div>
        </Col>
        <Col lg={4}>
          <div>
            <p>
              <strong className='font-weight-black'>Proxima Nova Black</strong>
            </p>
            <p>
              <strong className='font-weight-black'>
                <em>Proxima Nova Black Italic</em>
              </strong>
            </p>
          </div>
        </Col>
      </div>
    </Col>
  </Row>
)

export default componentTypography
