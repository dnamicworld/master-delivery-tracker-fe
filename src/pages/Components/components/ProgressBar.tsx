import {ProgressBar} from '../../../DS/components'

const componentProgressBar = () => (
  <>
    <h4>Basic</h4>
    <ProgressBar now={60} />

    <h4>Contextual alternatives</h4>
    <ProgressBar bsStyle='info' now={20} />
    <ProgressBar bsStyle='success' now={40} />
    <ProgressBar bsStyle='warning' now={60} />
    <ProgressBar bsStyle='danger' now={80} />

    <h4>Striped</h4>
    <ProgressBar striped bsStyle='info' now={20} />
    <ProgressBar striped bsStyle='success' now={40} />
    <ProgressBar striped bsStyle='warning' now={60} />
    <ProgressBar striped bsStyle='danger' now={80} />

    <h4>Animated</h4>
    <ProgressBar active now={45} />

    <h4>Stacked</h4>
    <ProgressBar>
      <ProgressBar striped bsStyle='success' now={35} />
      <ProgressBar striped bsStyle='warning' now={20} />
      <ProgressBar striped bsStyle='danger' now={10} />
    </ProgressBar>
  </>
)

export default componentProgressBar
