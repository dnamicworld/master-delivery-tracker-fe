import {ReactNode} from 'react'

import {Col, Row} from '../../../DS/components/bootstrap'

interface ComponentContainerProps {
  name: string
  children: ReactNode
}

const ComponentContainer = ({name, children}: ComponentContainerProps) => (
  <Row className='justify-content-md-center align-items-center'>
    <Col xs={12}>
      <h1>Name: {name}</h1>
    </Col>
    <Col xs={12}>{children}</Col>
  </Row>
)

export default ComponentContainer
