import {DashboardCard} from '../../../components'

const DashboardCards = () => (
  <DashboardCard
    cardHeader={<h1>Title</h1>}
    leftContent={<p>Left content</p>}
    rightContent={<p>Right content</p>}
  />
)

export default DashboardCards
