import {CSSProperties} from 'react'

import {Loader} from '../../../DS/components'

const componentLoader = () => {
  const loaderExampleContainer: CSSProperties = {
    height: '100px',
    width: '300px',
    border: '1px solid lightgray',
    position: 'relative',
  }
  return (
    <div style={loaderExampleContainer}>
      <Loader />
    </div>
  )
}

export default componentLoader
