import {Navbar, NavbarProps} from '../../../DS/components'

const items: NavbarProps['navItems'] = [
  {text: 'Sample 1', href: '/#'},
  {text: 'Sample 2', href: '/#'},
  {
    text: 'Sample 3',
    subItems: [
      {text: 'Sub item 1', href: '/#'},
      {text: 'Sub item 2', href: '/#'},
    ],
  },
]

const NavbarComponent = () => (
  <Navbar title='Dynamic Audio Clips' navItems={items} alwaysMobile />
)

export default NavbarComponent
