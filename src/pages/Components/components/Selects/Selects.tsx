import {ChangeEvent, MouseEvent, useCallback, useState} from 'react'

import {
  Col,
  Grid,
  Row,
  isSingleSelect,
  SelectValue,
} from '../../../../DS/components'

import {Select, SelectAction, SelectOption} from '../../../../components'
import {LabelsFilter} from '../../../../components/Filters'
import {SelectActions} from '../../../../utils/constants'
import {onAddOptions, cloneDeep} from '../../../../utils/helpers'

import {BASE_OPTIONS, searchableProps} from './index'

const Selects = () => {
  const [selectValues, setSelectValues] = useState({
    regular: '',
    multi: [],
    single_with_chips: '',
    single_with_search: '',
    single_with_create: '',
  })
  const [options, setOptions] = useState<SelectOption[]>(
    cloneDeep(BASE_OPTIONS),
  )
  const [selectedLabels, setSelectedLabels] = useState<string[]>([])
  const [selectedMultiple, setSelectedMultiple] = useState<string[]>([
    'AC/DC',
    'Metallica',
  ])

  const handleOnMultipleChange = useCallback(
    <T extends {}>(event: T, value: SelectValue, option: SelectAction) => {
      const {added, result} = onAddOptions(options, value, option)
      if (added) {
        setOptions(result)
      }

      if (typeof value === 'string') {
        const newValue =
          (option.action === SelectActions.deselectOption ||
            selectedMultiple.includes(value)) &&
          Array.isArray(selectedMultiple)
            ? selectedMultiple?.filter((item: string) => item !== value)
            : [...(selectedMultiple ?? []), value]

        setSelectedMultiple(newValue)
      }
    },
    [options, selectedMultiple],
  )

  const handleLabelChange = useCallback(
    <T extends {}>(
      _: KeyboardEvent | ChangeEvent<T> | MouseEvent<T>,
      value: SelectValue,
    ) => {
      if (isSingleSelect(value)) {
        const newSelectedLabels = [...selectedLabels, value]
        setSelectedLabels(newSelectedLabels)
      }
    },
    [selectedLabels],
  )

  // TODO: fix T
  const updateSelects = <T,>(
    event: T,
    value: SelectValue,
    option: SelectAction,
  ) => {
    const {added, result} = onAddOptions(options, value, option)
    if (added) {
      setOptions(result)
    }

    setSelectValues({
      ...cloneDeep(selectValues),
      [option.name ?? '']: value,
    })
  }

  return (
    <Grid>
      <Row>
        <Col xs={3}>
          <Row>
            <Col xs={12}>Regular select</Col>
            <Col xs={12}>
              <Select
                value={selectValues.regular}
                name='regular'
                label='Regular select'
                options={BASE_OPTIONS}
                onChange={updateSelects}
              />
            </Col>
          </Row>
        </Col>
        <Col xs={3}>
          <Row>
            <Col xs={12}>Regular Multi select</Col>
            <Col xs={12}>
              <Select
                multiple
                value={selectedMultiple}
                name='multi'
                label='Multi select'
                options={BASE_OPTIONS}
                onChange={handleOnMultipleChange}
              />
            </Col>
          </Row>
        </Col>
        <Col xs={3}>
          <Row>
            <Col xs={12}>Single with chips</Col>
            <Col xs={12}>
              <Select
                value={selectValues?.single_with_chips}
                name='single_with_chips'
                label='Chips'
                options={BASE_OPTIONS}
                onChange={updateSelects}
                chips
              />
            </Col>
          </Row>
        </Col>
        <Col xs={3}>
          <Row>
            <Col xs={12}>Single with search</Col>
            <Col xs={12}>
              <Select
                value={selectValues?.single_with_search}
                name='single_with_search'
                label='search'
                options={BASE_OPTIONS}
                onChange={updateSelects}
                searchable={searchableProps}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col xs={3}>
          <Row>
            <Col xs={12}>Single with create</Col>
            <Col xs={12}>
              <Select
                value={selectValues?.single_with_create}
                name='single_with_create'
                label='search'
                options={options}
                onChange={updateSelects}
                searchable={searchableProps}
                canCreate
              />
            </Col>
          </Row>
        </Col>
        <Col xs={4}>
          <Row>
            <Col xs={12}>Multiple with chips</Col>
            <Col xs={12}>
              <Select
                value={selectedMultiple}
                name='multiple'
                label='Multiple Chips'
                options={BASE_OPTIONS}
                onChange={handleOnMultipleChange}
                chips
                multiple
              />
            </Col>
          </Row>
        </Col>
        <Col xs={4}>
          <Row>
            <Col xs={12}>Multiple with chips and create</Col>
            <Col xs={12}>
              <Select
                value={selectedMultiple}
                name='multiple'
                label='Multiple chips and create'
                options={options}
                onChange={handleOnMultipleChange}
                chips
                searchable={searchableProps}
                multiple
                canCreate
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col xs={12}>
          <h4>Filter</h4>
          <Col xs={12}>
            <div className='filters'>
              <LabelsFilter
                value={selectedLabels}
                name='label'
                onChange={handleLabelChange}
              />
            </div>
          </Col>
        </Col>
      </Row>
    </Grid>
  )
}

export default Selects
