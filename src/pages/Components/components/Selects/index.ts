export const BASE_OPTIONS = [
  {
    name: 'AC/DC',
    value: 'AC/DC',
  },
  {
    name: 'Metallica',
    value: 'Metallica',
  },
  {
    name: 'Avenged Sevenfold',
    value: 'Avenged Sevenfold',
  },
]

export const searchableProps = {
  placeholder: 'Search band',
}

export {default as Selects} from './Selects'
