import {useState} from 'react'

import {RangeDatePicker} from '../../../DS/components'
import {DEFAULT_MAX_YEAR, DEFAULT_MIN_YEAR} from '../../../utils/constants'
import {cloneDeep} from '../../../utils/helpers'

const ComponentRangeDatePicker = () => {
  const [datePickers, setDatePickers] = useState<
    {name: string; value?: Date}[]
  >([
    {name: 'startDate', value: undefined},
    {name: 'endDate', value: undefined},
  ])

  const setDates = (name: string, date: Date) => {
    const datePickersCopy = datePickers.map((item) => {
      const itemCopy = cloneDeep(item)
      if (itemCopy.name === name) {
        itemCopy.value = date
      }
      return itemCopy
    })
    setDatePickers(datePickersCopy)
  }

  return (
    <RangeDatePicker
      name='rangePicker'
      fromDate={datePickers[0]}
      toDate={datePickers[1]}
      onChange={setDates}
      showYearMonths
      minYear={DEFAULT_MIN_YEAR}
      maxYear={DEFAULT_MAX_YEAR}
    />
  )
}

export default ComponentRangeDatePicker
