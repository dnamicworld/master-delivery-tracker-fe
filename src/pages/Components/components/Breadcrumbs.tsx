import {Breadcrumb, BreadcrumbItem, Col} from '../../../DS/components'

const breadcrumbItems: BreadcrumbItem[] = [
  {id: '1', label: 'Path', link: '/#'},
  {id: '2', label: 'to', link: '/#'},
  {id: '3', label: 'Current Page', active: true},
]

const Breadcrumbs = () => (
  <Col xs={12}>
    <Breadcrumb items={breadcrumbItems} />
  </Col>
)

export default Breadcrumbs
