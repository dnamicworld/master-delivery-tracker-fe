import {useState} from 'react'

import {DatePicker, DatePickerProps} from '../../../DS/components'
import {DEFAULT_MAX_YEAR, DEFAULT_MIN_YEAR} from '../../../utils/constants'

const ComponentDatePicker = () => {
  const [datePicker, setDatePicker] = useState<Date | undefined>(undefined)

  const onChange: DatePickerProps['onChange'] = (name, date) => {
    setDatePicker(date)
  }

  return (
    <DatePicker
      name='datePicker'
      value={datePicker}
      onChange={onChange}
      showYearMonths
      minYear={DEFAULT_MIN_YEAR}
      maxYear={DEFAULT_MAX_YEAR}
    />
  )
}

export default ComponentDatePicker
