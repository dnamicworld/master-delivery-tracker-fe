import {Button} from '../../../../../DS/components'
import {useModalDisplay} from '../../../../../context/modal'

const ModalFromContext = () => {
  const {displayModal} = useModalDisplay()

  const handleClick = () => {
    displayModal('Modal content', 'Title', {
      actionName: 'Ok',
      onActionClick: () => {
        // eslint-disable-next-line no-alert
        alert('Confirmation action clicked!')
      },
    })
  }

  return (
    <Button className='ml-2' onClick={handleClick}>
      Open modal from context (recommended)
    </Button>
  )
}

export default ModalFromContext
