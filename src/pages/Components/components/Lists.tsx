import {List} from '../../../DS/components'

const Lists = () => (
  <List>
    <li>Item 1</li>
    <li>Item 2</li>
    <li>Item 3</li>
  </List>
)

export default Lists
