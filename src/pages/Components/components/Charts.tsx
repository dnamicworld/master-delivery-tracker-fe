import {BarChart, DonutChart} from '../../../components/Charts'
import {SINGLE_LABEL} from '../../../utils/constants'

import {ComponentContainer} from '.'

const Charts = () => {
  const chartData = [
    {name: 'Page A', pv: 240},
    {name: 'B', pv: 2210},
    {name: 'C', pv: 2300},
    {name: 'Page D', pv: 2000},
    {name: 'Zero', pv: 0},
    {name: 'Hi', pv: 123},
    {name: 'Bye', pv: 2091},
  ]
  const donutData = {name: SINGLE_LABEL, value: 43}
  return (
    <>
      <ComponentContainer name='Bar Chart'>
        <BarChart data={chartData} xKey='name' yKey='pv' layout='vertical' />
      </ComponentContainer>
      <ComponentContainer name='Donut Chart'>
        <DonutChart
          index={1}
          data={donutData}
          text='Total Delivery'
          withLegend
        />
      </ComponentContainer>
    </>
  )
}

export default Charts
