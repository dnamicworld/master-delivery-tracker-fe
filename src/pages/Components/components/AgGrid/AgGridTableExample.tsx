import {ColDef} from 'ag-grid-community'

import {AgGridTable, AgGridColumn} from '../../../../components'
import {TABLE} from '../../../../utils/constants'

interface RowData {
  admin: string
  comments: string
  title: string
  total: string
  type: string
  date: string
  edit: string
}

const data: RowData[] = [
  {
    admin: 'Aretha Kofax1',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
    edit: 'edit',
  },
  {
    admin: 'Aretha Kofax2',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
    edit: 'edit',
  },
  {
    admin: 'Aretha Kofax3',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
    edit: 'edit',
  },
  {
    admin: 'Aretha Kofax4',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
    edit: 'edit',
  },
]
const AgGridTableExample = () => {
  const columnDefs: ColDef[] = [
    {headerName: 'A&R Admin', field: 'admin'},
    {
      headerName: 'Comments',
      field: 'comments',
      maxWidth: 160,
      wrapText: true,
    },
    {headerName: 'Title & Artist', field: 'title'},
    {headerName: 'Total %', field: 'total'},
    {headerName: 'Product Type', field: 'type'},
    {headerName: 'Release Date', field: 'date'},
    {
      headerName: 'Edit',
      field: 'edit',
      cellRenderer: TABLE.CELL_RENDERERS.WithEditButton,
    },
  ]
  return (
    <AgGridTable columnDefs={columnDefs} rowData={data}>
      <AgGridColumn field='admin' />
      <AgGridColumn field='comments' />
      <AgGridColumn field='title' />
      <AgGridColumn field='total' />
      <AgGridColumn field='type' />
      <AgGridColumn field='date' />
      <AgGridColumn field='edit' cellRenderer='editCellRenderer' />
    </AgGridTable>
  )
}

export default AgGridTableExample
