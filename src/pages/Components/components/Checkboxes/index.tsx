import {Checkboxes, CustomCheckboxes} from './components'

const CheckboxesComponent = () => (
  <>
    <Checkboxes />
    <CustomCheckboxes />
  </>
)

export default CheckboxesComponent
