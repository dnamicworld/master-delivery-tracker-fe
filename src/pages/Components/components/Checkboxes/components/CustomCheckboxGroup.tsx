import {ChangeEvent, useState} from 'react'

import {CheckboxBtn, CheckboxGroup} from '../../../../../DS/components'

const CustomCheckboxGroup = () => {
  const [checkboxesState, setCheckboxesState] = useState({
    checkbox1: false,
    checkbox2: false,
    checkbox3: false,
  })
  const onGroupCheckChange = (event: ChangeEvent<HTMLInputElement>) => {
    const {name: inputName, checked} = event.currentTarget
    setCheckboxesState({
      ...checkboxesState,
      [inputName]: checked,
    })
  }
  return (
    <CheckboxGroup>
      <CheckboxBtn
        label='Option 1'
        value={1}
        name='checkbox1'
        onChange={onGroupCheckChange}
      />
      <CheckboxBtn
        label='Option 2'
        value={2}
        name='checkbox2'
        onChange={onGroupCheckChange}
      />
      <CheckboxBtn
        label='Option 3'
        value={3}
        name='checkbox3'
        onChange={onGroupCheckChange}
      />
    </CheckboxGroup>
  )
}

export default CustomCheckboxGroup
