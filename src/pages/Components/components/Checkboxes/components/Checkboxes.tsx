import {Dispatch, SetStateAction, useState} from 'react'

import {Col, Radio, Row} from '../../../../../DS/components'
import {handleInputChange} from '../../../../../utils/helpers'
import {ComponentContainer} from '../../index'

type CheckboxValue = {[key: string]: boolean}

const Checkboxes = () => {
  const [radiosState, setRadiosState] = useState<CheckboxValue>({
    checked: true,
    option: false,
    disabledChecked: true,
    disabled: false,
  })

  const inputHandler =
    (fields: object, stateHandler: Dispatch<SetStateAction<CheckboxValue>>) =>
    (name: string, value: boolean) =>
      stateHandler({
        ...fields,
        [name]: value,
      })

  const setRadioFields = inputHandler(radiosState, setRadiosState)
  const changeRadioHandler = handleInputChange(setRadioFields)
  return (
    <Row>
      <Col sm={12}>
        <h1>Bootstrap</h1>
      </Col>
      <Col sm={6}>
        <ComponentContainer name='Radios'>
          <Radio
            name='option'
            checked={radiosState.option}
            onChange={changeRadioHandler}
          >
            Option checked
          </Radio>
          <Radio
            name='checked'
            checked={radiosState.checked}
            onChange={changeRadioHandler}
          >
            Option
          </Radio>
          <Radio
            name='disabledChecked'
            checked={radiosState.disabledChecked}
            onChange={changeRadioHandler}
          >
            Disabled checked
          </Radio>
          <Radio
            name='disabled'
            checked={radiosState.disabled}
            onChange={changeRadioHandler}
          >
            Disabled
          </Radio>
        </ComponentContainer>
      </Col>
    </Row>
  )
}

export default Checkboxes
