import {useState} from 'react'

import {RadioBtn, RadioGroup} from '../../../../../DS/components'
import {uniquely} from '../../../../../utils/helpers'

interface RadioOption {
  label: string
}

const CustomRadioButtons = () => {
  const [selected, setSelected] = useState<number>()

  const options: RadioOption[] = [
    {label: 'Option 1'},
    {label: 'Option 2'},
    {label: 'Option 3'},
  ]

  return (
    <RadioGroup inline>
      {options.map((option, index) => (
        <RadioBtn
          label={option.label}
          key={uniquely()}
          checked={index === selected}
          onChange={() => setSelected(index)}
        />
      ))}
    </RadioGroup>
  )
}

export default CustomRadioButtons
