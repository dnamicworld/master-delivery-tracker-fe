import {
  Col,
  Row,
  ControlLabel,
  Icon,
  FormGroup,
} from '../../../../../DS/components'
import {FA_ICONS, ICON_TYPES} from '../../../../../utils/constants'
import {ComponentContainer} from '../../index'

import CustomCheckboxGroup from './CustomCheckboxGroup'
import CustomRadioButtons from './CustomRadioButtons'

const CustomCheckboxes = () => (
  <Row>
    <Col sm={12}>
      <h1>Custom</h1>
    </Col>
    <Col sm={6}>
      <ComponentContainer name='Checkboxes'>
        <Row>
          <Col sm={12}>
            <ControlLabel>
              Required{' '}
              <Icon type={ICON_TYPES.fas} iconName={FA_ICONS.asterisk} />
            </ControlLabel>
            <FormGroup>
              <CustomCheckboxGroup />
            </FormGroup>
          </Col>
          <Col sm={12}>
            <ControlLabel>
              Success <Icon type={ICON_TYPES.fas} iconName={FA_ICONS.check} />
            </ControlLabel>
            <FormGroup>
              <CustomCheckboxGroup />
            </FormGroup>
          </Col>
          <Col sm={12}>
            <ControlLabel>
              Warning{' '}
              <Icon type={ICON_TYPES.fas} iconName={FA_ICONS.exclamation} />
            </ControlLabel>
            <FormGroup>
              <CustomCheckboxGroup />
            </FormGroup>
          </Col>
          <Col sm={12}>
            <ControlLabel>
              Error{' '}
              <Icon type={ICON_TYPES.fas} iconName={FA_ICONS.exclamation} />
            </ControlLabel>
            <FormGroup>
              <CustomCheckboxGroup />
            </FormGroup>
          </Col>
        </Row>
      </ComponentContainer>
    </Col>
    <Col sm={6}>
      <ComponentContainer name='Radios'>
        <Row>
          <Col sm={12}>
            <CustomRadioButtons />
          </Col>
        </Row>
      </ComponentContainer>
    </Col>
  </Row>
)

export default CustomCheckboxes
