import {Panel} from '../../../DS/components'

const Panels = () => (
  <Panel header={<span>Panel title</span>} withBottomDivider>
    <p>Panel content</p>
  </Panel>
)

export default Panels
