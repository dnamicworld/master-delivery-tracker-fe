import {useState} from 'react'

import {DatePicker} from '../../../DS/components'
import {DEFAULT_MAX_YEAR, DEFAULT_MIN_YEAR} from '../../../utils/constants'

const ComponentDatePickerIcon = () => {
  const [datePicker, setDatePicker] = useState<Date>()

  const onChange = (name: string, date: Date) => {
    setDatePicker(date)
  }

  return (
    <DatePicker
      name='datePicker'
      placeholder='mm/dd/yyyy'
      value={datePicker}
      onChange={onChange}
      showYearMonths
      showIcon
      minYear={DEFAULT_MIN_YEAR}
      maxYear={DEFAULT_MAX_YEAR}
    />
  )
}

export default ComponentDatePickerIcon
