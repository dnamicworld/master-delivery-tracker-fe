import {Col, List, ListItem} from '../../../../DS/components'
import {BarChart} from '../../../../components'

import {AssetTypeStat} from '../../../../models'
import {getBarData, union, uniquely} from '../../../../utils/helpers'

const noReportData = (reportType: string) => ({
  name: '-',
  pv: '',
  leyend: `No ${reportType} Data Available`,
})

const setBarData = (type: string, data?: AssetTypeStat) => {
  if (data) {
    return data.expectedCount ? getBarData(data) : noReportData(type)
  }
  return noReportData(type)
}

enum ReportChartLeyends {
  ALBUM = 'Album/EP',
  SINGLE = 'Singles',
}

const {ALBUM, SINGLE} = ReportChartLeyends

const getBarChartName = (
  albumsDataArray: AssetTypeStat[],
  singlesDataArray: AssetTypeStat[],
  assetId: number,
): string | undefined => {
  const albumsData = albumsDataArray.find((item) => item.id === assetId)
  const singlesData = singlesDataArray.find((item) => item.id === assetId)
  return albumsData?.longName ?? singlesData?.longName
}
const getBarChartData = (
  albumsDataArray: AssetTypeStat[],
  singlesDataArray: AssetTypeStat[],
  assetId: number,
): {name: string; pv: string | number; leyend?: string}[] => {
  const albumsData = albumsDataArray.find((item) => item.id === assetId)
  const singlesData = singlesDataArray.find((item) => item.id === assetId)
  const chartData = []
  chartData.push(setBarData(ALBUM, albumsData))
  chartData.push(setBarData(SINGLE, singlesData))
  return chartData
}
interface BarChartContentProps {
  assetsAlbums: AssetTypeStat[]
  assetsSingles: AssetTypeStat[]
}
const BarChartContent = ({
  assetsAlbums,
  assetsSingles,
}: BarChartContentProps) => {
  const assetsId: number[] = union(
    assetsAlbums?.map((item) => item.id),
    assetsSingles?.map((item) => item.id),
  )

  return (
    <List>
      {assetsId.map((assetId) => (
        <ListItem key={`listItem${uniquely()}`}>
          <Col sm={2} className='pl-0 pr-0 font-weight-normal text-black'>
            {getBarChartName(assetsAlbums, assetsSingles, assetId)}
          </Col>
          <Col sm={10} className='pl-0 pr-0 barchart'>
            <BarChart
              data={getBarChartData(assetsAlbums, assetsSingles, assetId)}
              xKey='name'
              yKey='pv'
              layout='vertical'
              withNumericAxis
            />
          </Col>
        </ListItem>
      ))}
    </List>
  )
}
export default BarChartContent
