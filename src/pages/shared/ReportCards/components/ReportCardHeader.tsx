import classNames from 'classnames'
import {ReactNode} from 'react'

import {useHistory} from 'react-router-dom'

import {Button, ButtonToolbar, Icon, IconProps} from '../../../../DS/components'
import {CardHeader} from '../../../../components'
import useUser from '../../../../hooks/user'
import {Report} from '../../../../models'
import {FA_ICONS, ICON_TYPES, ROUTES} from '../../../../utils/constants'
import {ReportCard, stringifyQuery} from '../../../../utils/helpers'
import {goToAddProduct} from '../../IndexPages'
import {getIndexQueryParams} from '../../IndexPages/IndexPages.controller'

interface ButtonProps {
  content: ReactNode
  icon?: IconProps
  onClick: () => void
  className?: string
}

const addBtn = ({content, onClick, icon, className}: ButtonProps) => (
  <Button
    className={classNames('pr-0 font-weight-sbold primary', className)}
    containerStyle='link'
    onClick={onClick}
  >
    {content}
    {icon ? (
      <Icon className='ml-5' type={icon.type} iconName={icon.iconName} />
    ) : null}
  </Button>
)

const {ALBUM_INDEX, SINGLE_INDEX, SINGLE_DETAIL} = ROUTES

interface ReportCardHeaderProps {
  reportCard: ReportCard
}

const getReportUrl = (rootUrl: string, report?: Report): string => {
  const albumRoute = `${rootUrl}/${report?.label.id}`
  const albumQueryParams = stringifyQuery(getIndexQueryParams(report))
  return `${albumRoute}?${albumQueryParams}`
}

const ReportCardHeader = ({reportCard}: ReportCardHeaderProps) => {
  const history = useHistory()
  const {user} = useUser()

  const {albums: reportAlbums, singles: reportSingles} = reportCard
  const report = reportAlbums ?? reportSingles
  const cardTitle = `${report?.label.name} Assets Delivered ${report?.year}`

  const albumRoute = getReportUrl(ALBUM_INDEX, reportAlbums)
  const singleRoute = getReportUrl(SINGLE_INDEX, reportSingles)

  return (
    <>
      <CardHeader title={cardTitle} img={report?.label.logo?.url} />
      <ButtonToolbar bsClass='align-self-center'>
        {reportAlbums
          ? addBtn({
              onClick: () => history.push(albumRoute),
              content: 'Album Index',
              className: 'mr-40',
            })
          : null}
        {reportSingles
          ? addBtn({
              onClick: () => history.push(singleRoute),
              content: 'Singles Index',
              className: 'mr-40',
            })
          : null}
        {reportAlbums && user.roles.hasFullWriteAccess()
          ? addBtn({
              icon: {
                type: ICON_TYPES.far,
                iconName: FA_ICONS.circlePlus,
              },
              onClick: () => goToAddProduct(history, reportAlbums.id),
              content: 'Add Album',
              className: 'mr-40',
            })
          : null}
        {reportSingles && user.roles.hasFullWriteAccess()
          ? addBtn({
              icon: {
                type: ICON_TYPES.far,
                iconName: FA_ICONS.circlePlus,
              },
              onClick: () =>
                goToAddProduct(history, reportSingles.id, SINGLE_DETAIL),
              content: 'Add Single',
            })
          : null}
      </ButtonToolbar>
    </>
  )
}

export default ReportCardHeader
