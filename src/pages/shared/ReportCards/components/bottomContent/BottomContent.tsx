import {useState} from 'react'

import {Button, Icon} from '../../../../../DS/components'
import {ReportQueryParams} from '../../../../../hooks/data/useReports'
import {FA_ICONS, ICON_TYPES} from '../../../../../utils/constants'

import {ReportCard} from '../../../../../utils/helpers'
import DeleteLabelModal from '../reportModal/ReportModal'

interface BottomContentProps {
  reportCard: ReportCard
  queryParams: ReportQueryParams
}
const BottomContent = ({reportCard, queryParams}: BottomContentProps) => {
  const [show, setShow] = useState(false)
  const {albums: reportAlbums, singles: reportSingles} = reportCard

  const getIds = () => {
    const values = []
    if (reportAlbums) {
      values.push(reportAlbums.id)
    }
    if (reportSingles) {
      values.push(reportSingles.id)
    }
    return values
  }
  const reportIds = getIds()
  const onClose = () => {
    setShow(false)
  }
  const showModal = () => {
    setShow(true)
  }
  return (
    <>
      <Button className='pr-0' containerStyle='link' onClick={showModal}>
        <Icon
          className='ml-5'
          type={ICON_TYPES.far}
          iconName={FA_ICONS.trash}
        />
      </Button>
      <DeleteLabelModal
        shown={show}
        onClose={onClose}
        reportIds={reportIds}
        queryParams={queryParams}
      />
    </>
  )
}
export default BottomContent
