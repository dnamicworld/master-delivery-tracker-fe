import {useState} from 'react'

import {mutate} from 'swr'

import {Checkbox} from '../../../../../DS/components'
import {ContentModal} from '../../../../../components'
import {appFetcher} from '../../../../../config/axios'
import {ReportQueryParams} from '../../../../../hooks/data/useReports'
import {PaginationMutateResponse, Report} from '../../../../../models'
import {
  ApiMethods,
  API_ROUTES,
  DELETE_REPORT,
} from '../../../../../utils/constants'
import {getEndpointURL} from '../../../../../utils/helpers'

interface DeleteLabelModalProps {
  shown: boolean
  onClose: () => void
  reportIds: number[]
  queryParams: ReportQueryParams
}

const DeleteLabelModal = ({
  shown,
  onClose,
  reportIds,
  queryParams,
}: DeleteLabelModalProps) => {
  const [checked, setChecked] = useState(false)

  const onCloseModal = () => {
    setChecked(false)
    onClose()
  }

  const deleteReport = async () => {
    try {
      mutate(
        getEndpointURL(API_ROUTES.REPORTS, queryParams),
        (response?: PaginationMutateResponse<Report>) => {
          const data = response?.data.filter(
            (report) => !reportIds.includes(report.id),
          )

          return {...response, data}
        },
      )
      const deletes = reportIds.map((reportId) => {
        const URL = getEndpointURL(API_ROUTES.REPORTS, {id: reportId})
        return appFetcher(URL, {method: ApiMethods.DELETE})
      })
      await Promise.all(deletes)
      mutate(getEndpointURL(API_ROUTES.REPORTS, queryParams))
      onCloseModal()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('Error')
    }
  }

  return (
    <ContentModal
      show={shown}
      title={DELETE_REPORT.title}
      onActionClick={deleteReport}
      onCancelAction={onCloseModal}
      disabledSuccessButton={!checked}
    >
      <Checkbox
        className='d-inline-block'
        checked={checked}
        onChange={() => {
          setChecked(!checked)
        }}
      >
        {DELETE_REPORT.check}
      </Checkbox>
    </ContentModal>
  )
}

export default DeleteLabelModal
