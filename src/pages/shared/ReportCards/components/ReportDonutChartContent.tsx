import AssetChart from '../../../../components/AssetChart/AssetChart'
import {AssetTypeStat} from '../../../../models'
import {
  ALBUM_TYPE,
  PRODUCT_TYPE_LABELS,
  SINGLE_TYPE,
} from '../../../../utils/constants'
import {getDonutChartData, isEmpty} from '../../../../utils/helpers'

interface ReportDonutChartContentProps {
  assetsAlbums: AssetTypeStat[]
  assetsSingles: AssetTypeStat[]
}

const ReportDonutChartContent = ({
  assetsAlbums,
  assetsSingles,
}: ReportDonutChartContentProps) => {
  const assetAlbumsData = getDonutChartData(
    assetsAlbums,
    PRODUCT_TYPE_LABELS.ALBUM,
  )
  const assetSinglesData = getDonutChartData(
    assetsSingles,
    PRODUCT_TYPE_LABELS.SINGLE,
  )
  return (
    <>
      {!isEmpty(assetsAlbums) ? (
        <AssetChart
          data={assetAlbumsData}
          listViewType={ALBUM_TYPE}
          text='Total Delivery'
        />
      ) : null}
      {!isEmpty(assetsSingles) ? (
        <AssetChart
          data={assetSinglesData}
          listViewType={SINGLE_TYPE}
          text='Total Delivery'
        />
      ) : null}
    </>
  )
}

export default ReportDonutChartContent
