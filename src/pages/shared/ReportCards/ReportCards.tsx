import {Dispatch, SetStateAction} from 'react'

import {DynamicPagination, PaginationOptions} from '../../../DS/components'

import {DashboardCard} from '../../../components'

import {ReportQueryParams} from '../../../hooks/data/useReports'
import {uniquely, ReportCard, getAssets} from '../../../utils/helpers'

import {
  BarChartContent,
  ReportDonutChartContent,
  ReportCardHeader,
  BottomContent,
} from './components'

interface ReportCardsProps {
  reports?: ReportCard[]
  noReportsMessage?: string
  paginationOptions: PaginationOptions
  setPaginationOptions: Dispatch<SetStateAction<PaginationOptions>>
  queryParams: ReportQueryParams
}

export const reportsPage = 7

const ReportCards = ({
  reports,
  noReportsMessage,
  paginationOptions,
  setPaginationOptions,
  queryParams,
}: ReportCardsProps) => (
  <>
    {reports?.length ? (
      <DynamicPagination
        paginationSize={paginationOptions.pages}
        paginationOptions={paginationOptions}
        setPaginationOptions={setPaginationOptions}
      >
        <>
          {reports.map((reportCard) => {
            const [assetsAlbums, assetsSingles] = getAssets(reportCard)

            return (
              <DashboardCard
                key={uniquely()}
                cardHeader={<ReportCardHeader reportCard={reportCard} />}
                leftContent={
                  <ReportDonutChartContent
                    assetsAlbums={assetsAlbums}
                    assetsSingles={assetsSingles}
                  />
                }
                rightContent={
                  <BarChartContent
                    assetsAlbums={assetsAlbums}
                    assetsSingles={assetsSingles}
                  />
                }
                bottomContent={
                  <BottomContent
                    reportCard={reportCard}
                    queryParams={queryParams}
                  />
                }
              />
            )
          })}
        </>
      </DynamicPagination>
    ) : (
      <h3 className='text-center'>{noReportsMessage}</h3>
    )}
  </>
)

export default ReportCards
