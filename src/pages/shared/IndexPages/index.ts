export {
  getIndexTableHeaders,
  IndexPagesTable,
  IndexPagesFilters,
  IndexPageSwitchers,
  IndexTableActions,
  IndexDashboardCard,
  IndexCardPlaceholder,
} from './components'
export type {IndexTableProps, IndexPagesTableProps} from './components'
export {
  filterProducts,
  getDetailPathname,
  getPageData,
  getPageLabelName,
  getIndexBreadcrumbs,
  goToAddProduct,
} from './IndexPages.controller'

export type {IndexPageData, ProductIndexRouteParams} from './IndexPages.model'
