import {RouterProps} from 'react-router-dom'
import {BreadcrumbItem} from 'src/DS/components'
import {filterByValue, shouldFilter} from 'src/utils/helpers'

import {FilterObject} from '../../../context/Filters/pageFilters'
import {Label, Product, ProductFilters, Report, Actions} from '../../../models'

import {
  ACTIONS,
  BREADCRUMBS,
  ROUTES,
  TOTAL_DELIVERED,
} from '../../../utils/constants'
import {
  cleanInt,
  cloneDeep,
  compareDates,
  getReleaseTypeFromString,
  isEmpty,
  stringifyQuery,
} from '../../../utils/helpers'

import {IndexPageData} from './index'

const {ALBUM_INDEX, ALBUM_DETAIL} = ROUTES

export const updateProductAction = (
  products: Product[],
  productToUpdate: Product,
  action: Actions,
): Product[] => {
  let newProducts: Product[] = []
  if (action === ACTIONS.UPDATE) {
    newProducts = products.map((product) =>
      cloneDeep(product.id !== productToUpdate.id ? product : productToUpdate),
    )
  }
  if (action === ACTIONS.DELETE) {
    newProducts = products.filter(
      (product) => product.id !== productToUpdate.id,
    )
  }
  return newProducts
}

export const getReleaseTypesFromFilterArray = (
  releaseType: ProductFilters['releaseType'][],
): string =>
  releaseType.map((release) => getReleaseTypeFromString(release)).toString()

export const getIndexQueryParams = (report?: Report) => ({
  artist: [],
  year: String(report?.year),
  releaseType: report?.listViewType ?? '',
})

export const getPageData = (
  pathname: string,
): Omit<IndexPageData, 'RELEASE_TYPE' | 'TITLE'> => {
  const isAlbumPage = pathname.includes(ALBUM_INDEX)
  return {
    BREAD_CRUMB: isAlbumPage
      ? {...BREADCRUMBS.ALBUM_INDEX, active: true}
      : BREADCRUMBS.SINGLE_INDEX,
    LABEL: '',
  }
}

export const getPageLabelName = (label?: Label | Label[]): string =>
  !Array.isArray(label) ? String(label?.name) : ''

export const getIndexBreadcrumbs = (breadcrumbItem: BreadcrumbItem) => {
  const breadCrumbs: BreadcrumbItem[] = [
    {
      ...BREADCRUMBS.HOME,
    },
  ]
  breadCrumbs.push({
    ...breadcrumbItem,
    link: '#',
    active: true,
  })
  return breadCrumbs
}

export const getDetailPathname = (detailRoute: string, id: number): string =>
  `${detailRoute}/${id}`

export const goToAddProduct = (
  history: RouterProps['history'],
  reportId: number,
  pathname: string = ALBUM_DETAIL,
) => {
  history.push({
    pathname,
    search: stringifyQuery({
      editing: 'true',
      reportId: String(reportId),
    }),
  })
}

const filterByReleaseDate = (filter: string, value?: string | null): boolean =>
  shouldFilter(filter, compareDates(filter, value))

const inDeliveredRange = (
  deliveryFilters: string[],
  value?: number,
): boolean => {
  if (
    isEmpty(deliveryFilters) ||
    deliveryFilters.length === TOTAL_DELIVERED.length
  ) {
    return true
  }

  if (value === undefined) {
    return false
  }

  return deliveryFilters.some((filter) => {
    const [from, to] = filter.split(' - ')
    return value >= cleanInt(from) && value <= cleanInt(to)
  })
}

const filterByTotalDeliveredRange = (
  filter: string[],
  value?: number,
): boolean => shouldFilter(filter, inDeliveredRange(filter, value))

const productsFilter = (filters: FilterObject, productsData: Product[]) => {
  const {
    artist: artistFilter,
    delivered,
    releaseDate: releaseDateFilter,
    subLabel,
  } = filters
  const checkArtist = artistFilter?.length
  const checkDelivered = delivered?.length
  const checkReleaseDate = !!releaseDateFilter
  const checkSubLabel = subLabel?.length
  const applyFilters =
    checkArtist || checkDelivered || checkReleaseDate || checkSubLabel
  return applyFilters
    ? productsData.filter(
        ({artist, releaseDate, marketingLabel, stats}) =>
          (!checkArtist || filterByValue(artistFilter, artist)) &&
          (!checkReleaseDate ||
            filterByReleaseDate(releaseDateFilter, releaseDate)) &&
          (!checkSubLabel || filterByValue(subLabel, marketingLabel)) &&
          (!checkDelivered ||
            filterByTotalDeliveredRange(delivered, stats?.deliveryPercent)),
      )
    : cloneDeep(productsData)
}

export const filterProducts = (
  filtersCriteria: FilterObject,
  productsToFilter?: Product[] | Product,
) =>
  Array.isArray(productsToFilter)
    ? productsFilter(filtersCriteria, productsToFilter)
    : []
