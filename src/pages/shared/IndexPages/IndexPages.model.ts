import {ReactElement} from 'react'

import {BreadcrumbItem} from '../../../DS/components'

export interface ProductIndexRouteParams {
  labelId: string
}

export interface IndexPageData {
  BREAD_CRUMB: BreadcrumbItem
  LABEL: string
  TITLE: ReactElement
  RELEASE_TYPE: string
}
