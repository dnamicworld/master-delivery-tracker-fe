import classNames from 'classnames'

import {Card, Row} from '../../../../../DS/components'
import {Placeholder} from '../../../../../components'

import {uniquely} from '../../../../../utils/helpers'

const placeholderRows = Array(9).fill(undefined)

const IndexCardPlaceholder = () => (
  <Card className='mb-40 px-30 py-40' header={<Placeholder width='124px' />}>
    <Row className={classNames('d-flex px-10')}>
      <Placeholder
        borderCircle
        height='159px'
        width='159px'
        className='px-10 py-5 d-flex'
      />

      <div className='py-5 ml-40 '>
        {placeholderRows.map(() => (
          <div key={uniquely()} className='mb-10'>
            <div className='d-inline-block mb-10'>
              <Placeholder width='96px' className='mr-40' />
              <Placeholder width='96px' className='ml-40 mr-20' />
              <Placeholder width='716px' className=' mr-20' />
              <Placeholder width='32px' />
            </div>
          </div>
        ))}
      </div>
    </Row>
  </Card>
)
export default IndexCardPlaceholder
