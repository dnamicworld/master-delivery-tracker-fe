import {DashboardCard} from '../../../../components'
import AssetChart from '../../../../components/AssetChart/AssetChart'
import {AssetTypeStat, Product, ListViewType} from '../../../../models'
import {PRODUCT_INDEXES} from '../../../../utils/constants'
import {
  cloneDeep,
  flatten,
  get,
  getDonutChartData,
  groupBy,
  values,
} from '../../../../utils/helpers'
import {barChartContentFromProducts, cardHeader} from '../../ProductCards'

interface IndexDashboardCardProps {
  products: Product[]
  releaseType: string
}

export const reduceAssets = (products: Product[]): AssetTypeStat[] =>
  values(
    groupBy(
      flatten(products.map((item) => get(item, 'stats.assetTypeStats'))),
      'id',
    ),
  ).map((assetsArrayGroup) =>
    assetsArrayGroup.reduce((prev, current) => {
      const prevCopy = cloneDeep(prev)
      return {
        ...prevCopy,
        deliveryCount: prevCopy.deliveryCount + current.deliveryCount,
        expectedCount: prevCopy.expectedCount + current.expectedCount,
      }
    }),
  )

const IndexDashboardCard = ({
  products,
  releaseType,
}: IndexDashboardCardProps) => {
  const assetsReduced = reduceAssets(products)
  const assetAlbumsData = getDonutChartData(assetsReduced, releaseType)
  const contentIndex = PRODUCT_INDEXES[releaseType]
  return (
    <DashboardCard
      cardHeader={cardHeader()}
      leftContent={
        <AssetChart
          big
          data={assetAlbumsData}
          listViewType={releaseType as keyof typeof ListViewType}
        />
      }
      rightContent={barChartContentFromProducts(assetsReduced, contentIndex)}
    />
  )
}
export default IndexDashboardCard
