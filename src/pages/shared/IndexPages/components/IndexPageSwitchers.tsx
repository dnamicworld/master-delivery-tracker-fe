import {useHistory} from 'react-router-dom'

import {
  ReleaseTypeFilter,
  SelectOption,
  SelectProps,
  YearFilter,
} from '../../../../components'
import {FilterObject} from '../../../../context/Filters/pageFilters'

import {Report} from '../../../../models'
import {
  INDEX_PAGES_INITIAL_FILTERS,
  PRODUCT_TYPE_LABELS,
  RELEASE_TYPE_MAPPER,
  ROUTES,
} from '../../../../utils/constants'
import {cloneDeep, isAlbum, stringifyQuery} from '../../../../utils/helpers'

const {ALBUM_INDEX, SINGLE_INDEX} = ROUTES

interface IndexPageSwitchersProps {
  report: Report
  filters: FilterObject
  label: string
  setFiltersState: <T>(filters: T, initial?: T) => void
  resetFilters: number
}

const IndexPageSwitchers = ({
  report,
  filters,
  label,
  setFiltersState,
  resetFilters,
}: IndexPageSwitchersProps) => {
  const history = useHistory()
  const releaseType = PRODUCT_TYPE_LABELS[filters.releaseType]
  const labelId = String(report?.label.id)

  const releaseTypeRedirect: SelectProps['onChange'] = (event, value) => {
    const currentEvent = (event.currentTarget ||
      event.target) as HTMLInputElement
    const productType = RELEASE_TYPE_MAPPER[String(value)]
    const route = isAlbum(String(productType)) ? ALBUM_INDEX : SINGLE_INDEX
    const productRoute = `${route}/${report?.label.id}`
    const filterstoSet = {
      year: filters.year,
      [currentEvent.name]: productType,
    }
    history.push(`${productRoute}?${stringifyQuery(filterstoSet)}`)
  }

  const setYear = (value: string) => {
    const filtersToSet = {
      ...cloneDeep(INDEX_PAGES_INITIAL_FILTERS),
      releaseType: filters.releaseType,
      year: value,
    }
    setFiltersState(filtersToSet, filtersToSet)
  }

  const handleYearChange = ({target: {value}}: {target: any}) => setYear(value)

  const handleCheckOnEnter = (_: string, filteredOptions: SelectOption[]) => {
    if (filteredOptions.length === 1) {
      setYear(filteredOptions[0].value)
    }
  }

  return (
    <>
      <YearFilter
        plain
        value={filters?.year ?? ''}
        labelId={labelId}
        name='year'
        onChange={handleYearChange}
        handleCheckOnEnter={handleCheckOnEnter}
        resetFilters={resetFilters}
      />{' '}
      {label}{' '}
      <ReleaseTypeFilter
        plain
        value={releaseType ?? ''}
        name='releaseType'
        onChange={releaseTypeRedirect}
      />
    </>
  )
}

export default IndexPageSwitchers
