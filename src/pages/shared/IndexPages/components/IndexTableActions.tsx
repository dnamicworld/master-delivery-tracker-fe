import {useHistory} from 'react-router-dom'

import {goToAddProduct} from '..'
import {
  Button,
  ButtonToolbar,
  Icon,
  HeaderTitle,
} from '../../../../DS/components'
import {TableActions, UnsavedPrompt} from '../../../../components'
import {useProductFilterContext} from '../../../../context/Filters/productsFilters'
import useUser from '../../../../hooks/user'
import {Report} from '../../../../models'
import {
  FA_ICONS,
  ICON_TYPES,
  PRODUCT_TYPE_ROUTES,
} from '../../../../utils/constants'

interface IndexTableActionsProps {
  report?: Report
}

const IndexTableActions = ({report}: IndexTableActionsProps) => {
  const history = useHistory()
  const {user} = useUser()
  const canEdit = user.roles.hasFullOrPartialWriteAccess()
  const {isEditing, noChanges, setIsEditing, handleCancel, handleSave} =
    useProductFilterContext()

  const productType = report?.listViewType ?? ''
  const blockNavigation = isEditing && !noChanges

  let tableActions = null

  if (canEdit) {
    if (isEditing) {
      tableActions = (
        <ButtonToolbar>
          <Button label='Save' className='pull-right' onClick={handleSave} />
          <Button
            label='Cancel'
            className='pull-right'
            containerStyle='link'
            onClick={handleCancel}
          />
        </ButtonToolbar>
      )
    } else {
      tableActions = (
        <ButtonToolbar bsClass='align-self-center'>
          <Button
            className='pull-right'
            onClick={
              report
                ? () =>
                    goToAddProduct(
                      history,
                      report.id,
                      PRODUCT_TYPE_ROUTES[productType],
                    )
                : undefined
            }
          >
            <Icon
              className='mr-10'
              type={ICON_TYPES.fas}
              iconName={FA_ICONS.plus}
            />
            New Project
          </Button>
          <Button
            className='pull-right mr-10'
            containerStyle='link'
            onClick={() => {
              setIsEditing(true)
            }}
          >
            Edit Projects
          </Button>
        </ButtonToolbar>
      )
    }
  }

  return (
    <>
      <UnsavedPrompt block={blockNavigation} />
      <TableActions
        title={<HeaderTitle>Projects</HeaderTitle>}
        actions={tableActions}
      />
    </>
  )
}

export default IndexTableActions
