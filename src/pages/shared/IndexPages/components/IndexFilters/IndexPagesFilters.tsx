import {ReactNode, useMemo} from 'react'

import {Col, Row} from '../../../../../DS/components'
import {
  ArtistFilter,
  ReleaseDateFilter,
  SelectOption,
  SubLabelsFilter,
  TotalDeliveredFilter,
} from '../../../../../components'
import {FilterWrapperProps} from '../../../../../context/Filters/productsFilters'
import {Product, Report} from '../../../../../models'
import {filterKeyFromArray} from '../../../../../utils/helpers'

interface IndexPagesFiltersProps extends FilterWrapperProps {
  report?: Report
  products: Product[]
  children?: ReactNode
  handleCheckOnEnter: (name: string, filteredOptions: SelectOption[]) => void
  setFiltersState: <T>(filters: T, initial?: T) => void
}

const IndexPagesFilters = ({
  filters,
  handleFilterChange,
  handleCheckOnEnter,
  report,
  products,
  children,
  resetFilters,
  setFiltersState,
}: IndexPagesFiltersProps) => {
  const subLabels = useMemo(
    () => filterKeyFromArray<Product>(products, 'marketingLabel', true),
    [products],
  )

  const artists = useMemo(
    () => filterKeyFromArray<Product>(products, 'artist', false),
    [products],
  )
  return (
    <Row className='filters album_filters'>
      <Col sm={3}>
        <SubLabelsFilter
          value={filters?.subLabel ?? ''}
          name='subLabel'
          label={report?.label.name ?? ''}
          subLabels={subLabels}
          fullWidth
          filters={filters}
          resetFilters={resetFilters}
          handleCheckOnEnter={handleCheckOnEnter}
          setFiltersState={setFiltersState}
        />
      </Col>
      <Col sm={3}>
        <ArtistFilter
          value={filters?.artist ?? ''}
          name='artist'
          options={artists}
          onChange={handleFilterChange}
          handleCheckOnEnter={handleCheckOnEnter}
          fullWidth
          resetFilters={resetFilters}
        />
      </Col>
      <Col sm={3}>
        <ReleaseDateFilter
          name='releaseDate'
          defaultYear={report?.year}
          reportId={report?.id}
          fullWidth
          setFiltersState={setFiltersState}
          filters={filters}
        />
      </Col>
      <Col sm={3}>
        <TotalDeliveredFilter
          value={filters?.delivered ?? []}
          name='delivered'
          onChange={handleFilterChange}
          fullWidth
        />
      </Col>
      {children}
    </Row>
  )
}

export default IndexPagesFilters
