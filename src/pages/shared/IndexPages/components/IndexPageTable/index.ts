export {default as IndexPagesTable} from './IndexPageTable'
export {getIndexTableHeaders} from './IndexPageTableController'
export type {
  IndexTableProps,
  IndexPagesTableProps,
} from './indexPageTable.model'
