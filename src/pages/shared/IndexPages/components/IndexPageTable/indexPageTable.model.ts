import {GridReadyEvent} from 'ag-grid-community'
import {Column} from 'ag-grid-community/dist/lib/entities/column'
import {Dispatch, SetStateAction} from 'react'

import {AgGridTableProps} from '../../../../../components/AgGrid/AggridTable'
import {Product, Actions} from '../../../../../models'

export interface IndexTableProps {
  product: Product[]
  setProduct: Dispatch<SetStateAction<Product[]>>
}

export interface IndexPagesTableProps
  extends Omit<AgGridTableProps<Product>, 'rowData'> {
  products: Product[]
  updateProduct: (
    product: Product,
    action: Actions,
    isEditing: boolean,
    column?: Column,
  ) => void
  detailRoute: string
  getGridReadyEvent?: (gridApi: GridReadyEvent) => void
}
