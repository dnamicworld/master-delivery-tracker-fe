import {ColDef, ValueGetterParams} from 'ag-grid-community'

import {
  getCheckTooltipText,
  getCheckTooltipTextColor,
} from '../../../../../DS/components'
import {CustomCellParams} from '../../../../../components/AgGrid/CellRenderers'
import {
  AppColors,
  FA_ICONS,
  ICON_TYPES,
  PRODUCT_TYPE_DISPLAY,
  TABLE,
} from '../../../../../utils/constants'
import {
  formatBlankValues,
  getAssetsDeliveredPercentage,
  getDateString,
} from '../../../../../utils/helpers'
import {
  comparatorAgGridArtist,
  sortDates,
  sortDefaultWMG,
  sortPercentages,
} from '../../../../../utils/helpers/sorts'

export const IndexTableColumns = {
  admin: (canEdit: boolean) => ({
    headerName: 'A&R Admin',
    editable: canEdit,
    field: 'adminName',
    cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
    valueFormatter: formatBlankValues,
    cellRendererParams: {
      cellRenderer: TABLE.CELL_RENDERERS.WithEditButton,
      className: 'align-items-center d-flex',
      showValueOnTooltip: true,
      fieldProps: {
        placeholder: 'A&R Admin',
      },
    },
    sortable: true,
    unSortIcon: true,
    flex: 4,
    comparator: sortDefaultWMG,
  }),
  comments: (canEdit: boolean) => ({
    headerName: 'Comments',
    field: 'arComments',
    flex: 5,
    editable: canEdit,
    cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
    valueFormatter: formatBlankValues,
    cellRendererParams: {
      cellRenderer: TABLE.CELL_RENDERERS.WithEditButton,
      className: 'align-items-center d-flex',
      showValueOnTooltip: true,
      fieldProps: {
        placeholder: 'Comments',
      },
    },
  }),
  titleArtist: (detailRoute: (data: any) => string) => ({
    headerName: 'Title & Artist',
    field: 'title',
    cellRenderer: 'titleRenderer',
    flex: 5,
    sortable: true,
    unSortIcon: true,
    comparator: comparatorAgGridArtist,
    cellRendererParams: {
      detailRoute,
    },
  }),
  totalPercent: (
    canEdit: boolean,
    handleAlbumComplete: (editableParams: CustomCellParams) => void,
  ) => ({
    headerName: 'Total %',
    field: 'stats',
    cellRenderer: TABLE.CELL_RENDERERS.CustomCell,
    flex: 2,
    valueGetter: (params: ValueGetterParams) =>
      `${getAssetsDeliveredPercentage(params.data.stats.assetTypeStats)}%`,
    sortable: true,
    unSortIcon: true,
    comparator: sortPercentages,
    cellRendererParams: {
      getClassNames: ({data}: CustomCellParams) =>
        data.complete ? `text-${AppColors.success}` : '',
      leftIcon: {
        disabled: !canEdit,
        getIconColor: getCheckTooltipTextColor,
        type: ICON_TYPES.fas,
        iconName: FA_ICONS.check,
        onClick: handleAlbumComplete,
        tooltip: {
          textBuilder: getCheckTooltipText,
        },
      },
    },
  }),
  complete: {
    headerName: 'complete',
    field: 'complete',
    hide: true,
  },
  productType: {
    headerName: 'Product Type',
    field: 'productType',
    flex: 2,
    cellRendererParams: {
      className: 'align-items-center d-flex',
    },
    valueGetter: (params: ValueGetterParams) => {
      const value = params.data.productType
      return PRODUCT_TYPE_DISPLAY[value]
    },
  },
  releaseDate: {
    headerName: 'Release Date',
    field: 'releaseDate',
    valueGetter: (params: ValueGetterParams) =>
      getDateString({
        dateString: params.data.releaseDate,
        shortMonth: true,
        dayFirst: true,
        dateSeparator: ' ',
      }),
    sortable: true,
    unSortIcon: true,
    flex: 2,
    comparator: sortDates,
  },
}

export const getIndexTableHeaders = (
  handleAlbumComplete: (editableParams: CustomCellParams) => void,
  handleEditRowClick: (editableColumn: CustomCellParams) => void,
  handleDeleteRowClick: (editableColumn: CustomCellParams) => void,
  canEdit: boolean,
  detailRoute: string,
  canUpdateProducts: boolean,
  isEditing: boolean,
): ColDef[] => {
  const deleteBtn = canUpdateProducts
    ? {
        type: ICON_TYPES.far,
        iconName: FA_ICONS.trash,
        onClick: handleDeleteRowClick,
        className: 'p-0',
      }
    : null
  const ARColumn = IndexTableColumns.admin(canEdit)
  const CommentsColumn = IndexTableColumns.comments(canEdit)

  return [
    {
      ...ARColumn,
      cellRendererParams: {
        ...ARColumn.cellRendererParams,
        editing: isEditing && canEdit,
      },
    },
    {
      ...CommentsColumn,
      cellRendererParams: {
        ...CommentsColumn.cellRendererParams,
        editing: isEditing && canEdit,
      },
    },
    {
      ...IndexTableColumns.titleArtist(() => detailRoute),
    },
    {
      ...IndexTableColumns.totalPercent(canEdit, handleAlbumComplete),
    },
    IndexTableColumns.complete,
    IndexTableColumns.productType,
    IndexTableColumns.releaseDate,
    {
      headerName: '',
      cellRenderer: TABLE.CELL_RENDERERS.CustomCell,
      cellRendererParams: {
        className: 'editable_row_button d-flex',
        leftIcon: canUpdateProducts
          ? {
              type: ICON_TYPES.far,
              iconName: FA_ICONS.pencil,
              className: 'p-0',
              onClick: handleEditRowClick,
            }
          : null,
        rightIcon: deleteBtn,
      },
      type: 'rightAligned',
      flex: 1,
    },
  ]
}
