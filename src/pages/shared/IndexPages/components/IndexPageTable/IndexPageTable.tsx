import {CellValueChangedEvent, ICellRendererParams} from 'ag-grid-community'
import {Column} from 'ag-grid-community/dist/lib/entities/column'
import {useCallback, useMemo} from 'react'
import {useHistory} from 'react-router-dom'
import {mutate} from 'swr'

import {
  getDetailPathname,
  getIndexTableHeaders,
  IndexPagesTableProps,
} from '../..'
import {AgGridTable} from '../../../../../components'
import {
  CustomCellParams,
  TitleCellRenderer,
} from '../../../../../components/AgGrid/CellRenderers'
import {appFetcher} from '../../../../../config/axios'
import {useProductFilterContext} from '../../../../../context/Filters/productsFilters'
import {useAlertDisplay} from '../../../../../context/alert'
import {useModalDisplay} from '../../../../../context/modal'
import useUser from '../../../../../hooks/user'
import {Product} from '../../../../../models'
import {
  ACTIONS,
  API_ROUTES,
  ApiMethods,
  MODALS,
  TGrowlColorType,
} from '../../../../../utils/constants'
import {COMPLETE_MODAL} from '../../../../../utils/constants/messages'
import {
  cloneDeep,
  productToProductPostOrPatch,
  onFormOperationProductSave,
  getEndpointURL,
} from '../../../../../utils/helpers'

const getProductsURL = (reportId: string) =>
  getEndpointURL(API_ROUTES.PRODUCTS, {reportId})

const getProductURL = (data: any) =>
  getEndpointURL(API_ROUTES.PRODUCTS, {id: data.id})

export const updateRecords =
  (updateProduct: IndexPagesTableProps['updateProduct'], isEditing?: boolean) =>
  async ({data, column, rowIndex}: CellValueChangedEvent) => {
    if (rowIndex !== null) {
      const payload = productToProductPostOrPatch(data)
      try {
        if (!isEditing) {
          const URL = getProductURL(data)
          const productsURL = getProductsURL(data.reportId)
          mutate(productsURL, async (cacheProducts: Product[] | undefined) =>
            cacheProducts?.map((product) =>
              product.id !== data.id ? product : data,
            ),
          )
          await appFetcher(URL, {
            method: ApiMethods.PATCH,
            data: payload,
          })
          mutate(productsURL)
        }
        updateProduct(data, ACTIONS.UPDATE, Boolean(isEditing), column)
      } catch (err) {
        // eslint-disable-next-line no-console
        console.log('ERROR UPDATING...', payload)
      }
    }
  }

export const performProductCompleteUpdate =
  (
    {data, column}: ICellRendererParams,
    updateProduct: IndexPagesTableProps['updateProduct'],
    display: (title: string, colorType?: TGrowlColorType | undefined) => void,
    isEditing?: boolean,
  ) =>
  async () => {
    const dataCopy = cloneDeep(data)
    dataCopy.complete = !dataCopy.complete
    try {
      if (!isEditing) {
        const URL = getProductURL(dataCopy)
        const productsURL = getProductsURL(dataCopy.reportId)
        mutate(productsURL, async (cacheProducts: Product[] | undefined) =>
          cacheProducts?.map((product) =>
            product.id !== dataCopy.id ? product : dataCopy,
          ),
        )
        await onFormOperationProductSave(URL, dataCopy)
        mutate(productsURL)
      }
      updateProduct(dataCopy, ACTIONS.UPDATE, Boolean(isEditing), column)
      const changePerformed = dataCopy.complete ? 'Complete' : 'Incomplete'
      display(`${dataCopy.title} marked as ${changePerformed}`)
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('ERROR UPDATING...', dataCopy)
    }
  }

const IndexPageTable = ({
  pagination,
  products,
  updateProduct,
  detailRoute,
  getGridReadyEvent,
}: IndexPagesTableProps) => {
  const history = useHistory()
  const {user} = useUser()
  const {display} = useAlertDisplay()
  const {displayModal} = useModalDisplay()
  const {isEditing} = useProductFilterContext()

  const canEdit = user.roles.hasFullOrPartialWriteAccess()
  const canUpdateProducts = user.roles.hasFullWriteAccess()

  const handleProductComplete = useMemo(
    () => (cellRendererParams: ICellRendererParams) => {
      const {
        data: {complete},
      } = cellRendererParams
      const productCompleteUpdate = performProductCompleteUpdate(
        cellRendererParams,
        updateProduct,
        display,
        isEditing,
      )
      if (complete) {
        productCompleteUpdate()
      } else {
        displayModal(
          COMPLETE_MODAL.body,
          COMPLETE_MODAL.title(cellRendererParams.data.title),
          {
            actionName: COMPLETE_MODAL.action,
            onActionClick: productCompleteUpdate,
            cancelButtonStyle: {
              containerStyle: 'outline',
              colorType: 'secondary-black',
            },
          },
        )
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isEditing, updateProduct, displayModal],
  )

  const updateTableRecords = useMemo(
    () => updateRecords(updateProduct, isEditing),
    [isEditing, updateProduct],
  )

  const handleEditRowClick = useCallback(
    ({data}: CustomCellParams) => {
      history.push({
        pathname: getDetailPathname(detailRoute, data.id),
      })
    },
    [detailRoute, history],
  )

  const onDeleteModal = useCallback(
    (data: any, column?: Column) => async () => {
      try {
        const URL = getProductURL(data)
        const productsURL = getProductsURL(data.reportId)
        mutate(productsURL, async (cacheProducts: Product[] | undefined) =>
          cacheProducts?.filter((product) => product.id !== data.id),
        )
        await appFetcher(URL, {
          method: ApiMethods.DELETE,
        })
        mutate(productsURL)
        updateProduct(data, ACTIONS.DELETE, false, column)
      } catch (err) {
        // eslint-disable-next-line no-console
        console.log('ERROR DELETING...')
      }
    },
    [updateProduct],
  )

  const handleDeleteRowClick = useCallback(
    ({data, column}: CustomCellParams) => {
      displayModal(MODALS.DELETE_MODAL(data.title), MODALS.TITLES.DELETE, {
        actionName: MODALS.ACTIONS.DELETE,
        onActionClick: onDeleteModal(data, column),
      })
    },
    [displayModal, onDeleteModal],
  )

  const columnDefs = useMemo(
    () =>
      getIndexTableHeaders(
        handleProductComplete,
        handleEditRowClick,
        handleDeleteRowClick,
        canEdit,
        detailRoute,
        canUpdateProducts,
        isEditing,
      ),
    [
      canEdit,
      canUpdateProducts,
      detailRoute,
      handleProductComplete,
      handleEditRowClick,
      handleDeleteRowClick,
      isEditing,
    ],
  )

  return (
    <AgGridTable
      pagination={pagination}
      columnDefs={columnDefs}
      frameworkComponents={{
        titleRenderer: TitleCellRenderer,
      }}
      fullWidth
      onCellValueChanged={updateTableRecords}
      rowData={products}
      stopEditingWhenCellsLoseFocus
      onTableReady={getGridReadyEvent}
    />
  )
}

export default IndexPageTable
