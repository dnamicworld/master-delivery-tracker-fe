import {BreadcrumbItem} from 'src/DS/components'

import {Product, Report} from '../../../models'
import {
  BREADCRUMBS,
  PRODUCT_TYPE_ROUTES,
  ROUTES,
  ALBUM_TYPE,
  SINGLE_TYPE,
  PRODUCT_TYPE_LABELS,
} from '../../../utils/constants'

import {getDateString, getEndpointURL, isEmpty} from '../../../utils/helpers'
import {getIndexQueryParams} from '../IndexPages/IndexPages.controller'

export const ProductPageInitialState = {
  editingOnlyTable: false,
  editingField: '',
  gpid: '',
  initial: undefined,
  product: undefined,
}

export const cancelEditing = {
  editing: false,
  editingOnlyTable: false,
}

export const DETAILS_MESSAGES = {
  MODAL_TITLES: {
    DELETE: 'Delete',
    WARNING: 'Warning',
    EDIT_COMMENT: 'Edit Comment',
  },
  DELETE_LAST_TRACK: 'Last track cannot be deleted.',
}

export const buildProductRoute = (root: Product['productType'], id: number) =>
  `${PRODUCT_TYPE_ROUTES[root]}/${id}`

export const getProductDetailBreadCrumbs = (
  isNewProduct: boolean,
  product?: Product,
  reports?: Report[] | Report,
): BreadcrumbItem[] => {
  const breadCrumbs: BreadcrumbItem[] = []
  let label: Report['label'] | undefined
  let productType = ''
  let year
  if (reports && !isEmpty(reports) && !Array.isArray(reports)) {
    label = reports.label
    productType = reports?.listViewType ?? ''
    year = reports.year
  }
  const indexSharedParams = !Array.isArray(reports)
    ? getIndexQueryParams(reports)
    : {}

  const buildIndexRoute = (root: string) =>
    getEndpointURL(root.substring(1), {id: label?.id, ...indexSharedParams})

  const breadcrumbOptions = {
    [ALBUM_TYPE]: {
      route: buildIndexRoute(ROUTES.ALBUM_INDEX),
    },
    [SINGLE_TYPE]: {
      route: buildIndexRoute(ROUTES.SINGLE_INDEX),
    },
  }
  const breadCrumbItem = breadcrumbOptions[productType]
  breadCrumbs.push({
    ...BREADCRUMBS.REPORT(
      `${year} ${label?.name} ${PRODUCT_TYPE_LABELS[productType]}`,
    ),
    link: breadCrumbItem?.route,
  })
  if (isNewProduct) {
    breadCrumbs.push({
      ...BREADCRUMBS.NEW_PROJECT,
      active: true,
    })
  } else if (productType && product) {
    breadCrumbs.push({
      ...BREADCRUMBS.PRODUCT(product),
      active: true,
    })
  }

  return breadCrumbs
}

export const buildProductFileName = (product?: Product) =>
  `${product?.artist}_${product?.title}_${getDateString({
    date: new Date(),
  })}`
