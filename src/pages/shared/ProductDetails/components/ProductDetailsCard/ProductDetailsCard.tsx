import {DashboardCard} from '../../../../../components'
import AssetChart from '../../../../../components/AssetChart/AssetChart'
import useProductDetails from '../../../../../hooks/productDetails'
import {AssetType, Product} from '../../../../../models'
import {
  PRODUCT_INDEXES,
  PRODUCT_TYPE_LABELS,
} from '../../../../../utils/constants'
import {
  AssetsForTracks,
  cloneDeep,
  getBarDataFromTracks,
  getPercentage,
  getTrackAssets,
  removeDeleteActionItems,
  values,
} from '../../../../../utils/helpers'
import {barChartContentFromTracks, cardHeader} from '../../../ProductCards'

interface ProductDetailsCardProps {
  product?: Product
  isNewProduct?: boolean
}

const getDonutChartDataFromTracks = (
  assetsData: AssetsForTracks[],
  labelName: string,
): {name: string; value: number} => {
  const calculations = assetsData.reduce(
    (prev: {total: number; delivered: number}, asset) => {
      const prevCopy = cloneDeep(prev)
      prevCopy.total += asset.total
      prevCopy.delivered += asset.delivered
      return prevCopy
    },
    {total: 0, delivered: 0},
  )
  return {
    name: PRODUCT_TYPE_LABELS[labelName] ?? labelName,
    value: getPercentage(calculations.total, calculations.delivered),
  }
}

const ProductDetailsCard = ({
  product,
  isNewProduct,
}: ProductDetailsCardProps) => {
  const {getProcessedAssets} = useProductDetails()
  const {filteredAssets} = getProcessedAssets()
  const trackAssets = removeDeleteActionItems(
    cloneDeep(product?.tracks?.[0].assets) ?? [],
  )
  const trackAssetIds = trackAssets.map((item) => item.assetTypeId)
  const productAssets = filteredAssets.filter((item) =>
    trackAssetIds?.includes(item.id),
  )

  const assetsFilteredNames =
    filteredAssets?.map((asset: AssetType) => asset.shortName) ?? []

  const assetsFromTracks = getTrackAssets(
    product?.tracks ?? [],
    assetsFilteredNames,
  )

  const assets = values(assetsFromTracks)

  const barChartData = filteredAssets?.reduce((prev: {}, asset: AssetType) => {
    const prevCopy = cloneDeep(prev)
    prevCopy[asset.shortName] = [
      getBarDataFromTracks(assetsFromTracks?.[asset.shortName]),
    ]
    return prevCopy
  }, {})

  const productType = product?.productType
  const title = isNewProduct ? 'Total Delivered' : productType ?? ''
  const donutChartData = getDonutChartDataFromTracks(assets, title)
  const contentIndex = PRODUCT_INDEXES[productType ?? '']

  return (
    <DashboardCard
      cardHeader={cardHeader(product)}
      leftContent={
        <AssetChart big data={donutChartData} listViewType={productType} />
      }
      rightContent={barChartContentFromTracks(
        barChartData,
        productAssets,
        contentIndex,
      )}
    />
  )
}

export default ProductDetailsCard
