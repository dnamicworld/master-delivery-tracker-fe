import {ReactElement} from 'react'

import {Button, Icon} from '../../../../DS/components'
import {FA_ICONS, ICON_TYPES} from '../../../../utils/constants'

const EditableField = (
  content: ReactElement,
  onEdit: () => void,
  disabled?: boolean,
) => (
  <span className='editable_field'>
    {content}
    <Button
      className='editable_row_button'
      onClick={onEdit}
      containerStyle='link'
      disabled={disabled}
    >
      <Icon iconName={FA_ICONS.pencil} type={ICON_TYPES.fas} />
    </Button>
  </span>
)

export default EditableField
