import classNames from 'classnames'

import {ReactNode, useCallback, useMemo, useState} from 'react'

import {EditableField, ImportGpidModal} from '..'
import {
  Button,
  Col,
  HeaderTitle,
  Row,
  TextField,
} from '../../../../../DS/components'
import {Placeholder, Select, SelectProps} from '../../../../../components'
import useProductDetails from '../../../../../hooks/productDetails'
import useUser from '../../../../../hooks/user'
import {Product} from '../../../../../models'
import {ALBUM_COVER_PLACEHOLDER, Status} from '../../../../../utils/constants'
import {
  cloneDeep,
  getStatus,
  handleInputChange,
  instanceUpdated,
  validateField,
  showElement,
} from '../../../../../utils/helpers'

interface ProductHeaderProps {
  headerActions?: ReactNode
  isLoading: boolean
}

const ProductHeader = ({headerActions, isLoading}: ProductHeaderProps) => {
  const {user} = useUser()
  const {
    onFieldEditing,
    productDetailState,
    onFormSave,
    setProductDetailState,
    updateProduct,
    removeCoverArtLink,
    albumCover,
  } = useProductDetails()
  const {editing, editingField, invalid, product} = productDetailState
  const updateProductInput = handleInputChange(updateProduct)
  const isEditingStatus = useMemo(
    () => editingField === 'status',
    [editingField],
  )
  const [showCoverModal, setShowCoverModal] = useState(false)
  const onEditStatus = () => onFieldEditing('status')
  const onStatusChange = useCallback(
    <T extends {}>(event: T, status: SelectProps['value']) => {
      const productCopy = cloneDeep(product)
      const newCompleteStatus = status === 'true'

      const tracksCopy = productCopy?.tracks?.map((track) => ({
        ...track,
        complete: newCompleteStatus,
      }))

      const newProduct = {
        ...productCopy,
        ...instanceUpdated(),
        updatedBy: user.name,
        complete: newCompleteStatus,
        tracks: tracksCopy,
      } as Product
      if (isEditingStatus) {
        onFormSave(newProduct)
      } else {
        setProductDetailState((prevProductState) => ({
          ...prevProductState,
          editingField: '',
          product: newProduct,
        }))
      }
    },
    [product, setProductDetailState, isEditingStatus, onFormSave, user.name],
  )

  return (
    <>
      <ImportGpidModal
        updateProduct={updateProduct}
        showCoverModal={showCoverModal}
        setShowCoverModal={setShowCoverModal}
      />
      <Col sm={12} className='pb-30'>
        <Col sm={3} className='px-0'>
          <div className='albumImage px-0'>
            <img
              src={
                product && product.coverArtLink
                  ? product.coverArtLink
                  : albumCover.src
              }
              onError={() => {
                albumCover.setImg(ALBUM_COVER_PLACEHOLDER)
                if (product?.coverArtLink) {
                  removeCoverArtLink()
                }
              }}
              alt='album logo'
            />
            {productDetailState.editing ? (
              <Button
                className='d-flex text-black'
                label='Import Cover Art by GPID'
                onClick={() => setShowCoverModal(true)}
                containerStyle='outline'
                colorType='secondary-black'
              />
            ) : null}
            {!product ? <h4>No Image Found</h4> : null}
          </div>
        </Col>
        <Col sm={editing ? 9 : 6} className='headerInfo'>
          {showElement(
            editing,
            showElement(
              isLoading,
              <HeaderTitle variant='h2' className='mb-2'>
                {product?.artist}
              </HeaderTitle>,
              <Placeholder className='d-block mt-2' width='716px' />,
            ),
            <div className='text-gray mb-15'>
              <HeaderTitle className='mb-2'>Artist</HeaderTitle>
              <TextField
                bsSize='lg'
                validationState={validateField(invalid, product?.artist)}
                type='text'
                name='artist'
                value={product?.artist}
                onChange={updateProductInput}
              />
            </div>,
          )}
          {showElement(
            editing,
            showElement(
              isLoading,
              <h2 className='m-0 text-gray font-weight-sbold mb-2'>
                {product?.title}
              </h2>,
              <Placeholder className='d-block  mt-2' width='473px' />,
            ),
            <div className='text-gray mb-15'>
              <HeaderTitle className='mb-2'>Title</HeaderTitle>
              <TextField
                bsSize='lg'
                validationState={validateField(invalid, product?.title)}
                type='text'
                name='title'
                value={product?.title}
                onChange={updateProductInput}
              />
            </div>,
          )}
          {showElement(
            editing || isEditingStatus,
            EditableField(
              showElement(
                isLoading,
                <HeaderTitle
                  variant='h2'
                  className={classNames('d-inline-block mb-2', {
                    'text-danger': !product?.complete,
                    'text-success': product?.complete,
                  })}
                >
                  Status: {getStatus(product?.complete)}
                </HeaderTitle>,
                <Placeholder className='d-block mt-2' width='240px' />,
              ),
              onEditStatus,
              user.roles.hasPartialReadAccess(),
            ),
            <Row className='mb-15 mt-5'>
              <Col sm={12}>
                <HeaderTitle className='mb-2'>Status</HeaderTitle>
                <Select
                  fullWidth
                  value={
                    product?.complete !== undefined
                      ? String(product?.complete)
                      : undefined
                  }
                  name='complete'
                  onChange={onStatusChange}
                  options={[
                    {
                      name: Status.COMPLETE,
                      value: 'true',
                    },
                    {
                      name: Status.INCOMPLETE,
                      value: 'false',
                    },
                  ]}
                />
              </Col>
            </Row>,
          )}
        </Col>
        {headerActions ? (
          <Col sm={3} className='text-right'>
            {headerActions}
          </Col>
        ) : null}
      </Col>
    </>
  )
}

export default ProductHeader
