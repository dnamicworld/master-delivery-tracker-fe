import {KeyboardEventHandler, useState} from 'react'
import {mutate} from 'swr'

import {
  Button,
  Card,
  Col,
  Row,
  TextField,
  HeaderTitle,
  BSTextFieldEvent,
  FormControl,
} from '../../../../../DS/components'
import {appFetcher} from '../../../../../config/axios'
import {useModalDisplay} from '../../../../../context/modal'
import useProductDetails from '../../../../../hooks/productDetails'
import {Product} from '../../../../../models'
import {
  ACTIONS,
  API_ROUTES,
  KEYS,
  PRODUCT_DETAIL_PLACEHOLDERS,
} from '../../../../../utils/constants'
import {VALIDATE_PRODUCT} from '../../../../../utils/constants/messages'
import {
  cloneDeep,
  isEmpty,
  validateExistProduct,
} from '../../../../../utils/helpers'

interface ProductImportProps {
  value?: string
  onChange: (event: BSTextFieldEvent) => void
}

const parseProductImport = (productImport: Product, productData?: Product) => ({
  ...productImport,
  reportId: productData?.reportId,
  tracks: productImport.tracks?.length
    ? productImport.tracks?.map((track) => ({
        ...track,
        id: track.trackNumber,
        action: ACTIONS.CREATE,
        assets: track.assets?.length
          ? track.assets
          : cloneDeep(productData?.tracks?.[0].assets),
      }))
    : productData?.tracks,
})

const ProductImport = ({onChange, value}: ProductImportProps) => {
  const {displayModal} = useModalDisplay()
  const valueCurated = (value ?? '').trim()
  const {
    updateState,
    productDetailState: {product: productData},
  } = useProductDetails()
  const [invalid, setInvalid] = useState(false)
  const [errorMessage, setErrorMessage] = useState<undefined | string>(
    undefined,
  )
  const productOnChange = () => {
    const length = value?.length || 0
    const sizes = [12, 13]
    const valueLength = sizes.includes(length)

    if (value && !valueLength) {
      setInvalid(true)
      setErrorMessage('GPID must be 12 or 13 digits')
    } else {
      setInvalid(false)
      setErrorMessage(undefined)
    }
  }

  const onEnterGPID = async () => {
    const URL = `${API_ROUTES.PRODUCTS}/${valueCurated}/${API_ROUTES.GPID_IMPORT}`
    try {
      const response = await appFetcher(URL)
      const parsedProduct = parseProductImport(response, productData)
      updateState('product', parsedProduct)
      mutate(URL, parsedProduct)
    } catch (err) {
      setInvalid(true)
      setErrorMessage('No data is available for the GPID entered')
    }
  }

  const validateGPID = async () => {
    try {
      const validation = await validateExistProduct({gpid: valueCurated})
      if (validation && validation.upcExists) {
        displayModal(VALIDATE_PRODUCT.gpid.body, VALIDATE_PRODUCT.gpid.title, {
          actionName: VALIDATE_PRODUCT.actionName,
          onActionClick: onEnterGPID,
          onCancelAction: () => updateState('gpid', ''),
        })
      } else {
        onEnterGPID()
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err)
    }
  }

  const onKeyPress: KeyboardEventHandler<FormControl> &
    KeyboardEventHandler<HTMLInputElement> = async (event) => {
    if (event.key === KEYS.enter) {
      await validateGPID()
    }
  }
  const noGPID = isEmpty(valueCurated)
  return (
    <Card className='cyan' radius>
      <Row className='container'>
        <Col sm={8}>
          <Row>
            <HeaderTitle className='mb-30' variant='h4'>
              Import Project Data
            </HeaderTitle>
            <HeaderTitle className='text-gray mb-2'>GPID</HeaderTitle>
            <div className='text-gray d-flex align-items-start mb-5'>
              <TextField
                className='flex-grow-1 m-0'
                type='text'
                name='gpid'
                placeholder={PRODUCT_DETAIL_PLACEHOLDERS.GPID}
                value={value}
                onChange={onChange}
                onKeyUp={productOnChange}
                validationState={invalid ? 'error' : null}
                errorMessage={errorMessage}
                onKeyPress={onKeyPress}
              />
              <Button
                bsStyle={noGPID ? undefined : 'primary'}
                disabled={noGPID}
                className='ml-20'
                onClick={validateGPID}
              >
                Import
              </Button>
            </div>
          </Row>
        </Col>
      </Row>
    </Card>
  )
}

export default ProductImport
