import {useCallback, useEffect, useState} from 'react'

import {EditableField} from '../..'
import {
  BSTextFieldEvent,
  Col,
  DatePicker,
  DatePickerProps,
  TextField,
  HeaderTitle,
} from '../../../../../../DS/components'
import {
  Placeholder,
  Select,
  SelectOption,
  SelectProps,
} from '../../../../../../components'
import {useModalDisplay} from '../../../../../../context/modal'
import useMarketingLabels from '../../../../../../hooks/data/useMarketingLabels'
import useProductDetails from '../../../../../../hooks/productDetails'
import useUser from '../../../../../../hooks/user'
import {Product} from '../../../../../../models'
import {
  PRODUCT_DETAIL_PLACEHOLDERS,
  MESSAGES,
  SelectActions,
  DEFAULT_MIN_YEAR,
  releaseDateRangeFutureYears,
  PRODUCT_TYPE_DISPLAY,
  CHANGE_PROJECT_DATE,
} from '../../../../../../utils/constants'
import {
  getLongTextResult,
  getDate,
  getDateString,
  getCurrentYear,
  showElement,
  cloneDeep,
} from '../../../../../../utils/helpers'

import './ProductDetailsForm.scss'

interface AlbumDetailsFormProps {
  editing: boolean
  product?: Product
  editingField: string
  updateProduct: (name: string, value: string) => void
  onAutoSave: (event: BSTextFieldEvent) => void
  onChange: (event: BSTextFieldEvent) => void
  onFormSave: (
    product?: Product | undefined,
    continueEditing?: boolean | undefined,
  ) => void
  isLoading: boolean
}

export const getDataFallback = (value?: string | null, msg = MESSAGES.EMPTY) =>
  !value ? msg : value

const getMarketingLabelOptions = (marketingLabels: string[]): SelectOption[] =>
  marketingLabels.map((label) => ({
    name: label,
    value: label,
  }))

const ProductDetailsForm = ({
  editing,
  editingField,
  onChange,
  onAutoSave,
  onFormSave,
  product,
  updateProduct,
  isLoading,
}: AlbumDetailsFormProps) => {
  const {user} = useUser()
  const {marketingLabels} = useMarketingLabels(
    {id: product?.reportId},
    () => !!product?.reportId,
  )

  const {displayModal} = useModalDisplay()
  const {onFieldEditing, report} = useProductDetails()

  const [marketingLabelsOptions, setMarketingLabelsOptions] = useState<
    string[]
  >([])
  const [initialMarketingLabel, setInitialMarketingLabel] = useState<string[]>(
    [],
  )

  const isEditing = (name: string) => editingField === name
  const editingAdmin = isEditing('adminName')
  const editingProducer = isEditing('producer')
  const editingMixer = isEditing('mixer')
  const editingMastering = isEditing('mastering')
  const editingComments = isEditing('arComments')
  const {value: commentsValue} = getLongTextResult(
    product?.arComments ?? '',
    300,
  )

  useEffect(() => {
    setMarketingLabelsOptions(marketingLabels ?? [])
    setInitialMarketingLabel(marketingLabels ?? [])
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(marketingLabels)])

  const marketingLabelOptions = getMarketingLabelOptions(marketingLabelsOptions)

  const productTypesOptions = Object.entries(PRODUCT_TYPE_DISPLAY).map(
    ([key, value]) => ({name: value, value: key}),
  )

  const onSelectChange: SelectProps['onChange'] = useCallback(
    (e, value, selectAction) => {
      if (selectAction.action === SelectActions.createOption) {
        setMarketingLabelsOptions((prevMarketingLabels) => [
          ...prevMarketingLabels,
          value,
        ])
        // TODO here api call to create label should be place
      }

      if (selectAction.action === SelectActions.deselectOption) {
        setMarketingLabelsOptions((prevMarketingLabels) => {
          const {value: deselectedValue} = selectAction.option
          const existingLabel = initialMarketingLabel.includes(deselectedValue)
          return existingLabel
            ? prevMarketingLabels
            : prevMarketingLabels.filter(
                (marketingOption) => marketingOption !== deselectedValue,
              )
        })
      }
      updateProduct(selectAction.name, value)
    },
    [initialMarketingLabel, updateProduct],
  )
  const getReportTitle = (name: string, year: number) =>
    `${name} Assets Delivered ${year}`

  const onDateChange: DatePickerProps['onChange'] = (name, date) => {
    const fullYear = date.getFullYear()
    const newDate = getDateString({date, dateSeparator: '-', yearFirst: true})
    if (report?.year && report.year !== fullYear) {
      const oldReport = getReportTitle(report.label.name, report.year)
      const newReport = getReportTitle(report.label.name, fullYear)
      displayModal(
        CHANGE_PROJECT_DATE.body(oldReport, newReport),
        CHANGE_PROJECT_DATE.title,
        {
          actionName: CHANGE_PROJECT_DATE.actionName,
          onActionClick: () => {
            const productCopy = cloneDeep(product)
            if (productCopy) {
              productCopy.releaseDate = newDate
            }
            onFormSave(productCopy)
          },
          cancelButtonStyle: {
            containerStyle: 'outline',
            colorType: 'secondary-black',
          },
        },
      )
    } else {
      updateProduct(name, newDate)
    }
  }

  const currentYear = getCurrentYear()
  const maxYear = currentYear + releaseDateRangeFutureYears

  const inputPlaceHolder = (
    <Placeholder className='d-inline-block mt-2' width='195px' />
  )

  return (
    <>
      <Col sm={3}>
        <HeaderTitle className='mb-15 text-grayDarker' variant='h4'>
          Release Info
        </HeaderTitle>
        <div className='mb-15'>
          <HeaderTitle className='text-black mb-2'>Release Date</HeaderTitle>
          {showElement(
            editing,
            <span className='text-black '>
              {getDataFallback(
                getDateString({dateString: product?.releaseDate}),
              )}
            </span>,
            <DatePicker
              name='releaseDate'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.RELEASE_DATE}
              value={getDate({
                dateString: product?.releaseDate,
                withCustomJoin: true,
                yearFirst: true,
              })}
              onChange={onDateChange}
              showYearMonths
              showIcon
              minYear={DEFAULT_MIN_YEAR}
              maxYear={maxYear}
              className='w-100'
            />,
          )}
        </div>
        <div className='mb-15'>
          <HeaderTitle className='text-black mb-2 w-100'>
            Marketing Label
          </HeaderTitle>
          {showElement(
            editing,
            <span className='text-black '>
              {getDataFallback(product?.marketingLabel)}
            </span>,
            <Select
              label={PRODUCT_DETAIL_PLACEHOLDERS.MARKETING_LABEL}
              value={product?.marketingLabel}
              name='marketingLabel'
              options={marketingLabelOptions}
              chips
              searchable={{
                placeholder: 'Search',
              }}
              onChange={onSelectChange}
              canCreate
              fullWidth
            />,
          )}
        </div>
        <div className='mb-15'>
          <HeaderTitle className='text-black mb-2'>Product Type</HeaderTitle>
          {showElement(
            editing,
            <span className='text-black '>
              {getDataFallback(
                PRODUCT_TYPE_DISPLAY[product?.productType ?? ''],
              )}
            </span>,
            <Select
              label={PRODUCT_DETAIL_PLACEHOLDERS.PRODUCT_TYPE}
              value={product?.productType}
              name='productType'
              options={productTypesOptions}
              onChange={onSelectChange}
              fullWidth
            />,
          )}
        </div>
      </Col>
      <Col sm={3}>
        <HeaderTitle className='mb-15 text-grayDarker' variant='h4'>
          Identifiers
        </HeaderTitle>
        <div className='text-black  mb-15'>
          <HeaderTitle className='text-black mb-2'>UPC</HeaderTitle>
          {showElement(
            editing,
            <span className='break-words'>
              {getDataFallback(product?.upc)}
            </span>,
            <TextField
              type='text'
              name='upc'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.UPC}
              value={product?.upc ?? ''}
              onChange={onChange}
            />,
          )}
        </div>
        <div className='text-black  mb-15'>
          <HeaderTitle className='text-black mb-2'>Selection #</HeaderTitle>
          {showElement(
            editing,
            <span className='break-words'>
              {getDataFallback(product?.selectionNumber)}
            </span>,
            <TextField
              type='text'
              name='selectionNumber'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.SELECTION_NUMBER}
              value={product?.selectionNumber ?? ''}
              onChange={onChange}
            />,
          )}
        </div>
      </Col>
      <Col sm={3}>
        <HeaderTitle className='mb-15 text-grayDarker' variant='h4'>
          Personnel
        </HeaderTitle>
        <div className='text-black  mb-15'>
          <HeaderTitle className='text-black mb-2'>A&R Admin</HeaderTitle>
          {showElement(
            editing || editingAdmin,
            showElement(
              isLoading,
              EditableField(
                <span>{getDataFallback(product?.adminName)}</span>,
                () => onFieldEditing('adminName'),
                user.roles.hasPartialReadAccess(),
              ),
              inputPlaceHolder,
            ),
            <TextField
              type='text'
              name='adminName'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.AR_ADMIN}
              value={product?.adminName ?? ''}
              onChange={onChange}
              onBlur={editingAdmin ? onAutoSave : undefined}
            />,
          )}
        </div>
        <div className='text-black  mb-15'>
          <HeaderTitle className='text-black mb-2'>Producer</HeaderTitle>
          {showElement(
            editing || editingProducer,
            showElement(
              isLoading,
              EditableField(
                <span>{getDataFallback(product?.producer)}</span>,
                () => onFieldEditing('producer'),
                user.roles.hasPartialReadAccess(),
              ),
              inputPlaceHolder,
            ),
            <TextField
              type='text'
              name='producer'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.PRODUCER}
              value={product?.producer ?? ''}
              onChange={onChange}
              onBlur={editingProducer ? onAutoSave : undefined}
            />,
          )}
        </div>
        <div className='text-black  mb-15'>
          <HeaderTitle className='text-black mb-2'>Mixer</HeaderTitle>
          {showElement(
            editing || editingMixer,
            showElement(
              isLoading,
              EditableField(
                <span>{getDataFallback(product?.mixer)}</span>,
                () => onFieldEditing('mixer'),
                user.roles.hasPartialReadAccess(),
              ),
              inputPlaceHolder,
            ),
            <TextField
              type='text'
              name='mixer'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.MIXER}
              value={product?.mixer ?? ''}
              onChange={onChange}
              onBlur={editingMixer ? onAutoSave : undefined}
            />,
          )}
        </div>
        <div className='text-black  mb-15'>
          <HeaderTitle className='text-black mb-2'>Mastering</HeaderTitle>
          {showElement(
            editing || editingMastering,
            showElement(
              isLoading,
              EditableField(
                <span>{getDataFallback(product?.mastering)}</span>,
                () => onFieldEditing('mastering'),
                user.roles.hasPartialReadAccess(),
              ),
              inputPlaceHolder,
            ),
            <TextField
              type='text'
              name='mastering'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.MASTERING}
              value={product?.mastering ?? ''}
              onChange={onChange}
              onBlur={editingMastering ? onAutoSave : undefined}
            />,
          )}
        </div>
      </Col>
      <Col sm={3}>
        <HeaderTitle className='mb-15 text-grayDarker' variant='h4'>
          Other
        </HeaderTitle>
        <HeaderTitle className='text-black mb-2'>Project Comments</HeaderTitle>
        {showElement(
          editing || editingComments,
          EditableField(
            <span className='heading-line-height d-inline-block'>
              {getDataFallback(commentsValue)}
            </span>,
            () => onFieldEditing('arComments'),
            user.roles.hasPartialReadAccess(),
          ),
          <TextField
            as='textarea'
            rows={3}
            componentClass='textarea'
            name='arComments'
            placeholder={PRODUCT_DETAIL_PLACEHOLDERS.COMMENTS}
            value={product?.arComments ?? ''}
            onChange={onChange}
            onBlur={editingComments ? onAutoSave : undefined}
          />,
        )}
      </Col>
    </>
  )
}

export default ProductDetailsForm
