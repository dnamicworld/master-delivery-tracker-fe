import {UnsavedPrompt} from 'src/components'

import {Button, Col, Icon, Row, Grid} from '../../../../../DS/components'
import useProductDetails from '../../../../../hooks/productDetails'
import {FA_ICONS, ICON_TYPES} from '../../../../../utils/constants'

interface ProductActionsProps {
  onEditing: () => void
  onSave: (redirectNew: boolean) => void
  onCancel: () => void
  editing: boolean
}
const ProductActions = ({
  editing,
  onCancel,
  onEditing,
  onSave,
}: ProductActionsProps) => {
  const {noChanges, fromSave} = useProductDetails()

  const showPrompt = fromSave ? false : !noChanges
  return (
    <>
      {editing ? (
        <Grid className='footerActions'>
          <UnsavedPrompt block={showPrompt} />
          <Row className='my-30'>
            <Col sm={9} className='pl-0 text-left'>
              <Icon
                type={ICON_TYPES.fal}
                iconName={FA_ICONS.penToSquare}
                className='align-self-center mr-5'
                color='primary'
              />
              <Button containerStyle='link' onClick={() => onSave(true)}>
                <h4 className='d-inline-block'>Create New Project</h4>
              </Button>
            </Col>
            <Col sm={3}>
              <>
                <Button
                  containerStyle='link'
                  label='Cancel'
                  className='mr-20'
                  onClick={onCancel}
                />
                <Button
                  label='Save'
                  disabled={noChanges}
                  onClick={() => onSave(false)}
                />
              </>
            </Col>
          </Row>
        </Grid>
      ) : (
        <Button label='Edit' colorType='secondary-white' onClick={onEditing} />
      )}
    </>
  )
}

export default ProductActions
