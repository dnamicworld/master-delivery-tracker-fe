import {ReactNode} from 'react'
import {PRODUCT_TYPE_DISPLAY} from 'src/utils/constants'

import {
  Col,
  Row,
  BSTextFieldEvent,
  HeaderTitle,
} from '../../../../../DS/components'

import useProductDetails from '../../../../../hooks/productDetails'

import {ProductDetailState} from '../../index'

import ProductImport from './ProductImport'

interface ProductNewProps extends Pick<ProductDetailState, 'gpid'> {
  actions?: ReactNode
  updateGPID: (event: BSTextFieldEvent) => void
}

const ProductNew = ({actions, gpid, updateGPID}: ProductNewProps) => {
  const {productType} = useProductDetails()
  const product = PRODUCT_TYPE_DISPLAY[productType ?? ''] ?? ''
  const productLower = product.toLowerCase()
  return (
    <>
      <Col sm={12} className='pb-20'>
        <Row>
          <Col sm={9}>
            <h2 className='m-0 mb-10'>New Project</h2>
            <p className='text-gray'>
              Create a new project by importing the data with a GPID or by
              manually entering the {productLower} data
            </p>
          </Col>
          <Col sm={3} className='text-right'>
            {actions}
          </Col>
        </Row>
      </Col>
      <Col sm={9} className='pb-40'>
        <ProductImport onChange={updateGPID} value={gpid} />
      </Col>
      <Col sm={12} className='pb-5'>
        <HeaderTitle variant='h4' className='mb-15'>
          Project Information
        </HeaderTitle>
      </Col>
    </>
  )
}

export default ProductNew
