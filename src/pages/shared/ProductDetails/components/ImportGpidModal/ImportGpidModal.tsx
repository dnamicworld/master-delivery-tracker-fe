import {Alert} from '@wmg-ae/legato'

import {Dispatch, SetStateAction, useState} from 'react'

import {Col, HeaderTitle, Row, TextField} from '../../../../../DS/components'
import {ContentModal} from '../../../../../components'
import {PRODUCT_DETAIL_PLACEHOLDERS} from '../../../../../utils/constants'
import {
  handleGpidImport,
  handleInputChange,
  isEmpty,
} from '../../../../../utils/helpers'

interface ImportGpidProps {
  updateProduct: (name: string, value: any) => void
  showCoverModal: boolean
  setShowCoverModal: Dispatch<SetStateAction<boolean>>
}
const ImportGpidModal = ({
  updateProduct,
  showCoverModal,
  setShowCoverModal,
}: ImportGpidProps) => {
  const [gpid, setGpid] = useState('')
  const updateGpidImport = (name: string, value: any) => {
    setGpid(value)
  }

  const handleGpidInput = handleInputChange(updateGpidImport)
  const [errorMessage, setErrorMessage] = useState<string>('')
  return (
    <ContentModal
      show={showCoverModal}
      title='Import Cover Art from GPID'
      actionName='Import'
      onActionClick={() =>
        handleGpidImport(
          gpid,
          updateProduct,
          setErrorMessage,
          setShowCoverModal,
          setGpid,
        )
      }
      onCancelAction={() => {
        setShowCoverModal(false)
        setGpid('')
        setErrorMessage('')
      }}
    >
      <Row>
        <Col sm={12}>
          <HeaderTitle>Enter a GPID</HeaderTitle>
          <div className='d-flex'>
            <Col sm={7} className='p-0'>
              <Alert
                className='p-1 mt-2'
                colorType='danger'
                containerStyle='solid'
                isVisible={!isEmpty(errorMessage)}
                title={errorMessage}
                dissmisable={false}
              />
            </Col>
          </div>
          <div className='text-gray d-flex align-items-start'>
            <TextField
              className='flex-grow-1 m-0 mt-2'
              type='text'
              name='gpid'
              placeholder={PRODUCT_DETAIL_PLACEHOLDERS.GPID}
              value={gpid}
              onChange={handleGpidInput}
            />
          </div>
        </Col>
      </Row>
    </ContentModal>
  )
}
export default ImportGpidModal
