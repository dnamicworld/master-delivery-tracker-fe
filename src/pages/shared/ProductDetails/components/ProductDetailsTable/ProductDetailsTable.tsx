import {
  CellValueChangedEvent,
  GridApi,
  ICellRendererParams,
} from 'ag-grid-community'
import {
  ChangeEvent,
  MouseEvent,
  useEffect,
  useRef,
  useState,
  useCallback,
  useMemo,
} from 'react'
import {useHistory, useParams} from 'react-router-dom'
import {mutate} from 'swr'

import {
  Button,
  CheckboxBtn,
  Col,
  HeaderTitle,
  Row,
  SelectOption,
} from '../../../../../DS/components'
import {AgGridTable} from '../../../../../components'
import {appFetcher} from '../../../../../config/axios'
import {useModalDisplay} from '../../../../../context/modal'
import useProductDetails from '../../../../../hooks/productDetails'
import useUser from '../../../../../hooks/user'
import {
  DeliveryStatusActions,
  Product,
  ProductTrack,
  ProductTrackAsset,
} from '../../../../../models'
import {
  ACTIONS,
  ApiMethods,
  API_ROUTES,
  EXTENDED_ASSETS_SIZE,
  MODALS,
  REMOVE_ASSET_COLUMN,
  ROUTES,
} from '../../../../../utils/constants'
import {
  cloneDeep,
  getEndpointURL,
  isEmpty,
  isEqual,
  isTableCreateAction,
  isTableDeleteAction,
  QueryString,
  set,
} from '../../../../../utils/helpers'
import {getAssetType} from '../../../../../utils/helpers/assets'
import {DETAILS_MESSAGES} from '../../ProductDetails.controller'
import {DetailsRouteParams} from '../../ProductDetails.model'

import AddCustomAssetTypeModal from './AddCustomAssetTypeModal'
import {ExportProductHandler, ModalTextArea} from './components'

import {
  buildAsset,
  ProductDetailsTableHandler,
  ProductDetailsTableProps,
  HandleCellChange,
  ProductDetailsTableBuilder,
} from '.'

const {CREATE, UPDATE, DELETE} = ACTIONS

const checkCompleteProduct = (tracks: ProductTrack[]) => {
  const valuesSet = tracks.reduce(
    (prev, track) => (track.complete ? prev + 1 : prev),
    0,
  )
  return valuesSet === tracks.length
}

const ProductDetailsTable = ({title}: ProductDetailsTableProps) => {
  const {
    assetTypes,
    handleEditTable: onEdit,
    getProcessedAssets,
    productDetailState,
    onFormCancel,
    onFormSave,
    updateProduct,
    setProductDetailState,
    options,
  } = useProductDetails()

  const {
    editingOnlyTable,
    editing: tableEditing,
    invalid,
    product,
  } = productDetailState

  const editing = useMemo(
    () => editingOnlyTable || tableEditing,
    [editingOnlyTable, tableEditing],
  )

  const tracks = useMemo(() => product?.tracks ?? [], [product])
  const history = useHistory()
  const [gridApi, setGridApi] = useState<GridApi | null>(null)
  const [showAssetTypeModal, setShowModal] = useState<boolean>(false)
  const commentsContentRef = useRef(null)
  const {user} = useUser()
  const {productId} = useParams<DetailsRouteParams>()
  const {roles} = user
  const canEdit = roles.hasFullOrPartialWriteAccess()
  const canFullEdit = roles.hasFullWriteAccess()
  const {displayModal} = useModalDisplay()
  const {optionalAssets} = getProcessedAssets()

  const assetsCustomNames =
    optionalAssets?.map((asset) => asset.shortName) ?? []

  const isNewAlbum = isEmpty(productId)

  const [comment, setComment] = useState<string>('')

  const handleCommentChange = useCallback(
    (event: ChangeEvent<HTMLTextAreaElement>) => {
      const target = event.target ?? event.currentTarget
      if (commentsContentRef.current) {
        ;(commentsContentRef.current as HTMLTextAreaElement).value =
          target.value
      }
    },
    [commentsContentRef],
  )

  const onSave = useCallback(
    async (
      newTracks: Product['tracks'],
      productComplete?: boolean,
      continueEditing?: boolean,
    ) => {
      await onFormSave(
        {
          ...cloneDeep((product ?? {}) as Product),
          complete: productComplete ?? product?.complete,
          tracks: newTracks,
        },
        continueEditing,
      )
    },
    [onFormSave, product],
  )

  const editCommentsContent = useMemo(
    () => (
      <ModalTextArea
        inputRef={commentsContentRef}
        name='comment'
        handleChange={handleCommentChange}
      />
    ),
    [commentsContentRef, handleCommentChange],
  )

  useEffect(() => {
    if (commentsContentRef.current) {
      const content = commentsContentRef.current as HTMLTextAreaElement
      const inputValue = content.value
      content.value = inputValue || comment
    }
  }, [commentsContentRef, comment])

  const onEditComments = useCallback(
    (editableParams: ICellRendererParams) => {
      const {value, rowIndex} = editableParams
      setComment(value)
      displayModal(
        editCommentsContent,
        DETAILS_MESSAGES.MODAL_TITLES.EDIT_COMMENT,
        {
          actionName: MODALS.ACTIONS.SAVE,
          onActionClick: () => {
            if (commentsContentRef.current) {
              const tracksCopy = cloneDeep(tracks)
              tracksCopy[rowIndex].comments = (
                commentsContentRef.current as HTMLTextAreaElement
              ).value
              if (!tracksCopy[rowIndex].action) {
                tracksCopy[rowIndex].action = UPDATE
              }
              updateProduct('tracks', tracksCopy)
              if (!editing) {
                onSave(tracksCopy)
              }
            }
          },
          onCancelAction: () => {
            setComment('')
          },
        },
      )
    },
    [displayModal, editCommentsContent, editing, onSave, tracks, updateProduct],
  )

  const submitChanges = useCallback(
    (newTracks: ProductTrack[]) => {
      const productComplete = checkCompleteProduct(newTracks)
      if (editing) {
        setProductDetailState((prevState) => ({
          ...cloneDeep(prevState),
          product: {
            ...cloneDeep(prevState.product),
            complete: productComplete,
            tracks: newTracks,
          } as Product,
        }))
      } else {
        onSave(newTracks, productComplete)
      }
    },
    [setProductDetailState, editing, onSave],
  )

  const albumTableHandler = useMemo(
    () => new ProductDetailsTableHandler(submitChanges, tracks, assetTypes),
    [submitChanges, tracks, assetTypes],
  )

  const editOptions = useMemo(
    () => ({
      canEdit,
      canFullEdit,
      isEditing: editing,
    }),
    [canEdit, canFullEdit, editing],
  )

  const tableBuilder = useMemo(
    () =>
      new ProductDetailsTableBuilder(
        editOptions,
        albumTableHandler,
        assetTypes,
        tracks,
        options,
      ),
    [albumTableHandler, assetTypes, editOptions, options, tracks],
  )

  const onDeleteItem = useCallback(
    (editableParams: ICellRendererParams, onSuccess: () => void) => {
      if (albumTableHandler.tracksLength > 1) {
        displayModal(
          MODALS.DELETE_MODAL(editableParams.data.title),
          MODALS.TITLES.DELETE,
          {
            actionName: MODALS.ACTIONS.DELETE,
            onActionClick: onSuccess,
          },
        )
      } else {
        displayModal(DETAILS_MESSAGES.DELETE_LAST_TRACK, MODALS.TITLES.DELETE)
      }
    },
    [displayModal, albumTableHandler],
  )

  const addTrack = () => {
    const tracksCopy = albumTableHandler.addNewTrack()
    updateProduct('tracks', tracksCopy)
  }

  const addTrackAndEdit = () => {
    const tracksCopy = albumTableHandler.addNewTrack()
    onEdit(tracksCopy)
    window.scrollTo(0, document.body.scrollHeight)
  }

  const getCheckboxOption = ({value, onClick}: SelectOption) => {
    const assetType = getAssetType(assetTypes, value)
    const trackAssetTypes = tracks?.[0].assets
    const assetAdded = !!trackAssetTypes?.find(
      (asset) =>
        isEqual(asset.assetType, value) &&
        !isTableDeleteAction<ProductTrackAsset>(asset),
    )
    return (
      <CheckboxBtn
        name={value}
        multiple
        readOnly
        checked={assetAdded}
        key={value}
        label={assetType?.longName}
        value={assetType?.shortName}
        onClick={(event: MouseEvent<HTMLInputElement>) => onClick?.(event)}
      />
    )
  }
  const mapTracks = (products: ProductTrack[], name: any) =>
    products.map((track) => {
      const trackCopy = cloneDeep(track)
      if (!trackCopy.action) {
        trackCopy.action = UPDATE
      }
      const assetIndex = trackCopy.assets.findIndex(
        (asset) =>
          isEqual(asset.assetType, name) &&
          !isTableDeleteAction<ProductTrackAsset>(asset),
      )
      if (
        isTableCreateAction<ProductTrackAsset>(trackCopy.assets[assetIndex])
      ) {
        trackCopy.assets.splice(assetIndex, 1)
      } else {
        trackCopy.assets[assetIndex].action = DELETE
      }
      return trackCopy
    })
  const shouldToggleBubble = (assetsLength: number) =>
    assetsLength === EXTENDED_ASSETS_SIZE ||
    assetsLength === EXTENDED_ASSETS_SIZE + 1

  function onKabobAssetCheckbox<T extends {target?: {[key: string]: any}}>(
    event: T,
  ) {
    const {name, checked} = event.target ?? {}
    const assetTypesCopy = cloneDeep(assetTypes)
    let tracksCopy = cloneDeep(tracks)
    if (checked) {
      const assetToAdd = optionalAssets?.find(
        (asset) => asset.shortName === name,
      )
      if (assetToAdd) {
        const {
          id,
          longName,
          optional,
          shortName,
          excludeFromTotal,
          assetOptions,
          defaultValue,
        } = assetToAdd
        if (tracksCopy) {
          tracksCopy = tracksCopy.map((track) => {
            const trackCopy = cloneDeep(track)
            if (!trackCopy.action) {
              trackCopy.action = UPDATE
            }
            const assetIndex = trackCopy.assets.findIndex(
              (asset) => asset.assetTypeId === id,
            )
            if (assetIndex >= 0) {
              trackCopy.assets[assetIndex].action = UPDATE
            } else {
              trackCopy.assets.push({
                assetTypeId: id,
                action: CREATE,
                ...buildAsset(defaultValue, assetOptions, name),
              })
            }
            return trackCopy
          })
        }

        assetTypesCopy.push({
          excludeFromTotal,
          id,
          optional,
          longName,
          shortName,
          assetOptions,
          defaultValue,
        })
      }
      onEdit(tracksCopy)
    } else if (tracksCopy) {
      const showModal = tracksCopy.find(
        (track) =>
          track.assets.find((asset) => asset.assetType === name)
            ?.deliveryStatus !== DeliveryStatusActions.NOT_REQUIRED,
      )
      tracksCopy = mapTracks(tracksCopy, name)
      if (showModal) {
        displayModal(REMOVE_ASSET_COLUMN.body, REMOVE_ASSET_COLUMN.title, {
          actionName: REMOVE_ASSET_COLUMN.actionName,
          onCancelAction: editing ? undefined : onFormCancel,
          onActionClick: () => {
            if (editing) {
              onEdit(tracksCopy)
            } else {
              onSave(tracksCopy)
            }
          },
        })
      } else {
        onEdit(tracksCopy)
      }
    }

    if (shouldToggleBubble(albumTableHandler.filteredAssets.length))
      gridApi?.refreshView()
  }

  const addAssetsOptions = assetsCustomNames.map((value) => ({
    multiple: true,
    value,
    onClick: onKabobAssetCheckbox,
    renderItem: getCheckboxOption,
  }))

  const kabobOptions: SelectOption[] = [
    ...(isNewAlbum
      ? []
      : [
          {
            name: 'Export as Excel',
            value: 'Export as Excel',
            // eslint-disable-next-line no-console
            onClick: () => {
              const excelHandler = new ExportProductHandler(
                tracks,
                title,
                assetTypes,
                product,
              )
              excelHandler.download()
            },
          },
          {
            name: 'Export as PDF',
            value: 'Export as PDF',
            onClick: () => {
              if (product) {
                history.push(ROUTES.PRODUCT_PDF, product)
              }
            },
          },
        ]),
    ...(canEdit
      ? [
          {
            name: 'Edit Table',
            value: 'Edit Table',
            onClick: () => onEdit(),
          },
        ]
      : []),
    ...(canFullEdit
      ? [
          {
            name: 'Add Track',
            value: 'Add Track',
            onClick: addTrackAndEdit,
          },
          {
            name: 'Add Asset Column',
            value: 'Add Asset Column',
            className: 'inline',
            options: addAssetsOptions,
          },
          {
            name: 'Custom Asset Column',
            value: 'Custom Asset Column',
            onClick: () => {
              setShowModal(true)
            },
          },
        ]
      : []),
  ]

  const handleValueChanged = useCallback(
    (event: CellValueChangedEvent) => {
      const handleCellChange: HandleCellChange = (
        {rowIndex, colDef}: CellValueChangedEvent,
        newValue: string,
      ) => {
        const tracksCopy = cloneDeep(tracks) || []
        if (rowIndex !== null) {
          if (!tracksCopy[rowIndex].action) {
            tracksCopy[rowIndex].action = UPDATE
          }
          set(tracksCopy, `${rowIndex}.${colDef?.field}`, newValue)
          submitChanges(tracksCopy)
        }
      }
      const {newValue, oldValue} = event
      if (newValue !== oldValue) {
        handleCellChange(event, newValue)
      }
    },
    [tracks, submitChanges],
  )

  const columnDefs = useMemo(
    () => tableBuilder.getTableHeaders(onDeleteItem, onEditComments, invalid),
    [tableBuilder, onDeleteItem, onEditComments, invalid],
  )

  return (
    <>
      <AddCustomAssetTypeModal
        shown={showAssetTypeModal}
        onClose={() => setShowModal(false)}
        onSuccess={async (name: string, optionsOnSuccess: string[]) => {
          const URL = API_ROUTES.ASSET_TYPES
          const newAssetType = await appFetcher(URL, {
            method: ApiMethods.POST,
            data: {
              columnName: name,
              options: optionsOnSuccess,
            },
          })
          mutate(
            getEndpointURL(API_ROUTES.ASSET_TYPES, {} as QueryString),
            (response?: any) => {
              if (newAssetType) {
                return [...response, newAssetType]
              }
              return response
            },
          )
          mutate(getEndpointURL(API_ROUTES.ASSET_TYPES, {} as QueryString))
        }}
      />
      <Col sm={12}>
        <Row>
          <Col sm={9}>
            <HeaderTitle variant='h4' className='mb-25'>
              Assets
            </HeaderTitle>
          </Col>
        </Row>
      </Col>
      <Col sm={12} className='pb-5'>
        <AgGridTable
          columnDefs={columnDefs}
          kabob={{options: kabobOptions, multiple: true}}
          onTableReady={(params) => setGridApi(params.api)}
          onCellValueChanged={handleValueChanged}
          onRowDragEnd={albumTableHandler.handleRowDragEnd}
          rowData={tracks}
          stopEditingWhenCellsLoseFocus
        />
      </Col>
      {editing && canFullEdit ? (
        <Col sm={12}>
          <Button
            containerStyle='link'
            className='mr-20 font-weight-sbold'
            onClick={addTrack}
          >
            Add Track
          </Button>
        </Col>
      ) : null}
    </>
  )
}

export default ProductDetailsTable
