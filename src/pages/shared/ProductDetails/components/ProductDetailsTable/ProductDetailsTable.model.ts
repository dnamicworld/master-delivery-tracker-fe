import {CellValueChangedEvent} from 'ag-grid-community'

export type HandleCellChange = (
  params: CellValueChangedEvent,
  newValue: string,
) => void

export interface ProductDetailsTableProps {
  title: string
}
