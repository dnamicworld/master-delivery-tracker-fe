import {
  AssetOption,
  DeliveryStatusActions,
  ProductTrackAsset,
} from '../../../../../models'

export const buildAsset = (
  selection: string,
  options: AssetOption[],
  name?: string,
) => {
  const status = options.find(
    (option) => option.display === selection,
  )?.deliveryStatus
  return {
    assetType: name,
    deliveryStatus: status ?? DeliveryStatusActions.NOT_REQUIRED,
    sampleRate: null,
    bitDepth: null,
    selection,
  }
}

export const getAssetBySelection = <T extends {assets: ProductTrackAsset[]}>(
  data: T,
  field: string,
): ProductTrackAsset | null => {
  const assetPosition = field.split('.')
  const assetPositionExist = assetPosition.length > 0

  if (assetPositionExist) {
    const assetOption = data.assets[Number(assetPosition[1])]

    return assetOption
  }
  return null
}
