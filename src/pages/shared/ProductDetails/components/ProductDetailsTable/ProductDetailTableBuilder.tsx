import {ColDef, ICellRendererParams} from 'ag-grid-community'

import classnames from 'classnames'

import {ReactNode} from 'react'

import {
  getCheckTooltipText,
  getCheckTooltipTextColor,
  TextVariant,
} from '../../../../../DS/components'
import {CustomCellParams} from '../../../../../components/AgGrid/CellRenderers'
import {
  AssetType,
  ProductTrackAsset,
  ProductTrack,
  AssetOption,
} from '../../../../../models'
import {
  AppColors,
  FA_ICONS,
  ICON_TYPES,
  notRequiredOption,
  PRODUCT_DETAIL_PLACEHOLDERS,
  TABLE,
} from '../../../../../utils/constants'
import {
  isEqual,
  isTableDeleteAction,
  NOT_VALUE,
  getAssetType,
  getNoOptionalAssetsLength,
} from '../../../../../utils/helpers'

import {} from '../../../../../utils/helpers/assets'

import {TooltipWithInfo} from './components'

import {getAssetBySelection, ProductDetailsTableHandler} from './index'

type ModalAction = (
  editableParams: ICellRendererParams,
  onSuccess: () => void,
) => void
// TODO will delete later

const assetTooltipTextBuilder = (
  data: ProductTrack,
  {field}: ColDef,
  options: AssetOption[],
): ReactNode | null => {
  const optionConf = getAssetBySelection(data, field || '')
  if (!optionConf) return null
  const displayValue =
    NOT_VALUE[optionConf.selection ?? ''] ?? optionConf.selection
  const tooltipOption =
    options.find((config) => config.display === displayValue) ?? options[0]
  if (tooltipOption.tooltip) {
    return (
      <TextVariant variant={AppColors[tooltipOption.color]}>
        {tooltipOption.tooltip}
      </TextVariant>
    )
  }

  return (
    <TooltipWithInfo
      color={AppColors[tooltipOption.color]}
      resolution={tooltipOption.resolution}
      sampleRate={tooltipOption.sampleRate}
      bitDepth={tooltipOption.bitDepth}
    />
  )
}

export default class ProductDetailsTableBuilder {
  editConfig: {
    canEdit: boolean
    canFullEdit: boolean
    isEditing: boolean
  }

  albumTableHandler: ProductDetailsTableHandler

  assetColumns: ColDef[]

  assetTypes: AssetType[]

  allOptions: AssetOption[]

  constructor(
    editConfig: {
      canEdit: boolean
      canFullEdit: boolean
      isEditing: boolean
    },
    albumTableHandler: ProductDetailsTableHandler,
    assetTypes: AssetType[],
    tracks: ProductTrack[],
    allOptions: AssetOption[],
  ) {
    this.editConfig = editConfig
    this.albumTableHandler = albumTableHandler
    this.assetTypes = assetTypes
    this.assetColumns = this.getAssetColumns(tracks?.[0])
    this.allOptions = allOptions
  }

  getAssetColumnDef = (asset: AssetType, colDef: ColDef): ColDef => {
    const assetConf = getAssetType(this.assetTypes, asset.shortName)

    const {canEdit, canFullEdit} = this.editConfig
    return {
      ...colDef,
      colId: asset.shortName,
      headerComponent: TABLE.HEADER_COMPONENTS.HeaderWithIcon,
      headerComponentParams: {
        disabled: !canEdit,
        title: assetConf?.shortName,
        icon: FA_ICONS.pencil,
        valueKey: 'display',
        displayKey: 'display',
        options: canFullEdit ? assetConf?.assetOptions : [notRequiredOption],
        colId: asset.shortName,
        iconShowOnHover: true,
        ...colDef.headerComponentParams,
      },
      headerClass: 'justify-content-center',
      editable: canEdit,
      flex: 1,
      cellRenderer: TABLE.CELL_RENDERERS.WithEditButton,
      cellEditor: TABLE.CELL_EDITORS.SelectCellEditor,
      cellRendererParams: {
        ...colDef.cellRendererParams,
        contentClassName: 'pl-15',
        getClassNames: (params: CustomCellParams) => {
          const optionConf = getAssetBySelection(
            params.data,
            colDef.field ?? '',
          )
          const color = assetConf?.assetOptions?.find(
            (conf) =>
              conf.display ===
              (NOT_VALUE[optionConf?.selection ?? ''] ?? optionConf?.selection),
          )?.color
          return classnames(
            'justify-content-center text-center font-weight-sbold d-flex align-items-center',
            {[`text-${color}`]: color},
          )
        },
      },
      minWidth: 110,
      valueGetter: (params) => {
        const optionConf = getAssetBySelection(
          params.data,
          colDef.field ?? '',
        )?.selection

        return NOT_VALUE[optionConf ?? ''] ?? optionConf
      },
    }
  }

  getAssetColumns = (firstTrack?: ProductTrack): ColDef[] => {
    const {canFullEdit} = this.editConfig
    return this.assetTypes.reduce((columns: ColDef[], asset) => {
      const assetIndex =
        firstTrack?.assets.findIndex(
          (item) =>
            isEqual(item.assetTypeId, asset.id) &&
            !isTableDeleteAction<ProductTrackAsset>(item),
        ) ?? -1
      if (assetIndex >= 0) {
        const assetConf = getAssetType(this.assetTypes, asset.shortName)

        columns.push(
          this.getAssetColumnDef(asset, {
            headerComponentParams: {
              onChange: this.albumTableHandler.handleColumnSelect,
            },
            field: `assets.${assetIndex}`,
            cellRendererParams: {
              tooltip: {
                textBuilder: (data: ProductTrack, colDef: ColDef) =>
                  assetTooltipTextBuilder(data, colDef, this.allOptions),
              },
            },
            cellEditorParams: {
              valueKey: 'display',
              displayKey: 'display',
              options: canFullEdit
                ? assetConf?.assetOptions
                : [notRequiredOption],
              onSelect: this.albumTableHandler.handleSelectAsset,
              field: `assets.${assetIndex}`,
            },
          }),
        )
      }
      return columns
    }, [])
  }

  getTableHeaders = (
    onDeleteItem: ModalAction,
    onEditComments: (editableParams: ICellRendererParams) => void,
    invalid?: boolean,
  ): ColDef[] => {
    const {canEdit, canFullEdit, isEditing} = this.editConfig
    const activateFixColumns =
      this.assetColumns.length > getNoOptionalAssetsLength(this.assetTypes)

    const pinnedOptions = {
      left: activateFixColumns ? 'left' : undefined,
      right: activateFixColumns ? 'right' : undefined,
    }

    return [
      {
        headerName: 'Track',
        field: 'trackNumber',
        cellRenderer: TABLE.CELL_RENDERERS.CustomCell,
        width: 184,
        rowDrag: isEditing,
        pinned: pinnedOptions.left,
        headerClass: 'justify-content-center',
        cellRendererParams: {
          className: 'justify-content-center d-flex align-items-center',
          rightIcon: {
            disabled: !canEdit,
            type: ICON_TYPES.fas,
            iconName: FA_ICONS.check,
            onClick: this.albumTableHandler.handleCheckClick,
            className: 'ml-20',
            getIconColor: getCheckTooltipTextColor,
            tooltip: {
              textBuilder: getCheckTooltipText,
            },
          },
        },
      },
      {
        headerName: 'complete',
        field: 'complete',
        hide: true,
      },
      {
        headerName: 'Track Title',
        field: 'title',
        cellClass: 'font-weight-sbold',
        cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
        suppressSizeToFit: true,
        cellRendererParams: {
          className: 'align-items-center d-flex overflow-hidden',
          fieldProps: {
            placeholder: PRODUCT_DETAIL_PLACEHOLDERS.TRACK_TITLE_TABLE,
            invalid,
          },
          editing: isEditing && canFullEdit,
          wrapWithEllipsis: true,
          showValueOnTooltip: true,
        },
        maxWidth: 150,
        pinned: pinnedOptions.left,
        editable: isEditing,
        cellEditorParams: {
          placeholder: PRODUCT_DETAIL_PLACEHOLDERS.TRACK_TITLE_TABLE,
        },
      },
      ...this.assetColumns,
      {
        field: 'comments',
        editable: canEdit,
        pinned: pinnedOptions.right,
        cellRenderer: TABLE.CELL_RENDERERS.TextFieldCell,
        width: 292,
        suppressSizeToFit: true,
        cellRendererParams: {
          editable: canEdit,
          cellRenderer: TABLE.CELL_RENDERERS.WithEditButton,
          className: 'align-items-center d-flex overflow-hidden',
          wrapWithEllipsis: true,
          editing: isEditing,
          fieldProps: {
            placeholder: PRODUCT_DETAIL_PLACEHOLDERS.COMMENTS_TABLE,
          },
          showValueOnTooltip: true,
          rightIcon: {
            onClick: (editableParams: ICellRendererParams) =>
              onEditComments(editableParams),
          },
        },
      },
      {
        headerName: '',
        cellClass: 'justify-content-center',
        maxWidth: 60,
        pinned: 'right',
        cellRenderer:
          isEditing && canFullEdit ? TABLE.CELL_RENDERERS.CustomCell : '',
        cellRendererParams: {
          className: 'd-flex align-items-center',
          rightIcon: {
            type: ICON_TYPES.fal,
            iconName: FA_ICONS.trash,
            color: AppColors.lightGray,
            hoverColor: AppColors.danger,
            onClick: (editableParams: ICellRendererParams) => {
              onDeleteItem(editableParams, () =>
                this.albumTableHandler.handleDeleteClick(editableParams),
              )
            },
          },
        },
      },
    ]
  }
}
