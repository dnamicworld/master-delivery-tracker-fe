import {ICellRendererParams, RowDragEndEvent} from 'ag-grid-community'

import {HeaderSelectOptions} from '../../../../../components/AgGrid/HeaderComponents'
import {
  AssetOption,
  AssetType,
  DeliveryStatusActions,
  ProductTrack,
  ProductTrackAsset,
} from '../../../../../models'
import {ACTIONS} from '../../../../../utils/constants'
import {
  cloneDeep,
  get,
  isEqual,
  isTableCreateAction,
  isTableDeleteAction,
  removeDeleteActionItems,
  removeElementInmutable,
  set,
  timestamp,
} from '../../../../../utils/helpers'

import {buildAsset} from './ProductDetailsTable.controller'

interface AssetDelivery {
  field: string
  rowIndex: number
  options: AssetOption[]
}

const {CREATE, UPDATE, DELETE} = ACTIONS

const {DELIVERED, NOT_REQUIRED} = DeliveryStatusActions

const statusTarget = [DELIVERED, NOT_REQUIRED]

const isTrackCompleted = (track: ProductTrack): boolean =>
  removeDeleteActionItems(track.assets).reduce(
    (prev: boolean, asset: ProductTrackAsset) =>
      prev ? statusTarget.includes(asset.deliveryStatus) : false,
    true,
  )

const reOrderTracks = (tracks: ProductTrack[]) => {
  let index = 1
  return tracks.map((track) => {
    const trackCopy = cloneDeep(track)
    if (!isTableDeleteAction<ProductTrack>(trackCopy)) {
      if (!isEqual(trackCopy.trackNumber, index) && !trackCopy.action) {
        trackCopy.action = UPDATE
      }
      trackCopy.trackNumber = index
      index += 1
    }
    return trackCopy
  })
}

export default class ProductDetailsTableHandler {
  submitChanges: (newTracks: ProductTrack[], productComplete?: boolean) => void

  tracks: ProductTrack[]

  assetTypes: AssetType[]

  newTrack: ProductTrack

  tracksLength: number

  filteredAssets: ProductTrackAsset[]

  constructor(
    submitChanges: (
      newTracks: ProductTrack[],

      productComplete?: boolean,
    ) => void,
    tracks: ProductTrack[],
    assetTypes: AssetType[],
  ) {
    this.assetTypes = assetTypes
    this.tracks = tracks
    this.tracksLength = removeDeleteActionItems(this.tracks).length
    const tracksAssets = this.tracks?.[0]?.assets ?? []
    this.filteredAssets = removeDeleteActionItems(tracksAssets)
    this.submitChanges = submitChanges
    this.newTrack = {
      title: '',
      isrc: null,
      comments: null,
      version: null,
      majorGenre: null,
      marketingLabel: null,
      complete: false,
      trackNumber: 0,
      assets: tracksAssets.map(
        ({assetTypeId, assetType, action}: ProductTrackAsset) => {
          const assetTypeFounded = this.assetTypes.find(
            (assetTypeObject) => assetTypeObject.id === assetTypeId,
          )
          return {
            assetTypeId,
            action,
            ...buildAsset(
              assetTypeFounded?.defaultValue ?? '',
              assetTypeFounded?.assetOptions ?? [],
              assetType,
            ),
          }
        },
      ),
    }
  }

  handleRowDragEnd = (event: RowDragEndEvent) => {
    const {node, overIndex} = event
    let tracksCopy = cloneDeep(
      this.tracks.filter((track) => track.id !== node.data.id),
    )
    tracksCopy.splice(overIndex, 0, node.data)
    tracksCopy = reOrderTracks(tracksCopy)
    this.submitChanges(tracksCopy)
  }

  handleColumnSelect = ({value, headerParams}: HeaderSelectOptions) => {
    const newTracks = cloneDeep(this.tracks)
    const assetOption = headerParams?.options.find(
      (option) => option.display === value,
    )
    if (assetOption) {
      const {deliveryStatus, sampleRate, bitDepth} = assetOption
      const newTracksUpdated = newTracks.map((track) => {
        const trackCopy = cloneDeep(track)
        const assetIndex = trackCopy.assets.findIndex(
          (asset) =>
            asset.assetType === headerParams?.colId &&
            !isTableDeleteAction<ProductTrackAsset>(asset),
        )
        if (!isEqual(trackCopy.assets[assetIndex].selection, value)) {
          if (!trackCopy.action) {
            trackCopy.action = UPDATE
          }
          set(trackCopy.assets, assetIndex, {
            ...get(trackCopy.assets, assetIndex),
            deliveryStatus,
            sampleRate,
            bitDepth,
            selection: value,
            action: trackCopy.assets[assetIndex].action ?? UPDATE,
          })
          trackCopy.complete = isTrackCompleted(trackCopy)
        }
        return trackCopy
      })
      this.submitChanges(newTracksUpdated)
    }
  }

  handleDeleteClick = (editableParams: ICellRendererParams) => {
    if (this.tracksLength > 1) {
      const {rowIndex} = editableParams
      let tracksCopy = cloneDeep(this.tracks)
      if (isTableCreateAction<ProductTrack>(this.tracks[rowIndex])) {
        tracksCopy = removeElementInmutable<ProductTrack>(this.tracks, rowIndex)
      } else {
        tracksCopy[rowIndex].action = DELETE
      }
      tracksCopy = reOrderTracks(tracksCopy)
      this.submitChanges(tracksCopy)
    }
  }

  handleCheckClick = ({data, node, rowIndex}: ICellRendererParams) => {
    const tracksCopy = cloneDeep(this.tracks)
    if (!tracksCopy[rowIndex].action) {
      tracksCopy[rowIndex].action = UPDATE
    }
    set(tracksCopy, `[${rowIndex}].complete`, !data.complete)
    node.setDataValue('complete', !data.complete)
  }

  // This value comes from ag-grid must be any
  handleSelectAsset = (value: any, editorParams: AssetDelivery) => {
    const {field, rowIndex, options} = editorParams
    const newTracks = cloneDeep(this.tracks)
    const assetOption = options.find((option) => option.display === value)
    if (assetOption) {
      const {deliveryStatus, sampleRate, bitDepth} = assetOption
      const assetField = `${rowIndex}.${field}`
      const assetValue = get(newTracks, assetField)
      const trackValue = get(newTracks, rowIndex)
      if (assetValue) {
        const newAssetField = {
          ...cloneDeep(assetValue),
          deliveryStatus,
          sampleRate,
          bitDepth,
          selection: value,
          action: assetValue.action ?? UPDATE,
        }
        set(newTracks, assetField, newAssetField)
        const newTrack = {
          ...trackValue,
          action: trackValue.action ?? UPDATE,
          complete: isTrackCompleted(trackValue),
        }
        set(newTracks, rowIndex, newTrack)
        this.submitChanges(newTracks)
      }
    }
  }

  addNewTrack = (): ProductTrack[] => {
    const tracksCopy = cloneDeep(this.tracks)
    tracksCopy.push({
      id: timestamp(),
      ...cloneDeep(this.newTrack),
      trackNumber: this.tracksLength + 1,
      action: CREATE,
    })
    return tracksCopy
  }
}
