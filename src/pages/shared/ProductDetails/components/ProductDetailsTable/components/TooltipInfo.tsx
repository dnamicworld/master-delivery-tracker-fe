import {TextVariant} from '../../../../../../DS/components'
import {AssetOption} from '../../../../../../models'

interface TooltipWithInfoProps {
  color: AssetOption['color']
  resolution?: AssetOption['resolution']
  sampleRate?: AssetOption['sampleRate']
  bitDepth?: AssetOption['bitDepth']
}
const TooltipWithInfo = ({
  color,
  resolution,
  sampleRate,
  bitDepth,
}: TooltipWithInfoProps) => (
  <p className='text-left'>
    <TextVariant variant={color}>{resolution} Resolution Delivered</TextVariant>
    <br />
    {sampleRate && bitDepth ? (
      <span>
        {sampleRate} kHz / {bitDepth}-bit
      </span>
    ) : null}
  </p>
)

export default TooltipWithInfo
