import {ChangeEventHandler, LegacyRef} from 'react'

import styles from './ModalTextArea.module.scss'

interface ModalTextAreaProps {
  inputRef: LegacyRef<HTMLTextAreaElement>
  name: string
  handleChange: ChangeEventHandler<HTMLTextAreaElement>
}

const ModalTextArea = ({inputRef, name, handleChange}: ModalTextAreaProps) => (
  <textarea
    className={styles.modalTextArea}
    ref={inputRef}
    name={name}
    onChange={handleChange}
  />
)

export default ModalTextArea
