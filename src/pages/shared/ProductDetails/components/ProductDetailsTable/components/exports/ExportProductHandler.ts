import {
  Product,
  ProductTrackAsset,
  ProductTrack,
  AssetType,
} from '../../../../../../../models'
import {
  MESSAGES,
  PRODUCT_TYPE_DISPLAY,
  Status,
} from '../../../../../../../utils/constants'
import {
  boldRow,
  CellValue,
  downloadExcelFile,
  getXlsxValue,
  Downloader,
} from '../../../../../../../utils/helpers'
import {getAssetTypeById} from '../../../../../../../utils/helpers/assets'
import {buildProductFileName} from '../../../../ProductDetails.controller'

export class ExportProductHandler implements Downloader<ProductTrack> {
  private readonly tracks: ProductTrack[] = []

  private readonly tracksTitle: string = ''

  private readonly product?: Product

  private readonly assetTypes: AssetType[] = []

  private mergedCells = [
    {s: {r: 5, c: 0}, e: {r: 5, c: 1}},
    {s: {r: 6, c: 0}, e: {r: 6, c: 1}},
    {s: {r: 7, c: 0}, e: {r: 7, c: 1}},
    {s: {r: 8, c: 0}, e: {r: 8, c: 1}},
    {s: {r: 9, c: 0}, e: {r: 9, c: 1}},
    {s: {r: 10, c: 0}, e: {r: 10, c: 1}},
    {s: {r: 11, c: 0}, e: {r: 11, c: 1}},
    {s: {r: 5, c: 3}, e: {r: 5, c: 4}},
    {s: {r: 6, c: 3}, e: {r: 6, c: 4}},
    {s: {r: 7, c: 3}, e: {r: 7, c: 4}},
    {s: {r: 8, c: 3}, e: {r: 8, c: 4}},
    {s: {r: 9, c: 3}, e: {r: 9, c: 4}},
    {s: {r: 5, c: 5}, e: {r: 5, c: 6}},
    {s: {r: 6, c: 5}, e: {r: 6, c: 6}},
    {s: {r: 7, c: 5}, e: {r: 7, c: 6}},
    {s: {r: 8, c: 5}, e: {r: 8, c: 6}},
    {s: {r: 9, c: 5}, e: {r: 9, c: 6}},
    {s: {r: 10, c: 5}, e: {r: 10, c: 6}},
    {s: {r: 11, c: 5}, e: {r: 11, c: 6}},
    {s: {r: 12, c: 5}, e: {r: 12, c: 6}},
    {s: {r: 5, c: 7}, e: {r: 5, c: 8}},
    {s: {r: 7, c: 7}, e: {r: 13, c: 8}}, // below comments H8-I14
  ]

  constructor(
    tracks: ProductTrack[],
    tracksTitle: string,
    assetTypes: AssetType[],
    product?: Product,
  ) {
    this.tracks = tracks
    this.tracksTitle = tracksTitle
    this.product = product
    this.assetTypes = assetTypes
  }

  getHeaders = (assets?: ProductTrackAsset[]): CellValue[] =>
    boldRow([
      'Track',
      'Track Title',
      'Complete',
      ...(assets
        ? assets.map((asset) => {
            const assetConf = getAssetTypeById(
              this.assetTypes,
              asset.assetTypeId,
            )
            return assetConf?.shortName ?? ''
          })
        : []),
      'Comments',
    ])

  getInfo = (tracks: ProductTrack[]) =>
    tracks.map((track) => [
      track.trackNumber.toString(),
      track.title,
      track.complete ? 'Yes' : 'No',
      ...track.assets.map((asset) => asset.selection ?? ''),
      track.comments ?? '',
    ])

  private getProductInfo = (product?: Product): CellValue[][] => [
    boldRow([getXlsxValue(product?.artist)]),
    boldRow([getXlsxValue(product?.title)]),
    boldRow([
      `Status: ${product?.complete ? Status.COMPLETE : Status.INCOMPLETE}`,
    ]),
    [],
    [],
    boldRow([
      'Release Info',
      '',
      '',
      'Identifiers',
      '',
      'Personnel',
      '',
      'Comments',
    ]),
    [
      ...boldRow(['Release Date', '', '', 'UPC', '', 'A&R Admin', '']),
      getXlsxValue(product?.arComments, MESSAGES.NO_COMMENTS),
    ],
    [
      getXlsxValue(product?.releaseDate),
      '',
      '',
      getXlsxValue(product?.upc),
      '',
      getXlsxValue(product?.adminName),
    ],
    boldRow(['Marketing Label', '', '', 'Selection #', '', 'Producer']),
    [
      getXlsxValue(product?.marketingLabel),
      '',
      '',
      getXlsxValue(product?.selectionNumber),
      '',
      getXlsxValue(product?.producer),
    ],
    boldRow(['Product Type', '', '', '', '', 'Mixer']),
    [
      product?.productType
        ? getXlsxValue(PRODUCT_TYPE_DISPLAY[product.productType])
        : '',
      '',
      '',
      '',
      '',
      getXlsxValue(product?.mixer),
    ],
    boldRow(['', '', '', '', '', 'Mastering']),
    ['', '', '', '', '', getXlsxValue(product?.mastering)],
    [],
  ]

  transformData = (): CellValue[][] => [
    ...this.getProductInfo(this.product),
    boldRow([this.tracksTitle]),
    this.getHeaders(this.tracks[0].assets),
    ...this.getInfo(this.tracks),
  ]

  public download = () => {
    downloadExcelFile(
      buildProductFileName(this.product),
      this.transformData(),
      this.mergedCells,
      true,
    )
  }
}
