import {useState} from 'react'

import {
  Button,
  Col,
  HeaderTitle,
  Icon,
  Row,
  TextField,
} from '../../../../../DS/components'
import {ContentModal} from '../../../../../components'
import {
  CUSTOM_ASSET_COLUMN,
  ERRORS,
  FA_ICONS,
  ICON_TYPES,
} from '../../../../../utils/constants'
import {cloneDeep, upperCase, validateField} from '../../../../../utils/helpers'

export interface AddCustomAssetTypeModalProps {
  shown: boolean
  onClose: () => void
  onSuccess: (name: string, options: string[]) => void
}

const AddCustomAssetTypeModal = ({
  shown,
  onClose,
  onSuccess,
}: AddCustomAssetTypeModalProps) => {
  const [options, setOptions] = useState<string[]>([])
  const [columnName, setColumName] = useState('')
  const [error, setError] = useState<{error: boolean; message?: string}>({
    error: false,
  })

  const handleSetOption = (value: string, key: number) => {
    const optionsCopy = cloneDeep(options)
    optionsCopy[key] = value
    setOptions(optionsCopy)
  }

  const removeSetOption = (key: number) =>
    setOptions(options.filter((_, index) => index !== key))

  const handleClose = () => {
    setColumName('')
    setOptions([])
    setError({error: false})
    onClose()
  }

  const optionField = (key: number) => (
    <Row key={key}>
      <Col sm={11}>
        <HeaderTitle className='mb-2'>Option</HeaderTitle>
        <TextField
          value={options[key]}
          onChange={(event) => handleSetOption(event.target.value, key)}
        />
      </Col>
      <Col sm={1}>
        <Icon
          type={ICON_TYPES.fas}
          iconName={FA_ICONS.close}
          onClick={() => removeSetOption(key)}
        />
      </Col>
    </Row>
  )

  const addCustomAssetColumn = (
    <>
      <div className='mb-20'>{CUSTOM_ASSET_COLUMN.message}</div>
      <HeaderTitle className='mb-2'>Column Name</HeaderTitle>
      <TextField
        onChange={(event) => {
          const {value} = event.target
          setColumName(value)
          const isAllUpperCase = value === upperCase(value)
          if (value.length > 13 && !isAllUpperCase) {
            setError({
              error: true,
              message: ERRORS.COLUM_NAME_CHARACTERS('mixed', 13),
            })
          } else if (value.length > 9 && isAllUpperCase) {
            setError({
              error: true,
              message: ERRORS.COLUM_NAME_CHARACTERS('upper case', 9),
            })
          } else {
            setError({error: false})
          }
        }}
        value={columnName}
        errorMessage={error.message}
        validationState={validateField(error.error)}
      />
      {options.map((_, index) => optionField(index))}
      <Button
        containerStyle='link'
        className='font-weight-sbold pl-0'
        onClick={() => setOptions([...options, ''])}
      >
        <Icon
          type={ICON_TYPES.fal}
          iconName={FA_ICONS.plus}
          className='align-self-center mr-5'
          color='primary'
        />{' '}
        Add Option
      </Button>
    </>
  )

  return (
    <ContentModal
      show={shown}
      title={CUSTOM_ASSET_COLUMN.title}
      actionName={CUSTOM_ASSET_COLUMN.actionName}
      onCancelAction={handleClose}
      onActionClick={() => {
        onSuccess(columnName, options)
        setError({error: false})
        handleClose()
      }}
      disabledSuccessButton={error.error}
    >
      {addCustomAssetColumn}
    </ContentModal>
  )
}

export default AddCustomAssetTypeModal
