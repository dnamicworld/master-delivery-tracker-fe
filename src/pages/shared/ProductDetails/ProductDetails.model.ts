import {Product} from '../../../models'

export interface DetailsRouteParams {
  productId: string
}

export interface ProductDetailState {
  editing: boolean
  editingOnlyTable: boolean
  editingField: string
  initial?: Product
  product?: Product
  gpid?: string
  invalid?: boolean
}
