import {ReactNode} from 'react'

import {Col, List, ListItem, HeaderTitle} from '../../DS/components'
import {BarChart} from '../../components'
import {AssetType, AssetTypeStat, Product} from '../../models'
import {
  capitalize,
  get,
  getBarData,
  getDateString,
  uniquely,
} from '../../utils/helpers'

// Type guard
export function isAssetTypeStat(
  asset: AssetType | AssetTypeStat,
): asset is AssetTypeStat {
  return (asset as AssetTypeStat).deliveryCount !== undefined
}

const noData = {name: '-', pv: '', leyend: 'No Data Available'}

const getNameFromEmail = (email: string): string => {
  let name = email
  if (email.includes('@')) {
    const [nameParts] = email.split('@')
    if (nameParts.includes('.')) {
      const [firstName, lastName] = nameParts.split('.')
      name = `${capitalize(firstName)} ${capitalize(lastName)}`
    }
  }
  return name
}

type DataObject = {[key: string]: any}

export const cardHeader = (product?: Product): ReactNode => (
  <div>
    <HeaderTitle variant='h4'>Assets Delivered</HeaderTitle>
    {product?.updatedAt ? (
      <h5 className='m-0'>
        Last updated{' '}
        {getDateString({
          dateString: product.updatedAt,
          withTime: true,
          localDate: true,
        })}
        {product?.updatedBy
          ? ` by ${getNameFromEmail(product?.updatedBy)}`
          : ''}
      </h5>
    ) : null}
  </div>
)

interface ChartParams {
  item: AssetType | AssetTypeStat
  barChartData?: DataObject
}

type BarChartSource = 'fromProducts' | 'fromTracks'

const barChartSource = {
  products: 'fromProducts' as BarChartSource,
  tracks: 'fromTracks' as BarChartSource,
}

const barChartContentProperties = {
  fromProducts: {
    firstCol: {
      sm: 3,
      pl: 'pl-0',
    },
    secondCol: {
      sm: 9,
      chartData: ({item}: ChartParams) =>
        isAssetTypeStat(item) ? [getBarData(item)] : [noData],
    },
  },
  fromTracks: {
    firstCol: {
      sm: 2,
      pl: 'pl-30',
    },
    secondCol: {
      sm: 10,
      chartData: ({item, barChartData}: ChartParams) =>
        get(barChartData, item.shortName, []),
    },
  },
}

const barChartList = (
  assets: AssetType[] | AssetTypeStat[],
  source: BarChartSource,
  index?: number,
  barChartData?: DataObject,
): ReactNode => {
  const properties = barChartContentProperties[source]
  return (
    <List>
      {assets.map((item: AssetType) => (
        <ListItem spacing='sm' key={`listItem${uniquely()}`}>
          <Col
            sm={properties.firstCol.sm}
            className={`${properties.firstCol.pl} pr-0 pt-1 asset-label`}
          >
            {item.longName}
          </Col>
          <Col sm={properties.secondCol.sm} className='pl-0 pr-0'>
            <BarChart
              data={properties.secondCol.chartData({item, barChartData})}
              colorIndex={index}
              xKey='name'
              yKey='pv'
              layout='vertical'
              withNumericAxis
            />
          </Col>
        </ListItem>
      ))}
    </List>
  )
}

export const barChartContentFromProducts = (
  assetsProducts: AssetTypeStat[],
  index?: number,
): ReactNode => barChartList(assetsProducts, barChartSource.products, index)

export const barChartContentFromTracks = (
  barChartData: DataObject,
  filteredAssets: AssetType[],
  index?: number,
): ReactNode =>
  barChartList(filteredAssets, barChartSource.tracks, index, barChartData)
