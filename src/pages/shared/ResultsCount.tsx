import {ReactNode} from 'react'
import {Col, Row} from 'src/DS/components'

interface ResultCountProps {
  children?: ReactNode
  display?: number
  count?: number
  label?: string
  linkText?: string
  displayReset: boolean
  handleReset: () => void
}

const ResultCount = ({
  children,
  display = 0,
  count = 0,
  label = 'results',
  linkText,
  displayReset,
  handleReset,
}: ResultCountProps) => (
  <Row className='mb-4'>
    <Col sm={12}>
      <p>
        Showing {display} of {count} {label} {children}
        {displayReset ? (
          // eslint-disable-next-line jsx-a11y/anchor-is-valid,jsx-a11y/no-static-element-interactions
          <a
            className='ml-30 font-weight-sbold text-decoration-none'
            onClick={handleReset}
          >
            {linkText ?? 'Reset Filters'}
          </a>
        ) : null}
      </p>
    </Col>
  </Row>
)

export default ResultCount
