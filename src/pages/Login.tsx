import {useOktaAuth} from '@okta/okta-react'
import {useEffect} from 'react'
import {useHistory} from 'react-router-dom'

import useUser from '../hooks/user'
import {ROUTES} from '../utils/constants'

const Login = () => {
  const {authState, oktaAuth} = useOktaAuth()
  const history = useHistory()
  const {user} = useUser()

  const hasRolesToUse = user?.groups && user?.labels

  useEffect(() => {
    if (authState?.isAuthenticated) {
      if (hasRolesToUse) {
        history.push(user.home)
      }
    } else {
      oktaAuth
        .signInWithRedirect({
          originalUri: ROUTES.LOGIN,
        })
        .catch((err) => {
          // eslint-disable-next-line no-console
          console.log('Sign with redirect error', err)
        })
    }
  }, [history, oktaAuth, user.home, authState?.isAuthenticated, hasRolesToUse])

  if (!authState) {
    return <div>Loading...</div>
  }

  return null
}

export default Login
