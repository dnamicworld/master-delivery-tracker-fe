import {useEffect, useMemo} from 'react'

import {useHistory} from 'react-router-dom'

import {BreadcrumbItem, Col, Row} from '../../DS/components'
import {useAppContext} from '../../context/coreProvider'
import {useModalDisplay} from '../../context/modal'
import useProductDetails from '../../hooks/productDetails'
import useUser from '../../hooks/user'
import {BREADCRUMBS} from '../../utils/constants'
import {handleInputChange, validateProduct} from '../../utils/helpers'
import {IndexCardPlaceholder} from '../shared/IndexPages'
import {
  ProductActions,
  ProductDetailsForm,
  ProductDetailsTable,
  ProductHeader,
  ProductNew,
  getProductDetailBreadCrumbs,
  ProductDetailsCard,
} from '../shared/ProductDetails'

const SingleDetails = () => {
  const {
    isNewProduct,
    onFormCancel,
    onFormSave,
    productDetailState,
    report,
    updateProduct,
    updateState,
    isLoading,
    toInitial,
  } = useProductDetails()
  const {displayModal} = useModalDisplay()
  const history = useHistory()
  const {user} = useUser()
  const {setBreadcrumbs} = useAppContext()
  const {product: stateProduct} = productDetailState

  const breadCrumbData = useMemo(
    () => getProductDetailBreadCrumbs(isNewProduct, stateProduct, report),
    [isNewProduct, report, stateProduct],
  )

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  useEffect(() => {
    const breadCrumbs: BreadcrumbItem[] = [
      {
        ...BREADCRUMBS.HOME,
      },
      ...breadCrumbData,
    ]
    setBreadcrumbs(breadCrumbs)
  }, [breadCrumbData, setBreadcrumbs])

  const updateProductInput = handleInputChange(updateProduct)
  const updateGPID = handleInputChange(updateState)
  const updateAutoSaveInput = handleInputChange(() => onFormSave(stateProduct))

  const Actions = user.roles.hasFullWriteAccess() ? (
    <ProductActions
      editing={
        productDetailState.editing || productDetailState.editingOnlyTable
      }
      onCancel={onFormCancel}
      onEditing={() => updateState('editing', true)}
      onSave={(redirectNew: boolean) => {
        validateProduct(
          history,
          onFormSave,
          toInitial,
          displayModal,
          redirectNew,
          isNewProduct,
          stateProduct,
        )
      }}
    />
  ) : null

  const importProduct = isNewProduct ? (
    <ProductNew
      actions={Actions}
      gpid={productDetailState?.gpid}
      updateGPID={updateGPID}
    />
  ) : null
  return (
    <>
      <Row className='albumDetailPage pt-30 pb-30'>
        {importProduct}
        <ProductHeader
          headerActions={isNewProduct ? undefined : Actions}
          isLoading={isLoading}
        />
        <Col sm={12} className='pb-40 pt-25'>
          <Row>
            <ProductDetailsForm
              editingField={productDetailState.editingField}
              editing={productDetailState.editing}
              updateProduct={updateProduct}
              onChange={updateProductInput}
              onAutoSave={updateAutoSaveInput}
              onFormSave={onFormSave}
              product={stateProduct}
              isLoading={isLoading}
            />
          </Row>
        </Col>
        <Col sm={12}>
          {isLoading ? (
            <IndexCardPlaceholder />
          ) : (
            <ProductDetailsCard product={stateProduct} />
          )}
        </Col>
        <ProductDetailsTable title='Single Asset Table' />
      </Row>
    </>
  )
}

export default SingleDetails
