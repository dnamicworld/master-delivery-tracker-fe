import {act, render} from '@testing-library/react'

import 'jest-canvas-mock'

import ReactRouterDom, {BrowserRouter} from 'react-router-dom'

import {WithMainLayout} from '../../components/Layout'
import {ProductDetailsContextProvider} from '../../hooks/productDetails'
import {TableContextProvider} from '../../hooks/table'

import {SingleDetails} from '.'

describe('Single Detail Page', () => {
  test('renders Single details page', async () => {
    const promise = Promise.resolve()
    const SingleDetailsPage = WithMainLayout(() => (
      <TableContextProvider>
        <ProductDetailsContextProvider>
          <SingleDetails />
        </ProductDetailsContextProvider>
      </TableContextProvider>
    ))
    jest
      .spyOn(ReactRouterDom, 'useParams')
      .mockReturnValue({productId: undefined})
    await act(() => promise)
    const renderSingleDetails = render(
      <BrowserRouter>
        <SingleDetailsPage />
      </BrowserRouter>,
    )
    expect(renderSingleDetails).toMatchSnapshot()
  })
})
