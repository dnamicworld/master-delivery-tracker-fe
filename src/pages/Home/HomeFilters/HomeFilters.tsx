import {
  AssetTypesFilter,
  LabelsFilter,
  ReleaseTypeFilter,
  YearFilter,
} from 'src/components'
import {FilterWrapperProps} from 'src/context/Filters/pageFilters'

import {Col, Row} from '../../../DS/components'

const HomeFilters = ({
  filters,
  handleFilterChange,
  resetFilters,
  handleCheckOnEnter,
}: FilterWrapperProps) => (
  <Row className='filters w-100'>
    <Col sm={3}>
      <YearFilter
        value={filters?.year ?? []}
        name='year'
        onChange={handleFilterChange}
        handleCheckOnEnter={handleCheckOnEnter}
        multiple
        fullWidth
        resetFilters={resetFilters}
      />
    </Col>
    <Col sm={3}>
      <LabelsFilter
        value={filters?.label ?? []}
        name='label'
        onChange={handleFilterChange}
        handleCheckOnEnter={handleCheckOnEnter}
        fullWidth
        resetFilters={resetFilters}
      />
    </Col>
    <Col sm={3}>
      <ReleaseTypeFilter
        value={filters?.releaseType ?? []}
        name='releaseType'
        onChange={handleFilterChange}
        multiple
        fullWidth
      />
    </Col>
    <Col sm={3}>
      <AssetTypesFilter
        value={filters?.assetType ?? []}
        name='assetType'
        onChange={handleFilterChange}
        handleCheckOnEnter={handleCheckOnEnter}
        multiple
        fullWidth
        resetFilters={resetFilters}
      />
    </Col>
  </Row>
)

export default HomeFilters
