import {useEffect, useState} from 'react'
import {useHistory} from 'react-router-dom'

import {PaginationOptions} from '../../DS/components'
import {usePageFilterContext} from '../../context/Filters/pageFilters'
import {useAlertDisplay} from '../../context/alert'
import {useAppContext} from '../../context/coreProvider'
import {useCreateReport} from '../../context/createReport'
import useAssetTypes from '../../hooks/data/useAssetTypes'
import useLabels from '../../hooks/data/useLabels'
import useReports from '../../hooks/data/useReports'
import {AssetType, Label, Report, ReportFilters} from '../../models'
import {
  normalizeFilters,
  getNoReportsMessage,
  ReportCard,
  omit,
  mutateReports,
  uniquely,
  getReportCounts,
  getQueryParam,
  cloneDeep,
} from '../../utils/helpers'
import {
  createObjectMapper,
  groupReportsByFilter,
} from '../../utils/helpers/filters'
import {getReleaseTypesFromFilterArray} from '../shared/IndexPages/IndexPages.controller'
import {ReportCards, reportsPage} from '../shared/ReportCards'
import ResultCount from '../shared/ResultsCount'

import {HomeFilters} from './HomeFilters'
import {HomePlaceHolder} from './HomePlaceHolder'

const filterInitialValue: ReportFilters = {
  year: [],
  assetType: [],
  releaseType: [],
  label: [],
}

export const LOADING_TIME_OUT = 1000

const Home = () => {
  const {setTitle} = useAppContext()
  const {display: alertDisplay} = useAlertDisplay()
  const [stateReports, setReports] = useState<ReportCard[]>()
  const {
    filters,
    filtersChanged,
    handleFilterChange,
    handleCheckOnEnter,
    setFiltersState,
    setFiltersBarContent,
    setWithTags,
    setFilterMappers,
    handleResetAllFilters,
    resetFilters,
  } = usePageFilterContext()
  const {setSuccessAction} = useCreateReport()
  const [showPlaceholder, setShowPlaceholder] = useState(true)
  const [paginationOptions, setPaginationOptions] = useState<PaginationOptions>(
    {
      page: 1,
      pages: 0,
      count: 0,
      size: reportsPage * 2,
    },
  )

  const history = useHistory()
  const releaseTypes = getReleaseTypesFromFilterArray(filters.releaseType ?? [])
  const {labels} = useLabels()
  const {assetTypes} = useAssetTypes({
    filterDefaultAssets: true,
  })
  const reportQueryParams = {
    ...omit(paginationOptions, ['pages', 'count']),
    years: filters.year,
    labelIds: filters.label,
    releaseTypes,
  }

  const {reports, count, pages, pageNumber, isLoading} =
    useReports(reportQueryParams)

  const {
    location: {search},
  } = history
  const newReport = getQueryParam(search, 'newReport')

  useEffect(() => {
    if (newReport) {
      alertDisplay('Report Successfully Created!')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newReport])

  useEffect(() => {
    window.scrollTo(0, 0)
    setTitle(<>Label Reports</>)
    const filtersToSet = normalizeFilters(search, filterInitialValue)
    setFiltersState<ReportFilters>(filtersToSet, cloneDeep(filterInitialValue))
    setWithTags(true)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    let timer: NodeJS.Timeout
    if (isLoading) {
      setShowPlaceholder(true)
    } else {
      timer = setTimeout(() => setShowPlaceholder(false), LOADING_TIME_OUT)
    }
    return () => clearTimeout(timer)
  }, [isLoading])

  useEffect(() => {
    if (count && pages && pageNumber) {
      setPaginationOptions((prevState) => ({
        ...prevState,
        count: count / 2,
        page: pageNumber,
        pages,
      }))
    }
  }, [count, pages, pageNumber])

  const handleReports = () => {
    if (Array.isArray(reports)) {
      const reportsGrouped = groupReportsByFilter(
        reports,
        filters as ReportFilters,
      )
      setReports(reportsGrouped)
    }
  }

  useEffect(() => {
    setSuccessAction((report?: Report) => {
      mutateReports(reportQueryParams, report)
      const year = report?.year ? [report.year.toString()] : filters.year
      const label = report?.label ? [report.label.id] : filters.label
      const {assetType} = filters
      const {releaseType} = filters
      setFiltersState<ReportFilters>({year, assetType, releaseType, label})
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(reportQueryParams)])

  useEffect(() => {
    setPaginationOptions((prevState) => ({
      ...prevState,
      page: 1,
    }))
    handleReports()
    setFiltersBarContent(
      <HomeFilters
        filters={filters}
        handleFilterChange={handleFilterChange}
        resetFilters={resetFilters}
        handleCheckOnEnter={handleCheckOnEnter}
      />,
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(filters)])

  useEffect(() => {
    handleReports()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(reports)])

  useEffect(() => {
    if (labels && assetTypes) {
      const labelsMapper = createObjectMapper<Label>('name', labels)
      const assetsMapper = createObjectMapper<AssetType>('longName', assetTypes)
      setFilterMappers({label: labelsMapper, assetType: assetsMapper})
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(labels), JSON.stringify(assetTypes)])

  const [display, reportCount] = getReportCounts(
    paginationOptions,
    stateReports?.length ?? 0,
  )

  const cards = [...Array(reportsPage).keys()]

  return showPlaceholder ? (
    <>
      {cards.map(() => (
        <HomePlaceHolder key={uniquely()} />
      ))}
    </>
  ) : (
    <>
      <ResultCount
        display={display}
        count={reportCount}
        label='reports'
        displayReset={filtersChanged}
        handleReset={handleResetAllFilters}
      />
      <ReportCards
        reports={stateReports}
        noReportsMessage={getNoReportsMessage(filters, labels)}
        paginationOptions={paginationOptions}
        setPaginationOptions={setPaginationOptions}
        queryParams={reportQueryParams}
      />
    </>
  )
}

export default Home
