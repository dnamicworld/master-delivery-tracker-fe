import classNames from 'classnames'

import {Card, Row} from '../../../DS/components'
import {Placeholder} from '../../../components'

import {uniquely} from '../../../utils/helpers'

const placeHolderButtons = <Placeholder className='mr-4' width='113px' />
const cardHeader = (
  <div className='d-flex w-100 p-0 d-flex align-items-center'>
    <div className='d-flex w-100 align-items-center'>
      <Placeholder height='40px' width='60px' noRadius />
      <Placeholder className='ml-10' width='249px' />
    </div>

    <div className='d-flex justify-content-end align-items-center h-100'>
      {placeHolderButtons}
      {placeHolderButtons}
      {placeHolderButtons}
      {placeHolderButtons}
    </div>
  </div>
)
const placeholderRows = [...Array(9).keys()]
const information = (
  <div className='mb-5'>
    <Placeholder className='mr-4' width='116px' />
    <Placeholder className='mr-6' width='726px' />
    <Placeholder className='ml-2' width='31px' />
  </div>
)

const HomePlaceHolder = () => (
  <Card className='mb-40 px-30 py-40' header={cardHeader}>
    <Row className={classNames('d-flex px-10')}>
      <div className='pr-30 py-5'>
        <Placeholder
          borderCircle
          height='159px'
          width='159px'
          className='mb-20 d-flex'
        />
        <Placeholder
          borderCircle
          height='159px'
          width='159px'
          className='d-flex'
        />
      </div>
      <div className='py-5 ml-20'>
        {placeholderRows.map(() => (
          <Placeholder
            key={uniquely()}
            className='d-block mr-4 mb-40'
            width='116px'
          />
        ))}
      </div>
      <div className='py-5 ml-40 '>
        {placeholderRows.map(() => (
          <div key={uniquely()} className='mb-20'>
            {information} {information}
          </div>
        ))}
      </div>
    </Row>
  </Card>
)
export default HomePlaceHolder
