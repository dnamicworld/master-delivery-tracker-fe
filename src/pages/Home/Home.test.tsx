import {act, render} from '@testing-library/react'

import 'jest-canvas-mock'

import ReactRouterDom, {BrowserRouter} from 'react-router-dom'

import {Home} from '..'
import {withMainFilters} from '../../components/AppRouter'

describe('Home Page', () => {
  test('renders Home page', async () => {
    const promise = Promise.resolve()
    const HomePage = withMainFilters(Home)
    const renderHomePage = render(
      <BrowserRouter>
        <HomePage />
      </BrowserRouter>,
    )
    jest
      .spyOn(ReactRouterDom, 'useParams')
      .mockReturnValue({labelId: undefined})
    await act(() => promise)
    expect(renderHomePage).toMatchSnapshot()
  })
})
