import {Grid, Row} from '../DS/components/bootstrap'

const UnAuthorized = () => (
  <Grid fluid className='error-container'>
    <Row className='justify-content-md-center align-items-center p-15'>
      <h2 className='pb-10'>You do not have access :(</h2>
      <h4>You do not have access to Master Delivery Report.</h4>
      <h4>Please email support@wmg.com to request access.</h4>
    </Row>
  </Grid>
)

export default UnAuthorized
