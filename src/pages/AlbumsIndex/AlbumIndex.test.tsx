import {render, act} from '@testing-library/react'

import 'jest-canvas-mock'

import ReactRouterDom, {BrowserRouter} from 'react-router-dom'

import {withProductFilters} from '../../components/AppRouter'

import {AlbumsIndex} from '.'

describe('Album Index Page', () => {
  test('renders Album index page', async () => {
    const promise = Promise.resolve()
    const AlbumIndexPage = withProductFilters(AlbumsIndex)
    jest.spyOn(ReactRouterDom, 'useParams').mockReturnValue({labelId: '1'})
    await act(() => promise)
    const renderAlbumIndex = render(
      <BrowserRouter>
        <AlbumIndexPage />
      </BrowserRouter>,
    )
    expect(renderAlbumIndex).toMatchSnapshot()
  })
})
