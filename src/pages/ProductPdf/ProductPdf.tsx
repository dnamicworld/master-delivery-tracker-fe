import classNames from 'classnames'
import html2canvas from 'html2canvas'
import {jsPDF} from 'jspdf'
import {ReactNode, useEffect, useState} from 'react'
import {useHistory, useLocation} from 'react-router-dom'

import {Col, Navbar, Row, TextVariantForPDFRender} from '../../DS/components'
import useAllOptions from '../../hooks/data/useAllOptions'
import useAssetTypes from '../../hooks/data/useAssetTypes'
import {AssetType, Product} from '../../models'
import {PRODUCT_TYPE_DISPLAY, PDF_MAX_ASSETS} from '../../utils/constants'
import {
  commentLabel,
  getDateString,
  getStatus,
  NOT_VALUE,
} from '../../utils/helpers'
import {getAssetTypeById} from '../../utils/helpers/assets'
import {
  buildProductFileName,
  buildProductRoute,
} from '../shared/ProductDetails/ProductDetails.controller'

import ProductPDFRender from './ProductPDFRender'
import styles from './ProductPdf.module.scss'

interface FormHeaderProps {
  children: ReactNode
  className?: string
}

const FormHeader = ({children, className}: FormHeaderProps) => (
  <h4 className={classNames('m-0 font-weight-bold', className)}>{children}</h4>
)

const IDS = {
  table: 'page-table',
  pdfHeader: 'pdf-header',
  form: 'form-section',
}
const getFormValue = (value?: string | null) => value || 'No Data Available'

const getTextColor = (value?: string | null) =>
  value ? 'text-gray' : 'text-gray-light'

const ProductPdf = () => {
  const history = useHistory()
  const {state: stateProduct} = useLocation<Product>()
  const [product, setProduct] = useState<Product>()
  const [headers, setHeaders] = useState<string[] | undefined>()
  const [stateAssetTypes, setStateAssetTypes] = useState<AssetType[]>([])
  const {assetTypes: assetTypesResponse} = useAssetTypes()
  const {options = []} = useAllOptions()

  useEffect(() => {
    const assetTypes = Array.isArray(assetTypesResponse)
      ? assetTypesResponse
      : []
    setStateAssetTypes(assetTypes)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(assetTypesResponse)])

  const assetsLength = headers?.length || 1
  const rowsLength =
    product?.tracks?.reduce((sum, track) => {
      const rows = track.comments ? 3 : 2
      return sum + rows
    }, 0) || 1

  useEffect(() => {
    if (stateProduct) {
      setProduct(stateProduct)
    }
  }, [stateProduct])
  useEffect(() => {
    if (product?.tracks?.[0].assets) {
      const assetsFromTrack = product.tracks[0].assets.filter(
        (_, index) => index < PDF_MAX_ASSETS,
      )
      const assetTypes = assetsFromTrack.reduce((prev: string[], asset) => {
        const currentAssetType = getAssetTypeById(
          stateAssetTypes,
          asset.assetTypeId,
        )
        if (asset.assetType) prev.push(currentAssetType?.shortName ?? '')
        return prev
      }, [])
      setHeaders(assetTypes)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [product, stateAssetTypes])

  useEffect(() => {
    if (product && headers) {
      const formSection = document.getElementById(IDS.form)
      const pdfHeader = document.getElementById(IDS.pdfHeader)

      if (formSection && pdfHeader) {
        // eslint-disable-next-line new-cap
        const doc = new jsPDF({
          orientation: 'l',
          unit: 'mm',
          format: [372.5, 287.9],
        })
        const pdfRenderer = new ProductPDFRender(
          doc,
          headers,
          product,
          stateAssetTypes,
          options,
        )
        html2canvas(pdfHeader, {scale: 2}).then((canvas) => {
          pdfRenderer.setPDFHeader(canvas)
          html2canvas(formSection, {scale: 2}).then((pdfSectionCanvas) => {
            pdfRenderer.setPDFForm(pdfSectionCanvas)
            pdfRenderer.setTableLegend(true)
            pdfRenderer.setTableContent(IDS.table, rowsLength, canvas)
            pdfRenderer.setPageNumber()
            doc.save(`${buildProductFileName(product)}.pdf`)
            if (product.id) {
              history.push(buildProductRoute(product.productType, product.id))
            }
          })
        })
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [product, headers])

  return (
    <>
      <Row id={IDS.pdfHeader}>
        <Navbar title='Master Delivery Report' className='pdf'>
          <Row className='d-inline-block ml-4'>
            <Col sm={12}>
              <TextVariantForPDFRender className='m-0'>
                {product?.artist}
              </TextVariantForPDFRender>
            </Col>
            <Col sm={12}>
              <TextVariantForPDFRender variant='gray' className='m-0'>
                {product?.title}
              </TextVariantForPDFRender>
            </Col>
            <Col sm={12}>
              <TextVariantForPDFRender
                className={classNames('m-0', {
                  'text-danger': !product?.complete,
                  'text-success': product?.complete,
                })}
              >
                Status: {getStatus(product?.complete)}
              </TextVariantForPDFRender>
            </Col>
          </Row>
        </Navbar>
      </Row>
      <Row className={styles.pdfSection}>
        <Row id={IDS.form} className={styles.formSection}>
          <Col className='p-0' sm={12}>
            <Row>
              <Col sm={2}>
                <FormHeader className='mb-15 text-black'>
                  Release Info
                </FormHeader>
                <div className='mb-15'>
                  <FormHeader className='text-gray'>Release Date</FormHeader>
                  <TextVariantForPDFRender
                    className={getTextColor(product?.releaseDate)}
                  >
                    {getFormValue(
                      getDateString({dateString: product?.releaseDate}),
                    )}
                  </TextVariantForPDFRender>
                </div>
                <div className='mb-15'>
                  <FormHeader className='text-gray'>Marketing Label</FormHeader>
                  <TextVariantForPDFRender
                    className={getTextColor(product?.marketingLabel)}
                  >
                    {getFormValue(product?.marketingLabel)}
                  </TextVariantForPDFRender>
                </div>
                <div className='mb-15'>
                  <FormHeader className='text-gray'>Product Type</FormHeader>
                  <TextVariantForPDFRender className='text-gray'>
                    {PRODUCT_TYPE_DISPLAY[product?.productType ?? '']}
                  </TextVariantForPDFRender>
                </div>
              </Col>
              <Col sm={2}>
                <FormHeader className='mb-15 text-black'>
                  Identifiers
                </FormHeader>
                <div className='text-gray mb-15'>
                  <FormHeader>UPC</FormHeader>
                  <TextVariantForPDFRender
                    className={classNames(getTextColor(product?.upc), {
                      'break-words': product?.upc,
                    })}
                  >
                    {getFormValue(product?.upc)}
                  </TextVariantForPDFRender>
                </div>
                <div className='text-gray mb-15'>
                  <FormHeader>Selection #</FormHeader>
                  <TextVariantForPDFRender
                    className={classNames(
                      getTextColor(product?.selectionNumber),
                      {'break-words': product?.selectionNumber},
                    )}
                  >
                    {getFormValue(product?.selectionNumber)}
                  </TextVariantForPDFRender>
                </div>
              </Col>
              <Col sm={2}>
                <FormHeader className='mb-15 text-black'>Personnel</FormHeader>
                <div className='text-gray mb-15'>
                  <FormHeader>A&R Admin</FormHeader>
                  <TextVariantForPDFRender
                    className={getTextColor(product?.adminName)}
                  >
                    {getFormValue(product?.adminName)}
                  </TextVariantForPDFRender>
                </div>
                <div className='text-gray mb-15'>
                  <FormHeader>Producer</FormHeader>
                  <TextVariantForPDFRender
                    className={getTextColor(product?.producer)}
                  >
                    {getFormValue(product?.producer)}
                  </TextVariantForPDFRender>
                </div>
                <div className='text-gray mb-15'>
                  <FormHeader>Mixer</FormHeader>
                  <TextVariantForPDFRender
                    className={getTextColor(product?.mixer)}
                  >
                    {getFormValue(product?.mixer)}
                  </TextVariantForPDFRender>
                </div>
              </Col>
              <Col sm={2}>
                <div
                  className={classNames(styles.formColum, 'text-gray mb-15')}
                >
                  <FormHeader>Mastering</FormHeader>
                  <TextVariantForPDFRender
                    className={getTextColor(product?.mastering)}
                  >
                    {getFormValue(product?.mastering)}
                  </TextVariantForPDFRender>
                </div>
              </Col>
              <Col sm={3}>
                <FormHeader className='mb-15 text-black'>Comments</FormHeader>
                <TextVariantForPDFRender
                  className={classNames(
                    getTextColor(product?.arComments),
                    'heading-line-height d-inline-block',
                  )}
                >
                  {getFormValue(product?.arComments)}
                </TextVariantForPDFRender>
              </Col>
            </Row>
          </Col>
        </Row>
        <table id={IDS.table} className={styles.tableSection}>
          {product?.tracks?.map((track) => (
            <>
              <tr>
                <td colSpan={1}>{track.trackNumber}</td>
                <td colSpan={assetsLength - 1}>
                  <span>{track.title}</span>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  {track.complete ? 'Complete' : 'Incomplete'}
                </td>
              </tr>
              <tr>
                <td> </td>
                {track.assets.map((asset, index) =>
                  index < PDF_MAX_ASSETS ? (
                    <td>
                      {NOT_VALUE[asset.selection ?? ''] ?? asset.selection}
                    </td>
                  ) : null,
                )}
              </tr>
              {track.comments ? (
                <tr>
                  <td colSpan={1}> </td>
                  <td colSpan={1}>{commentLabel}</td>
                  <td colSpan={assetsLength - 2}>{track.comments}</td>
                </tr>
              ) : null}
            </>
          ))}
        </table>
      </Row>
    </>
  )
}

export default ProductPdf
