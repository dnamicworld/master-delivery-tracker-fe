/* eslint-disable no-param-reassign */
import {jsPDF} from 'jspdf'

import autoTable from 'jspdf-autotable'

import {AssetType, Product} from '../../models'
import {AssetOption} from '../../models/AssetOption'
import {PRODUCT_TYPE_DISPLAY} from '../../utils/constants'
import {
  getCellHAlign,
  getCellPadding,
  getCellWidth,
  getFontStyle,
  getImageData,
  getLineWidthObject,
  getNoOptionalAssetsLength,
} from '../../utils/helpers'
// TODO will delete later
const OTHER_NO_CUSTOM_ASSET_SIZE = 8
interface PDFCoordinates {
  x: number
  y: number
}

interface PDFMargins {
  top: number
  horizontal: number
}

class ProductPDFRender {
  doc: jsPDF

  pdfAxis: PDFCoordinates = {
    x: 0,
    y: 0,
  }

  headerSize = 20

  componentsMargins: PDFMargins = {
    top: 40,
    horizontal: 5,
  }

  columnHeaders: string[] | undefined

  product: Product

  assetTypes: AssetType[]

  assetOptions: AssetOption[]

  colors = {
    primary: '#0032A0',
    warning: '#e8be04',
    success: '#00813D',
    danger: '#D42B3A',
    info: '#383838',
    yellow: '#f5a623',
    gray: '#c1c4cc',
    lightGray: '#929292',
  }

  constructor(
    doc: jsPDF,
    headers: string[] | undefined,
    product: Product,
    assetTypes: AssetType[],
    assetOptions: AssetOption[],
  ) {
    this.doc = doc
    this.columnHeaders = headers ? ['', ...headers] : undefined
    this.product = product
    this.assetTypes = assetTypes
    this.assetOptions = assetOptions
  }

  setPageNumber = () => {
    const {pageSize} = this.doc.internal
    const pageCount = this.doc.getNumberOfPages()

    // For each page, print the page number and the total pages
    for (let i = 1; i <= pageCount; i += 1) {
      this.doc.setPage(i)
      this.doc.setFontSize(10)
      const pageText = `Page ${i} of ${pageCount}`
      const pageTextSize =
        this.doc.getTextDimensions(pageText, {fontSize: 10}).h / 2
      this.doc.text(
        pageText,
        pageSize.getWidth() - 10,
        this.headerSize / 2 - pageTextSize, // header is 80px height 40- text height
        {
          align: 'right',
        },
        null,
      )
    }
  }

  setPDFHeader = (canvas: HTMLCanvasElement) => {
    const [data, width, height] = getImageData(this.doc, canvas)
    this.headerSize = height
    this.doc.addImage(data, 'JPG', 0, 0, width, height)
    this.pdfAxis.y += height + 5 // padding-bottom
  }

  setPDFForm = (canvas: HTMLCanvasElement) => {
    const [data, width, height] = getImageData(this.doc, canvas)
    this.doc.addImage(
      data,
      'JPG',
      this.componentsMargins.horizontal,
      this.pdfAxis.y,
      width,
      height,
    )
    this.pdfAxis.y += height + 10 // 10 padding between form and table legend
  }

  setTableLegend = (firstTime?: boolean) => {
    const tableHeaderText = `${
      PRODUCT_TYPE_DISPLAY[this.product.productType]
    } Asset Table`
    this.doc.setFontSize(16)
    this.doc.setTextColor(40)
    this.doc.text(
      tableHeaderText,
      this.componentsMargins.horizontal,
      this.pdfAxis.y,
    )
    if (firstTime) {
      this.pdfAxis.y += this.doc.getTextDimensions(tableHeaderText, {
        fontSize: 16,
      }).h
    }
  }

  setTableContent = (
    id: string,
    rowsLength: number,
    canvas: HTMLCanvasElement,
  ) => {
    const that = this
    const assetsLength = (this.columnHeaders?.length || 1) - 1

    const defaultsSize =
      getNoOptionalAssetsLength(this.assetTypes) + OTHER_NO_CUSTOM_ASSET_SIZE
    autoTable(this.doc, {
      theme: 'plain',
      headStyles: {
        lineColor: '#ddd',
        fillColor: '#fff',
        fontSize: 11,
      },
      bodyStyles: {
        lineColor: '#ddd',
        fillColor: '#fff',
        fontSize: 12,
      },
      margin: this.componentsMargins,
      html: `#${id}`,
      columns: this.columnHeaders,
      startY: this.pdfAxis.y,
      showHead: 'everyPage',
      tableWidth: 'auto',
      didParseCell: (data) => {
        const {
          section,
          cell: {colSpan, text},
          column: {index: indexCol},
          row: {raw, index: indexRow},
        } = data
        const firstCellHasValue = !!raw[0].content
        const cellValue = text[0]
        data.cell.styles.cellWidth = getCellWidth(
          indexCol,
          assetsLength,
          defaultsSize,
        )
        data.cell.styles.cellPadding = getCellPadding(firstCellHasValue)
        data.cell.styles.lineWidth = getLineWidthObject(
          firstCellHasValue,
          indexRow === rowsLength - 1,
        )
        data.cell.styles.halign = getCellHAlign(indexCol, assetsLength, colSpan)
        data.cell.styles.fontStyle = getFontStyle(section, indexCol, cellValue)
        const assetType = this.assetOptions.find(
          (ass) => ass.display === cellValue,
        )
        if (assetType) {
          data.cell.styles.textColor = this.colors[assetType.color]
        }
      },
      didDrawPage(data) {
        if (data.pageNumber !== 1) {
          that.pdfAxis.y = 0
          that.setPDFHeader(canvas)
          that.setTableLegend()
        }
      },
    })
  }
}

export default ProductPDFRender
