import {render} from '@testing-library/react'

import {SWRConfig} from '../components'
import {UserContextProvider} from '../context/user'

import {Login} from '.'

describe('Login Page', () => {
  test('renders learn react link', () => {
    const component = render(
      <SWRConfig>
        <UserContextProvider>
          <Login />
        </UserContextProvider>
      </SWRConfig>,
    )
    expect(component).toMatchSnapshot()
  })
})
