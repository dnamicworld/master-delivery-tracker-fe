import {AssetTypesFilter, ReleaseTypeFilter, YearFilter} from 'src/components'
import {FilterWrapperProps} from 'src/context/Filters/pageFilters'

const LabelFilters = ({
  filters,
  handleFilterChange,
  resetFilters,
  handleCheckOnEnter,
}: FilterWrapperProps) => (
  <div className='filters'>
    <YearFilter
      value={filters?.year ?? []}
      name='year'
      multiple
      onChange={handleFilterChange}
      resetFilters={resetFilters}
      handleCheckOnEnter={handleCheckOnEnter}
    />
    <ReleaseTypeFilter
      value={filters?.releaseType ?? []}
      name='releaseType'
      onChange={handleFilterChange}
      multiple
    />
    <AssetTypesFilter
      value={filters?.assetType ?? []}
      name='assetType'
      onChange={handleFilterChange}
      handleCheckOnEnter={handleCheckOnEnter}
      multiple
      resetFilters={resetFilters}
    />
  </div>
)

export default LabelFilters
