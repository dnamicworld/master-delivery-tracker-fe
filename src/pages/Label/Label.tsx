import {useEffect, useState} from 'react'
import {useHistory, useParams} from 'react-router-dom'

import {PaginationOptions} from '../../DS/components'
import {usePageFilterContext} from '../../context/Filters/pageFilters'
import {useAlertDisplay} from '../../context/alert'
import {useAppContext} from '../../context/coreProvider'
import {useCreateReport} from '../../context/createReport'
import useAssetTypes from '../../hooks/data/useAssetTypes'
import useLabels from '../../hooks/data/useLabels'
import useReports from '../../hooks/data/useReports'
import {AssetType, Report, ReportFilters} from '../../models'
import {BREADCRUMBS} from '../../utils/constants'
import {
  getNoReportsMessage,
  getReportCounts,
  mutateReports,
  normalizeFilters,
  omit,
  ReportCard,
  uniquely,
  getQueryParam,
  cloneDeep,
} from '../../utils/helpers'
import {
  createObjectMapper,
  groupReportsByFilter,
} from '../../utils/helpers/filters'
import {LOADING_TIME_OUT} from '../Home/Home'
import {HomePlaceHolder} from '../Home/HomePlaceHolder'
import {ProductIndexRouteParams} from '../shared/IndexPages'
import {getReleaseTypesFromFilterArray} from '../shared/IndexPages/IndexPages.controller'
import {ReportCards, reportsPage} from '../shared/ReportCards'
import ResultCount from '../shared/ResultsCount'

import LabelFilters from './LabelFilters/LabelFilters'

const filterInitialValue: ReportFilters = {
  year: [],
  assetType: [],
  releaseType: [],
}

const Labels = () => {
  const {setTitle, setBreadcrumbs} = useAppContext()
  const {display: alertDisplay} = useAlertDisplay()
  const {
    filters,
    filtersChanged,
    handleFilterChange,
    handleCheckOnEnter,
    setFiltersState,
    setFiltersBarContent,
    setWithTags,
    setFilterMappers,
    handleResetAllFilters,
    resetFilters,
  } = usePageFilterContext()
  const history = useHistory()
  const {labelId} = useParams<ProductIndexRouteParams>()
  const [stateReports, setReports] = useState<ReportCard[]>()
  const {setSuccessAction} = useCreateReport()
  const [showPlaceholder, setShowPlaceholder] = useState(true)
  const [paginationOptions, setPaginationOptions] = useState<PaginationOptions>(
    {
      page: 1,
      pages: 0,
      count: 0,
      size: reportsPage * 2,
    },
  )

  const {
    location: {search},
  } = history
  const newReport = getQueryParam(search, 'newReport')

  useEffect(() => {
    if (newReport) {
      alertDisplay('Report Successfully Created!')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newReport])

  const {labels} = useLabels(
    {
      id: Number(labelId),
    },
    () => labelId !== undefined,
  )
  const {assetTypes} = useAssetTypes({
    filterDefaultAssets: true,
  })
  const queryParams = {
    ...omit(paginationOptions, ['pages', 'count']),
    years: filters.year,
    labelIds: [labelId],
    releaseTypes: getReleaseTypesFromFilterArray(filters.releaseType ?? []),
  }
  const {reports, count, pages, pageNumber, isLoading} = useReports(
    queryParams,
    () => labelId !== undefined,
  )

  useEffect(() => {
    setSuccessAction((report?: Report) => {
      mutateReports(queryParams, report)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(queryParams)])

  const handleReports = () => {
    if (Array.isArray(reports)) {
      const reportsGrouped = groupReportsByFilter(
        reports,
        filters as ReportFilters,
      )
      setReports(reportsGrouped)
    }
  }

  useEffect(() => {
    setPaginationOptions((prevState) => ({
      ...prevState,
      page: 1,
    }))
    handleReports()
    setFiltersBarContent(
      <LabelFilters
        filters={filters}
        handleFilterChange={handleFilterChange}
        resetFilters={resetFilters}
        handleCheckOnEnter={handleCheckOnEnter}
      />,
    )
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(filters)])

  useEffect(() => {
    let timer: NodeJS.Timeout
    if (isLoading) {
      setShowPlaceholder(true)
    } else {
      timer = setTimeout(() => setShowPlaceholder(false), LOADING_TIME_OUT)
    }
    return () => clearTimeout(timer)
  }, [isLoading])

  useEffect(() => {
    if (count && pages && pageNumber) {
      setPaginationOptions((prevState) => ({
        ...prevState,
        count: count / 2,
        page: pageNumber,
        pages,
      }))
    }
  }, [count, pages, pageNumber])

  useEffect(() => {
    handleReports()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(reports)])

  useEffect(() => {
    if (!Array.isArray(labels)) {
      setTitle(<>{labels?.name} Label Reports</>)
    }
  }, [setTitle, labels])

  useEffect(() => {
    window.scrollTo(0, 0)
    setBreadcrumbs([
      {
        ...BREADCRUMBS.HOME,
      },
      {
        ...BREADCRUMBS.LABELS,
        active: true,
      },
    ])
    setFiltersState<ReportFilters>(
      normalizeFilters(search, filterInitialValue),
      cloneDeep(filterInitialValue),
    )
    setWithTags(true)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (assetTypes) {
      const assetsMapper = createObjectMapper<AssetType>('longName', assetTypes)
      setFilterMappers({assetType: assetsMapper})
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(assetTypes)])

  const cards = [...Array(reportsPage).keys()]
  const [display, reportCount] = getReportCounts(
    paginationOptions,
    stateReports?.length ?? 0,
  )

  return showPlaceholder ? (
    <>
      {cards.map(() => (
        <HomePlaceHolder key={uniquely()} />
      ))}
    </>
  ) : (
    <>
      <ResultCount
        display={display}
        count={reportCount}
        label='reports'
        displayReset={filtersChanged}
        handleReset={handleResetAllFilters}
      />
      <ReportCards
        reports={stateReports}
        noReportsMessage={getNoReportsMessage(
          {...filters, label: [labelId]},
          labels,
        )}
        paginationOptions={paginationOptions}
        setPaginationOptions={setPaginationOptions}
        queryParams={queryParams}
      />
    </>
  )
}

export default Labels
