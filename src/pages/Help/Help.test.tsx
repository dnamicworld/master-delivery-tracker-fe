import {act, render} from '@testing-library/react'

import 'jest-canvas-mock'

import ReactRouterDom, {BrowserRouter} from 'react-router-dom'

import {Help} from '..'
import {withMainFilters} from '../../components/AppRouter'

describe('Help Page', () => {
  test('renders Help page', async () => {
    const promise = Promise.resolve()
    const HelpPage = withMainFilters(Help)
    const renderHomePage = render(
      <BrowserRouter>
        <HelpPage />
      </BrowserRouter>,
    )
    jest
      .spyOn(ReactRouterDom, 'useParams')
      .mockReturnValue({labelId: undefined})
    await act(() => promise)
    expect(renderHomePage).toMatchSnapshot()
  })
})
