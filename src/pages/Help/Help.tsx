import {useEffect} from 'react'

import {Card, Col, Row} from '../../DS/components'
import {useAppContext} from '../../context/coreProvider'
import {BREADCRUMBS, HelpPageContent, NAVBAR_PDF} from '../../utils/constants'

const Help = () => {
  const {setBreadcrumbs} = useAppContext()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  useEffect(() => {
    setBreadcrumbs([
      BREADCRUMBS.HOME,
      {
        ...BREADCRUMBS.HELP,
        active: true,
      },
    ])
  }, [setBreadcrumbs])

  const questions = [
    {
      question: 'How do I submit files for archiving?',
      content: (
        <p>
          To request a secure upload link please email&nbsp;
          <a
            target='_blank'
            href='mailto:wea.library.services@wmg.com'
            rel='noreferrer'
          >
            wea.library.services@wmg.com
          </a>
          . Links from producers and engineers can also be forwarded to this
          email address.
        </p>
      ),
    },
    {
      question: 'What are delivery specifications for audio assets?',
      content: (
        <p>
          Information regarding asset types, technical requirements, and
          delivery methods can be found{' '}
          <a download href={NAVBAR_PDF}>
            here
          </a>
          .
        </p>
      ),
    },
    {
      question: 'Who can I contact for information regarding asset deliveries?',
      content: (
        <p>
          Please email{' '}
          <a
            target='_blank'
            href='mailto:wea.library.services@wmg.com'
            rel='noreferrer'
          >
            wea.library.services@wmg.com
          </a>{' '}
          for questions and support.
        </p>
      ),
    },
    {
      question: 'What is Master Delivery Report?',
      content: (
        <p>
          MDR is a web-based platform designed to provide Labels with accurate,
          real-time visibility to the delivery status of audio masters for new
          releases. The goal is to provide a user-friendly platform for Labels
          to know what has or has not been delivered for each project down to
          individual song level. Statistical analysis is integrated to help
          quickly identify what has been delivered, what is outstanding, and to
          facilitate a collaborative workflow between A&R and Archive teams to
          track which asset types are required for each song and when all
          expected assets are in.
        </p>
      ),
    },
    {
      question: 'Who has access to Master Delivery Report?',
      content: (
        <p>
          Master Delivery Report is available to WMG employees who are involved
          in the master delivery process or who require visibility to delivery
          status of masters sent to the Library.
        </p>
      ),
    },
    {
      question: 'How do I request access for a colleague?',
      content: (
        <p>
          Please visit the Helpdesk website to request access for a
          colleague&nbsp;
          <a
            target='_blank'
            href='https://warnermusic.service-now.com/servicedesk'
            rel='noreferrer'
          >
            https://warnermusic.service-now.com/servicedesk
          </a>{' '}
          &gt; Request Something &gt; Application Access
        </p>
      ),
    },
    {
      question: 'How are permissions assigned?',
      content: (
        <p>
          The system utilizes multiple Read Only and Read/Write permissions
          levels with dedicated Label access for each user to ensure
          confidentiality and accurate reporting.
        </p>
      ),
    },
    {
      question: 'Can I access the system remotely?',
      content: (
        <p>
          Yes! MDR supports Okta integration using WMG network credentials. If
          working from outside of a WMG office, you must be connected to VPN to
          access the website.
        </p>
      ),
    },
    {
      question: 'What are the different pages available?',
      content: (
        <>
          <p>
            Label Report / Home – this is the home page displaying statistics
            for assets delivered for each Label by year.
          </p>
          <p>
            Album Index – a consolidated view of project level info for Albums
            and EP’s by year of release.
          </p>
          <p>
            Singles Index – a consolidated view of project level info for
            Singles by year of release.
          </p>
          <p>
            Project Details – a detailed view of project information including
            track titles, asset delivery status, release info, personnel, and
            comments.
          </p>
        </>
      ),
    },
    {
      question: 'How are calculations measured?',
      content: (
        <p>
          Once an asset is delivered to the Library, it is archived, entered
          into the Master Library System, and logged to the corresponding
          project in MDR. The audio file resolution (or Immersive file type) is
          selected indicating it has been archived. By default, core
          deliverables (MULTI-TRACKS, STEMS, MAIN MIX [un-mastered],
          INSTRUMENTAL, TV MIX and A CAPPELLA) display as N/D (Not Delivered)
          until marked as delivered or changed to N/R to indicate not required.
          Mastered Instrumentals (INSTRUMENTAL – EQ), as well as Dolby Atmos and
          Sony 360RA default to N/R unless specified as required by A&R.
        </p>
      ),
    },
    {
      question: 'Asset delivery status selections',
      content: (
        <p>
          Sample Rate / Bit depth = resolution of files delivered (e.g.,
          96kHz/24bit)
          <br />
          ADM or MP4 = Immersive mix delivered
          <br />
          N/D = Not Delivered but expected (affects delivery percentages)
          <br />
          N/R = Not Required (does NOT affect delivery percentages)
        </p>
      ),
    },
    {
      question:
        'As an A&R Administrator, what should I do if an asset is not required for a song but shows as N/D?',
      content: (
        <p>
          A&R Amin users have write access to select fields including selecting
          N/R for asset types not required. Alternatively, a track level comment
          can be entered and the Library will update the status.
        </p>
      ),
    },
    {
      question: 'When should a song or project be marked COMPLETE?',
      content: (
        <p>
          When all expected assets for a song are delivered, the COMPLETE check
          box on Project Details can be selected indicating all expected assets
          are in. The same can be applied at Project level on either the Project
          Details pages or by clicking on the Check Mark next to the delivery
          percentage on the Album or Single Index. COMPLETE may not always mean
          100% of assets are delivered but that all assets expected have been
          sent to the Library.
        </p>
      ),
    },
    {
      question: 'Is the system supported on mobile devices?',
      content: (
        <p>
          Currently the website is optimized for Mac and Windows computers,
          however limited access is available on a mobile device when connected
          to the WMG network. Full mobile support will be added in future
          development.
        </p>
      ),
    },
  ]
  return (
    <Row>
      <Col smOffset={2} sm={8} className='text-center'>
        <h2 className='pb-20'>{HelpPageContent.title}</h2>
      </Col>
      <Col sm={12}>
        <Row className='d-flex flex-wrap'>
          {questions.map((questionItem) => (
            <Col sm={6} className='d-flex pb-40' key={questionItem.question}>
              <Card radius backgroundColor='white'>
                <Row>
                  <Col sm={12}>
                    <h3 className='m-0 pb-30'>{questionItem.question}</h3>
                  </Col>
                  <Col sm={12}>{questionItem.content}</Col>
                </Row>
              </Card>
            </Col>
          ))}
        </Row>
      </Col>
      <Col smOffset={4} sm={4} className='text-center'>
        <h2 className='m-0 pb-40'>{HelpPageContent.contactTitle}</h2>
      </Col>
      <Col sm={12} className='pb-30'>
        <Card radius>
          <Row className='d-flex  flex-column'>
            <Col sm={12} className='text-center'>
              <h3 className='m-0 pb-30 pt-10'>
                {HelpPageContent.footerCard.title}
              </h3>
            </Col>
            <Col smOffset={2} sm={8} className='text-center'>
              <p>
                For access, questions and feedback:{' '}
                <a href='mailto:mike.wilson@wmg.com'>mike.wilson@wmg.com</a>
              </p>
              <p>
                For all other issues contact{' '}
                <a
                  target='_blank'
                  href='https://warnermusic.service-now.com/servicedesk'
                  rel='noreferrer'
                >
                  HelpDesk
                </a>
              </p>
              <p>U.S. 844-HELPWMG / +1 844-4357964</p>
            </Col>
          </Row>
        </Card>
      </Col>
    </Row>
  )
}

export default Help
