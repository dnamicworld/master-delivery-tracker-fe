import {act, render} from '@testing-library/react'

import 'jest-canvas-mock'

import ReactRouterDom, {BrowserRouter} from 'react-router-dom'

import {WithMainLayout} from '../../components/Layout'
import {ProductDetailsContextProvider} from '../../hooks/productDetails'
import {TableContextProvider} from '../../hooks/table'

import {AlbumDetails} from '.'

describe('Album Detail Page', () => {
  test('renders Album details page', async () => {
    const promise = Promise.resolve()
    const AlbumDetailsPage = WithMainLayout(() => (
      <TableContextProvider>
        <ProductDetailsContextProvider>
          <AlbumDetails />
        </ProductDetailsContextProvider>
      </TableContextProvider>
    ))
    jest
      .spyOn(ReactRouterDom, 'useParams')
      .mockReturnValue({productId: undefined})
    await act(() => promise)
    const renderAlbumDetails = render(
      <BrowserRouter>
        <AlbumDetailsPage />
      </BrowserRouter>,
    )
    expect(renderAlbumDetails).toMatchSnapshot()
  })
})
