import {useMemo, useState, useEffect} from 'react'
import {ListViewType} from 'src/models'

import {
  Button,
  Col,
  Row,
  Select,
  SelectOption,
  SelectProps,
} from '../../DS/components'

import useLabels from '../../hooks/data/useLabels'

import {
  releaseDateRangeFutureYears,
  DEFAULT_MIN_YEAR,
  ALBUM_TYPE,
  SINGLE_TYPE,
} from '../../utils/constants'
import {
  createRange,
  createReport,
  generateReportData,
  getCurrentYear,
  getLabelOptions,
} from '../../utils/helpers'

const typesOptions = [
  {
    name: ALBUM_TYPE,
    value: ALBUM_TYPE,
  },
  {
    name: SINGLE_TYPE,
    value: SINGLE_TYPE,
  },
]

const Report = () => {
  const [options, setOptions] = useState({
    label: '',
    year: '',
    type: '',
  })
  const {labels} = useLabels()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  const currentYear = getCurrentYear()
  const maxYear = currentYear + releaseDateRangeFutureYears
  const yearRange = createRange(DEFAULT_MIN_YEAR, maxYear)

  const labelsArray = useMemo(() => {
    if (labels) {
      return Array.isArray(labels) ? labels : [labels]
    }
    return []
  }, [labels])

  const labelsOptions = getLabelOptions(labelsArray)
  const yearsOptions = yearRange.reduce((prev: SelectOption[], year) => {
    prev.push({
      name: year,
      value: year,
    })
    return prev
  }, [])

  const onSelectChange: SelectProps['onChange'] = (_, value, selectAction) => {
    if (selectAction.name) {
      setOptions((oldOptions) => ({
        ...oldOptions,
        [selectAction.name as string]: value,
      }))
    }
  }

  const handleCreateReport = async () => {
    const currentLabel = labelsArray.find(
      (label) => label && String(label?.id) === options.label,
    )

    if (currentLabel) {
      const reportData = generateReportData(
        currentLabel,
        options.type as keyof typeof ListViewType,
        options.year,
      )

      try {
        await createReport(reportData)
      } catch (err) {
        // eslint-disable-next-line no-console
        console.log(err)
      }
    }
  }

  return (
    <Row className='pt-15'>
      <Col sm={3}>
        <Select
          label='Label'
          value={options.label}
          name='label'
          options={labelsOptions}
          onChange={onSelectChange}
        />
      </Col>
      <Col sm={3}>
        <Select
          label='Year'
          value={options.year}
          name='year'
          options={yearsOptions}
          onChange={onSelectChange}
        />
      </Col>
      <Col sm={3}>
        <Select
          label='Product Type'
          value={options.type}
          name='type'
          options={typesOptions}
          onChange={onSelectChange}
        />
      </Col>
      <Col sm={3}>
        <Button onClick={handleCreateReport}>Create</Button>
      </Col>
    </Row>
  )
}

export default Report
