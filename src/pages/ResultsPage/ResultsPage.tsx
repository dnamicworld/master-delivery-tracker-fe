import {ColDef, GridApi, ICellRendererParams} from 'ag-grid-community'
import {useCallback, useEffect, useMemo, useState} from 'react'
import {useHistory} from 'react-router-dom'

import {HeaderTitle, NoResults, Pagination} from '../../DS/components'
import {AgGridTable, DashboardCard, TableActions} from '../../components'
import {
  CustomCellParams,
  TitleCellRenderer,
} from '../../components/AgGrid/CellRenderers'
import {SEARCH_SOURCE} from '../../components/Search/SearchComponent.controller'
import {useAlertDisplay} from '../../context/alert'
import {useAppContext} from '../../context/coreProvider'
import {useModalDisplay} from '../../context/modal'
import useSearchProducts from '../../hooks/data/useSearchProducts'
import useUser from '../../hooks/user'
import {Actions, Product} from '../../models'
import {
  BREADCRUMBS,
  PRODUCT_TYPE_ROUTES,
  productPage,
} from '../../utils/constants'
import {COMPLETE_MODAL} from '../../utils/constants/messages'
import {
  cloneDeep,
  deleteDuplicates,
  getQueryParam,
  isAlbum,
  isEmpty,
  parseQuery,
} from '../../utils/helpers'
import {updateProductAction} from '../shared/IndexPages/IndexPages.controller'
import {reduceAssets} from '../shared/IndexPages/components/IndexDashboardCard'
import {
  performProductCompleteUpdate,
  updateRecords,
} from '../shared/IndexPages/components/IndexPageTable/IndexPageTable'
import {IndexTableColumns} from '../shared/IndexPages/components/IndexPageTable/IndexPageTableController'
import {
  BarChartContent,
  ReportDonutChartContent,
} from '../shared/ReportCards/components'
import ResultCount from '../shared/ResultsCount'

export interface ResultState {
  result: string[]
}
const getTableColumns = (
  canEdit: boolean,
  handleAlbumComplete: (editableParams: CustomCellParams) => void,
): ColDef[] => [
  IndexTableColumns.admin(canEdit),
  IndexTableColumns.comments(canEdit),
  IndexTableColumns.titleArtist(
    (data) => PRODUCT_TYPE_ROUTES[data.productType],
  ),
  IndexTableColumns.totalPercent(canEdit, handleAlbumComplete),
  IndexTableColumns.complete,
  IndexTableColumns.productType,
  IndexTableColumns.releaseDate,
]

const ResultsPage = () => {
  const {setTitle, setBreadcrumbs} = useAppContext()
  const history = useHistory<ResultState>()
  const {displayModal} = useModalDisplay()
  const {display} = useAlertDisplay()
  const [gridApi, setGridApi] = useState<GridApi | null>(null)
  const {user} = useUser()
  const [resultProducts, setResultProducts] = useState<Product[]>([])

  const {
    location: {search, state},
  } = history

  const dataSource = getQueryParam(search, 'dataSource')
  const phrase = getQueryParam(search, 'phrase')
  const requestData = dataSource !== SEARCH_SOURCE

  const {products} = useSearchProducts(
    parseQuery(search),
    () => !isEmpty(search) && requestData,
  )

  const canEdit = user.roles.hasFullOrPartialWriteAccess()

  const productsGrouped = useMemo(
    () =>
      resultProducts.reduce(
        (prev: {albums: Product[]; singles: Product[]}, product) => {
          const productCopy = cloneDeep(product)
          const prevCopy = cloneDeep(prev)
          const key = isAlbum(productCopy.productType) ? 'albums' : 'singles'
          prevCopy[key].push(productCopy)
          return prevCopy
        },
        {albums: [], singles: []},
      ),
    [resultProducts],
  )

  useEffect(() => {
    window.scrollTo(0, 0)
    setTitle(<>Singles and Albums &#38; EP&apos;s</>)
    setBreadcrumbs([
      {
        ...BREADCRUMBS.HOME,
      },
      {
        ...BREADCRUMBS.RESULTS,
        active: true,
      },
    ])
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (products && Array.isArray(products)) {
      setResultProducts(products)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(products)])

  useEffect(() => {
    if (!requestData) {
      const {result} = state
      const historyStateProduct: Product[] = result.map((item: string) =>
        JSON.parse(item),
      )
      const withoutDuplicates = deleteDuplicates(historyStateProduct)
      setResultProducts(withoutDuplicates)
    }
  }, [requestData, state])

  const updateProduct = useCallback(
    (productToUpdate: Product, action: Actions) => {
      const newProducts: Product[] = updateProductAction(
        resultProducts,
        productToUpdate,
        action,
      )
      setResultProducts(newProducts)
    },
    [resultProducts],
  )

  const updateTableRecords = useMemo(
    () => updateRecords(updateProduct),
    [updateProduct],
  )

  const handleProductComplete = useMemo(
    () => (cellRendererParams: ICellRendererParams) => {
      const {
        data: {complete},
      } = cellRendererParams
      const productCompleteUpdate = performProductCompleteUpdate(
        cellRendererParams,
        updateProduct,
        display,
      )
      if (complete) {
        productCompleteUpdate()
      } else {
        displayModal(
          COMPLETE_MODAL.body,
          COMPLETE_MODAL.title(cellRendererParams.data.title),
          {
            actionName: COMPLETE_MODAL.action,
            onActionClick: productCompleteUpdate,
            cancelButtonStyle: {
              containerStyle: 'outline',
              colorType: 'secondary-black',
            },
          },
        )
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [displayModal, updateProduct],
  )

  const columnDefs = useMemo(
    () => getTableColumns(canEdit, handleProductComplete),
    [canEdit, handleProductComplete],
  )

  const albumAssets = reduceAssets(productsGrouped.albums)
  const singleAssets = reduceAssets(productsGrouped.singles)
  const totalItems = resultProducts.length
  const currentItems = Math.min(totalItems, productPage)

  const onClear = () => {
    history.go(-1)
  }
  return (
    <>
      <ResultCount
        display={currentItems}
        count={totalItems}
        linkText='Clear'
        displayReset
        handleReset={onClear}
      >
        <strong>
          &ldquo;
          {phrase}&rdquo;
        </strong>
      </ResultCount>
      <DashboardCard
        leftContent={
          <ReportDonutChartContent
            assetsAlbums={albumAssets}
            assetsSingles={singleAssets}
          />
        }
        rightContent={
          <BarChartContent
            assetsAlbums={albumAssets}
            assetsSingles={singleAssets}
          />
        }
      />
      <TableActions title={<HeaderTitle>Projects</HeaderTitle>} />
      {!isEmpty(resultProducts) ? (
        <Pagination
          items={resultProducts}
          onPreviousPage={gridApi?.paginationGoToPreviousPage.bind(gridApi)}
          onNextPage={gridApi?.paginationGoToNextPage.bind(gridApi)}
          onGoToPage={gridApi?.paginationGoToPage.bind(gridApi)}
        >
          {() => (
            <AgGridTable
              pagination
              columnDefs={columnDefs}
              frameworkComponents={{
                titleRenderer: TitleCellRenderer,
              }}
              fullWidth
              onCellValueChanged={updateTableRecords}
              rowData={resultProducts}
              stopEditingWhenCellsLoseFocus
              onTableReady={(params) => {
                setGridApi(params.api)
              }}
            />
          )}
        </Pagination>
      ) : (
        <NoResults />
      )}
    </>
  )
}

export default ResultsPage
