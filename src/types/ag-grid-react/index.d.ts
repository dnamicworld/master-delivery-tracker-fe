import 'ag-grid-react'

declare module 'ag-grid-react' {
  interface AgGridReactProps<T> {
    rowData: T[]
  }
  interface GridOptions {
    getRowHeight?: (node: RowNode) => void
    getRowStyle?: (node: RowNode) => void
  }
}
