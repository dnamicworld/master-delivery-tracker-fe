import classNames from 'classnames'
import {ReactNode, useEffect} from 'react'

import {Icon} from '../..'
import {FA_ICONS, ICON_TYPES} from '../../../../utils/constants'

export interface AccordionToggleProps {
  children?: ReactNode
  className?: string
  expanded: boolean
  accordionId: string
  onChange?: (value: boolean) => void
}

const AccordionToggle = ({
  accordionId,
  children,
  className,
  expanded,
  onChange,
}: AccordionToggleProps) => {
  useEffect(() => {
    $(`#${accordionId}`).collapse(expanded ? 'show' : 'hide')
  }, [expanded, accordionId])

  const iconName = expanded ? FA_ICONS.chevronUp : FA_ICONS.chevronDown

  return (
    <div
      role='button'
      tabIndex={0}
      onClick={() => onChange?.(!expanded)}
      data-toggle='collapse'
      data-target={`#${accordionId}`}
      aria-expanded={expanded}
      aria-controls={accordionId}
      className={classNames(
        'd-flex align-items-center justify-content-between',
        className,
      )}
    >
      {children}

      <Icon className='ml-20' type={ICON_TYPES.far} iconName={iconName} />
    </div>
  )
}

export default AccordionToggle
