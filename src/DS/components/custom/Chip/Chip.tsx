import classNames from 'classnames'

import {Icon, IconProps} from '../..'
import {AppColors, FA_ICONS, ICON_TYPES} from '../../../../utils/constants'

import styles from './Chip.module.scss'

export interface ChipProps {
  content: string
  iconName?: string
  className?: string
  iconClick?: IconProps['onClick']
  variant?: keyof typeof AppColors
}

const defaultProps: Partial<ChipProps> = {
  iconName: FA_ICONS.close,
}

const Chip = ({
  content,
  iconName,
  className,
  iconClick,
  variant = 'info',
}: ChipProps) => (
  <div
    className={classNames(
      styles.chipContainer,
      styles[AppColors[variant]],
      className,
      'py-5 px-15',
    )}
  >
    <span>{content}</span>
    {iconName ? (
      <Icon
        className='pl-10'
        type={ICON_TYPES.fas}
        iconName={iconName}
        onClick={iconClick}
      />
    ) : null}
  </div>
)

Chip.defaultProps = defaultProps

export default Chip
