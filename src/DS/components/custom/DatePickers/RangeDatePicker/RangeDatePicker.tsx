import classNames from 'classnames'

import {DatePicker, DatePickerProps} from '../DatePicker'

interface DateItem {
  name: string
  value?: Date
}

export interface RangeDatePickerProps extends DatePickerProps {
  fromDate: DateItem
  toDate: DateItem
}

const RangeDatePicker = ({
  placeholder = '##/##/#### - ##/##/####',
  disabled = false,
  showYearMonths,
  className,
  fromDate,
  toDate,
  onChange,
  small,
  minYear,
  maxYear,
  name,
  minDate,
  maxDate,
  fromFilter,
}: RangeDatePickerProps) => (
  <DatePicker
    className={classNames(className)}
    small={small}
    startDate={fromDate.value}
    value={fromDate.value}
    endDate={toDate.value}
    onChange={onChange}
    disabled={disabled}
    placeholder={placeholder}
    showYearMonths={showYearMonths}
    minYear={minYear}
    maxYear={maxYear}
    minDate={minDate}
    maxDate={maxDate}
    selectsRange
    name={name}
    fromFilter={fromFilter}
  />
)

export default RangeDatePicker
