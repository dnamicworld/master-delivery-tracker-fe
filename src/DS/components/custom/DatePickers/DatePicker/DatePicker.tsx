import {Datepicker as LegatoDatepicker, IDatepickerProps} from '@wmg-ae/legato'
import classNames from 'classnames'
import {Children, cloneElement, isValidElement, ReactNode} from 'react'
import {ReactDatePickerProps} from 'react-datepicker'
import {Portal} from 'react-overlays'

import styles from './DatePicker.module.scss'

export interface DatePickerProps
  extends Omit<IDatepickerProps, 'value' | 'onChange' | 'name'> {
  name: string
  minYear?: number
  maxYear?: number
  minDate?: Date
  maxDate?: Date
  onChange: (name: string, date: Date) => void
  value?: Date
  placeholder?: string
  showYearMonths?: boolean
  small?: boolean
  fromFilter?: boolean
}

const startYear = (year: number): Date => new Date(year, 0, 1)

const endYear = (year: number): Date => new Date(year, 11, 31)

const CalendarContainer: ReactDatePickerProps['popperContainer'] = ({
  children,
}) => {
  const el = document.getElementById('root')
  const content = Children.map<ReactNode, ReactNode>(children, (child, index) =>
    isValidElement(child) ? cloneElement(child, {key: index}) : child,
  )
  return (
    <Portal container={el}>
      <>{content}</>
    </Portal>
  )
}

const DatePicker = ({
  placeholder = '##/##/####',
  disabled = false,
  showYearMonths,
  className,
  name,
  minYear,
  maxYear,
  minDate,
  maxDate,
  value,
  onChange,
  onSelect,
  popperClassName,
  small,
  selectsRange,
  startDate,
  endDate,
  customInputRef,
  fromFilter,
}: DatePickerProps) => {
  const minDateCalendar = minYear ? startYear(minYear) : minDate
  const maxDateCalendar = maxYear ? endYear(maxYear) : maxDate

  return (
    <LegatoDatepicker
      popperClassName={popperClassName}
      popperContainer={CalendarContainer}
      name={name}
      size={small ? 'sm' : undefined}
      className={classNames(className, {[styles.filter]: fromFilter})}
      selected={value}
      onChange={(date) => onChange(name, date as Date)}
      onSelect={onSelect}
      placeholderText={placeholder}
      customInputRef={customInputRef}
      disabled={disabled}
      autoComplete='off'
      showYearDropdown={showYearMonths}
      showMonthDropdown={showYearMonths}
      minDate={minDateCalendar}
      maxDate={maxDateCalendar}
      selectsRange={selectsRange}
      startDate={startDate}
      endDate={endDate}
    />
  )
}

export default DatePicker
