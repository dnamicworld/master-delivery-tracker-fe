import {ReactNode} from 'react'

import {AppColors} from '../../../../utils/constants'

export interface TextVariantProps {
  children: ReactNode
  variant?: keyof typeof AppColors
  className?: string
}

type HeaderSizes = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'

export interface HeaderTitleProps {
  className?: string
  variant?: HeaderSizes
  children: ReactNode
}
