import {Story} from '@storybook/react'

import {TextVariant, TextVariantProps} from '.'

export default {
  title: 'TextVariant',
  component: TextVariant,
  args: {},
  argTypes: {
    variant: {
      options: [
        'primary',
        'success',
        'info',
        'warning',
        'danger',
        'yellow',
        'gray',
        'lightGray',
      ] as TextVariantProps['variant'][],
    },
  },
}

const Template: Story<TextVariantProps> = ({children, ...args}) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <TextVariant {...args}>{children}</TextVariant>
)

const children = <>An example of plain text</>

export const Default = Template.bind({})
Default.args = {children}
