import classNames from 'classnames'

import {HeaderTitleProps, TextVariantProps} from './Text.model'

export const TextVariant = ({
  children,
  className,
  variant,
}: TextVariantProps) => (
  <span
    className={classNames(
      'font-weight-sbold',
      {[`text-${variant}`]: variant},
      className,
    )}
  >
    {children}
  </span>
)

export const HeaderTitle = ({
  className,
  variant = 'h5',
  children,
}: HeaderTitleProps) => {
  const Tag = variant
  return (
    <Tag className={classNames('m-0 font-weight-sbold', className)}>
      {children}
    </Tag>
  )
}

export const TextVariantForPDFRender = ({
  children,
  className,
  variant,
}: TextVariantProps) => (
  <h5
    className={classNames(
      'font-weight-bold',
      {[`text-${variant}`]: variant},
      className,
    )}
  >
    {children}
  </h5>
)
