export {TextVariant, HeaderTitle, TextVariantForPDFRender} from './Text'
export type {TextVariantProps, HeaderTitleProps} from './Text.model'
