import {ChangeEvent, MouseEvent} from 'react'

import {SearchProps} from '../../bootstrap/Search'

import {RenderItemFunction} from './Select'
import {SelectOption} from './SelectOptions'

export type ActionTypes =
  | 'select-option'
  | 'deselect-option'
  | 'remove-value'
  | 'pop-value'
  | 'set-value'
  | 'clear'
  | 'create-option'

export interface SelectAction {
  action: ActionTypes
  name?: string
  removedValue?: SelectOption
  option?: SelectOption
}

interface SelectSearch extends SearchProps {
  searchKey?: string
}

export type SelectValue = string | string[] | null | undefined

export function isMultipleSelect(value: SelectValue): value is string[] {
  return Array.isArray(value)
}

export function isSingleSelect(value: SelectValue): value is string {
  return typeof value === 'string'
}

export interface SelectProps {
  disabled?: boolean
  allFeature?: boolean
  fullWidth?: boolean
  noPlaceholder?: boolean
  manualToggle?: boolean
  multiple?: boolean
  name?: string
  label?: string
  optionsClassNames?: string
  className?: string
  options?: SelectOption[]
  value?: SelectValue
  renderItem?: RenderItemFunction
  searchable?: SelectSearch
  chips?: Boolean
  onChange?: <T>(
    event: KeyboardEvent | ChangeEvent<T> | MouseEvent<T>,
    value: SelectValue,
    action: SelectAction,
  ) => void
  handleCheckOnEnter?: (name: string, filteredOptions: SelectOption[]) => void
  canCreate?: boolean
}
