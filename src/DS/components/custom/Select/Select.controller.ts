import {cloneDeep, get} from '../../../../utils/helpers'

import {SelectOption, SelectProps} from '.'

export const getSelectDisplay = (
  options: SelectProps['options'] = [],
  value: SelectProps['value'],
  {
    label,
    allFeature,
  }: {label?: SelectProps['label']; allFeature?: SelectProps['allFeature']},
  multiple?: boolean,
) => {
  let result = ''
  if (multiple) {
    const optionResults = options.reduce((prev: string[], option) => {
      const prevCopy = cloneDeep(prev)
      if (value?.includes(option.value)) {
        prevCopy.push(option.name ?? '')
      }
      return prevCopy
    }, [])
    result = optionResults?.join(', ')
    if (allFeature && optionResults?.length === options.length) {
      result = label ?? ''
    }
  } else {
    const currentOption = options.find((option) => option.value === value)
    result = currentOption?.name ?? ''
  }
  return result
}

const search = <T>(value: string, items: T[], searchKey?: string) => {
  let result: T[] = items

  if (value && searchKey) {
    result = items.filter((item) =>
      get(item, searchKey, '').toLowerCase().includes(value.toLowerCase()),
    )
  } else if (value) {
    result = items.filter((item) =>
      String(item).toLowerCase().includes(value.toLowerCase()),
    )
  }

  return result
}

export const getFilteredOptions = (
  options: SelectOption[] = [],
  newSearchText: string,
  searchKey?: string,
): SelectOption[] =>
  newSearchText
    ? search<SelectOption>(newSearchText, options, searchKey)
    : options
