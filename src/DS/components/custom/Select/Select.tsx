import classNames from 'classnames'
import {
  ChangeEvent,
  cloneElement,
  ReactNode,
  useEffect,
  useState,
  PropsWithChildren,
  ReactElement,
  useCallback,
  useRef,
  KeyboardEventHandler,
} from 'react'

import {Dropdown, FormControl, Icon, TextVariant} from '../..'
import {
  AppColors,
  FA_ICONS,
  GLYPHICONS,
  ICON_TYPES,
  KEYS,
} from '../../../../utils/constants'
import {isEmpty, isNull, isUndefined, uniquely} from '../../../../utils/helpers'

import {getFilteredOptions, getSelectDisplay} from './Select.controller'
import {SelectValue} from './Select.model'
import styles from './Select.module.scss'
import {SelectChips, SelectSearch} from './components'

import {SelectOption, SelectOptions, SelectProps} from '.'

export type RenderItemFunction = (
  item: SelectOption,
  {value, onChange}: Pick<SelectProps, 'value' | 'onChange'>,
) => ReactNode

const evaluateEmptySingleValue = (value?: string | number | boolean | null) => {
  if (typeof value === 'string') {
    return !isEmpty(value)
  }
  return !isUndefined(undefined) || !isNull(value)
}

// Type guard
export function isArrayValue(value: SelectValue | string[]): value is string[] {
  return Array.isArray(value)
}

interface CustomSelect extends PropsWithChildren<SelectProps> {
  resetFilters?: number
  clearIcon?: boolean
  plain?: boolean
}

const Select = ({
  allFeature,
  fullWidth,
  manualToggle,
  multiple,
  name,
  children,
  label,
  disabled,
  plain,
  className,
  options = [],
  value,
  renderItem,
  searchable,
  chips,
  optionsClassNames,
  noPlaceholder,
  onChange,
  handleCheckOnEnter,
  canCreate,
  clearIcon,
  resetFilters,
}: CustomSelect) => {
  const isManuallyToggled = Boolean(multiple || manualToggle)
  const [searchText, setSearchText] = useState<string>('')
  const [filteredOptions, setFilteredOptions] =
    useState<SelectOption[]>(options)
  const [selectName, setSelectName] = useState<string>('')
  const cleanSearch = useCallback(() => {
    setSearchText('')
    setFilteredOptions(options)
  }, [options])
  const selectInputSearchRef = useRef<HTMLInputElement>()

  useEffect(() => {
    cleanSearch()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [resetFilters])
  useEffect(() => {
    setFilteredOptions(options)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [setFilteredOptions, JSON.stringify(options)])

  const display = getSelectDisplay(
    options,
    value,
    {
      label,
      allFeature,
    },
    multiple,
  )

  useEffect(() => {
    if (display) {
      setSelectName(display)
    }
  }, [display])

  const searchKey = searchable?.searchKey ?? 'name'

  const handleSearchChange = (event: ChangeEvent<HTMLInputElement>) => {
    const {value: newSearchText} = event.target ?? event.currentTarget
    setSearchText(newSearchText)

    searchable?.onChange?.(event)

    if (options?.length)
      setFilteredOptions(getFilteredOptions(options, newSearchText, searchKey))
  }

  let valueNotEmpty = false

  if (isArrayValue(value)) {
    if (multiple) valueNotEmpty = Boolean(value?.length)
  } else {
    valueNotEmpty = evaluateEmptySingleValue(value)
  }

  const getLabel = () =>
    noPlaceholder ? label : <span className='placeholder'>{label}</span>

  const selectLabel = valueNotEmpty ? selectName : getLabel()
  const selectComponent = plain ? (
    <>{selectLabel}</>
  ) : (
    <h4 className='select__label'>{selectLabel}</h4>
  )

  const selectedContent =
    chips && valueNotEmpty ? (
      <SelectChips
        options={options}
        multiple={multiple}
        name={name}
        onChange={onChange}
        value={value}
      />
    ) : (
      selectComponent
    )

  const getSelectOptions = (
    selectOptions: SelectProps['options'],
  ): ReactNode => {
    let optionsResult: ReactNode
    if (selectOptions?.length) {
      if (children) {
        optionsResult = searchable
          ? cloneElement(children as ReactElement, {options: filteredOptions})
          : children
      } else {
        optionsResult = (
          <SelectOptions
            className={optionsClassNames}
            multiple={multiple}
            name={name}
            onChange={onChange}
            renderItem={renderItem}
            options={filteredOptions}
            value={value}
          />
        )
      }
    } else if (children) {
      optionsResult = children
    }
    return optionsResult
  }

  const SelectOpt = getSelectOptions(options)

  const onOptionAdded: SelectProps['onChange'] = (
    event,
    addedValue,
    selectAction,
  ) => {
    setSearchText('')
    const newOption: SelectOption = {
      name: !Array.isArray(addedValue) && addedValue !== null ? addedValue : '',
      value: addedValue,
    }
    setFilteredOptions((prevOptions) => [...prevOptions, newOption])
    onChange?.(event, addedValue, selectAction)
  }

  const showClearIcon = clearIcon && searchText

  const iconOnClick = showClearIcon ? cleanSearch : undefined

  const customSearchable = searchable
    ? {
        ...searchable,
        ...(showClearIcon
          ? {
              appendIcon: {
                name: GLYPHICONS.removeCircle,
                type: ICON_TYPES.glyphicon,
                color: AppColors.lightGray,
              },
            }
          : {}),
      }
    : undefined

  const onKeyPress:
    | (KeyboardEventHandler<FormControl> &
        KeyboardEventHandler<HTMLInputElement>)
    | undefined = handleCheckOnEnter
    ? async (event) => {
        if (event.key === KEYS.enter && searchText && filteredOptions.length) {
          handleCheckOnEnter(name ?? '', filteredOptions)
        }
      }
    : undefined

  return (
    <Dropdown
      key={options ? JSON.stringify(options) : uniquely()}
      manualToggle={isManuallyToggled}
      className={classNames(
        {
          searchable,
        },
        className,
        styles.dropdown,
      )}
      fullWidth={fullWidth}
      trigger={
        <div
          className={classNames('select', {
            'px-15': !plain,
          })}
          data-toggle={isManuallyToggled ? null : 'dropdown'}
        >
          {selectedContent}
          {!plain ? (
            <div className='select__icon_container pl-10'>
              <Icon
                type={ICON_TYPES.fas}
                iconName={FA_ICONS.chevronDown}
                className='mt-1'
              />
            </div>
          ) : null}
        </div>
      }
      disabled={disabled}
      plain={plain}
      activeElement={selectInputSearchRef}
    >
      <SelectSearch
        name={name}
        onAdd={onOptionAdded}
        searchKey={searchKey}
        filteredOptions={filteredOptions}
        searchText={searchText}
        searchable={customSearchable}
        iconOnClick={iconOnClick}
        onSearchChange={handleSearchChange}
        inputRef={(ref) => {
          selectInputSearchRef.current = ref
        }}
        onKeyPress={onKeyPress}
      />
      {SelectOpt}
      {canCreate && !filteredOptions?.length ? (
        <p className='px-15'>
          <i>
            Press enter to add <TextVariant>{searchText}</TextVariant>
          </i>
        </p>
      ) : null}
    </Dropdown>
  )
}

export default Select
