import classNames from 'classnames'

import {SelectProps, SelectOptions, SelectOption} from '..'
import {uniquely} from '../../../../../utils/helpers'

export interface SelectOptionsItemProps
  extends Pick<SelectProps, 'value' | 'onChange' | 'name' | 'className'> {
  option: SelectOption
}

const SelectOptionsItem = ({
  className,
  option,
  onChange,
  name,
  value,
}: SelectOptionsItemProps) => {
  const itemValue = option.value
  const active = Array.isArray(value)
    ? value.includes(itemValue)
    : value === itemValue

  return (
    // eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions
    <li
      key={String(itemValue ?? uniquely())}
      className={classNames({active}, className)}
      onClick={(e) =>
        onChange?.(e, itemValue, {
          name,
          action: 'select-option',
          option,
        })
      }
      value={option.value}
    >
      <span>{option.name}</span>
      {option.options?.length ? (
        <SelectOptions
          name={name}
          onChange={onChange}
          className={option.className}
          options={option.options}
          value={option.value}
        />
      ) : null}
    </li>
  )
}

export default SelectOptionsItem
