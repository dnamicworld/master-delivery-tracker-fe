import {ReactNode} from 'react'
import {DeliveryStatusActions} from 'src/models'

import {SelectProps} from '../../Select.model'

export interface SelectOption
  extends Pick<
    SelectProps,
    'renderItem' | 'options' | 'className' | 'multiple'
  > {
  name?: string
  display?: string
  deliveryStatus?: DeliveryStatusActions
  sampleRate?: number | null
  bitDepth?: number | null
  value: any
  onClick?: <T>(event: T) => void
}

export interface SelectOptionsProps
  extends Pick<
    SelectProps,
    | 'options'
    | 'value'
    | 'renderItem'
    | 'className'
    | 'onChange'
    | 'name'
    | 'multiple'
  > {}

export interface SelectWrapperProps {
  className?: string
  children: ReactNode
  customSelect?: boolean
}
