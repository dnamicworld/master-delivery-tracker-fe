import classnames from 'classnames'
import {Fragment} from 'react'

import {
  SelectOption,
  SelectOptionsItem,
  SelectOptionsProps,
} from '../../../../../../components'
import {cloneDeep} from '../../../../../../utils/helpers'

import {SelectWrapperProps} from './SelectOptions.model'

const SelectWrapper = ({
  className,
  customSelect,
  children,
}: SelectWrapperProps) =>
  customSelect ? (
    <div className={className}>{children}</div>
  ) : (
    <ul className={className}>{children}</ul>
  )

const SelectOptions = ({
  name,
  className,
  multiple,
  options,
  renderItem,
  value,
  onChange,
}: SelectOptionsProps) => {
  const renderItems = (option: SelectOption) => {
    const renderFunction = renderItem || option.renderItem
    const optionAction = onChange ?? option.onClick
    return renderFunction ? (
      <Fragment key={option.value}>
        {renderFunction(option, {value, onChange})}
        <SelectOptions
          multiple={multiple || option.multiple}
          name={name}
          onChange={optionAction}
          className={className || option.className}
          value={option.value}
          options={option.options}
        />
      </Fragment>
    ) : (
      <SelectOptionsItem
        className={option.className}
        name={name}
        key={option.value}
        option={option}
        value={value}
        onChange={optionAction}
      />
    )
  }
  const items = options?.map((option) => renderItems(option)) ?? []
  const {hasCustomItems, hasMultiple} = options?.reduce(
    (prev, option) => {
      const prevCopy = cloneDeep(prev)
      if (!prev.hasCustomItems) {
        prevCopy.hasCustomItems = !!option.renderItem
      }
      if (!prev.hasMultiple) {
        prevCopy.hasMultiple = Boolean(option.multiple)
      }
      return prevCopy
    },
    {
      hasCustomItems: false,
      hasMultiple: false,
    },
  ) ?? {hasCustomItems: false, hasMultiple: false}

  const renderSelect = options?.length ? (
    <SelectWrapper
      className={classnames(
        'list',
        {
          inline: hasMultiple,
          'btn-group': hasMultiple,
          checkboxes: hasMultiple,
        },
        className,
      )}
      customSelect={hasCustomItems}
    >
      {items}
    </SelectWrapper>
  ) : (
    <>{items}</>
  )
  return <>{renderSelect}</>
}

export default SelectOptions
