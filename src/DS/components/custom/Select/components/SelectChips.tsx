import {Chip, IconProps} from '../../..'
import {SelectActions} from '../../../../../utils/constants'
import {SelectProps, SelectValue} from '../Select.model'

interface SelectChipsProps
  extends Pick<
    SelectProps,
    'options' | 'multiple' | 'name' | 'onChange' | 'value'
  > {}
const SelectChips = ({
  options,
  onChange,
  multiple,
  name,
  value,
}: SelectChipsProps) => {
  const handleChipClose =
    (clickedValue: SelectValue): IconProps['onClick'] =>
    (event) => {
      onChange?.(event, multiple ? clickedValue : '', {
        action: SelectActions.deselectOption,
        name,
        option: {
          name,
          value: clickedValue,
        },
      })
    }
  const getChipsMultiple = () =>
    options
      ?.filter((option) => value?.includes(option.value))
      .map((option) => (
        <Chip
          key={option.value}
          content={option.name}
          iconClick={handleChipClose(option.value)}
        />
      ))

  const chipContent: string = !Array.isArray(value) ? value ?? '' : ''

  return (
    <div className='select__label'>
      {Array.isArray(value) && multiple ? (
        getChipsMultiple()
      ) : (
        <Chip content={chipContent} iconClick={handleChipClose(value)} />
      )}
    </div>
  )
}

export default SelectChips
