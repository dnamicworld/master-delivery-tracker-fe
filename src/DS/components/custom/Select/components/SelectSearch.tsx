import {ChangeEvent, KeyboardEventHandler, useEffect} from 'react'

import {FormControl, Search} from '../../..'
import {
  AppColors,
  FA_ICONS,
  KEYS,
  SelectActions,
} from '../../../../../utils/constants'

import {SelectProps} from '../Select.model'
import {SelectOption} from '../SelectOptions'

interface SelectSearchProps extends Pick<SelectProps, 'searchable' | 'name'> {
  filteredOptions: SelectOption[]
  searchText: string
  searchKey: string
  onAdd: SelectProps['onChange']
  onSearchChange: (event: ChangeEvent<HTMLInputElement>) => void
  iconOnClick?: () => void
  inputRef?: (instance: HTMLInputElement) => void
  onKeyPress?: KeyboardEventHandler<FormControl>
}

const {createOption, selectOption} = SelectActions

const isExistingOption = (
  filteredOptions: SelectSearchProps['filteredOptions'],
  searchKey: SelectSearchProps['searchKey'],
  searchText: SelectSearchProps['searchText'],
) =>
  filteredOptions.length
    ? filteredOptions.map((option) => option[searchKey]).includes(searchText)
    : false

const SelectSearch = ({
  filteredOptions,
  searchable,
  searchText,
  searchKey,
  name,
  onSearchChange,
  onAdd,
  iconOnClick,
  inputRef,
  onKeyPress,
}: SelectSearchProps) => {
  useEffect(() => {
    const handlePressEnter = (event: KeyboardEvent) => {
      if (!onKeyPress) {
        const {key} = event
        if (key === KEYS.enter && searchText) {
          const optionExists = isExistingOption(
            filteredOptions,
            searchKey,
            searchText,
          )
          const action = optionExists ? selectOption : createOption
          onAdd?.(event, searchText, {
            action,
            name,
          })
        }
      }
    }
    document.addEventListener('keyup', handlePressEnter)
    return () => document.removeEventListener('keyup', handlePressEnter)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filteredOptions, name, searchKey, searchText])

  return searchable ? (
    <Search
      value={searchText}
      appendIcon={{
        name: FA_ICONS.search,
        ...searchable?.appendIcon,
        color: AppColors.lightGray,
      }}
      iconOnClick={iconOnClick}
      autoComplete='off'
      placeholder={searchable?.placeholder}
      onChange={onSearchChange}
      inputRef={inputRef}
      onKeyPress={onKeyPress}
    />
  ) : null
}

export default SelectSearch
