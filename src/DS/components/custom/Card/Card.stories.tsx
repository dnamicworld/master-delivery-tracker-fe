import {Story} from '@storybook/react'

import {Row} from '../..'

import Card, {CardProps} from './Card'

export default {
  title: 'Card',
  component: Card,
}

const Template: Story<CardProps> = ({children, ...args}) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Card {...args}>{children}</Card>
)

const children = (
  <Row className='px-10'>
    <p>Card content</p>
  </Row>
)
export const WithHeader = Template.bind({})
WithHeader.args = {
  children,
  header: <p>Header content</p>,
}

export const WithFooter = Template.bind({})
WithFooter.args = {
  children,
  footer: <p>Footer content</p>,
}

export const WithAllContent = Template.bind({})
WithAllContent.args = {
  children,
  header: <p>Header content</p>,
  footer: <p>Footer content</p>,
}
