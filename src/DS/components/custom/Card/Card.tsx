import classNames from 'classnames'
import {ReactNode} from 'react'

import styles from './Card.module.scss'

type BackgroundColor = 'gray' | 'white'

export interface CardProps {
  backgroundColor?: BackgroundColor
  radius?: boolean
  className?: string
  children: ReactNode
  footer?: ReactNode
  header?: ReactNode
}
const Card = ({
  backgroundColor = 'gray',
  className,
  children,
  footer,
  header,
  radius,
}: CardProps) => (
  <div
    className={classNames(
      styles.card,
      'p-20',
      className,
      styles[backgroundColor],
      {
        [styles.radius]: radius,
      },
    )}
  >
    {header ? <div className='card__header pb-40'>{header}</div> : null}
    <div className='card__content'>{children}</div>
    {footer ? <div className='card__footer'>{footer}</div> : null}
  </div>
)

export default Card
