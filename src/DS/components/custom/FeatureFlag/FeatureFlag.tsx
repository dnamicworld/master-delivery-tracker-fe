import {ReactNode} from 'react'

import {isFeatureFlagEnabled} from '../../../../utils/helpers'

interface FeatureProps {
  children: ReactNode
  flag: string
  fallbackComponent?: ReactNode
}

const FeatureFlag = ({children, flag, fallbackComponent}: FeatureProps) => {
  if (isFeatureFlagEnabled(flag)) {
    return <>{children}</>
  }

  if (fallbackComponent) {
    return <>{fallbackComponent}</>
  }

  return null
}

export default FeatureFlag
