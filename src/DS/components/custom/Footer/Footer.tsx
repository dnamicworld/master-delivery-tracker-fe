import classNames from 'classnames'

import {Row, Col} from '../..'
import wmgLogo from '../../../../assets/images/wmg_logo.svg'

import {getCurrentYear} from '../../../../utils/helpers'

import {GoToTop} from './components/GoToTop'
import styles from './footer.module.scss'

interface FooterProps {
  className?: string
}

const Footer = ({className}: FooterProps) => (
  <Row className={classNames(styles.footer, className, 'pt-5 pl-4')}>
    <Row className={classNames(className, 'd-flex mt-4')}>
      <img
        className={classNames(styles.footerLogo, 'mr-3')}
        src={wmgLogo}
        alt='WMG Logo'
      />
      <Col sm={8}>
        <h4>Master Delivery Report</h4>
        <p className={classNames('small')}>
          <span className='text-uppercase d-block'>Warner Music Group</span>
          <span className='d-block'>
            &#169; Copyright {getCurrentYear()} Warner Music Group
          </span>
        </p>
      </Col>
    </Row>
    <GoToTop />
  </Row>
)

export default Footer
