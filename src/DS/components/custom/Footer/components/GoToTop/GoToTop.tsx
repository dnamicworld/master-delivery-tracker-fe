import classNames from 'classnames'

import {Row} from '../../../..'
import {FA_ICONS, ICON_TYPES} from '../../../../../../utils/constants'
import {Icon} from '../../../Icon'

import styles from './GoToTop.module.scss'

const GoToTop = () => (
  <Row className='d-flex justify-content-end'>
    <div
      role='button'
      tabIndex={0}
      className={classNames(
        styles.square,
        'mr-20 d-flex align-items-center justify-content-center',
      )}
      onClick={() => {
        window.scrollTo(0, 0)
      }}
    >
      <Icon type={ICON_TYPES.fas} className='co' iconName={FA_ICONS.arrowUp} />
    </div>
  </Row>
)

export default GoToTop
