import classNames from 'classnames'
import {ReactNode} from 'react'
import {ButtonGroup} from 'react-bootstrap'

export interface RadioGroupProps {
  className?: string
  children: ReactNode
  inline?: boolean
}
const RadioGroup = ({className, children, inline}: RadioGroupProps) => (
  <ButtonGroup className={classNames('radiobuttons', {inline}, className)}>
    {children}
  </ButtonGroup>
)

export default RadioGroup
