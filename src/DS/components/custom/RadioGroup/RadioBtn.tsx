import classNames from 'classnames'
import {ChangeEvent, InputHTMLAttributes} from 'react'

import {Button} from 'react-bootstrap'

import {isNumber} from '../../../../utils/helpers'

export interface RadioBtnProps extends InputHTMLAttributes<HTMLInputElement> {
  onChange: (event: ChangeEvent<HTMLInputElement>) => void
  label?: string
}

const RadioBtn = ({
  name,
  multiple,
  checked,
  label,
  type,
  onChange,
  value,
}: RadioBtnProps) => (
  <Button
    bsStyle={null}
    className={classNames({
      active: checked,
    })}
  >
    <label className={classNames({'font-number': isNumber(label)})}>
      {label}
    </label>
    <input
      multiple={multiple}
      value={value}
      type={type ?? 'radio'}
      checked={checked}
      name={name}
      onChange={onChange}
    />
  </Button>
)

export default RadioBtn
