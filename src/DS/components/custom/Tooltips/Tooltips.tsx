import {TextVariant} from '../..'
import {AppColors} from '../../../../utils/constants'
import {get} from '../../../../utils/helpers'

export const getCheckTooltipText = (data: any) =>
  data.complete ? (
    <TextVariant variant='success'>Complete</TextVariant>
  ) : (
    <TextVariant variant='danger'>Incomplete</TextVariant>
  )

export const getTooltipValueFromField = (field: string) => (data: any) => {
  const value = get(data, field) ?? ''
  return <span>{value}</span>
}

export const getCheckTooltipTextColor = (value: any, data: any): string =>
  data.complete ? AppColors.success : AppColors.lightGray
