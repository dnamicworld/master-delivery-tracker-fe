import {Icon} from '../..'
import {FA_ICONS, ICON_TYPES} from '../../../../utils/constants'

import styles from './loader.module.scss'

export interface LoaderProps {}

const Loader = () => (
  <div className={styles.loader}>
    <Icon
      type={ICON_TYPES.fas}
      iconName={FA_ICONS.spinner}
      className='loader__icon fa-lg fa-spin'
    />
  </div>
)

export default Loader
