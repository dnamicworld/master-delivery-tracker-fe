import {Story} from '@storybook/react'

import Loader, {LoaderProps} from './Loader'

export default {
  title: 'Loader',
  component: Loader,
}

const Template: Story<LoaderProps> = () => <Loader />

export const WithoutProps = Template.bind({})
