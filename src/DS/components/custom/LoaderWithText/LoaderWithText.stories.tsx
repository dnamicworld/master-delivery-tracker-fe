import {Story} from '@storybook/react'

import LoaderWithText, {LoaderProps} from './LoaderWithText'

export default {
  title: 'LoaderWithText',
  component: LoaderWithText,
}

const Template: Story<LoaderProps> = () => <LoaderWithText />

export const WithoutProps = Template.bind({})
