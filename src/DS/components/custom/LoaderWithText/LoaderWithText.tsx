import {Icon} from '../..'
import {FA_ICONS, ICON_TYPES} from '../../../../utils/constants'

import styles from './LoaderWithText.module.scss'

export interface LoaderProps {
  loadingMessage?: string
}

const LoaderWithText = ({loadingMessage}: LoaderProps) => (
  <div className={styles.loaderWithText}>
    <Icon
      type={ICON_TYPES.fas}
      iconName={FA_ICONS.spinner}
      className='loader__icon fa-lg fa-spin'
    />
    <span className='ml-10'>{loadingMessage}</span>
  </div>
)

export default LoaderWithText
