import classNames from 'classnames'
import {ReactNode} from 'react'

import {Row} from '../../bootstrap'

import styles from './accordion_body.module.scss'

interface AccordionBodyProps {
  children?: ReactNode
  header?: ReactNode
  className?: string
  accordionId: string
}
const AccordionBody = ({
  accordionId,
  header,
  children,
  className,
}: AccordionBodyProps) => (
  <div className={classNames('collapse', className)} id={accordionId}>
    {header ? (
      <Row className={styles.accordionDetailsHeader}>{header}</Row>
    ) : null}
    <Row className={styles.accordionDetailsBody}>{children}</Row>
  </div>
)

export default AccordionBody
