import classNames from 'classnames'
import {MouseEventHandler} from 'react'

import {Glyphicon, GlyphiconProps} from '../..'

import {AppColors, FAPrefix} from '../../../../utils/constants'

import styles from './Icon.module.scss'

export type IconElement = Glyphicon | HTMLElement

export type IconPrefix = 'fas' | 'far' | 'fal' | 'fat' | 'fad' | 'fab' | 'fak'

export type IconSizes = 'xs' | 'sm' | 'lg' | 'xl'

export type IconType = 'glyphicon' | IconPrefix

export interface IconProps extends Omit<GlyphiconProps, 'glyph' | 'size'> {
  type: IconType
  iconName: string
  className?: string
  onClick?: MouseEventHandler<IconElement>
  color?: keyof typeof AppColors
  size?: IconSizes
  hoverColor?: string
}

function isFA(keyInput: string): keyInput is IconPrefix {
  return FAPrefix.includes(keyInput)
}

export const Icon = ({
  type,
  iconName,
  className,
  onClick,
  color,
  hoverColor,
  size,
}: IconProps) => {
  const fontAwesomeIcon = (faVariant: IconPrefix) => (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    <i
      className={classNames(faVariant, iconName, className, styles.fa, {
        [`text-${color}`]: color,
        [`fa-${size}`]: size,
        [hoverColor ? `hover_${hoverColor}` : '']: hoverColor,
      })}
      onClick={onClick}
    />
  )
  const glyphiconIcon = (
    <Glyphicon
      glyph={iconName}
      className={classNames(className, styles.glyphicon, {
        [color || '']: color,
        [`hover_${hoverColor}`]: hoverColor,
      })}
      onClick={onClick}
    />
  )

  return isFA(type) ? fontAwesomeIcon(type) : glyphiconIcon
}

export default Icon
