export {default as Icon} from './Icon'
export type {IconElement, IconProps, IconType} from './Icon'
