import {CheckboxGroup, CheckboxBtn, CheckboxBtnProps} from '../..'

interface MultiCheckboxItem {
  name: string
  value: string
}

export interface MultipleCheckboxProps extends CheckboxBtnProps {
  options: MultiCheckboxItem[]
  value: string[]
}

const MultipleCheckbox = ({
  options,
  value,
  label,
  name,
  onChange,
}: MultipleCheckboxProps) => {
  const isAllSelected = value?.length === options?.length
  const isPartiallySelected = Boolean(value?.length && !isAllSelected)

  return (
    <CheckboxGroup inline>
      <CheckboxBtn
        value={label}
        label={label}
        name={name}
        checked={isAllSelected}
        onChange={onChange}
        partialCheck={isPartiallySelected}
      />
      <div className='ml-18'>
        {options.map((item) => (
          <CheckboxBtn
            name={item.name}
            multiple
            checked={value.includes(item.value)}
            key={item.value}
            label={item.name}
            value={item.value}
            onChange={onChange}
          />
        ))}
      </div>
    </CheckboxGroup>
  )
}

export default MultipleCheckbox
