import classNames from 'classnames'
import {ReactNode} from 'react'

import styles from './ListItem.module.scss'

export interface ListItemProps {
  children: ReactNode
  spacing?: 'sm' | 'md'
}

const listItemPropsDefault = {
  spacing: 'md',
}

const ListItem = ({children, spacing}: ListItemProps) => {
  const classes = classNames('row list-item ml-1', {
    'py-5': spacing === 'md',
    [styles.sm]: spacing === 'sm',
  })

  return <li className={classes}>{children}</li>
}

ListItem.defaultProps = listItemPropsDefault

export default ListItem
