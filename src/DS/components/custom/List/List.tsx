import {ReactNode} from 'react'

import styles from './List.module.scss'

export interface ListProps {
  children: ReactNode
}

const List = ({children}: ListProps) => (
  <ul className={styles.list}>{children}</ul>
)

export default List
