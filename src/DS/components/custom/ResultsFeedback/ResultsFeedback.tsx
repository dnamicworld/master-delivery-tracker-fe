import classNames from 'classnames'
import {PropsWithChildren} from 'react'

import {Col, Row} from '../../index'

import styles from './ResultsFeedback.module.scss'

export interface ResultsFeedbackProps {}

const ResultsFeedback = ({
  children,
}: PropsWithChildren<ResultsFeedbackProps>) => (
  <Col xs={12} className={classNames(styles.resultsFeedback, 'py-40')}>
    <Row className='d-flex flex-column align-items-center'>{children}</Row>
  </Col>
)

export default ResultsFeedback
