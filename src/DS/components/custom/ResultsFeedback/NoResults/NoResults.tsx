import lostInSpaceLogo from '../../../../../assets/images/lost_in_space.svg'
import {ButtonProps} from '../../../bootstrap'
import {Button, ResultsFeedback} from '../../../index'

export interface NoResultsProps extends Pick<ButtonProps, 'onClick'> {}

const NoResults = ({onClick}: NoResultsProps) => (
  <ResultsFeedback>
    <img src={lostInSpaceLogo} alt='lostInSpace' />
    <h3>No Results Found</h3>
    <Button containerStyle='link' onClick={onClick} label='Reset Filters' />
  </ResultsFeedback>
)

export default NoResults
