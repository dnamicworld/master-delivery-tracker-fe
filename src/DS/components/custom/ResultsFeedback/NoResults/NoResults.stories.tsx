import {Story} from '@storybook/react'

import NoResults, {NoResultsProps} from './NoResults'

export default {
  title: 'NoResults',
  component: NoResults,
}

const Template: Story<NoResultsProps> = (args) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <NoResults {...args} />
)

export const WithOnClick = Template.bind({})
WithOnClick.args = {onClick: () => {}}
