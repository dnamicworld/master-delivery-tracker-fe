import {Story} from '@storybook/react'
import {PropsWithChildren} from 'react'

import {Button} from '../../bootstrap'

import ResultsFeedback, {ResultsFeedbackProps} from './ResultsFeedback'

export default {
  title: 'ResultsFeedback',
  component: ResultsFeedback,
}

const Template: Story<PropsWithChildren<ResultsFeedbackProps>> = ({
  children,
}) => <ResultsFeedback>{children}</ResultsFeedback>

const children = (
  <>
    <h3>All Results Displayed</h3>
    <Button containerStyle='link' label='Clear Filters' onClick={() => {}} />
  </>
)

export const WithChildren = Template.bind({})
WithChildren.args = {children}
