import classNames from 'classnames'
import {HTMLProps, ReactNode} from 'react'

import styles from './td.module.scss'

interface TdProps extends Pick<HTMLProps<HTMLTableDataCellElement>, 'colSpan'> {
  children?: ReactNode
  className?: string
  withAccordion?: boolean
}
const Td = ({className, withAccordion, children, ...tdProps}: TdProps) => (
  <td
    // This prop usage is fine, because it extend native HTML props
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...tdProps}
    className={classNames(className, {
      [styles.withAccordion]: withAccordion,
    })}
  >
    {children}
  </td>
)

export default Td
