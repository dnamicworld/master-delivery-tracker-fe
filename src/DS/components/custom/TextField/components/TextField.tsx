import classNames from 'classnames'

import {ChangeEvent, FormEvent} from 'react'

import {
  FormControl,
  FormControlProps,
  FormGroup,
  FormGroupProps,
  Icon,
  HelpBlock,
  InputGroup,
} from '../../..'

import {
  AppColors,
  ICON_TYPES,
  throttleTime,
} from '../../../../../utils/constants'
import {isUndefined, throttle} from '../../../../../utils/helpers'

import {IconType} from '../../Icon'

export type BSTextFieldProps = Pick<FormGroupProps, 'validationState'> &
  Omit<FormControlProps, 'onChange'>

interface TextFieldIcon {
  name: string
  addon?: boolean
  addonClass?: string
  color?: keyof typeof AppColors
  type?: IconType
}
export interface TextFieldProps extends BSTextFieldProps {
  appendIcon?: TextFieldIcon
  prependIcon?: TextFieldIcon
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void
  errorMessage?: string
  iconOnClick?: () => void
}

export type BSTextFieldEvent =
  | FormEvent<FormControl & FormControlProps>
  | ChangeEvent<HTMLInputElement>

const getIconContent = (
  icon: TextFieldIcon,
  onClick?: TextFieldProps['iconOnClick'],
) => (
  <Icon
    onClick={onClick}
    type={icon.type ?? ICON_TYPES.fas}
    iconName={icon.name}
    color={icon.color}
  />
)

const getFeedbackIcon = (
  icon?: TextFieldIcon,
  onClick?: TextFieldProps['iconOnClick'],
) =>
  !isUndefined(icon) && !icon?.addon ? (
    <FormControl.Feedback
      className={classNames(icon.color, {
        'pointer-events': !!onClick,
      })}
    >
      {getIconContent(icon, onClick)}
    </FormControl.Feedback>
  ) : null

const getInputGroup = ({
  appendIcon,
  prependIcon,
  as,
  autoComplete,
  componentClass,
  iconOnClick,
  placeholder,
  value,
  name,
  onChange,
  ref,
  rows,
  onBlur,
  onKeyPress,
  bsSize,
  onKeyUp,
  inputRef,
}: TextFieldProps) => {
  const handleOnKeyUp = onKeyUp
    ? throttle((event) => onKeyUp(event), throttleTime)
    : undefined
  const FormElement = (
    <FormControl
      as={as}
      autoComplete={autoComplete}
      ref={ref}
      onBlur={onBlur}
      componentClass={componentClass}
      onChange={(event) =>
        onChange?.(event as unknown as ChangeEvent<HTMLInputElement>)
      }
      rows={rows}
      value={value}
      bsSize={bsSize}
      name={name}
      type='text'
      placeholder={placeholder}
      onKeyUp={handleOnKeyUp}
      onKeyPress={onKeyPress}
      inputRef={inputRef}
      autoFocus
    />
  )
  return prependIcon?.addon || appendIcon?.addon ? (
    <InputGroup>
      {prependIcon?.addon ? (
        <InputGroup.Addon
          bsClass={classNames('input-group-addon', prependIcon?.addonClass)}
        >
          {getIconContent(prependIcon, iconOnClick)}
        </InputGroup.Addon>
      ) : null}
      {FormElement}
      {appendIcon?.addon ? (
        <InputGroup.Addon
          bsClass={classNames('input-group-addon', appendIcon?.addonClass)}
        >
          {getIconContent(appendIcon, iconOnClick)}
        </InputGroup.Addon>
      ) : null}
    </InputGroup>
  ) : (
    FormElement
  )
}
export const TextField = (textFieldProps: TextFieldProps) => {
  const {
    appendIcon,
    prependIcon,
    validationState,
    className,
    errorMessage,
    iconOnClick,
  } = textFieldProps
  return (
    <FormGroup className={className} validationState={validationState}>
      {getFeedbackIcon(prependIcon, iconOnClick)}
      {getInputGroup(textFieldProps)}
      {errorMessage ? (
        <HelpBlock
          className={classNames({
            'd-inline-block': validationState !== 'error' && errorMessage,
          })}
        >
          {errorMessage}
        </HelpBlock>
      ) : null}
      {getFeedbackIcon(appendIcon, iconOnClick)}
    </FormGroup>
  )
}

export default TextField
