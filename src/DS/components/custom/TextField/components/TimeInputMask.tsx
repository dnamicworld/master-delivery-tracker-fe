import {ChangeEvent, ReactNode} from 'react'
import MaskedInput, {MaskedInputProps} from 'react-text-mask'

import {toInt} from '../../../../../utils/helpers'

export interface TimeInputMaskProps extends Omit<MaskedInputProps, 'onChange'> {
  inputRef?: (ref: any) => ReactNode
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void
}

function getTimeFraction(from: number, to: number, value: string) {
  const twoDigitValue = toInt(value.slice(from, to))
  const oneDigitValue = toInt(value.slice(from + 1, to))
  let validValue = 0
  if (!Number.isNaN(twoDigitValue)) {
    validValue = twoDigitValue
  } else if (!Number.isNaN(oneDigitValue)) {
    validValue = oneDigitValue
  }
  return `0${validValue}`.slice(-2)
}

const TimeInputMask = ({
  onChange,
  inputRef,
  placeholder,
  ...timeInputMaskProps
}: TimeInputMaskProps) => {
  const customRef = (ref: any) => {
    if (inputRef) {
      inputRef(ref ? ref.inputElement : null)
    }
  }

  const timeClean = (event: ChangeEvent<HTMLInputElement>) => {
    const eventCopy = {...event}
    const {value} = eventCopy.target
    if (value) {
      const hours = getTimeFraction(0, 2, value)
      const minutes = getTimeFraction(3, 5, value)
      const seconds = getTimeFraction(6, 8, value)
      eventCopy.target.value = `${hours}:${minutes}:${seconds}`
      onChange?.(eventCopy)
    }
  }

  const timeMask = () => {
    // Using RegExp generates an error on the concat
    const hours: any[] = [/[0-9]/, /[0-9]/]
    const minSecs: any[] = [/[0-5]/, /[0-9]/]
    return hours.concat(':').concat(minSecs).concat(':').concat(minSecs)
  }

  return (
    <MaskedInput
      // This prop usage is fine, because it extend MaskedInputProps which contains HTMLInputElement
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...timeInputMaskProps}
      ref={customRef}
      mask={timeMask}
      onBlur={timeClean}
      onChange={onChange}
      placeholder={placeholder || '00:00:00'}
      keepCharPositions
    />
  )
}

export default TimeInputMask
