import classNames from 'classnames'
import {FC, ReactNode} from 'react'

import {MaskedInputProps} from 'react-text-mask'

import {
  TextField as BSTextField,
  TextFieldProps as BSTextFieldProps,
} from './components/TextField'

import TimeInputMask, {TimeInputMaskProps} from './components/TimeInputMask'

type CustomTextFieldProps = BSTextFieldProps &
  Omit<MaskedInputProps, 'onChange' | 'mask'> &
  Partial<Pick<MaskedInputProps, 'mask'>>

interface TextFieldProps extends CustomTextFieldProps {
  className?: string
  errorMessage?: string
  iconOnClick?: () => void
}

interface Components {
  time: ReactNode
  default: ReactNode
}

function getComponent(type: string): FC<TimeInputMaskProps | TextFieldProps> {
  const components: Components = {
    time: TimeInputMask,
    default: BSTextField,
  }

  return type in components ? components[type] : components.default
}

export const TextField = ({
  type,
  className,
  name,
  placeholder,
  value,
  onChange,
  validationState,
  appendIcon,
  autoComplete,
  onBlur,
  as,
  rows,
  componentClass,
  errorMessage,
  onKeyUp,
  onKeyPress,
  bsSize,
  iconOnClick,
  inputRef,
  autoFocus,
  onFocus,
}: TextFieldProps) => {
  const InputComponent = getComponent(type ?? '')
  return (
    <InputComponent
      as={as}
      rows={rows}
      name={name}
      placeholder={placeholder}
      value={value}
      validationState={validationState}
      appendIcon={appendIcon}
      autoComplete={autoComplete}
      onChange={onChange}
      onBlur={onBlur}
      componentClass={componentClass}
      className={classNames(className)}
      errorMessage={errorMessage}
      onKeyUp={onKeyUp}
      onKeyPress={onKeyPress}
      bsSize={bsSize}
      iconOnClick={iconOnClick}
      inputRef={inputRef}
      autoFocus={autoFocus}
      onFocus={onFocus}
    />
  )
}

export default TextField
