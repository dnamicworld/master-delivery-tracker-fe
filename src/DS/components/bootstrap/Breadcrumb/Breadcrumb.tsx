import classNames from 'classnames'
import {ReactNode} from 'react'
import {
  Breadcrumb as BSBreadcrumb,
  BreadcrumbItem as BSBreadcrumbItem,
} from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'

import {uniquely} from '../../../../utils/helpers'

import {BreadcrumbItem} from './Breadcrumb.model'

interface BreadcrumbsProps {
  items: BreadcrumbItem[]
}

const itemBody = (item: BreadcrumbItem): ReactNode => (
  <BSBreadcrumbItem key={uniquely()} active={item.active}>
    <span
      className={classNames('', {
        'font-weight-sbold text-black': item.active,
        'text-primary font-weight-normal': !item.active,
      })}
    >
      {item.label}
    </span>
  </BSBreadcrumbItem>
)

const Breadcrumb = ({items}: BreadcrumbsProps) => (
  <BSBreadcrumb className='p-0 m-0'>
    {items.map((item) => {
      const breadcrumbItem = itemBody(item)
      return item.link ? (
        <LinkContainer key={item.id ?? uniquely()} to={item.link ?? ''}>
          {breadcrumbItem}
        </LinkContainer>
      ) : (
        breadcrumbItem
      )
    })}
  </BSBreadcrumb>
)

export default Breadcrumb
