export interface BreadcrumbItem {
  id: string
  label: string
  link?: string
  active?: boolean
}
