import {ReactNode} from 'react'

import {Table as BSTable, TableProps as BSTableProps} from 'react-bootstrap'

interface TableProps extends Omit<BSTableProps, 'headers' | 'responsive'> {
  headers: string[]
  children: ReactNode
}

const Table = ({headers, children, ...tableProps}: TableProps) => (
  // This prop usage is fine, because it extend native HTML props inside BSTableProps
  // eslint-disable-next-line react/jsx-props-no-spreading
  <BSTable {...tableProps} responsive>
    <thead>
      <tr>
        {headers.map((header) => (
          <th key={header}>{header}</th>
        ))}
      </tr>
    </thead>
    <tbody>{children}</tbody>
  </BSTable>
)

export default Table
