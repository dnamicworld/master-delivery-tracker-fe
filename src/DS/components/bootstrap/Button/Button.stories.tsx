import {Story} from '@storybook/react'

import Button, {ButtonProps} from './index'

export default {
  title: 'Button',
  component: Button,
  args: {},
  argTypes: {
    disabled: {
      control: {type: 'boolean'},
    },
    bsStyle: {
      options: [
        'primary',
        'success',
        'info',
        'warning',
        'danger',
        'link',
      ] as ButtonProps['bsStyle'][],
      control: {type: 'select'},
    },
    bsSize: {
      options: ['xs', 'sm', 'lg'],
      control: {type: 'select'},
    },
  },
}

const Template: Story<ButtonProps> = ({children, ...args}) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Button {...args}>{children}</Button>
)

export const Default = Template.bind({})

Default.args = {
  children: 'Default',
  bsStyle: 'success',
}
