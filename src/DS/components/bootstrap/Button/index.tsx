import {Button as LegatoButton, IButtonProps} from '@wmg-ae/legato'
import classNames from 'classnames'

import {throttleTime} from '../../../../utils/constants'

import {throttle} from '../../../../utils/helpers'

export interface ButtonProps extends Omit<IButtonProps, 'bsStyle'> {
  uppercase?: boolean
  bsStyle?: IButtonProps['colorType']
  bsSize?: IButtonProps['size']
}

const Button = ({
  children,
  className,
  bsStyle,
  containerStyle,
  bsSize,
  ref,
  label,
  uppercase,
  disabled,
  onClick,
  onMouseOver,
  onMouseOut,
  onFocus,
  onBlur,
  icon,
}: ButtonProps) => {
  const handleClick = throttle((event) => onClick?.(event), throttleTime)
  return (
    <LegatoButton
      className={classNames('font-weight-normal', className, {
        'text-uppercase': uppercase,
      })}
      label={label}
      ref={ref}
      size={bsSize}
      containerStyle={containerStyle}
      colorType={bsStyle}
      onClick={handleClick}
      disabled={disabled}
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
      onFocus={onFocus}
      onBlur={onBlur}
      icon={icon}
    >
      {children}
    </LegatoButton>
  )
}

export default Button
