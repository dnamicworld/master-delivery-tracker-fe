import classNames from 'classnames'
import {ReactNode} from 'react'
import {
  Popover as BSPopover,
  PopoverProps as BSPopoverProps,
  OverlayTrigger,
  OverlayTriggerProps,
} from 'react-bootstrap'
import {DefaultComponentStyleTypes} from 'src/utils/constants'

export interface PopoverProps
  extends BSPopoverProps,
    Pick<OverlayTriggerProps, 'placement'> {
  id: string
  children: ReactNode
  popoverChildren: ReactNode
  className?: string
  trigger?: string[]
  type?: DefaultComponentStyleTypes
}

export interface PopoverDisplayProps extends Omit<PopoverProps, 'children'> {
  children?: ReactNode
}

export const PopoverDisplay = ({
  id,
  className,
  children,
  type,
  popoverChildren,
  ...tooltipProps
}: PopoverDisplayProps) => (
  <BSPopover
    id={id}
    className={classNames('popover', type, className)}
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...tooltipProps}
  >
    {popoverChildren}
  </BSPopover>
)

const Popover = (popoverProps: PopoverProps) => {
  const {children, trigger, placement = 'top'} = popoverProps
  return (
    <OverlayTrigger
      placement={placement}
      trigger={trigger}
      rootClose
      // eslint-disable-next-line react/jsx-props-no-spreading
      overlay={<PopoverDisplay {...popoverProps} />}
    >
      {children}
    </OverlayTrigger>
  )
}

export default Popover
