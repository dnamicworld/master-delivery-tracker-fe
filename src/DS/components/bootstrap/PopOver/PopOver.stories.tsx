import {Story} from '@storybook/react'

import {Button, Popover, PopoverProps} from '../index'

export default {
  title: 'Popover',
  component: Popover,
  args: {
    id: 'popover',
    popoverChildren: <p>Popover content</p>,
  },
  argTypes: {
    type: {
      options: ['primary', 'warning', 'success', 'danger'],
      control: {type: 'select'},
    },
  },
}

const Template: Story<PopoverProps> = ({children, ...args}) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Popover {...args}>{children}</Popover>
)

export const Default = Template.bind({})

Default.args = {
  children: <Button bsStyle='primary'>I can be any element</Button>,
}
