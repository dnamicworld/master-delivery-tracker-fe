import classNames from 'classnames'
import {ChangeEvent, InputHTMLAttributes, MouseEvent} from 'react'
import {Button} from 'react-bootstrap'

import {isNumber} from '../../../../utils/helpers'

export interface CheckboxBtnProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, 'onClick'> {
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void
  onClick?: (event: MouseEvent<HTMLInputElement>) => void
  label?: string
  partialCheck?: boolean
}
const CheckboxBtn = ({
  name,
  multiple,
  checked,
  label,
  type,
  partialCheck,
  onChange,
  onClick,
  value,
  readOnly,
}: CheckboxBtnProps) => (
  <Button
    bsStyle={null}
    className={classNames({
      active: checked,
      partial: partialCheck,
      normal: !partialCheck,
    })}
  >
    <label className={classNames({'font-number': isNumber(label)})}>
      {label}
    </label>
    <input
      type={type ?? 'checkbox'}
      checked={checked}
      name={name}
      multiple={multiple}
      value={value}
      onChange={onChange}
      onClick={onClick}
      readOnly={readOnly}
    />
  </Button>
)

export default CheckboxBtn
