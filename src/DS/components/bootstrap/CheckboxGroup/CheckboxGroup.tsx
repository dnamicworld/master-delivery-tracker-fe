import classNames from 'classnames'
import {ReactNode} from 'react'
import {ButtonGroup} from 'react-bootstrap'

export interface CheckboxGroupProps {
  children: ReactNode
  inline?: boolean
}
const CheckboxGroup = ({children, inline}: CheckboxGroupProps) => (
  <ButtonGroup className={classNames('checkboxes', {inline})}>
    {children}
  </ButtonGroup>
)

export default CheckboxGroup
