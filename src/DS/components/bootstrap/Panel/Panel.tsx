import {ReactNode} from 'react'
import {Panel as BSPanel, PanelProps as BSPanelProps} from 'react-bootstrap'

import {Icon} from '../..'
import {FA_ICONS, ICON_TYPES} from '../../../../utils/constants'

export interface PanelProps extends BSPanelProps {
  header: ReactNode
  withTopDivider?: boolean
  withBottomDivider?: boolean
}

const Panel = ({
  expanded,
  onToggle,
  header,
  children,
  withTopDivider = true,
  withBottomDivider,
}: PanelProps) => {
  const iconName = expanded ? FA_ICONS.chevronUp : FA_ICONS.chevronDown

  return (
    <BSPanel expanded={expanded} onToggle={onToggle}>
      {withTopDivider ? <hr /> : null}
      <BSPanel.Heading>
        <BSPanel.Title toggle>
          {header}
          <Icon
            type={ICON_TYPES.fas}
            iconName={iconName}
            className='mt-1 gray'
          />
        </BSPanel.Title>
      </BSPanel.Heading>
      <BSPanel.Collapse>
        <BSPanel.Body>{children}</BSPanel.Body>
      </BSPanel.Collapse>
      {withBottomDivider ? <hr /> : null}
    </BSPanel>
  )
}

export default Panel
