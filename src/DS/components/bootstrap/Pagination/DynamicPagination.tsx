import classNames from 'classnames'
import {
  Component,
  Dispatch,
  ReactElement,
  ReactNode,
  SetStateAction,
} from 'react'
import {Pagination as BSPagination, Row} from 'react-bootstrap'

import {productPage} from '../../../../utils/constants'

export interface PaginationOptions {
  page: number
  pages: number
  count: number
  size: number
}

interface DynamicPaginationProps {
  children: ReactNode
  paginationSize?: number
  paginationOptions: PaginationOptions
  setPaginationOptions: Dispatch<SetStateAction<PaginationOptions>>
}

interface DynamicPaginationState {
  start: number
  amount: number
  offset?: number
}

class DynamicPagination extends Component<
  DynamicPaginationProps,
  DynamicPaginationState
> {
  static defaultProps = {
    paginationSize: productPage,
  }

  amount = 15

  constructor(props: DynamicPaginationProps) {
    super(props)
    this.state = {
      start: 1,
      amount: this.amount,
      offset: props.paginationOptions.pages,
    }
  }

  static getDerivedStateFromProps(
    props: DynamicPaginationProps,
    state: DynamicPaginationState,
  ) {
    return {
      ...state,
      offset: props.paginationOptions.pages,
    }
  }

  get totalPages() {
    const {
      paginationOptions: {count},
      paginationSize = 1,
    } = this.props
    return Math.ceil(count / paginationSize)
  }

  setNewPage = (page: number) => {
    window.scrollTo(0, 0)
    const {setPaginationOptions} = this.props
    setPaginationOptions((prevState) => ({...prevState, page}))
  }

  prev = (amount = 1) => {
    const {
      paginationOptions: {page},
    } = this.props
    if (page && page > 1) {
      this.setNewPage(page - amount)
    }
  }

  next = (amount = 1) => {
    const {
      paginationOptions: {page, pages},
    } = this.props
    if (page && page < pages) {
      this.setNewPage(page + amount)
    }
  }

  setPage = (page: number) => {
    const {
      paginationOptions: {page: currentPage},
    } = this.props
    if (page !== currentPage) {
      this.setNewPage(page)
    }
  }

  render() {
    const {
      children,
      paginationOptions: {page, pages},
    } = this.props

    const {amount, offset, start} = this.state

    const collapsed = pages > 15

    const buttonsToShow = 5
    const hasNextRange = page + 2 < pages
    const hasNextButtons = page + buttonsToShow < pages
    const displayPagination = Boolean(pages)
    let pagesLength = pages
    const collapsedPagination = collapsed && displayPagination
    if (collapsedPagination) {
      pagesLength = buttonsToShow
    }
    const increaseFunc = collapsed
      ? (num: number) => (hasNextButtons ? page + num : page - num)
      : (num: number) => num + 1
    const pagesArray = Array.from({length: pagesLength}, (_, i) =>
      increaseFunc(i),
    ).sort((a, b) => a - b)

    const displayElement = (cond: boolean, element: ReactElement) =>
      cond ? element : null
    const hasPreviousRange = page - 2 > 0
    const increaseFromStart = page > amount ? amount : page - start

    const increaseFromLast = page + amount < pages ? amount : pages - page

    const checkActive = (num?: number) =>
      num !== undefined ? num === page : false
    return (
      <>
        {children}
        <Row>
          <BSPagination>
            {displayElement(
              collapsedPagination,
              <>
                <BSPagination.First onClick={() => this.setPage(1)} />
                <BSPagination.Prev onClick={() => this.prev()} />
                <BSPagination.Item
                  className={classNames({'d-none': page === 1})}
                  active={checkActive(start)}
                  onClick={() => this.setPage(start)}
                >
                  {start}
                </BSPagination.Item>
                {hasPreviousRange ? (
                  <BSPagination.Ellipsis
                    onClick={() => this.prev(increaseFromStart)}
                  />
                ) : null}
              </>,
            )}
            {displayElement(
              !collapsedPagination,
              <BSPagination.Prev onClick={() => this.prev()} />,
            )}

            {pagesArray.map((position) => (
              <BSPagination.Item
                key={position}
                onClick={() => this.setPage(position)}
                active={checkActive(position)}
              >
                {position}
              </BSPagination.Item>
            ))}
            {displayElement(
              !collapsedPagination,
              <BSPagination.Next onClick={() => this.next()} />,
            )}
            {displayElement(
              collapsedPagination,
              <>
                {hasNextRange ? (
                  <BSPagination.Ellipsis
                    onClick={() => this.next(increaseFromLast)}
                  />
                ) : null}
                <BSPagination.Item
                  active={checkActive(offset)}
                  className={classNames({
                    'd-none': page === offset || page + buttonsToShow === pages,
                  })}
                  onClick={() => {
                    if (offset) this.setPage(offset)
                  }}
                >
                  {offset}
                </BSPagination.Item>
                <BSPagination.Next onClick={() => this.next()} />
                <BSPagination.Last onClick={() => this.setPage(pages)} />
              </>,
            )}
          </BSPagination>
        </Row>
      </>
    )
  }
}

export default DynamicPagination
