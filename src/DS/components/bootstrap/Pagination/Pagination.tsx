import {Component, ReactNode} from 'react'
import {Pagination as BSPagination, Row} from 'react-bootstrap'

import {productPage} from '../../../../utils/constants'
import {isEqual} from '../../../../utils/helpers'

import {PaginationOptions} from './DynamicPagination'

interface PaginationProps<T> {
  children: (paginationOptions: PaginationOptions) => ReactNode
  paginationSize?: number
  items: T[]
  onPreviousPage?: () => void
  onNextPage?: () => void
  onGoToPage?: (page: number) => void
}

interface PaginationState {
  currentPage: number
}

class Pagination<T> extends Component<PaginationProps<T>, PaginationState> {
  static defaultProps = {
    paginationSize: productPage,
  }

  constructor(props: PaginationProps<T>) {
    super(props)
    this.state = {
      currentPage: props.items.length ? 1 : 0,
    }
    this.setState = this.setState.bind(this)
  }

  componentDidUpdate(prevProps: PaginationProps<T>) {
    const {props, setState} = this
    if (!isEqual(prevProps.items, props.items)) {
      setState({
        currentPage: props.items.length ? 1 : 0,
      })
    }
  }

  get pageItems() {
    const {paginationSize = 1, items} = this.props
    const {currentPage} = this.state
    const fromIndex = (currentPage - 1) * paginationSize
    return items.slice(fromIndex, fromIndex + paginationSize)
  }

  get totalPages() {
    const {items, paginationSize = 1} = this.props
    return Math.ceil(items.length / paginationSize)
  }

  setNewPage = (currentPage: number) => {
    window.scrollTo(0, 0)
    this.setState((oldState) => ({...oldState, currentPage}))
  }

  prev = () => {
    const {currentPage} = this.state
    const {onPreviousPage} = this.props
    if (currentPage > 1) {
      this.setNewPage(currentPage - 1)
    }
    if (onPreviousPage) {
      onPreviousPage()
    }
  }

  next = () => {
    const {totalPages} = this
    const {currentPage} = this.state
    const {onNextPage} = this.props
    if (currentPage < totalPages) {
      this.setNewPage(currentPage + 1)
    }
    if (onNextPage) {
      onNextPage()
    }
  }

  setPage = (page: number) => {
    const {currentPage} = this.state
    const {onGoToPage} = this.props
    if (page !== currentPage) {
      this.setNewPage(page)
    }
    if (onGoToPage) {
      onGoToPage(page - 1)
    }
  }

  render() {
    const {totalPages} = this
    const {children, paginationSize, items} = this.props
    const {currentPage} = this.state
    const pages = Array.from({length: totalPages}, (_, i) => i + 1)
    const paginationOptions = {
      page: currentPage,
      pages: totalPages,
      count: items.length,
      size: paginationSize ?? Pagination.defaultProps.paginationSize,
    }
    return (
      <>
        {children(paginationOptions)}
        <Row>
          <BSPagination>
            <BSPagination.Prev onClick={this.prev} />
            {pages.map((position) => (
              <BSPagination.Item
                key={position}
                onClick={() => this.setPage(position)}
                active={position === currentPage}
              >
                {position}
              </BSPagination.Item>
            ))}
            <BSPagination.Next onClick={this.next} />
          </BSPagination>
        </Row>
      </>
    )
  }
}

export default Pagination
