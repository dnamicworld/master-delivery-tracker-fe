import {TextField, TextFieldProps} from '..'
import {ICON_TYPES} from '../../../../utils/constants'

export interface SearchProps extends TextFieldProps {}

const defaultProps: SearchProps = {
  appendIcon: {
    name: 'search',
    type: ICON_TYPES.fas,
  },
  name: 'search',
}

const Search = ({
  appendIcon,
  placeholder,
  validationState,
  value,
  name,
  onChange,
  iconOnClick,
  onKeyPress,
  ref,
  autoComplete,
  inputRef,
}: SearchProps) => (
  <TextField
    ref={ref}
    value={value}
    name={name}
    onChange={onChange}
    placeholder={placeholder}
    validationState={validationState}
    appendIcon={appendIcon}
    autoComplete={autoComplete}
    iconOnClick={iconOnClick}
    inputRef={inputRef}
    onKeyPress={onKeyPress}
  />
)

Search.defaultProps = defaultProps

export default Search
