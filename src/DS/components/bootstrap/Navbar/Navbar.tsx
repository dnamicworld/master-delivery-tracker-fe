import classNames from 'classnames'
import {ReactNode, useState} from 'react'

import {
  MenuItem,
  Nav,
  Navbar as BSNavbar,
  NavbarProps as BSNavbarProps,
  NavDropdown,
  NavItem,
} from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'

import {Icon} from '../..'
import wmgLogo from '../../../../assets/images/wmg_logo.svg'
import {GLYPHICONS} from '../../../../utils/constants'

import styles from './Navbar.module.scss'

export interface NavItemObject {
  text: string
  href?: string
  subItems?: NavItemObject[]
}

export interface NavbarProps extends BSNavbarProps {
  className?: string
  alwaysMobile?: boolean
  children?: ReactNode
  navItems?: NavItemObject[]
  title: string
}

const defaultProps: Pick<NavbarProps, 'alwaysMobile'> = {
  alwaysMobile: false,
}

const Navbar = ({
  alwaysMobile,
  children,
  className,
  fixedTop,
  navItems,
  title,
}: NavbarProps) => {
  const [isCollapsed, setIsCollapsed] = useState(true)

  const handleClick = () => {
    setIsCollapsed(!isCollapsed)
  }

  const renderItem = (item: NavItemObject, index: number) => {
    if (item.subItems) {
      return (
        <NavDropdown key={index} id={item.text} title={item.text}>
          {item.subItems.map((subItem, subIndex) => {
            const key = `${index}.${subIndex}`
            return (
              <MenuItem key={key} eventKey={key} href={subItem.href}>
                {subItem.text}
              </MenuItem>
            )
          })}
        </NavDropdown>
      )
    }
    return (
      <NavItem eventKey={index} key={index} href={item.href}>
        {item.text}
      </NavItem>
    )
  }

  const hasNavItems = navItems?.length
  return (
    <BSNavbar
      fixedTop={fixedTop}
      className={classNames({alwaysMobile}, className)}
    >
      <BSNavbar.Header
        className={classNames({
          'flex-grow': !children,
        })}
      >
        <BSNavbar.Brand className='p-0 d-flex align-items-center'>
          <img
            className={classNames(styles.navLogo, 'mr-20')}
            src={wmgLogo}
            alt='WMG Logo'
          />
          <LinkContainer to='/#'>
            <a href='/#' className='text-reset text-decoration-none'>
              <h4>{title}</h4>
            </a>
          </LinkContainer>
        </BSNavbar.Brand>
        <BSNavbar.Toggle className={classNames('mb-2')} onClick={handleClick}>
          <Icon
            type='glyphicon'
            className={classNames({'d-none': !hasNavItems})}
            iconName={GLYPHICONS.menuHamburger}
          />
        </BSNavbar.Toggle>
      </BSNavbar.Header>
      {children}
      {hasNavItems ? (
        <BSNavbar.Collapse>
          <Nav
            className={classNames({
              collapse: alwaysMobile && isCollapsed,
            })}
          >
            {navItems.map((item, index) => renderItem(item, index))}
          </Nav>
        </BSNavbar.Collapse>
      ) : null}
    </BSNavbar>
  )
}

Navbar.defaultProps = defaultProps

export default Navbar
