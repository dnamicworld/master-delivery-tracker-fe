import {Story} from '@storybook/react'

import {createElement, ReactNode} from 'react'

export default {
  title: 'Typography',
  args: {
    type: '1',
  },
  argTypes: {
    type: {
      options: ['1', '2', '3', '4', '5'],
      control: {type: 'select'},
    },
  },
}

interface HeadingProps {
  children: ReactNode
  type: string
}
const Template: Story<HeadingProps> = ({children, type}) =>
  createElement(`h${type}`, {}, children)

export const Heading = Template.bind({})

Heading.args = {
  children: 'Heading',
}
