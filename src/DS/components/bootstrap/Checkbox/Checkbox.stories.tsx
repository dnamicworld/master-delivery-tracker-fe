import {Story} from '@storybook/react'

import {useEffect, useState} from 'react'

import {Checkbox, CheckboxProps} from '../index'

export default {
  title: 'Checkbox',
  component: Checkbox,
  args: {
    children: 'Checkbox',
  },
  argTypes: {
    children: {
      control: {type: 'text'},
    },
    checked: {
      control: {type: 'boolean'},
    },
    readOnly: {
      control: {type: 'boolean'},
    },
  },
}

const Template: Story<CheckboxProps> = ({children, checked, ...args}) => {
  const [checkedState, setCheckedState] = useState<boolean>(!!checked)
  useEffect(() => {
    setCheckedState(!!checked)
  }, [checked])
  return (
    <Checkbox
      checked={checkedState}
      /* eslint-disable-next-line react/jsx-props-no-spreading */
      {...args}
      onChange={() => {
        setCheckedState((check) => !check)
      }}
    >
      {children}
    </Checkbox>
  )
}

export const Default = Template.bind({})

Default.args = {}
