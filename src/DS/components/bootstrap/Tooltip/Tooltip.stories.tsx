import {Story} from '@storybook/react'

import {Button, Tooltip, TooltipProps} from '../index'

export default {
  title: 'Tooltip',
  component: Tooltip,
  args: {
    id: 'tooltip',
    tooltipChildren: <p>Tooltip content</p>,
  },
  argTypes: {
    type: {
      options: ['primary', 'warning', 'success', 'danger'],
      control: {type: 'select'},
    },
  },
}

const Template: Story<TooltipProps> = ({children, ...args}) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <Tooltip {...args}>{children}</Tooltip>
)

export const Default = Template.bind({})

Default.args = {
  children: <Button bsStyle='primary'>I can be any element</Button>,
}
