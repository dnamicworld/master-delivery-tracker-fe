import classNames from 'classnames'
import {ReactNode} from 'react'
import {
  Tooltip as BSTooltip,
  TooltipProps as BSTooltipProps,
  OverlayTrigger,
  OverlayTriggerProps,
} from 'react-bootstrap'
import {DefaultComponentStyleTypes} from 'src/utils/constants'

export interface TooltipProps
  extends BSTooltipProps,
    Pick<OverlayTriggerProps, 'placement'> {
  id: string
  children: ReactNode
  tooltipChildren: ReactNode
  className?: string
  type?: DefaultComponentStyleTypes
}

export interface TooltipDisplayProps extends Omit<TooltipProps, 'children'> {
  children?: ReactNode
}

export const TooltipDisplay = ({
  id,
  className,
  children,
  type,
  tooltipChildren,
  ...tooltipProps
}: TooltipDisplayProps) => (
  <BSTooltip
    id={id}
    className={classNames('tooltip', type, className)}
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...tooltipProps}
  >
    {tooltipChildren}
  </BSTooltip>
)

const Tooltip = (tooltipProps: TooltipProps) => {
  const {children, placement = 'top'} = tooltipProps
  return (
    <OverlayTrigger
      placement={placement}
      // eslint-disable-next-line react/jsx-props-no-spreading
      overlay={<TooltipDisplay {...tooltipProps} />}
    >
      {children}
    </OverlayTrigger>
  )
}

export default Tooltip
