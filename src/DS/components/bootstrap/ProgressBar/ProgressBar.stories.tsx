import {Story} from '@storybook/react'
import {ProgressBarProps} from 'react-bootstrap/lib/ProgressBar'

import {ProgressBar} from '..'

export default {
  title: 'ProgressBar',
  component: ProgressBar,
  args: {
    now: 50,
  },
  argTypes: {
    striped: {
      control: {type: 'boolean'},
    },
    active: {
      control: {type: 'boolean'},
    },
    bsStyle: {
      options: ['info', 'success', 'warning', 'danger'],
      control: {type: 'select'},
    },
  },
} as ProgressBarProps

const Template: Story<ProgressBarProps> = ({...args}) => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <ProgressBar {...args} />
)

export const Default = Template.bind({})

Default.args = {}
