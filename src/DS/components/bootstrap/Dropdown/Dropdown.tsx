import classNames from 'classnames'
import {
  ReactElement,
  ReactNode,
  cloneElement,
  SyntheticEvent,
  useRef,
  useEffect,
  MutableRefObject,
} from 'react'

import {DOM} from '../../../../utils/constants'

import styles from './Dropdown.module.scss'

export interface DropdownProps {
  fullWidth?: boolean
  manualToggle?: boolean
  className?: string
  disabled?: boolean
  plain?: boolean
  trigger: ReactElement
  children: ReactNode
  classNameMenu?: string
  activeElement?: MutableRefObject<HTMLInputElement | undefined>
}

const Dropdown = ({
  className,
  disabled,
  plain,
  children,
  fullWidth,
  manualToggle,
  trigger,
  classNameMenu,
  activeElement,
}: DropdownProps) => {
  const dropdownRef = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const body = document.querySelector('body')

    const handleClickOutside = (event: MouseEvent) => {
      const element = event.target as HTMLElement

      if (
        !dropdownRef.current?.contains(element) &&
        !(
          typeof element?.parentElement?.className === 'string' &&
          (element?.parentElement?.className.includes(
            DOM.DATE_PICKER.toLocaleLowerCase(),
          ) ||
            element?.parentElement?.className.includes(
              DOM.CALENDAR.toLocaleLowerCase(),
            ) ||
            element?.className.includes(DOM.DATE_PICKER.toLocaleLowerCase())) &&
          !(document.activeElement === activeElement?.current)
        )
      ) {
        dropdownRef.current?.classList.remove('open')
      }
    }

    if (manualToggle) {
      body?.addEventListener('click', handleClickOutside)
    }
    return () =>
      manualToggle
        ? body?.removeEventListener('click', handleClickOutside)
        : undefined
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [manualToggle])

  let isOpen = dropdownRef.current?.classList.contains('open')

  const handleClick = (event: SyntheticEvent) => {
    event.preventDefault()
    if (manualToggle) {
      const isCurrentlyOpen = dropdownRef.current?.classList.contains('open')
      isOpen = isCurrentlyOpen
      if (isCurrentlyOpen) dropdownRef.current?.classList.remove('open')
      else dropdownRef.current?.classList.add('open')
    }
  }

  return (
    <div
      ref={dropdownRef}
      className={classNames(styles.dropdown, 'dropdown', className, {
        [styles.disabled]: disabled,
        'full-width': fullWidth,
        [styles.plain]: plain,
      })}
    >
      {cloneElement(trigger, {
        'aria-haspopup': 'true',
        'aria-expanded': isOpen,
        'dropdown-toggle': String(!manualToggle),
        'data-toggle': manualToggle ? null : 'dropdown',
        onClick: handleClick,
      })}
      <div
        className={classNames('dropdown-menu m-0', classNameMenu, {
          'full-width': fullWidth,
        })}
      >
        {children}
      </div>
    </div>
  )
}

export default Dropdown
