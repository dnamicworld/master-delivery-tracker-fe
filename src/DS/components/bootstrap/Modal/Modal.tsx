import {
  TButtonColorType,
  TButtonContainerStyle,
} from '@wmg-ae/legato/build/components/Button'
import classNames from 'classnames'
import {ReactNode} from 'react'
import {Modal as BSModal, ModalProps as BSModalProps} from 'react-bootstrap'

import {Button} from '../..'
import {FA_ICONS, ICON_TYPES} from '../../../../utils/constants'
import {isEmpty} from '../../../../utils/helpers'
import {Icon} from '../../custom'

import styles from './Modal.module.scss'

export interface ModalProps extends Omit<BSModalProps, 'content'> {
  show: boolean
  title: string
  children: ReactNode
  cancelAction?: {
    name: string
    action: () => void
    containerStyle?: TButtonContainerStyle
    colorType?: TButtonColorType
  }
  successAction?: {
    name: string
    action: () => void
  }
  vCenter?: boolean
  disabledSuccessButton?: boolean
}
const Modal = ({
  className,
  cancelAction,
  children,
  title,
  onHide,
  show,
  successAction,
  vCenter = true,
  disabledSuccessButton,
}: ModalProps) => {
  const cancelOptions = cancelAction ?? {
    name: 'Cancel',
    action: () => {},
  }
  return (
    <BSModal
      dialogClassName={classNames(className, {
        'modal-dialog-centered': vCenter,
      })}
      show={show}
      onHide={onHide}
      restoreFocus
    >
      <BSModal.Header>
        <div className='d-flex justify-content-between mt-10 ml-20 mr-10'>
          <h3 className='font-weight-sbold d-inline-block m-0'>{title}</h3>
          <Button
            className={classNames(styles.closeButton)}
            containerStyle='link'
            onClick={() => onHide()}
          >
            <Icon iconName={FA_ICONS.close} type={ICON_TYPES.fas} />
          </Button>
        </div>
      </BSModal.Header>

      <BSModal.Body className='ml-10 mb-0'>{children}</BSModal.Body>

      <BSModal.Footer className='mt-0 mr-10'>
        <Button
          className='mr-10'
          bsStyle={cancelAction?.colorType}
          containerStyle={cancelAction?.containerStyle}
          onClick={cancelOptions?.action}
          label={cancelOptions.name}
        />
        {!isEmpty(successAction) ? (
          <Button
            colorType='primary'
            className='ml-10'
            label={successAction?.name}
            onClick={successAction?.action}
            disabled={disabledSuccessButton}
          />
        ) : null}
      </BSModal.Footer>
    </BSModal>
  )
}

export default Modal
