import {Story} from '@storybook/react'

import {useState} from 'react'

import {Button, Modal, ModalProps} from '../index'

export default {
  title: 'Modal',
  component: Modal,
  args: {
    title: 'Title',
    cancelAction: {
      name: 'Cancel',
    },
    successAction: {
      name: 'Ok',
    },
  },
  argTypes: {
    children: {
      control: {type: 'text'},
    },
  },
}

const Template: Story<ModalProps> = ({
  cancelAction,
  children,
  title,
  successAction,
}) => {
  const [show, setShow] = useState(false)
  const handleModal = (newShowValue: boolean) => () => {
    setShow(newShowValue)
  }

  const handleActionClick = () => {
    // eslint-disable-next-line no-alert
    alert('Confirmation action clicked!')
    setShow(false)
  }
  return (
    <>
      <Button label='Open modal' onClick={handleModal(true)} />
      <Modal
        show={show}
        onHide={handleModal(false)}
        cancelAction={{
          name: cancelAction?.name ?? '',
          action: handleModal(false),
        }}
        successAction={{
          name: successAction?.name ?? '',
          action: handleActionClick,
        }}
        title={title}
      >
        <p>{children}</p>
      </Modal>
    </>
  )
}

export const Default = Template.bind({})

Default.args = {}
