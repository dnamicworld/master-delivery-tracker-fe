import {useOktaAuth} from '@okta/okta-react'
import React, {createContext, ReactNode, useEffect} from 'react'

import useLabels from '../hooks/data/useLabels'
import {CurrentUser} from '../models'

export interface UserContextValue {
  user: CurrentUser
  updateUser: (user: CurrentUser) => void
}

export interface UserContextProviderProps {
  children: ReactNode
}

const UserContext = createContext<UserContextValue | undefined>(undefined)

export const UserContextProvider = ({children}: UserContextProviderProps) => {
  const {authState, oktaAuth} = useOktaAuth()
  const {labels} = useLabels()
  const [user, setUser] = React.useState<CurrentUser>(new CurrentUser())
  const updateUser = (updatedUser: CurrentUser) => {
    setUser(updatedUser)
  }

  useEffect(() => {
    let isCancelled = false
    async function handleUser() {
      if (authState?.isAuthenticated && labels) {
        try {
          const userData = await oktaAuth.getUser()
          if (!isCancelled) {
            setUser(new CurrentUser(userData, labels))
          }
        } catch (err) {
          // eslint-disable-next-line no-console
          console.log('OKTA ERROR', err)
        }
      }
    }

    handleUser()

    return () => {
      isCancelled = true
    }
  }, [oktaAuth, authState, labels])
  return (
    <UserContext.Provider value={{user, updateUser}}>
      {children}
    </UserContext.Provider>
  )
}

export {UserContext}
