import React, {createContext, ReactNode, useContext} from 'react'

import {BACK_MODAL} from '../utils/constants'

import {ModalOptions, useModalDisplay} from './modal'

interface OnBackContent
  extends Pick<ModalOptions, 'onActionClick' | 'onCancelAction'> {
  show: boolean
}
export interface HandleOnBackContextValue
  extends Pick<ModalOptions, 'onActionClick' | 'onCancelAction'> {
  onBack: OnBackContent
  setOnBackParams: (
    show: boolean,
    onActionClick?: () => void,
    onCancelAction?: () => void,
  ) => void
}

export interface HandleOnBackContextProviderProps {
  children: ReactNode
}

const HandleOnBackContext = createContext<HandleOnBackContextValue | undefined>(
  undefined,
)

// TODO remove if doesn't work after fixing routing

export const HandleOnBackProvider = ({
  children,
}: HandleOnBackContextProviderProps) => {
  const {displayModal} = useModalDisplay()
  const [onBack, setOnBack] = React.useState<OnBackContent>({
    show: false,
  })

  const setOnBackParams: HandleOnBackContextValue['setOnBackParams'] = (
    show,
    onActionClick,
    onCancelAction,
  ) => {
    setOnBack({
      show,
      onActionClick,
      onCancelAction,
    })
  }

  const {show, onCancelAction, onActionClick} = onBack

  window.addEventListener('popstate', () => {
    if (show) {
      displayModal(BACK_MODAL.body, BACK_MODAL.title, {
        onActionClick,
        onCancelAction,
        actionName: BACK_MODAL.action,
        cancelName: BACK_MODAL.cancel,
      })
    }
  })

  return (
    <HandleOnBackContext.Provider value={{onBack, setOnBackParams}}>
      {children}
    </HandleOnBackContext.Provider>
  )
}
export function useHandleOnBack() {
  const context = useContext(HandleOnBackContext)
  if (context === undefined) {
    throw new Error('useHandleOnBack must be used within a HandleOnBackContext')
  }
  return context
}
export {HandleOnBackContext}
