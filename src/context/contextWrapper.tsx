import {ReactNode} from 'react'

import {AlertContextProvider} from './alert'

import {LoaderContextProvider} from './loader'
import {ModalContextProvider} from './modal'
import {SearchContextProvider} from './search'

const ContextWrapper = ({children}: {children: ReactNode}) => (
  <ModalContextProvider>
    <AlertContextProvider>
      <SearchContextProvider>
        <LoaderContextProvider>{children}</LoaderContextProvider>
      </SearchContextProvider>
    </AlertContextProvider>
  </ModalContextProvider>
)

export default ContextWrapper
