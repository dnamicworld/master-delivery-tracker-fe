import classNames from 'classnames'
import {
  useContext,
  useState,
  createContext,
  Dispatch,
  ReactNode,
  ChangeEvent,
  MouseEvent,
  SetStateAction,
  useCallback,
  cloneElement,
  ReactElement,
  useEffect,
  useMemo,
} from 'react'
import {useHistory} from 'react-router-dom'

import {Button, Chip, Col, Row, SelectOption} from '../../DS/components'
import {
  stringifyQuery,
  cloneDeep,
  get,
  isUndefined,
  set,
  isEqual,
  isEmpty,
  uniquely,
} from '../../utils/helpers'

export type FilterObject = {[key: string]: any}

export interface FiltersContentProps
  extends Pick<PageFilterContextValue, 'handleFilterChange' | 'filters'> {}

export type FilterChangeEvent<T> =
  | KeyboardEvent
  | ChangeEvent<T>
  | MouseEvent<T>

export interface PageFilterContextValue {
  filters: FilterObject
  filtersChanged: boolean
  setFiltersState: <T>(filters: T, initial?: T) => void
  handleFilterChange: <T>(event: FilterChangeEvent<T>) => void
  handleCheckOnEnter: (name: string, filteredOptions: SelectOption[]) => void
  filtersBarContent: ReactNode | undefined
  setFiltersBarContent: Dispatch<SetStateAction<ReactNode | undefined>>
  setFilterMappers: Dispatch<SetStateAction<Object>>
  setWithTags: Dispatch<SetStateAction<boolean>>
  setIgnoreOnTags: Dispatch<SetStateAction<string[]>>
  handleResetAllFilters: () => void
  resetFilters: number
}

export const PageFilterContext = createContext<
  PageFilterContextValue | undefined
>(undefined)

export interface FilterWrapperProps
  extends Pick<
    PageFilterContextValue,
    'filters' | 'handleFilterChange' | 'resetFilters'
  > {
  handleCheckOnEnter?: (name: string, filteredOptions: SelectOption[]) => void
}

interface Tag {
  filter: string
  option: string
  value: string
}

export const PageFilterContextProvider = ({
  children,
}: {
  children: ReactNode
}) => {
  const history = useHistory()
  const [withTags, setWithTags] = useState<boolean>(false)
  const [ignoreOnTags, setIgnoreOnTags] = useState<string[]>([])
  const [filters, setFilters] = useState<FilterObject>({})
  const [filterMappers, setFilterMappers] = useState({})
  const [originalFilters, setOriginalFilters] = useState<FilterObject>({})
  const [filtersBarContent, setFiltersBarContent] = useState<
    ReactNode | undefined
  >(undefined)
  const [resetFilters, setResetFilters] = useState<number>(Date.now())

  const tags: Tag[] = useMemo(
    () =>
      withTags
        ? Object.entries(filters).reduce(
            (tagsArray: Tag[], [filter, options]) => {
              const tagsArrayCopy = cloneDeep(tagsArray)
              if (!ignoreOnTags.includes(filter)) {
                let optionsCopy = []
                if (Array.isArray(options)) {
                  optionsCopy = options
                } else if (!isEmpty(options)) {
                  optionsCopy = [options]
                }
                const newTags = optionsCopy.map((option: string) => ({
                  filter,
                  option,
                  value: filterMappers[filter]
                    ? filterMappers[filter][option]
                    : option,
                }))
                return tagsArrayCopy.concat(newTags)
              }
              return tagsArrayCopy
            },
            [],
          )
        : [],
    [withTags, filters, ignoreOnTags, filterMappers],
  )

  const filtersChanged: boolean = useMemo(
    () => !isEqual(filters, originalFilters),
    [filters, originalFilters],
  )

  const setQueryParams = useCallback(
    (filterObject: FilterObject) => {
      history.push({
        search: stringifyQuery(filterObject),
      })
    },
    [history],
  )

  useEffect(() => {
    if (!isEmpty(filters)) {
      setQueryParams(filters)
    }
  }, [setQueryParams, filters])

  const handleFilterChange: PageFilterContextValue['handleFilterChange'] =
    useCallback(
      (event) => {
        const currentEvent = (event.currentTarget ||
          event.target) as HTMLInputElement
        if (currentEvent) {
          const {value, name, checked, multiple} = currentEvent
          setFilters((oldFilters) => {
            const stateCopy = cloneDeep(oldFilters)
            const elementToUpdate = get(stateCopy, name ?? '')
            if (multiple) {
              const findIdx = elementToUpdate?.indexOf(value)
              if (checked) {
                elementToUpdate?.push(value)
              } else {
                elementToUpdate?.splice(findIdx, 1)
              }
            } else {
              set(stateCopy, name ?? '', value)
            }
            return stateCopy
          })
        }
      },
      [setFilters],
    )

  const handleCheckOnEnter: PageFilterContextValue['handleCheckOnEnter'] =
    useCallback(
      (name: string, filteredOptions: SelectOption[]) => {
        setFilters((oldFilters) => {
          const stateCopy = cloneDeep(oldFilters)
          const elementToUpdate = get(stateCopy, name)
          filteredOptions.forEach((option) => {
            const findIdx = elementToUpdate?.indexOf(option.value)
            if (findIdx === -1) {
              elementToUpdate?.push(option.value)
            }
          })
          return stateCopy
        })
      },
      [setFilters],
    )

  const setFiltersState = useCallback(
    (newFilters: FilterObject, initialFilters?: FilterObject) => {
      if (initialFilters) {
        setOriginalFilters(initialFilters)
      } else if (isEmpty(filters)) {
        setOriginalFilters(newFilters)
      }
      setFilters(newFilters)
    },
    [filters, setFilters, setOriginalFilters],
  )

  const handleResetAllFilters = useCallback(() => {
    setResetFilters(Date.now())
    setFilters(cloneDeep(originalFilters))
  }, [originalFilters, setFilters])
  const FilterComponent = filtersBarContent
    ? cloneElement(filtersBarContent as ReactElement, {handleFilterChange})
    : undefined

  const removeTag = (tag: Tag) => {
    const filtersCopy = cloneDeep(filters)
    let tagFilter = filtersCopy[tag.filter]
    tagFilter = Array.isArray(tagFilter)
      ? tagFilter.filter((item: string) => item !== tag.option)
      : ''
    filtersCopy[tag.filter] = tagFilter
    setFilters(filtersCopy)
  }

  return (
    <PageFilterContext.Provider
      value={{
        filters,
        filtersChanged,
        filtersBarContent,
        setFiltersState,
        handleFilterChange,
        handleCheckOnEnter,
        setFiltersBarContent,
        setFilterMappers,
        setWithTags,
        setIgnoreOnTags,
        handleResetAllFilters,
        resetFilters,
      }}
    >
      {FilterComponent ? (
        <Row className='filtersBarContent mb-40'>
          <Col sm={12} className='filtersBarContent__container'>
            {cloneElement(FilterComponent, {handleFilterChange})}
            <div className='actions'>
              <Button
                containerStyle='link'
                className={classNames({
                  'd-none': !filtersChanged || withTags,
                })}
                onClick={handleResetAllFilters}
              >
                Reset All Filters
              </Button>
            </div>
          </Col>
          {withTags && tags.length ? (
            <Col sm={12}>
              {tags.map((tag) => (
                <Chip
                  key={uniquely()}
                  className='mt-20 mr-20'
                  content={tag.value}
                  iconClick={() => removeTag(tag)}
                />
              ))}
            </Col>
          ) : null}
        </Row>
      ) : null}
      <Row>
        <Col sm={12}>{children}</Col>
      </Row>
    </PageFilterContext.Provider>
  )
}

export function usePageFilterContext() {
  const context = useContext(PageFilterContext)
  if (isUndefined(context)) {
    throw new Error(
      'usePageFilterContext should be used within PageFilterContext',
    )
  }
  return context
}
