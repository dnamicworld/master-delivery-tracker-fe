import {
  useContext,
  useState,
  createContext,
  Dispatch,
  cloneElement,
  ReactElement,
  SetStateAction,
  useEffect,
  ReactNode,
  useCallback,
} from 'react'

import {useParams} from 'react-router-dom'

import {mutate} from 'swr'

import {appFetcher} from '../../config/axios'
import useProducts from '../../hooks/data/useProducts'
import useReports from '../../hooks/data/useReports'
import {Product, Report} from '../../models'
import {
  filterProducts,
  ProductIndexRouteParams,
} from '../../pages/shared/IndexPages'
import {
  API_ROUTES,
  ApiMethods,
  DISCARD_INDEX_MODAL,
} from '../../utils/constants'
import {
  cloneDeep,
  get,
  isUndefined,
  isEmpty,
  isEqual,
  differenceWith,
  productToProductPostOrPatch,
} from '../../utils/helpers'
import {useAlertDisplay} from '../alert'

import {useModalDisplay} from '../modal'

import {PageFilterContextValue, usePageFilterContext} from './pageFilters'

export interface FiltersContentProps
  extends Pick<ProductFilterContextValue, 'filters'> {}

export interface ProductFilterContextValue
  extends Pick<PageFilterContextValue, 'filters' | 'setFiltersBarContent'> {
  report: Report | undefined
  filteredProducts: Product[]
  products: Product[]
  isEditing: boolean
  noChanges: boolean
  showPlaceholder: boolean
  setNoChanges: Dispatch<SetStateAction<boolean>>
  setFilteredProducts: Dispatch<SetStateAction<Product[]>>
  setIsEditing: Dispatch<SetStateAction<boolean>>
  handleCancel: () => void
  handleSave: () => void
}

export const ProductFilterContext = createContext<
  ProductFilterContextValue | undefined
>(undefined)

export type FilterWrapperProps = Pick<ProductFilterContextValue, 'filters'> &
  Pick<PageFilterContextValue, 'handleFilterChange' | 'resetFilters'>

const LOADING_TIME_OUT = 2000

export const ProductFilterContextProvider = ({
  children,
}: {
  children: ReactNode
}) => {
  const {filters, setFiltersBarContent} = usePageFilterContext()
  const {labelId} = useParams<ProductIndexRouteParams>()
  const [noChanges, setNoChanges] = useState(true)
  const [initialProducts, setInitialProducts] = useState<Product[]>([])
  const [stateProducts, setProducts] = useState<Product[]>([])
  const [report, setReport] = useState<Report | undefined>(undefined)
  const [filteredProducts, setFilteredProducts] = useState<Product[]>([])
  const [isEditing, setIsEditing] = useState<boolean>(false)
  const [showPlaceholder, setShowPlaceholder] = useState(true)
  const {displayModal} = useModalDisplay()
  const {display} = useAlertDisplay()

  const {reports, isLoading: isLoadingReports} = useReports(
    {
      years: filters.year,
      labelIds: [labelId],
      releaseTypes: filters.releaseType,
    },
    () => filters.year && labelId && !isEmpty(filters.releaseType),
  )

  const {products, isLoading: isLoadingProducts} = useProducts(
    {
      reportId:
        report && report?.id !== undefined ? String(report?.id) : undefined,
    },
    () => Boolean(report !== undefined && report?.id),
  )

  useEffect(() => {
    setReport(undefined)
    setInitialProducts([])
    setProducts([])
  }, [filters.year])

  useEffect(() => {
    let timer: NodeJS.Timeout
    if (isLoadingReports || isLoadingProducts) {
      setShowPlaceholder(true)
    } else {
      timer = setTimeout(() => setShowPlaceholder(false), LOADING_TIME_OUT)
    }
    return () => clearTimeout(timer)
  }, [isLoadingReports, isLoadingProducts])

  useEffect(() => {
    // The endpoint we use to get the report on ALBUM / SINGLE index retrieves an array of reports,
    // even when the result is just one report, that's why we need to get the report from an array here
    const reportToSet = get(reports, '[0]')
    if (reportToSet) setReport(reportToSet)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(reports)])

  useEffect(() => {
    const productsToSet = Array.isArray(products) ? cloneDeep(products) : []
    setProducts(productsToSet)
    setInitialProducts(productsToSet)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(products)])

  useEffect(() => {
    const filtersToSet = filterProducts(filters, stateProducts)
    setFilteredProducts(filtersToSet)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(filters), stateProducts])

  useEffect(() => {
    setFiltersBarContent((content) =>
      content
        ? cloneElement(content as ReactElement, {
            filters,
            products: stateProducts,
            report,
          })
        : undefined,
    )
  }, [stateProducts, report, filters, setFiltersBarContent])

  const handleCancel = useCallback(() => {
    if (noChanges) {
      setIsEditing(false)
    } else {
      const onFormCancelSuccess = () => {
        setIsEditing(false)
        const filtersToSet = filterProducts(filters, stateProducts)
        setFilteredProducts(filtersToSet)
      }
      displayModal(DISCARD_INDEX_MODAL.body, DISCARD_INDEX_MODAL.title, {
        actionName: DISCARD_INDEX_MODAL.cancel,
        onActionClick: onFormCancelSuccess,
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [noChanges, filters, stateProducts])

  const handleSave = async () => {
    const productsToUpdate = differenceWith(
      filteredProducts,
      initialProducts,
      isEqual,
    )

    try {
      const operations: Promise<Response>[] = []

      productsToUpdate.forEach((product) => {
        const payload = productToProductPostOrPatch(product)
        const URL = `${API_ROUTES.PRODUCTS}/${product.id}`
        operations.push(
          appFetcher(URL, {
            method: ApiMethods.PATCH,
            data: payload,
          }),
        )
      })

      await Promise.all(operations).then((responses) => {
        responses.forEach((response) => {
          mutate(response.url, response)
        })
      })

      const productsToSet = initialProducts.map((product) => {
        const productCopy = cloneDeep(product)
        const foundIndex = productsToUpdate.findIndex(
          (updated) => updated.id === productCopy.id,
        )
        return foundIndex !== -1 ? productsToUpdate[foundIndex] : productCopy
      })
      setInitialProducts(productsToSet) // TODO this has to me removed and update with mutate state instead
      setIsEditing(false)
      display('Report Saved')
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('ERROR SAVING...', err)
      display('Error Saving the report', 'warning')
    }
  }

  return (
    <ProductFilterContext.Provider
      value={{
        filters,
        filteredProducts,
        products: stateProducts,
        report,
        isEditing,
        showPlaceholder,
        setFiltersBarContent,
        setFilteredProducts,
        setIsEditing,
        handleCancel,
        handleSave,
        noChanges,
        setNoChanges,
      }}
    >
      {children}
    </ProductFilterContext.Provider>
  )
}

export function useProductFilterContext() {
  const context = useContext(ProductFilterContext)
  if (isUndefined(context)) {
    throw new Error(
      'useProductFilterContext should be used within FilterContextProvider',
    )
  }
  return context
}
