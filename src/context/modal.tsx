import {
  TButtonColorType,
  TButtonContainerStyle,
} from '@wmg-ae/legato/build/components/Button'
import * as React from 'react'
import {ReactNode, useContext} from 'react'

import {ButtonProps, Modal} from '../DS/components'
import {MESSAGES} from '../utils/constants'

interface CancelButtonStyle {
  containerStyle: TButtonContainerStyle
  colorType: TButtonColorType
}
export interface ModalOptions {
  /** Text to be used on modal's action button */
  actionName?: string
  /** Text to be used on modal's cancel button */
  cancelName?: string
  /** Event triggered when modal's action button is clicked */
  onActionClick?: () => void
  /** Event triggered on modal's cancel action */
  onCancelAction?: () => void
  /**
   * Modal's action button variant.
   * Possible values are 'destructive' | 'outline' | 'primary' (default) | 'standard' | 'workflowAction'.
   */
  actionVariant?: ButtonProps['bsStyle']
  vCenter?: boolean
  cancelButtonStyle?: CancelButtonStyle
}

export type ModalBody = string | ReactNode
export type DisplayModal = (
  content: ModalBody,
  title?: string,
  options?: ModalOptions,
) => void

export interface ModalContext {
  displayModal: DisplayModal
  updateModalContent: (content: ModalBody) => void
}

/**
 * React Context that makes accessible to any component in the application
 * a function to display a modal window using DS *Dialog* component. Avoid using this context directly,
 * instead, use {@link useModalDisplay} React Hook to make code cleaner
 */
export const ModalDisplayContext = React.createContext<
  ModalContext | undefined
>(undefined)

type ModalContent = {
  title?: string
} & ModalOptions

/**
 * React Component that exposes a {@link ModalDisplayContext} provider. This Provider in included inside
 * {@link CoreProvider} so in rare scenarios we would need to place this component in our apps.
 */
export const ModalContextProvider = ({children}: {children: JSX.Element}) => {
  const [modalContent, setModalContent] = React.useState<ModalContent | null>(
    null,
  )
  const [modalBody, setModalBody] = React.useState<ModalBody>(null)

  const displayModal = (
    content: ModalBody,
    title?: string,
    options: ModalOptions = {},
  ) => {
    const optionsToSet = {vCenter: true, ...options}
    setModalContent({
      title,
      ...optionsToSet,
    })
    setModalBody(content)
  }

  const updateModalContent = (content: ModalBody) => {
    setModalBody(content)
  }

  const handleActionClick = () => {
    if (modalContent?.onActionClick) {
      modalContent.onActionClick()
    }
    setModalContent(null)
  }

  const handleClose = () => {
    if (modalContent?.onCancelAction) {
      modalContent.onCancelAction()
    }
    setModalContent(null)
  }

  return (
    <ModalDisplayContext.Provider value={{displayModal, updateModalContent}}>
      {children}
      {modalContent && (
        <Modal
          vCenter={modalContent?.vCenter}
          show
          title={modalContent?.title ?? ''}
          onHide={handleClose}
          cancelAction={{
            name: modalContent?.cancelName ?? MESSAGES.CANCEL,
            action: handleClose,
            containerStyle: modalContent.cancelButtonStyle?.containerStyle,
            colorType: modalContent.cancelButtonStyle?.colorType,
          }}
          successAction={{
            name: modalContent?.actionName ?? MESSAGES.OK,
            action: handleActionClick,
          }}
        >
          {modalBody}
        </Modal>
      )}
    </ModalDisplayContext.Provider>
  )
}

/**
 * React Hook to make easier using {@link ModalDisplayContext}
 * @example
 * ```js
 * import {useModalDisplay} from '@casecommons/cbp-core-typescript'
 * import React from 'react'
 *
 * const Component = () => {
 *  const displayModel = useModalDisplay()
 *
 *  const clickHandler = () => {
 *    displayModal(
 *      'Are you sure you want to delete the item?',
 *      'Item deletion',
 *      {
 *         actionName: 'Delete",
 *         actionVariant: 'destructive',
 *      },
 *    )
 *  }
 *  return <button onClick={clickHandler}>Delete</button>
 * }
 * ```
 */
export function useModalDisplay() {
  const context = useContext(ModalDisplayContext)
  if (context === undefined) {
    throw new Error(
      'useModalsDisplay must be used within a ModalDisplayContext',
    )
  }
  return context
}
