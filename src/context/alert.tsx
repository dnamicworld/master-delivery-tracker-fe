import {useGrowl} from '@wmg-ae/legato'
import * as React from 'react'

import {ReactNode, useContext} from 'react'

import {ECommonFunctionalType, TGrowlColorType} from '../utils/constants'

export type DisplayAlert = {
  display: (title: string, colorType?: TGrowlColorType) => void
}
const values = {
  destroyOnClick: true,
  placement: 'top',
  duration: 5,
  className: 'margin-from-navbar',
}
/**
 * React Context that makes accessible to any component in the application
 * a function to display a loader on the screen
 */
export const DisplayAlertContext = React.createContext<
  DisplayAlert | undefined
>(undefined)

export const AlertContextProvider = ({children}: {children: ReactNode}) => {
  const [growl, contextHolder] = useGrowl({closable: true})

  const display = (title: string, colorType?: TGrowlColorType) =>
    growl.open({
      message: title,
      colorType: colorType || ECommonFunctionalType.SUCCESS,
      ...values,
    })

  return (
    <DisplayAlertContext.Provider value={{display}}>
      {children}
      {contextHolder}
    </DisplayAlertContext.Provider>
  )
}

export function useAlertDisplay() {
  const context = useContext(DisplayAlertContext)
  if (context === undefined) {
    throw new Error('useAlertDisplay must be used within a DisplayAlertContext')
  }
  return context
}
