import {
  createContext,
  Dispatch,
  ReactNode,
  SetStateAction,
  useContext,
  useState,
} from 'react'

import {CreateReportModal} from '../components/AppNavbar/CreateReportModal'
import {CreateReportModalProps} from '../components/AppNavbar/CreateReportModal/CreateReportModal'

export interface CreateReportContextValue {
  showReportModal: boolean
  setShowReportModal: Dispatch<SetStateAction<boolean>>
  setSuccessAction: (cb: CreateReportModalProps['onSuccess']) => void
}

export interface CreateReportContextProviderProps {
  children: ReactNode
}

const CreateReportContext = createContext<CreateReportContextValue | undefined>(
  undefined,
)

export const CreateReportProvider = ({
  children,
}: CreateReportContextProviderProps) => {
  const [showReportModal, setShowReportModal] = useState(false)
  const [onSuccess, setOnSuccess] =
    useState<CreateReportModalProps['onSuccess']>()
  const setSuccessAction = (cb: CreateReportModalProps['onSuccess']) => {
    setOnSuccess(() => cb)
  }
  return (
    <>
      <CreateReportContext.Provider
        value={{showReportModal, setShowReportModal, setSuccessAction}}
      >
        {children}
      </CreateReportContext.Provider>
      <CreateReportModal
        shown={showReportModal}
        onSuccess={onSuccess}
        onClose={() => {
          setShowReportModal(false)
        }}
      />
    </>
  )
}

export function useCreateReport() {
  const context = useContext(CreateReportContext)
  if (context === undefined) {
    throw new Error('useCreateReport must be used within a CreateReportContext')
  }
  return context
}
