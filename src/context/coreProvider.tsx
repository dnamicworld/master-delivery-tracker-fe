import {
  createContext,
  Dispatch,
  ReactElement,
  ReactNode,
  SetStateAction,
  useContext,
  useState,
} from 'react'

import {Breadcrumb, BreadcrumbItem, Col, Row} from '../DS/components'
import {isUndefined} from '../utils/helpers'

export interface AppContextValue {
  setBreadcrumbs: (breadcrumbs: BreadcrumbItem[]) => void
  setTitle: (title: ReactElement) => void
  setActionContent: Dispatch<SetStateAction<ReactNode>>
}

export const CoreProvider = createContext<AppContextValue | undefined>(
  undefined,
)

export const AppContextProvider = ({children}: {children: ReactNode}) => {
  const [title, setTitle] = useState<ReactElement>()
  const [breadcrumbs, setBreadcrumbs] = useState<BreadcrumbItem[]>([])
  const [actionContent, setActionContent] = useState<ReactNode | undefined>(
    undefined,
  )

  return (
    <CoreProvider.Provider
      value={{
        setBreadcrumbs,
        setTitle,
        setActionContent,
      }}
    >
      {breadcrumbs.length ? (
        <Row className='mt-15'>
          <Col sm={12}>
            <Breadcrumb items={breadcrumbs} />
          </Col>
        </Row>
      ) : null}
      {title ? (
        <Row className='my-40'>
          <Col sm={10}>
            <h2 className='font-weight-sbold m-0'>{title}</h2>
          </Col>
          {actionContent ? (
            <Col sm={2} className='text-right'>
              {actionContent}
            </Col>
          ) : null}
        </Row>
      ) : null}
      {children}
    </CoreProvider.Provider>
  )
}

export function useAppContext() {
  const context = useContext(CoreProvider)
  if (isUndefined(context)) {
    throw new Error('useAppContext should be used within AppContextProvider')
  }
  return context
}
