import * as React from 'react'

import {ReactNode, useContext} from 'react'

import {Loader} from '../DS/components'

export type DisplayLoader = {
  /** Show the loader on screen */
  loaderDisplayed: boolean
  displayLoader: (display: boolean) => void
}

/**
 * React Context that makes accessible to any component in the application
 * a function to display a loader on the screen
 */
export const LoaderDisplayContext = React.createContext<
  DisplayLoader | undefined
>(undefined)

/**
 * React Component that exposes a {@link LoaderDisplayContext} provider. This Provider in included inside
 * {@link CoreProvider} so in rare scenarios we would need to place this component in our apps.
 */
export const LoaderContextProvider = ({children}: {children: ReactNode}) => {
  const [loaderDisplayed, setLoaderDisplayed] = React.useState<boolean>(false)

  const displayLoader = (display: boolean) => setLoaderDisplayed(display)

  return (
    <LoaderDisplayContext.Provider value={{loaderDisplayed, displayLoader}}>
      {children}
      {loaderDisplayed ? <Loader /> : null}
    </LoaderDisplayContext.Provider>
  )
}

/**
 * React Hook to make easier using {@link LoaderDisplayContext}
 * @example
 * ```js
 * import {useLoaderDisplay} from '@casecommons/cbp-core-typescript'
 * import React from 'react'
 *
 * const Component = () => {
 *  const {loaderDisplayed, displayLoader} = useLoaderDisplay()
 *
 *  const handler = () => {
 *    displayLoader(true)
 *  }
 *
 * return (!loaderDisplayed ? Component : null)
 * }
 * ```
 */
export function useLoaderDisplay() {
  const context = useContext(LoaderDisplayContext)
  if (context === undefined) {
    throw new Error(
      'useLoaderDisplay must be used within a LoaderDisplayContext',
    )
  }
  return context
}
