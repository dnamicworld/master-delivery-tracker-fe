import {CurrentUser, Label} from '../../models'
import {ROLES} from '../../utils/constants'

import {isEmpty} from '../../utils/helpers'

class Roles {
  roles: string[]

  constructor(user: CurrentUser) {
    this.roles = user.groups ?? []
  }

  hasRole(role: string): boolean {
    return this.roles.includes(role) ?? false
  }

  hasAnyRole(roles: string[]): boolean {
    if (!isEmpty(this.roles)) {
      return roles.reduce(
        (prev: boolean, role) => (!prev ? this.roles.includes(role) : prev),
        false,
      )
    }
    return false
  }

  hasFullOrPartialReadAccess(): boolean {
    return this.hasFullReadAccess() || this.hasPartialReadAccess()
  }

  hasLabel(labels: Label[]): Label[] {
    return labels.reduce((prev: Label[], label) => {
      if (this.roles.includes(`${ROLES.ROLE_PREFIX}${label.slug}`)) {
        prev.push(label)
      }
      return prev
    }, [])
  }

  hasFullOrPartialWriteAccess(): boolean {
    return this.hasFullWriteAccess() || this.hasPartialWriteAccess()
  }

  hasPartialReadOrWriteAccess(): boolean {
    return this.hasPartialReadAccess() || this.hasPartialWriteAccess()
  }

  hasFullReadAccess(): boolean {
    return this.hasRole(ROLES.ADMIN_ROLE)
  }

  hasPartialReadAccess(): boolean {
    return this.hasRole(ROLES.READ_ONLY_ROLE)
  }

  hasFullWriteAccess(): boolean {
    return this.hasAnyRole([ROLES.ADMIN_ROLE, ROLES.FULL_WRITE_ROLE])
  }

  hasPartialWriteAccess(): boolean {
    return this.hasRole(ROLES.LIMITED_WRITE_ROLE)
  }
}

export default Roles
