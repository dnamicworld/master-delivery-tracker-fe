import React, {createContext, ReactNode, useContext, useRef} from 'react'

export interface SearchContextValue {
  showSearch: boolean
  updateSearch: (user: boolean) => void
  handleClick: () => void
  ref: React.MutableRefObject<HTMLInputElement | undefined>
}

export interface SearchContextProviderProps {
  children: ReactNode
}

const SearchContext = createContext<SearchContextValue | undefined>(undefined)

export const SearchContextProvider = ({
  children,
}: SearchContextProviderProps) => {
  const [showSearch, setSearch] = React.useState<boolean>(false)
  const ref = useRef<HTMLInputElement>()

  const handleClick = () => {
    if (!showSearch) {
      window.scrollTo(0, 0)
      setTimeout(() => {
        if (ref.current) {
          ref.current.focus()
        }
      }, 1)
    }
  }
  const updateSearch = (updatedUser: boolean) => {
    setSearch(updatedUser)
  }

  return (
    <SearchContext.Provider
      value={{showSearch, updateSearch, handleClick, ref}}
    >
      {children}
    </SearchContext.Provider>
  )
}
export function useSearch() {
  const context = useContext(SearchContext)
  if (context === undefined) {
    throw new Error('useSearch must be used within a SearchContext')
  }
  return context
}
export {SearchContext}
