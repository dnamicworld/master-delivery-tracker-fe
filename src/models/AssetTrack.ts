import {Product, Label, Track, Report} from '.'

export interface AssetTrack
  extends Pick<
      Product,
      'marketingLabel' | Required<'artist'> | Required<'productType'>
    >,
    Pick<Label, Required<'name'>>,
    Pick<Track, Required<'title'> | Required<'id'>>,
    Pick<Report, Required<'year'>> {
  assetType: string
  deliveryStatus: string
  sampleRate: number
  bitDepth: number
  selection: string
  project: string
  assetTypeId: number
  labelId: number
  productId: number
}
