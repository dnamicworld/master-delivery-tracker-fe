import {PaginationResponse} from '../hooks/data/models'

export interface PaginationMutateResponse<T> extends PaginationResponse {
  data: T[]
}
