import {AppColors} from '../utils/constants'

import {DeliveryStatusActions} from './Product'

export interface AssetOption {
  color: keyof typeof AppColors
  tooltip?: string
  display: string
  deliveryStatus: DeliveryStatusActions
  sampleRate: number | null
  bitDepth: number | null
  resolution?: string
  isDefault?: boolean
}
