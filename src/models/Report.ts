import {AssetType} from './AssetType'
import {Actions, DBField} from './GeneralFields'
import {Label} from './Label'
import {Stats} from './Stats'

export enum ListViewType {
  ALBUM = 0,
  SINGLE = 1,
  EP = 0,
}

export interface ReportAssetType extends Omit<AssetType, 'excludeFromTotal'> {
  sortOrder: number
}

export interface Report extends DBField {
  id: number
  label: Label
  year: number
  title: string
  listViewType: keyof typeof ListViewType
  barColor: string
  stats?: Stats
  assetTypes?: ReportAssetType[]
}

export interface ReportFilters {
  year: string[]
  assetType: string[]
  releaseType: string[]
  label?: string[]
}

export interface ReportPostAssetType
  extends Pick<ReportAssetType, 'id' | 'sortOrder'> {}

export interface ReportPatchAssetType extends ReportPostAssetType {
  action?: Actions
}

export interface ReportPost
  extends Omit<Report, 'id' | 'label' | 'stats' | 'assetTypes'> {
  assetTypes?: ReportPostAssetType[]
  labelId: number
}

export interface ReportPatch extends Partial<Omit<ReportPost, 'assetTypes'>> {
  assetTypes?: ReportPatchAssetType[]
}
