export interface DBField {
  createdAt?: string
  updatedAt?: string
}

export type Actions = 'CREATE' | 'UPDATE' | 'DELETE'
