import {UserClaims} from '@okta/okta-auth-js'

import Roles from '../context/models/Roles'
import {ROUTES} from '../utils/constants'

import {Label} from './Label'

export class CurrentUser {
  public readonly firstName: string = ''

  public readonly lastName: string = ''

  public readonly email: string = ''

  public readonly groups?: string[]

  public roles: Roles

  public readonly labels?: Label | Label[]

  public get name(): string {
    const toString = (str?: string | null) => (str ? str.trim() : '')
    return `${toString(this.firstName)} ${toString(this.lastName)}`.trim()
  }

  public get home(): string {
    const userLabels = this.roles.hasLabel(
      Array.isArray(this.labels) ? this.labels : [],
    )
    const checkWrite = this.roles.hasFullOrPartialWriteAccess()

    const homeRedirect =
      this.roles.hasFullOrPartialReadAccess() ||
      (checkWrite && userLabels.length > 1)
    const labelRedirect = checkWrite && userLabels.length

    let redirectUrl = ROUTES.UNAUTHORIZED

    if (homeRedirect) {
      redirectUrl = ROUTES.HOME
    } else if (labelRedirect) {
      redirectUrl = `${ROUTES.LABEL}/${userLabels[0].id}`
    }
    return redirectUrl
  }

  constructor(info?: UserClaims, labels?: Label | Label[]) {
    this.email = info?.email ?? ''
    this.firstName = info?.given_name ?? ''
    this.lastName = info?.family_name ?? ''
    this.groups = info?.groups
    this.roles = new Roles(this)
    this.labels = labels
  }
}
