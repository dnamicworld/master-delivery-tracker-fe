import {AssetTypeStat} from './AssetType'

export interface Stats {
  assetTypeStats: AssetTypeStat[]
  deliveryCount: number
  expectedCount: number
  deliveryPercent: number
}
