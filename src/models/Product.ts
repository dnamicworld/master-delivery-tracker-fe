import {Actions, DBField} from './GeneralFields'
import {Report} from './Report'
import {Stats} from './Stats'
import {Track} from './Track'

export enum ProductType {
  ALBUM = 0,
  SINGLE = 1,
  EP = 0,
}

export interface Product extends DBField {
  id?: number
  reportId?: number
  report?: Report
  productType: keyof typeof ProductType
  artist?: string
  title?: string
  upc?: string | null
  selectionNumber?: string | null
  releaseDate?: string | null
  adminName?: string | null
  producer?: string | null
  mixer?: string | null
  mastering?: string | null
  marketingLabel?: string | null
  arComments?: string | null
  complete?: boolean
  updatedBy?: string | null
  stats?: Stats
  tracks?: ProductTrack[] | null
  coverArtLink?: string | null
}

export enum DeliveryStatusActions {
  DELIVERED = 'DELIVERED',
  MISSING = 'MISSING',
  NOT_REQUIRED = 'NOT_REQUIRED',
}
export interface ProductTrack extends Omit<Track, 'product'> {
  assets: ProductTrackAsset[]
}

export interface ProductTrackAsset {
  assetTypeId: number
  assetType?: string
  deliveryStatus: DeliveryStatusActions
  sampleRate: number | null
  bitDepth: number | null
  selection: string | null
  action?: Actions
}

export interface ProductPatchTrackAsset
  extends Omit<ProductTrackAsset, 'assetType'> {}

export interface ProductPost
  extends Partial<
    Omit<
      Product,
      | 'id'
      | 'artist'
      | 'title'
      | 'report'
      | 'stats'
      | 'createdAt'
      | 'updatedAt'
      | 'reportId'
    >
  > {
  reportId: number
  artist: string
  title: string
  selectionNumber?: string
}

interface ProductBulk extends Omit<ProductPost, 'tracks'> {
  tracks: Omit<ProductTrack, 'assets'>
}

export interface ProductBulkPost {
  products: ProductBulk[]
}

export interface ProductPatch extends ProductPost {}

export interface ProductFilters {
  releaseType: string
  subLabel: string[]
  artist: string[]
  year: string
  releaseDate: string
  delivered: string[]
}

export interface ProductSearch extends Product {
  trackTitle: string
}

export interface ProductValidationResponse {
  artistTitleExists: boolean
  upcExists: boolean
}
