import {AssetType} from './AssetType'
import {DBField} from './GeneralFields'
import {DeliveryStatusActions} from './Product'
import {Track} from './Track'

export interface AssetDelivery extends DBField {
  track: Track
  asset_type: AssetType
  delivery_status: DeliveryStatusActions
  sample_rate?: number
  bit_depth?: number
}
