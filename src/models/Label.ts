import {DBField} from './GeneralFields'

export interface Label extends DBField {
  id: number
  name: string
  slug: string
  logo: {
    url: string
    width: number
    height: number
  } | null
}

export interface LabelPost extends Omit<Label, 'id' | 'logo'> {}

export interface LabelPatch extends LabelPost {}
