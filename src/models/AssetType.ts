import {AssetOption} from './AssetOption'
import {DBField} from './GeneralFields'

export interface AssetType extends DBField {
  id: number
  shortName: string
  longName: string
  optional: boolean
  excludeFromTotal: boolean
  assetOptions: AssetOption[]
  defaultValue: string
}

export interface AssetTypeStat extends AssetType {
  deliveryCount: number
  expectedCount: number
  deliveryPercent: number
}

export interface AssetTypePost
  extends Omit<AssetType, 'id' | 'excludeFromTotal'> {
  excludeFromTotal?: boolean
}

export interface AssetTypePut extends AssetTypePost {}
