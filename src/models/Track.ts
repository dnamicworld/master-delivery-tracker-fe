import {DBField, Actions} from './GeneralFields'
import {Product} from './Product'

export interface Track extends DBField {
  id?: number
  product: Product
  trackNumber: number
  title: string
  isrc: string | null
  comments: string | null
  version: string | null
  majorGenre: string | null
  marketingLabel: string | null
  complete: boolean
  action?: Actions
}
