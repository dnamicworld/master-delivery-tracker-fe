import {toInt} from './helpers'

const sortResponse = (objectA: any, objectB: any) => {
  if (objectA === objectB) return 0
  return objectA < objectB ? -1 : 1
}

export const comparatorAgGridArtist = (
  _valueA: any,
  _valueB: any,
  nodeA: any,
  nodeB: any,
) => {
  const aCopy = nodeA.data.artist ? nodeA.data.artist.toLowerCase() : ''
  const bCopy = nodeB.data.artist ? nodeB.data.artist.toLowerCase() : ''
  return sortResponse(aCopy, bCopy)
}

export const sortDefaultWMG = (valueA: string, valueB: string) => {
  const aCopy = valueA ? valueA.toLowerCase() : ''
  const bCopy = valueB ? valueB.toLowerCase() : ''
  return sortResponse(aCopy, bCopy)
}

export const sortDates = (valueA: string, valueB: string) => {
  const checkValueA = valueA === ''
  const checkValueB = valueB === ''
  if (checkValueA && checkValueB) {
    return 0
  }
  if (checkValueA) {
    return -1
  }
  if (checkValueB) {
    return 1
  }
  const aCopy = new Date(valueA)
  const bCopy = new Date(valueB)
  return sortResponse(aCopy, bCopy)
}

export const sortPercentages = (valueA: any, valueB: any) => {
  const aCopy = toInt(valueA.replace('%'))
  const bCopy = toInt(valueB.replace('%'))
  return sortResponse(aCopy, bCopy)
}

export const sortFilterDatesDescending = (a: string, b: string) =>
  toInt(b) - toInt(a)
