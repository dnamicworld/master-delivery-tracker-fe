import {cloneDeep, flatten, get, groupBy, isAlbum, set, values} from '..'
import {FilterObject} from '../../../context/Filters/pageFilters'
import {AssetType, Label, Report, ReportFilters} from '../../../models'

export interface ReportCard {
  albums?: Report
  singles?: Report
}

const filterAssetTypes = (filter: string[], report: Report) => {
  const reportAssetTypes: AssetType[] = get(report, 'stats.assetTypeStats')
  if (reportAssetTypes.length && filter.length) {
    return reportAssetTypes.filter((assetType) =>
      filter.includes(String(assetType.id)),
    )
  }
  return reportAssetTypes
}

const reportFilter = (
  reportsData: Report[],
  filters: ReportFilters,
): Report[] => {
  const reportsDataCopy = cloneDeep(reportsData)
  return reportsDataCopy.reduce((prev: Report[], report) => {
    const reportCopy = cloneDeep(report)
    set(
      reportCopy,
      'stats.assetTypeStats',
      filterAssetTypes(filters.assetType ?? [], reportCopy),
    )
    prev.push(reportCopy)
    return prev
  }, [])
}

export const groupReports = (reportsData: Report[]): ReportCard[] => {
  const groupByYearObject = groupBy(reportsData, 'year')
  const groupArray = Object.keys(groupByYearObject).reduce(
    (prev: {labels: Report[][][]; years: Report[][]}, year) => {
      const prevCopy = cloneDeep(prev)
      const yearReport = groupByYearObject[year]
      prevCopy.years.push(yearReport)
      const labelValues = values(groupBy(yearReport, 'label.id'))
      prevCopy.labels.push(labelValues)
      return prevCopy
    },
    {labels: [], years: []},
  )
  const orderByYear = groupArray.labels.reverse()
  const groupByCard = flatten(orderByYear)
  return groupByCard.map((cardReports) =>
    cardReports.reduce((cardObject: ReportCard, report: Report) => {
      const newCard = cloneDeep(cardObject)
      const reportType = isAlbum(report.listViewType) ? 'albums' : 'singles'
      newCard[reportType] = report
      return newCard
    }, {}),
  )
}

export const groupReportsByFilter = (
  reportsData: Report[],
  currentFilters: ReportFilters,
): ReportCard[] => {
  const filteredReports = reportFilter(reportsData, currentFilters)
  return groupReports(filteredReports)
}

export const getNoReportsMessage = (
  filters: FilterObject,
  labels: Label | Label[] | undefined,
) => {
  const specificLabel = filters.label?.length === 1
  const specificYear = filters.year?.length === 1
  if (specificLabel) {
    const label = Array.isArray(labels)
      ? labels.find((item) => item.id === Number(filters.label[0]))
      : labels
    return `No Data for ${label?.name} ${specificYear ? filters.year[0] : ''}`
  }
  return 'No Report found for your selection'
}
