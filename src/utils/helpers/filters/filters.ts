import {SelectProps} from 'src/components'

import {cloneDeep, compact, isEmpty, isEqual, map, uniq} from '..'

export const getFilterChecked = (filter: SelectProps['value'], value: string) =>
  Array.isArray(filter) ? filter.includes(value) : isEqual(filter, value)

export const shouldFilter = (value: string | string[], cond: boolean) =>
  !isEmpty(value) ? cond : true

export const filterByValue = (
  filter: string[],
  value?: string | null,
): boolean => shouldFilter(filter, filter.includes(value ?? ''))

export const filterKeyFromArray = <T extends {}>(
  array: T[],
  key: string,
  sort: boolean,
) => {
  const arrayFiltered = uniq(map(array, key))
  return compact(sort ? arrayFiltered.sort() : arrayFiltered)
}

export const createObjectMapper = <T extends {id: number}>(
  field: string,
  items: T | T[],
) => {
  const itemsArray = Array.isArray(items) ? items : [items]
  return itemsArray.reduce((itemsObject, item) => {
    const itemsObjectCopy = cloneDeep(itemsObject)
    if (item) {
      itemsObjectCopy[item.id] = item[field]
    }
    return itemsObjectCopy
  }, {})
}
