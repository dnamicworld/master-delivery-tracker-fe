import {cloneDeep} from 'lodash'

import {AssetTrack, AssetType} from '../../models'
import {AssetOption} from '../../models/AssetOption'
import {AssetTrackData} from '../../pages/AssetReport/components/AssetCards/AssetsCards.model'
import {AppColors} from '../constants/ui'

export const toAssetTracksData = (assets: AssetTrack[]) =>
  assets.reduce((previous, current) => {
    const index = previous.findIndex(
      (item: AssetTrackData) => item.title === current.assetType,
    )
    if (index === -1) {
      const previousCopy = cloneDeep(previous)
      return [...previousCopy, {title: current.assetType, value: 1}]
    }
    const valueByIndex = previous[index]
    valueByIndex.value += 1
    return previous
  }, [] as AssetTrackData[])

export const getAssetType = (assetTypes: AssetType[], value: string) =>
  assetTypes.find((assetType) => assetType.shortName === value)

export const getAssetTypeById = (assetTypes: AssetType[], value: number) =>
  assetTypes.find((assetType) => assetType.id === value)

export const getNoOptionalAssetsLength = (assetTypes: AssetType[]) =>
  assetTypes.filter((assetType) => assetType.optional !== true).length

export const changeColorToLower = (item: AssetOption) => {
  const itemCopy = cloneDeep(item)
  return {...itemCopy, color: AppColors[itemCopy.color.toLowerCase()]}
}
