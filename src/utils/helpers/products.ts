import {Dispatch, SetStateAction} from 'react'

import {appFetcher} from '../../config/axios'
import {Product} from '../../models'
import {API_ROUTES} from '../constants'

export const deleteDuplicates = (products: Product[]) =>
  products.filter(
    (productFilter, index) =>
      products.findIndex(
        (productFind) => productFilter.id === productFind.id,
      ) === index,
  )

export const handleGpidImport = async (
  gpid: string,
  updateProduct: (name: string, value: any) => void,
  setErrorMessage: Dispatch<SetStateAction<string>>,
  setShowCoverModal: Dispatch<SetStateAction<boolean>>,
  setGpid: Dispatch<SetStateAction<string>>,
) => {
  const URL = `${API_ROUTES.PRODUCTS}/${gpid}/${API_ROUTES.GPID_IMPORT}`
  try {
    const response = await appFetcher(URL)
    if (response.coverArtLink) {
      updateProduct('coverArtLink', response.coverArtLink)
      setShowCoverModal(false)
      setGpid('')
    } else {
      setErrorMessage('No artwork available for GPID entered')
      setGpid('')
    }
  } catch (err) {
    setErrorMessage('No data is available for the GPID entered')
    setGpid('')
  }
}
