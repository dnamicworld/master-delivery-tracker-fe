import {Key} from 'swr'

import {appFetcher} from '../../config/axios'

import {
  Product,
  ProductPatch,
  ProductTrackAsset,
  Actions,
  Report,
  ReportPost,
  ProductPost,
  ProductTrack,
} from '../../models'
import {ACTIONS, ApiMethods, API_ROUTES} from '../constants'

import {cloneDeep, isEmpty, isEqual, omit, QueryString} from '.'

type ObjectWithAction<T> = T & {action?: Actions}

const isTableAction = <T>(action: string, item: ObjectWithAction<T>): boolean =>
  isEqual(item.action, action)

export const isTableCreateAction = <T>(item: ObjectWithAction<T>): boolean =>
  isTableAction(ACTIONS.CREATE, item)

const isTableUpdateAction = <T>(item: ObjectWithAction<T>): boolean =>
  isTableAction(ACTIONS.UPDATE, item)

export const isTableDeleteAction = <T>(item: ObjectWithAction<T>): boolean =>
  isTableAction(ACTIONS.DELETE, item)

export const removeDeleteActionItems = <T>(dataArray: T[]): T[] =>
  dataArray.filter((data) => !isTableDeleteAction<T>(data))

const filterItemsWithAction = <T>(
  dataArray: ObjectWithAction<T>[],
): ObjectWithAction<T>[] => dataArray.filter((data) => data.action)

const removeTrackRedundantData = (
  tracksArray: ProductTrack[],
  isNewProduct?: boolean,
): ProductTrack[] =>
  filterItemsWithAction<ProductTrack>(tracksArray).map((track) => {
    let trackCopy = cloneDeep(track)
    if (isTableCreateAction<ProductTrack>(trackCopy) || isNewProduct) {
      trackCopy = omit(trackCopy, ['id'])
      trackCopy.assets = removeDeleteActionItems<ProductTrackAsset>(
        trackCopy.assets,
      )
    } else if (isTableUpdateAction<ProductTrack>(trackCopy)) {
      trackCopy.assets = filterItemsWithAction<ProductTrackAsset>(
        trackCopy.assets,
      )
    }
    trackCopy.assets = trackCopy.assets.map((asset) => {
      const assetCopy = cloneDeep(asset)
      return omit(assetCopy, ['assetType'])
    })
    return omit(
      trackCopy,
      isNewProduct
        ? ['action', 'createdAt', 'updatedAt']
        : ['createdAt', 'updatedAt'],
    )
  })

type ProductOperation = ProductPost | ProductPatch

function isProductPath(product: Product) {
  return (
    product.reportId !== undefined &&
    product.productType !== undefined &&
    product.title !== undefined &&
    product.artist !== undefined
  )
}

export const productToProductPostOrPatch = (
  data: Product,
  isNewProduct?: boolean,
): ProductOperation | undefined => {
  const copyData: Product = cloneDeep(data)
  copyData.selectionNumber = copyData.selectionNumber || ''

  const fieldsToOmit = [
    'id',
    'report',
    'stats',
    'createdAt',
    'updatedAt',
    'trackTitle',
    'year',
    'label',
    'labelSlug',
    'titleArtist',
  ]
  const {tracks = []} = copyData

  if (isProductPath(copyData)) {
    return {
      ...(omit(copyData, fieldsToOmit) as ProductOperation),
      tracks: tracks
        ? removeTrackRedundantData(tracks, isNewProduct)
        : undefined,
    }
  }

  return undefined
}

export const onFormOperationProductSave = (
  URL: string,
  productToUpdate: Product,
  isNewProduct?: boolean,
): Promise<Product | undefined> => {
  const payload = productToProductPostOrPatch(productToUpdate, isNewProduct)
  const method = isNewProduct ? ApiMethods.POST : ApiMethods.PATCH

  if (payload) {
    return appFetcher(URL, {
      method,
      data: payload,
    })
  }

  return Promise.reject(new Error('Missing values'))
}

export const createReport = (data: ReportPost): Promise<Report | undefined> =>
  appFetcher(API_ROUTES.REPORTS, {method: ApiMethods.POST, data})

export const getApiURL = (url: string, shouldFetch?: () => boolean): Key => {
  let apiURL: Key = url
  if (shouldFetch) {
    apiURL = shouldFetch() ? apiURL : null
  }
  return apiURL
}

export const buildQueryString = <T>(queryParams: T) => {
  if (!isEmpty(queryParams)) {
    return `?${Object.entries(queryParams)
      .filter(
        ([key, value]) => !isEmpty(key) && (!isEmpty(value) || !!Number(value)),
      )
      .map(([key, value]) => `${key}=${value}`)
      .join('&')}`
  }
  return ''
}
export const getEndpointURL = (entityURL: string, queryParams: QueryString) => {
  const {id, ...remainingQueryParams} = queryParams
  const queryString = !isEmpty(remainingQueryParams)
    ? buildQueryString<QueryString>(remainingQueryParams)
    : ''
  const baseUrl = `/${entityURL}${id ? `/${id}` : ''}`
  return `${baseUrl}${queryString}`
}
