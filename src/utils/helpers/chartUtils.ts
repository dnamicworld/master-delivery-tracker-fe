import {AssetTypeStat, DeliveryStatusActions, ProductTrack} from '../../models'

import {CHARTNOTREQUIRED, PRODUCT_TYPE_LABELS} from '../constants'

import {cloneDeep, getPercentage, removeDeleteActionItems} from '.'

export interface AssetsForTracks {
  assetType: string
  total: number
  missing: number
  delivered: number
  notRequired: number
}

const formatNumberWithCommas = (number: number) =>
  number
    .toString()
    .split('')
    .reverse()
    .map((d, i) => (i !== 0 && i % 3 === 0 ? `${d},` : d))
    .reverse()
    .join('')

const addOneIf = (condition: boolean) => (condition ? 1 : 0)

const filterDeletedTracks = (
  tracks: ProductTrack[],
): ProductTrack[] | undefined => removeDeleteActionItems<ProductTrack>(tracks)

export const getBarDataFromTracks = (
  data: AssetsForTracks,
): {name: string; pv: string | number; leyend?: string; display?: string} =>
  data.total
    ? {
        name: `${formatNumberWithCommas(
          data.delivered,
        )} of ${formatNumberWithCommas(data.total)}`,
        pv: getPercentage(data.total, data.delivered),
      }
    : CHARTNOTREQUIRED

export const getTrackAssets = (
  tracks: ProductTrack[],
  assetsIncludedNames: string[],
): {[key: string]: AssetsForTracks} => {
  const initialResult = Object.fromEntries(
    assetsIncludedNames.map((name) => [
      name,
      {assetType: name, total: 0, missing: 0, delivered: 0, notRequired: 0},
    ]),
  )
  return (
    filterDeletedTracks(tracks)?.reduce(
      (tracksResult, track) =>
        track.assets.reduce((assetsResult, asset) => {
          const assetsResultCopy = cloneDeep(assetsResult)
          if (asset.assetType) {
            const assetResult = assetsResultCopy[asset.assetType]
            if (assetResult) {
              assetsResultCopy[asset.assetType] = {
                assetType: asset.assetType,
                total:
                  assetResult.total +
                  addOneIf(
                    asset.deliveryStatus !== DeliveryStatusActions.NOT_REQUIRED,
                  ),
                missing:
                  assetResult.missing +
                  addOneIf(
                    asset.deliveryStatus === DeliveryStatusActions.MISSING,
                  ),
                delivered:
                  assetResult.delivered +
                  addOneIf(
                    asset.deliveryStatus === DeliveryStatusActions.DELIVERED,
                  ),
                notRequired:
                  assetResult.notRequired +
                  addOneIf(
                    asset.deliveryStatus === DeliveryStatusActions.NOT_REQUIRED,
                  ),
              }
            }
          }
          return assetsResultCopy
        }, tracksResult),
      initialResult,
    ) ?? {}
  )
}

export const getBarData = (data: AssetTypeStat): {name: string; pv: number} => {
  const delivery = data.deliveryCount ?? 0
  const expected = data.expectedCount ?? 0
  return {
    name: `${formatNumberWithCommas(delivery)} of ${formatNumberWithCommas(
      expected,
    )}`,
    pv: getPercentage(expected, delivery),
  }
}

export const getAssetsDeliveredPercentage = (
  assetTypes: AssetTypeStat[],
): number => {
  const calculations = assetTypes.reduce(
    (prev: {total: number; value: number}, asset) => {
      const prevCopy = cloneDeep(prev)
      prevCopy.total += asset.expectedCount
      prevCopy.value += asset.deliveryCount
      return prevCopy
    },
    {total: 0, value: 0},
  )
  return getPercentage(calculations.total, calculations.value)
}

export const getDonutChartData = (
  assetsData: AssetTypeStat[],
  labelName: string,
): {name: string; value: number} => ({
  name: PRODUCT_TYPE_LABELS[labelName] ?? labelName,
  value: getAssetsDeliveredPercentage(assetsData),
})
