import queryString from 'query-string'

import {get} from '.'

type QueryParamValue = string | string[] | null | undefined | number

export interface QueryString {
  page?: number
  size?: number
  [key: string]: QueryParamValue
}

const ARRAY_FORMAT = 'comma'

export const parseQuery = (query: string): QueryString =>
  queryString.parse(query, {arrayFormat: ARRAY_FORMAT})

export const getQueryParam = (
  query: string,
  paramName: string,
): QueryParamValue => {
  const value = parseQuery(query)
  return get(value, [paramName])
}

export const getQueryParamAsString = (
  query: string,
  paramName: string,
): QueryParamValue => {
  const param = getQueryParam(query, paramName)
  if (Array.isArray(param)) {
    return param.length ? param.join(',') : undefined
  }
  return param
}

export const stringifyQuery = (query: QueryString): string =>
  queryString.stringify(query, {arrayFormat: ARRAY_FORMAT})

export const getQueryParamAsArray = (
  query: string,
  paramName: string,
): QueryParamValue => {
  const param = getQueryParam(query, paramName)
  return typeof param === 'string' ? [param] : param
}
