import {PaginationOptions} from 'src/DS/components'
import {mutate} from 'swr'

import {SelectOption} from '../../components'
import {
  AssetTypeStat,
  Label,
  ListViewType,
  PaginationMutateResponse,
  Report,
  ReportPost,
} from '../../models'

import {API_ROUTES} from '../constants'
import {SINGLE_TYPE} from '../constants/assets'

import {
  cloneDeep,
  createRange,
  get,
  getCurrentYear,
  getEndpointURL,
  QueryString,
  ReportCard,
} from '.'

export interface NewReportState {
  label?: string
  year?: string
  yearOptions?: SelectOption[]
}

export const newReportInitialState = {
  label: undefined,
  year: undefined,
  yearOptions: undefined,
}

export const getYearsFromArray = (years: number[] | undefined) => {
  const currentYear = getCurrentYear()
  const yearRange = createRange(currentYear - 22, currentYear + 28)
  const yearsToExclude = years?.map((year) => String(year))

  return yearRange.reduce((prev: SelectOption[], cYear) => {
    const prevCopy = cloneDeep(prev)
    if (!yearsToExclude?.includes(cYear)) {
      prevCopy.push({
        name: String(cYear),
        value: cYear,
      })
    }
    return prevCopy
  }, [])
}

const generateAssetTypes = () => {
  const assetsRange = createRange(1, 9)
  return assetsRange.map((asset) => ({
    id: Number(asset),
    sortOrder: Number(asset),
  }))
}

export const generateReportData = (
  label: Label,
  type: keyof typeof ListViewType,
  year: string,
): ReportPost => {
  const assetTypes = generateAssetTypes()
  const title = `${label?.name} - ${year}${
    type === SINGLE_TYPE ? ' - Singles' : ''
  }`
  return {
    assetTypes,
    title,
    barColor: '22B14C',
    labelId: Number(label.id),
    listViewType: type,
    year: Number(year),
  }
}

export const getLabelOptions = (labels: Label[]) =>
  labels.map((label) => ({
    name: label.name,
    value: String(label.id),
  }))

export const getAssets = (reportCard: ReportCard): AssetTypeStat[][] => [
  get(reportCard.albums, 'stats.assetTypeStats') ?? [],
  get(reportCard.singles, 'stats.assetTypeStats') ?? [],
]

export const mutateReports = (queryParams: QueryString, report?: Report) => {
  if (report) {
    mutate(
      getEndpointURL(API_ROUTES.REPORTS, queryParams),
      (response?: PaginationMutateResponse<Report>) => {
        if (report) {
          const data = {...response?.data, report}
          return {...response, data}
        }
        return response
      },
    )
    mutate(getEndpointURL(API_ROUTES.REPORTS, queryParams))
  }
}

export const getReportCounts = (
  {page, size, count}: PaginationOptions,
  reportsSize: number,
) => {
  const display = Math.round((page - 1) * (size / 2)) + Math.round(reportsSize)
  const reportCount = count > display ? Math.round(count) : display
  return [display, reportCount]
}
