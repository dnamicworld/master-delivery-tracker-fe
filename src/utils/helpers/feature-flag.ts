import {has} from '.'

export function isFeatureFlagEnabled(flagName: string): boolean {
  return Boolean(has(process.env, flagName) && process.env[flagName] === 'true')
}
