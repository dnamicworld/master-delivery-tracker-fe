import {jsPDF} from 'jspdf'
import {FontStyle, LineWidths, MarginPaddingInput} from 'jspdf-autotable'

type HAlign = 'center' | 'right' | 'left'

export const commentLabel = 'Comments:'

const getImgProps = (doc: jsPDF, imgCanvas: HTMLCanvasElement) => {
  const imgData = imgCanvas.toDataURL('image/jpeg')
  return doc.getImageProperties(imgData)
}

const getImgSize = (doc: jsPDF, imgCanvas: HTMLCanvasElement) => {
  const canvasImgSize = getImgProps(doc, imgCanvas)
  const {height, width} = canvasImgSize
  return {
    h: height / doc.internal.scaleFactor,
    w: width / doc.internal.scaleFactor,
  }
}

export const getCellHAlign = (
  index: number,
  lastIndex: number,
  colSpan: number,
): HAlign => {
  const halign: {[key: number]: HAlign} = {
    0: 'center',
    1: 'left',
    [lastIndex]: 'right',
  }
  const assetCell = colSpan === 1 ? 'center' : 'left'
  return halign[index] ?? assetCell
}

export const getCellWidth = (
  index: number,
  assetsLength: number,
  defaultAssets: number,
): number => {
  const widths = {0: 11, 1: 24}
  let defaultWidth = 20
  const assetMissing = defaultAssets - assetsLength
  const assetRequired = defaultAssets - 1 - assetMissing
  if (assetMissing && assetRequired) {
    const spaceLeft = defaultWidth * assetMissing
    defaultWidth += spaceLeft / assetRequired
  }
  return widths[index] ?? defaultWidth
}

const getLineWidth = (condition: boolean): number => (condition ? 0.4 : 0)

export const getLineWidthObject = (
  firstCell: boolean,
  lastRow: boolean,
): Partial<LineWidths> => ({
  top: getLineWidth(firstCell),
  bottom: getLineWidth(lastRow),
})

export const getCellPadding = (extraTop: boolean): MarginPaddingInput => ({
  top: extraTop ? 6 : 0,
  bottom: 3.5,
  left: 0,
  right: 0,
})

export const getFontStyle = (
  section: string,
  index: number,
  value: string,
): FontStyle =>
  !index || section === 'head' || value === commentLabel ? 'bold' : 'normal'

const getDimensions = (
  doc: jsPDF,
  height: number,
  width: number,
): [number, number] => {
  const pageWidth = doc.internal.pageSize.getWidth()
  const scaledHeight = (height * pageWidth) / width
  return [pageWidth, scaledHeight]
}

export const getImageData = (
  doc: jsPDF,
  canvas: HTMLCanvasElement,
): [string, number, number] => {
  const data = canvas.toDataURL('image/jpeg')
  const {h, w} = getImgSize(doc, canvas)
  const [width, height] = getDimensions(doc, h, w)
  return [data, width, height]
}
