import {MONTHS_SHORT, JOIN_STRING, JOIN_DATE} from '../constants'

import {get, toInt} from '.'

interface DateProps {
  date?: Date | null
  dateString?: string | null
  withCustomJoin?: boolean
  withTime?: boolean
  shortMonth?: boolean
  dayFirst?: boolean
  yearFirst?: boolean
  stringISO?: boolean
  localDate?: boolean
  stringSeparator?: string
  dateSeparator?: string
  timeSeparator?: string
}

const leadingZero = (value: string | number) => `0${String(value)}`.slice(-2)

const getUtcSeconds = ({
  date,
  dateString,
  withCustomJoin,
  dayFirst = false,
  yearFirst = false,
  dateSeparator = JOIN_DATE,
}: DateProps) => {
  let utcSeconds
  let localDate = date
  if (dateString) {
    if (withCustomJoin) {
      const dateParts = dateString.split(dateSeparator)
      if (dateParts.length === 3) {
        const year = dateParts[yearFirst ? 0 : 2]
        const dayMonthIndex = yearFirst ? 1 : 0
        const month = dateParts[dayMonthIndex + Number(dayFirst)]
        const day = dateParts[dayMonthIndex + Number(!dayFirst)]
        utcSeconds = Date.UTC(toInt(year), toInt(month) - 1, toInt(day), 11)
      }
    } else {
      localDate = new Date(dateString)
    }
  }
  if (localDate) {
    utcSeconds = Date.UTC(
      localDate.getUTCFullYear(),
      localDate.getUTCMonth(),
      localDate.getUTCDate(),
      11,
    )
  }
  return utcSeconds
}

export const isDate = (value?: Date | string) =>
  value !== null && value !== undefined

export const getDate = ({
  date,
  dateString,
  withCustomJoin,
  dayFirst,
  yearFirst,
  dateSeparator = JOIN_DATE,
}: DateProps) => {
  const utcSeconds = getUtcSeconds({
    date,
    dateString,
    withCustomJoin,
    dayFirst,
    yearFirst,
    dateSeparator,
  })
  return utcSeconds ? new Date(utcSeconds) : undefined
}

export const getDateString = ({
  date,
  dateString = '',
  withTime,
  shortMonth,
  dayFirst,
  yearFirst,
  stringISO,
  localDate,
  stringSeparator = ' at ',
  dateSeparator = '/',
  timeSeparator = ':',
}: DateProps) => {
  let dateToParse = date
  let formattedString = stringISO ? new Date().toISOString() : ''
  if (dateString) {
    dateToParse = new Date(dateString)
  }
  if (dateToParse) {
    const [day, month, year, hours, minutes] = localDate
      ? [
          dateToParse.getDate(),
          dateToParse.getMonth(),
          dateToParse.getFullYear(),
          dateToParse.getHours(),
          dateToParse.getMinutes(),
        ]
      : [
          dateToParse.getUTCDate(),
          dateToParse.getUTCMonth(),
          dateToParse.getUTCFullYear(),
          dateToParse.getUTCHours(),
          dateToParse.getUTCMinutes(),
        ]
    const dayString = leadingZero(day)
    const monthString = shortMonth
      ? get(MONTHS_SHORT, month)
      : leadingZero(month + 1)
    const dayMonth = dayFirst
      ? `${dayString}${dateSeparator}${monthString}`
      : `${monthString}${dateSeparator}${dayString}`
    const time = withTime
      ? `${stringSeparator}${hours % 12 || 12}${timeSeparator}${leadingZero(
          minutes,
        )} ${hours >= 12 ? 'PM' : 'AM'}`
      : ''
    const formattedDate = yearFirst
      ? `${year}${dateSeparator}${dayMonth}`
      : `${dayMonth}${dateSeparator}${year}`
    formattedString = `${formattedDate}${time}`
  }
  return formattedString
}

export const compareDates = (filter: string, date?: string | null) => {
  if (date) {
    const deserializedDate = getUtcSeconds({dateString: date})
    if (deserializedDate) {
      if (filter.includes(JOIN_STRING)) {
        const [start, end] = filter.split(JOIN_STRING)
        const startDate = getUtcSeconds({
          dateString: start,
          withCustomJoin: true,
        })
        const endDate = getUtcSeconds({dateString: end, withCustomJoin: true})
        if (startDate && endDate) {
          return startDate <= deserializedDate && deserializedDate <= endDate
        }
      } else {
        const filterDate = getUtcSeconds({
          dateString: filter,
          withCustomJoin: true,
        })
        return filterDate === deserializedDate
      }
    }
  }
  return false
}

export const getCurrentYear = () => new Date().getFullYear()
