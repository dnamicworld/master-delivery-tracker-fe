type Validations = 'success' | 'warning' | 'error' | null

export const validateField = (
  invalidState?: boolean,
  field?: string,
): Validations => (invalidState && !field ? 'error' : null)
