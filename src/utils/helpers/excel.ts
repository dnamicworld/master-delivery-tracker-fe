import xlsx, {CellObject, Range, ColInfo} from 'xlsx-with-styles'

import {BACK_GROUND_EXCEL_COLOR, MESSAGES} from '../constants'

import {cloneDeep, has} from '.'

export type CellValue = string | Partial<CellObject>

const boldCell = (value: string): Partial<CellObject> => ({
  v: value,
  s: {font: {bold: true}},
})
const boldAndGrayCell = (value: string): Partial<CellObject> => ({
  v: value,
  s: {
    font: {bold: true},
    fill: {
      patternType: 'solid',
      bgColor: {rgb: BACK_GROUND_EXCEL_COLOR},
      fgColor: {rgb: BACK_GROUND_EXCEL_COLOR},
    },
  },
})
export const boldRow = (values: string[], gray?: boolean) =>
  values.map((value) => (gray ? boldAndGrayCell(value) : boldCell(value)))

export const getXlsxValue = (
  value?: string | null,
  fallback?: string,
): string => {
  if (value) return value
  if (fallback) return fallback
  return MESSAGES.NO_DATA_AVAILABLE
}

function fitToColumn(arrayOfArray: CellValue[][]): ColInfo[] {
  // get maximum character of each column
  const colIndex = 1
  return arrayOfArray[0].reduce((prev: ColInfo[]) => {
    const prevCopy = cloneDeep(prev)
    prevCopy[colIndex] = {
      wch: Math.max(
        ...arrayOfArray.map((a2: object | string[]) => {
          let value: string = ''
          if (a2[colIndex]) {
            if (has(a2[colIndex], 'v')) {
              value = a2[colIndex].v
            } else if (typeof a2[colIndex] === 'string') {
              value = a2[colIndex]
            }
          }
          return value.toString().length
        }),
      ),
    }
    return prevCopy
  }, [])
}

export const downloadExcelFile = (
  name: string,
  data: CellValue[][],
  merge?: Range[],
  autosize?: boolean,
) => {
  const workbook = xlsx.utils.book_new()
  const worksheet = xlsx.utils.aoa_to_sheet(data)
  if (merge) {
    worksheet['!merges'] = merge
  }
  if (autosize) {
    worksheet['!cols'] = fitToColumn(data)
  }
  xlsx.utils.book_append_sheet(workbook, worksheet)
  xlsx.writeFile(workbook, `${name}.xlsx`)
}

export interface Downloader<T> {
  getHeaders: () => CellValue[]
  getInfo: (values: T[]) => string[][]
  transformData: () => CellValue[][]
  download: (fileName: string) => void
}
