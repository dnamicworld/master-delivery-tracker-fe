import {ReactElement} from 'react'
import {RouterProps} from 'react-router-dom'

import {BSTextFieldEvent} from '../../DS/components'
import {appFetcher} from '../../config/axios'
import {ModalBody, ModalOptions} from '../../context/modal'
import {ProductValidationResponse, Product} from '../../models/Product'
import {
  ALBUM_TYPE,
  API_ROUTES,
  PRODUCT_TYPE_ROUTES,
  RELEASE_TYPE_MAPPER,
  Status,
} from '../constants'
import {EP_TYPE} from '../constants/assets'
import {VALIDATE_PRODUCT} from '../constants/messages'

import {parseQuery, QueryString, stringifyQuery} from './query-string'

import {
  getDateString,
  get,
  cloneDeep,
  isEmpty,
  isUndefined,
  toNumber,
  getEndpointURL,
} from '.'

export const FIELD_FE_SEPARATOR = ' '
const FIELD_BE_SEPARATOR = ' / '
export const normalizeData = <T, U>(initialState: T, queryState: U) => {
  const stateCopy = cloneDeep(initialState)
  Object.keys(stateCopy).forEach((key) => {
    const stateValue = get(stateCopy, key)
    const queryValue = get(queryState, key)
    if (Array.isArray(stateValue)) {
      if (!isUndefined(queryValue)) {
        stateCopy[key] = Array.isArray(queryValue) ? queryValue : [queryValue]
      }
    } else {
      stateCopy[key] = isUndefined(queryValue) ? stateValue : queryValue
    }
  })
  return stateCopy
}

// There are too many different inputs using this, changing value type generates conflicts
export const handleInputChange =
  (callback: (name: string, value: any, checked?: boolean) => void) =>
  (event: BSTextFieldEvent) => {
    const {
      value,
      name: inputName,
      checked,
      type,
    } = event.currentTarget || event.target
    let finalValue = value
    if (type === 'checkbox') {
      finalValue = checked ? value : undefined
    }
    callback(inputName ?? '', finalValue, checked)
  }

export const isNumber = (value?: string | number) =>
  typeof value === 'string'
    ? !Number.isNaN(toNumber(value))
    : !Number.isNaN(value)

export const getPercentage = (total: number, value: number) =>
  total ? Math.round((value / total) * 100) : 0

export const uniquely = () => {
  let d = Date.now()
  if (
    typeof performance !== 'undefined' &&
    typeof performance.now === 'function'
  ) {
    d += performance.now()
  }
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    // eslint-disable-next-line no-bitwise
    const r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    // eslint-disable-next-line no-bitwise
    return (c === 'x' ? r : (r & 0x3) | 0x8).toString(16)
  })
}

export const timestamp = () => new Date().getTime()

export const toInt = (value: string) => parseInt(value, 10)

export const cleanInt = (stringToClean: string) =>
  toInt(stringToClean.replace(/\D/g, ''))

export const normalizeFilters = <T>(filters: string, initialFilters: T) => {
  const queryFilters = parseQuery(filters)
  let filtersToSet = initialFilters
  if (!isEmpty(queryFilters)) {
    filtersToSet = normalizeData<T, QueryString>(filtersToSet, queryFilters)
  }
  return filtersToSet
}

export const getStatus = (status?: boolean) =>
  status ? Status.COMPLETE : Status.INCOMPLETE

export const getLongTextResult = (value: string, MAX_LENGTH = 54) => {
  let result = value
  const longer = result && result.length > MAX_LENGTH
  if (longer) {
    result = `${result.substring(0, MAX_LENGTH)}`
  }
  return {longer, value: result}
}

export const getLongText = (textInput: string, MAX_LENGTH = 54) => {
  const {longer, value} = getLongTextResult(textInput, MAX_LENGTH)
  const result = longer ? `${value}...` : value
  return {longer, value: result}
}

export const instanceUpdated = () => ({
  updatedAt: getDateString({stringISO: true}),
})

export const replaceSeparators = (
  text: string,
  oldSeparator: string = FIELD_BE_SEPARATOR,
  newSeparator: string = FIELD_FE_SEPARATOR,
) => text.replace(new RegExp(oldSeparator, 'g'), newSeparator)

export const removeElementInmutable = <T>(array: T[], index: number) => [
  ...array.slice(0, index),
  ...array.slice(index + 1),
]

export const getMultipleSelectStyles = (multiple?: boolean) => ({
  checkboxes: multiple,
  radiobuttons: !multiple,
})

export const getReleaseTypeFromString = (releaseType: string) =>
  RELEASE_TYPE_MAPPER[releaseType]

export const isAlbum = (productType: string) =>
  productType === ALBUM_TYPE || productType === EP_TYPE

export const createRange = (start?: number, end?: number): string[] =>
  start && end
    ? Array(end - start + 1)
        .fill(0)
        .map((_, idx) => String(start + idx))
    : []

export const trimValue = (value?: string | null) => value?.trim() ?? ''

export const isMacOS = () => navigator.userAgent.includes('Mac OS')

export const showElement = (
  show: boolean,
  def: ReactElement,
  election: ReactElement,
) => (show ? election : def)

interface ValidationProps {
  artist?: string
  title?: string
  gpid?: string
}

export const validateExistProduct = async ({
  artist,
  title,
  gpid,
}: ValidationProps): Promise<ProductValidationResponse | undefined> => {
  const URL = getEndpointURL(API_ROUTES.VALIDATE_PRODUCTS, {
    artist: escape(artist ?? ''),
    title: escape(title ?? ''),
    upc: gpid,
  })
  try {
    const response = appFetcher(URL)

    return response
  } catch (err) {
    return Promise.reject(err)
  }
}

const saveAndRedirect = async (
  history: RouterProps['history'],
  onFormSave: (product?: Product) => void,
  redirectNew: boolean,
  stateProduct?: Product,
) => {
  await onFormSave(stateProduct)
  if (redirectNew) {
    history.push({
      pathname: PRODUCT_TYPE_ROUTES[stateProduct?.productType ?? ''],
      search: stringifyQuery({
        editing: 'true',
        reportId: String(stateProduct?.reportId),
      }),
    })
  }
}

export const validateProduct = async (
  history: RouterProps['history'],
  onFormSave: (product?: Product | undefined) => void,
  toInitial: () => void,
  displayModal: (
    content: ModalBody,
    title?: string | undefined,
    options?: ModalOptions | undefined,
  ) => void,
  redirectNew: boolean,
  isNewProduct?: boolean,
  stateProduct?: Product,
) => {
  const validation = await validateExistProduct({
    artist: stateProduct?.artist,
    title: stateProduct?.title,
  })
  if (isNewProduct && validation && validation.artistTitleExists) {
    displayModal(
      VALIDATE_PRODUCT.artistTitle.body,
      VALIDATE_PRODUCT.artistTitle.title,
      {
        actionName: VALIDATE_PRODUCT.actionName,
        onActionClick: () =>
          saveAndRedirect(history, onFormSave, redirectNew, stateProduct),
        onCancelAction: toInitial,
      },
    )
  } else {
    saveAndRedirect(history, onFormSave, redirectNew, stateProduct)
  }
}
export const NOT_VALUE = {
  'Not Delivered': 'N / D',
  'Not Required': ' N / R',
}
