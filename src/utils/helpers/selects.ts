import {SelectAction, SelectOption, SelectProps} from '../../components'
import {SelectActions} from '../constants'

import {cloneDeep} from '.'

export const onAddOptions = (
  selectOptions: SelectOption[],
  value: SelectProps['value'],
  option: SelectAction,
) => {
  const result = cloneDeep(selectOptions)
  const created = option.action === SelectActions.createOption
  if (created) {
    result.push({
      name: option.name,
      value,
    })
  }
  return {added: created, result}
}
