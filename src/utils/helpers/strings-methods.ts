import {ValueFormatterParams} from 'ag-grid-community'

const TABLE_BLANK_VALUE = '—'
export const formatBlankValues = ({value}: ValueFormatterParams): string =>
  value || TABLE_BLANK_VALUE
