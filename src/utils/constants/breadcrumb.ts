import {Product} from '../../models'

import {ROUTES} from '.'

export const BREADCRUMBS = {
  LABEL: (label: string, labelId: number) => ({
    id: 'label',
    label,
    link: `${ROUTES.HOME}?label=${labelId}`,
  }),
  ALBUM_INDEX: {
    id: 'album_index',
    label: 'Album Index',
  },
  SINGLE_INDEX: {
    id: 'single_index',
    label: 'Single Index',
  },
  REPORT: (label: string) => ({
    id: `${label}`,
    label: `${label}`,
  }),
  HELP: {
    id: 'help',
    label: 'Help',
    link: ROUTES.HELP,
  },
  ASSET_REPORT: {
    id: 'assetReport',
    label: 'Assets Delivered',
    link: ROUTES.ASSET_REPORT,
  },
  HOME: {
    id: 'home',
    label: 'All Reports',
    link: ROUTES.HOME,
  },
  LABELS: {
    id: 'labels',
    label: 'Label Reports',
    link: ROUTES.HOME,
  },
  RESULTS: {
    id: 'results',
    label: 'Album Report Search Results',
    link: ROUTES.RESULTS,
  },
  NEW_PROJECT: {
    id: 'new',
    label: 'New Project',
  },
  PRODUCT: (product: Product) => ({
    id: `product_${product.id}`,
    label: `${product.title}`,
  }),
}
