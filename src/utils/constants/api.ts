export enum ACTIONS {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE',
  DELETE = 'DELETE',
  GET = 'GET',
}

export enum ApiMethods {
  POST = 'POST',
  PATCH = 'PATCH',
  DELETE = 'DELETE',
  GET = 'GET',
}
