export const CHART_BLANK = '#fff'

export const CHART_LABELS_COLOR = '#71767d'

export const CHART_COLORS = ['#8040E1', '#2473FF']

export const GRAY_COLOR = '#3F3F3F'

export const BACK_GROUND_EXCEL_COLOR = 'A5A5A5'
