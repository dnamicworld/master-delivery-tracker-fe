export declare type TGrowlColorType =
  | 'primary'
  | 'secondary-white'
  | 'secondary-black'
  | 'success'
  | 'warning'
  | 'danger'
  | 'info'
export enum ECommonFunctionalType {
  DEFAULT = 'default',
  PRIMARY = 'primary',
  SECONDARY_WHITE = 'secondary-white',
  SECONDARY_BLACK = 'secondary-black',
  SUCCESS = 'success',
  WARNING = 'warning',
  DANGER = 'danger',
  INFO = 'info',
  LINK = 'link',
}
