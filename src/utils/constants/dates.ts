export const MONTHS_SHORT = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
]

export const JOIN_DATE = '-'

export const releaseDateRangeFutureYears = 10

export const DEFAULT_MIN_YEAR = 2000

export const DEFAULT_MAX_YEAR = 2050
