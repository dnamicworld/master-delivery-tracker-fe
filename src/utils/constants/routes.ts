export const ROUTES = {
  ALBUM: '/album',
  ALBUM_INDEX: '/album-index',
  ALBUM_DETAIL: '/album-detail',
  ASSET_REPORT: '/asset-report',
  HELP: '/help',
  PRODUCT_PDF: '/product-pdf',
  LOGIN: '/',
  UNAUTHORIZED: '/unauthorized',
  LOGIN_CB: '/implicit/callback',
  COMPONENTS: '/components',
  HOME: '/home',
  SINGLE_INDEX: '/single-index',
  SINGLE_DETAIL: '/single-detail',
  LABEL: '/label',
  REPORT: '/report',
  RESULTS: '/results',
}

const REPORTS = 'reports'
const TRACKS = 'tracks'
const PRODUCTS = 'products'
const ASSET_TYPES = 'asset_types'

export const API_ROUTES = {
  ALL_YEARS: `${REPORTS}/allYears`,
  ASSET_TYPES: `${ASSET_TYPES}`,
  ASSET_TRACKS: `${TRACKS}/asset-tracks`,
  ASSET_TRACKS_DATA: `${TRACKS}/asset-tracks-data`,
  ASSET_TRACKS_FILTERS: `${TRACKS}/asset-tracks-filters`,
  ASSET_TRACKS_SEARCH: `${TRACKS}/asset-tracks-by-search`,
  GPID_IMPORT: 'oms-gpid',
  LABELS: 'labels',
  PRODUCTS,
  PRODUCTS_SEARCH: `${PRODUCTS}/products-by-search`,
  VALIDATE_PRODUCTS: `${PRODUCTS}/validate-fields`,
  REPORTS,
  MARKETING_LABELS: 'marketing-labels',
  SAMPLES: 'samples',
  TRACKS,
  YEARS_RANGE: 'years',
  ALL_OPTIONS: `${ASSET_TYPES}/allOptions`,
}

export const PRODUCT_TYPE_ROUTES = {
  ALBUM: `${ROUTES.ALBUM_DETAIL}`,
  SINGLE: `${ROUTES.SINGLE_DETAIL}`,
  EP: `${ROUTES.ALBUM_DETAIL}`,
}
