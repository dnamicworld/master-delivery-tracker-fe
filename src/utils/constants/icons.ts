import {IconType} from '../../DS/components'
import {cloneDeep} from '../helpers'

export const FAPrefix = ['fas', 'far', 'fal', 'fat', 'fad', 'fab', 'fak']

export const FA_ICONS = {
  arrowUp: 'fa-arrow-up',
  asterisk: 'fa-asterisk',
  check: 'fa-check',
  pencil: 'fa-pencil',
  chevronUp: 'fa-chevron-up',
  chevronDown: 'fa-chevron-down',
  invoice: 'fa-file-invoice',
  spinner: 'fa-spinner',
  close: 'fa-times',
  exclamation: 'fa-exclamation-triangle',
  circle: 'fa-circle',
  circlePlus: 'fa-circle-plus',
  calendar: 'fa-calendar-o',
  doubleRight: 'fa-angle-double-right',
  filter: 'fa-filter',
  plus: 'fa-plus',
  ellipsisV: 'fa-ellipsis-v',
  trash: 'fa-trash',
  bubbleSolid: 'fa-comment',
  search: 'fa-search',
  circleQuestion: 'fa-circle-question',
  disappointed: 'fa-face-disappointed',
  spinnerThird: 'fa-spinner-third',
  penToSquare: 'fa-pen-to-square',
  cloudArrowUp: 'fa-cloud-arrow-up',
  badgeCheck: 'fa-badge-check',
}

export const GLYPHICONS = {
  filter: 'filter',
  menuHamburger: 'menu-hamburger',
  removeCircle: 'remove-circle',
}

export const LEGATO_ICONS = {
  magnifyingGlass: 'magnifying-glass',
}

type IconVariant = {[key in IconType]: IconType}

export const ICON_TYPES: IconVariant = [...FAPrefix, 'glyphicon'].reduce(
  (prev, current) => {
    const newPrev = cloneDeep(prev)
    newPrev[current] = current
    return newPrev
  },
  {} as IconVariant,
)
