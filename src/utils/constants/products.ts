import {ProductFilters, ProductTrackAsset} from '../../models'

import {ALBUM_TYPE} from '.'

export const INDEX_PAGES_INITIAL_FILTERS: ProductFilters = {
  releaseType: '',
  subLabel: [],
  artist: [],
  year: '',
  releaseDate: '',
  delivered: [],
}

export const productPage = 50

export const NEW_PRODUCT = (assets: ProductTrackAsset[]) => ({
  artist: '',
  title: '',
  productType: ALBUM_TYPE,
  tracks: [
    {
      id: 1,
      trackNumber: 1,
      title: '',
      isrc: null,
      comments: null,
      version: null,
      majorGenre: null,
      marketingLabel: null,
      complete: false,
      assets,
    },
  ],
})
