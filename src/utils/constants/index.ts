export {
  ALL_ARTISTS,
  LOADING_RESULTS,
  ALBUM_COVER_PLACEHOLDER,
  CHARTNOTREQUIRED,
  JOIN_STRING,
  NAVBAR_PDF,
  ASPERA_URL,
  ASPERA_BUTTON_LABEL,
  RELEASE_TYPES,
  RELEASE_TYPE_MAPPER,
  ALBUM_TYPE,
  SINGLE_TYPE,
  PRODUCT_INDEXES,
  PRODUCT_TYPE_LABELS,
  ALBUM_LABEL,
  SINGLE_LABEL,
  PRODUCT_TYPE_DISPLAY,
  Status,
} from './assets'

export {ACTIONS, ApiMethods} from './api'
export {API_ROUTES, PRODUCT_TYPE_ROUTES, ROUTES} from './routes'
export {BREADCRUMBS} from './breadcrumb'
export {FA_ICONS, FAPrefix, ICON_TYPES, GLYPHICONS, LEGATO_ICONS} from './icons'
export {SelectActions} from './selects'
export {
  CHART_BLANK,
  CHART_LABELS_COLOR,
  GRAY_COLOR,
  CHART_COLORS,
  BACK_GROUND_EXCEL_COLOR,
} from './styling'

export {
  ERRORS,
  MESSAGES,
  MODALS,
  PRODUCT_DETAIL_PLACEHOLDERS,
  BACK_MODAL,
  CREATE_REPORT,
  DELETE_REPORT,
  REMOVE_ASSET_COLUMN,
  CHANGE_PROJECT_DATE,
  DISCARD_INDEX_MODAL,
  CUSTOM_ASSET_COLUMN,
} from './messages'

export {
  DEFAULT_MAX_YEAR,
  DEFAULT_MIN_YEAR,
  MONTHS_SHORT,
  releaseDateRangeFutureYears,
  JOIN_DATE,
} from './dates'

export {HelpPageContent} from './lang'
export {ROLES} from './roles'

export {INDEX_PAGES_INITIAL_FILTERS, NEW_PRODUCT, productPage} from './products'

export {
  EXTENDED_ASSETS_SIZE,
  PDF_MAX_ASSETS,
  NOT_REQUIRED_VALUES,
  notRequiredOption,
} from './assets'

export {AppColors, DOM, KEYS, TABLE, TOTAL_DELIVERED, throttleTime} from './ui'
export type {DefaultComponentStyleTypes} from './ui'

export const FEATURE_FLAGS = {
  componentsPage: 'REACT_APP_COMPONENTS_PAGE',
  loaders: 'REACT_APP_FEATURE_LOADERS',
  newReportPage: 'REACT_APP_NEW_REPORT_PAGE',
}
export {ECommonFunctionalType} from './legato'
export type {TGrowlColorType} from './legato'
