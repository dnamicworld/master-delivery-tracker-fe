export enum SelectActions {
  selectOption = 'select-option',
  deselectOption = 'deselect-option',
  removeOption = 'remove-value',
  popValue = 'pop-value',
  setValue = 'set-value',
  clear = 'clear',
  createOption = 'create-option',
}
