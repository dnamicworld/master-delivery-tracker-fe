import {capitalize} from '../helpers'

export const ALBUM_TYPE = 'ALBUM'

export const SINGLE_TYPE = 'SINGLE'

export const EP_TYPE = 'EP'

export const ALBUM_LABEL = 'Albums & EP’s'

export const SINGLE_LABEL = 'Singles'

export const PRODUCT_INDEXES = {
  [ALBUM_TYPE]: 0,
  [SINGLE_TYPE]: 1,
  [EP_TYPE]: 0,
}

export const PRODUCT_TYPE_LABELS = {
  [ALBUM_TYPE]: ALBUM_LABEL,
  [SINGLE_TYPE]: SINGLE_LABEL,
  [EP_TYPE]: ALBUM_LABEL,
}

export const PRODUCT_TYPE_DISPLAY = {
  [ALBUM_TYPE]: capitalize(ALBUM_TYPE),
  [SINGLE_TYPE]: capitalize(SINGLE_TYPE),
  [EP_TYPE]: EP_TYPE,
}

export const RELEASE_TYPES = [ALBUM_LABEL, SINGLE_LABEL]

export const RELEASE_TYPE_MAPPER = {
  [ALBUM_LABEL]: [ALBUM_TYPE || EP_TYPE],
  [SINGLE_LABEL]: SINGLE_TYPE,
}

export const ALL_ARTISTS = 'All Artists'

export const LOADING_RESULTS = 'Loading Results'

export const JOIN_STRING = '.'

export const ALBUM_COVER_PLACEHOLDER = '/albums/placeholder.png'

export const NAVBAR_PDF = 'WMG-AudioMasterDeliverySpecs-2022.pdf'

export const ASPERA_URL = 'https://aspera.pub/wqUY2_0'

export const ASPERA_BUTTON_LABEL = {
  POST_COPY: 'Link Copied',
  PRE_COPY: 'Copy Link',
  UPLOAD: 'Upload',
  UPLOAD_TOOLTIP: 'Upload Files to Library',
}

export enum Status {
  COMPLETE = 'Complete',
  INCOMPLETE = 'Incomplete',
}

export const EXTENDED_ASSETS_SIZE = 11

export const PDF_MAX_ASSETS = 17

export const NOT_REQUIRED_VALUES = {
  display: 'N / R',
  value: 'Not Required',
}

export const notRequiredOption = {
  name: NOT_REQUIRED_VALUES.value,
  value: NOT_REQUIRED_VALUES.value,
}

export const CHARTNOTREQUIRED = {
  name: '-- / --',
  pv: '',
  leyend: NOT_REQUIRED_VALUES.value,
  display: NOT_REQUIRED_VALUES.display,
}

export const NOT_DELIVERED_VALUES = {
  display: 'N / D',
  value: 'Not Delivered',
}

export const missingOptions = [
  notRequiredOption,
  {name: NOT_DELIVERED_VALUES.value, value: NOT_DELIVERED_VALUES.value},
]
