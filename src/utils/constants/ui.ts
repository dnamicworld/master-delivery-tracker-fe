export const DOM = {
  AG_TABLE: 'ag-table',
  DATE_PICKER: 'datePicker',
  CALENDAR: 'calendar',
}

export const KEYS = {
  enter: 'Enter',
  escape: 'Escape',
}

export const TABLE = {
  HEADER_COMPONENTS: {
    HeaderWithIcon: 'HeaderWithIcon',
    CustomHeader: 'CustomHeader',
  },
  CELL_RENDERERS: {
    CustomCell: 'CustomCell',
    TextFieldCell: 'TextFieldCell',
    WithEditButton: 'WithEditButton',
  },
  CELL_EDITORS: {
    SelectCellEditor: 'SelectCellEditor',
    PopupTextCellEditor: 'PopupTextCellEditor',
  },
}

export const TOTAL_DELIVERED = [
  '0% - 25%',
  '25% - 50%',
  '50% - 75%',
  '75% - 100%',
]

export enum AppColors {
  primary = 'primary',
  warning = 'warning',
  success = 'success',
  danger = 'danger',
  info = 'info',
  yellow = 'yellow',
  gray = 'gray',
  lightGray = 'lightGray',
}
export const throttleTime = 1000

export declare type DefaultComponentStyleTypes =
  | 'default'
  | 'primary'
  | 'warning'
  | 'success'
  | 'danger'
