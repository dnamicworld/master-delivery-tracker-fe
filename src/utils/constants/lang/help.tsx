export const HelpPageContent = {
  icons: {
    help: 'Help',
    PDF: 'Master Delivery Spec. PDF',
    search: 'Search',
  },
  title: (
    <>
      Questions about Master Delivery Report?
      <br />
      We’ve got answers.
    </>
  ),
  contactTitle: 'Still have questions? Contact us.',
  footerCard: {
    title:
      'You can get direct access to Master Delivery Report expert who can help answer questions',
  },
}
