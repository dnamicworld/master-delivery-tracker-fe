export const MESSAGES = {
  NO_DATA_AVAILABLE: 'No Data Available',
  CANCEL: 'Cancel',
  OK: 'OK',
  NO_COMMENTS: 'No Comments Provided',
  EMPTY: '_',
}
export const CREATE_REPORT = {
  title: 'Create New Report',
  action: 'Create Report',
}
export const MODALS = {
  TITLES: {
    DELETE: 'Delete',
    WARNING: 'Warning',
  },
  ACTIONS: {
    DELETE: 'Delete',
    DISCARD: 'Discard',
    SAVE: 'Save',
  },
  UNSAVED_MODAL:
    'You have unsaved changes, do you want to discard the changes?',
  DISCARD_MODAL: 'Are you sure you want to discard changes before saving?',
  DELETE_MODAL: (element: string) =>
    `Are you sure you want to delete ${element || 'element'}?`,
}

export const COMPLETE_MODAL = {
  title: (element: string) => `Mark ${element} Complete`,
  body: 'This will mark the assets in this project as complete. Would you like to continue?',
  action: 'Mark Complete',
}

export const BACK_MODAL = {
  title: 'You have unsaved changes',
  body: 'You are about to leave the page without saving changes. Would you like to save or discard the changes?',
  cancel: 'Discard Changes',
  action: 'Save',
}

export const DELETE_REPORT = {
  title:
    'All data for this report will be permanently deleted, click check box to proceed',
  check: 'Yes, I wish to delete this report',
}
export const REMOVE_ASSET_COLUMN = {
  title: 'Remove Asset Column',
  body: 'You are about to remove an asset column and it will remove all the data from this column. Do you want to continue?',
  actionName: 'Remove Column',
}

export const CHANGE_PROJECT_DATE = {
  title: 'Move Project',
  body: (oldReport: string, newReport: string) =>
    `Selecting a new release year will move this project from the ${oldReport} to a ${newReport}.`,
  actionName: 'Move and save',
}

export const ERRORS = {
  NO_ACCESS: 'User is not assigned to the client application.',
  COLUM_NAME_CHARACTERS: (type: string, quantity: number) =>
    `The maximum of characters for ${type} characters are ${quantity}`,
}
export const DISCARD_INDEX_MODAL = {
  title: 'Discard Changes?',
  body: 'You made changes to these records. Would you like to discard them?',
  cancel: 'Discard Edits',
}
export const VALIDATE_PRODUCT = {
  artistTitle: {
    title: 'Duplicate Artist & Title found',
    body: 'A project already exists under this Artist & Title',
  },
  gpid: {
    title: 'Duplicate GPID entry found',
    body: 'A project already exists under this GPID',
  },
  actionName: 'Continue',
}
export const CUSTOM_ASSET_COLUMN = {
  title: 'Custom Asset Column',
  message:
    'The default sample rate/bit depth selections including N/D and N/R will automatically be applied. ',
  actionName: 'Save',
}
const getDefaultPlaceholder = (field: string) => `Enter ${field}`
const getDefaultSelectPlaceholder = (field: string) => `Select ${field}`

export const PRODUCT_DETAIL_PLACEHOLDERS = {
  AR_ADMIN: getDefaultPlaceholder('A&R Name'),
  ARTIST: getDefaultPlaceholder('Artist Name'),
  COMMENTS: getDefaultPlaceholder('Comment'),
  COMMENTS_TABLE: getDefaultPlaceholder('Comment'),
  GPID: getDefaultPlaceholder('GPID'),
  PRODUCER: getDefaultPlaceholder('Producer Name'),
  PRODUCT_TYPE: getDefaultSelectPlaceholder('Product Type'),
  RELEASE_DATE: getDefaultPlaceholder('Release Date'),
  SELECTION_NUMBER: getDefaultPlaceholder('Selection #'),
  STATUS: getDefaultSelectPlaceholder('Status'),
  MARKETING_LABEL: getDefaultSelectPlaceholder('Marketing Label'),
  MASTERING: getDefaultPlaceholder('Mastering Name'),
  MIXER: getDefaultPlaceholder('Mixer Name'),
  UPC: getDefaultPlaceholder('UPC Number'),
  TITLE: getDefaultPlaceholder('Title'),
  TRACK_TITLE_TABLE: getDefaultPlaceholder('Track Title'),
}
