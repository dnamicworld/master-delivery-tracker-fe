import axios, {AxiosRequestConfig} from 'axios'

import oktaConfig from './okta'

const {REACT_APP_API_URL} = process.env

const client = axios.create({
  baseURL: REACT_APP_API_URL,
  timeout: 10000,
  responseType: 'json',
  maxContentLength: 100000000,
  maxBodyLength: 100000000,
})

const setInterceptorToken = async (config: AxiosRequestConfig) => {
  const configCopy = {...config}
  if (!configCopy.headers) {
    configCopy.headers = {}
  }
  const token = await oktaConfig.getAccessToken()
  if (token) {
    configCopy.headers.Authorization = `Bearer ${token}`
  }
  return configCopy
}

const onRequestError = (error: any) => {
  Promise.reject(error)
}
client.interceptors.request.use(setInterceptorToken, onRequestError)

export const appFetcher = (url: string, params?: AxiosRequestConfig) => {
  const method = params?.method?.toLowerCase() ?? 'get'
  return client[method](url, params?.data).then(
    (res: Pick<AxiosRequestConfig, 'data'>) => res.data,
  )
}

export default client
