import {OktaAuth} from '@okta/okta-auth-js'

const {REACT_APP_CLIENT_ID, REACT_APP_ISSUER} = process.env

const OKTA = {
  CLIENT_ID: REACT_APP_CLIENT_ID || '{clientId}',
  ISSUER: REACT_APP_ISSUER,
  REDIRECT_URL: `${window.location.origin}/implicit/callback`,
  SCOPES: ['openid', 'profile', 'email', 'groups'],
}

const oktaConfig = new OktaAuth({
  clientId: OKTA.CLIENT_ID,
  issuer: OKTA.ISSUER,
  redirectUri: OKTA.REDIRECT_URL,
  scopes: OKTA.SCOPES,
  pkce: true,
})

export default oktaConfig
