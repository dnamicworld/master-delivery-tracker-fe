import classNames from 'classnames'
import {ReactNode} from 'react'

import {Card, Row} from 'src/DS/components'

import styles from './DashboardCard.module.scss'

interface DashboardCardProps {
  cardHeader?: ReactNode
  leftContent: ReactNode
  rightContent: ReactNode
  bottomContent?: ReactNode
}

const DashboardCard = ({
  cardHeader,
  leftContent,
  rightContent,
  bottomContent,
}: DashboardCardProps) => (
  <Card className='mb-40 px-30 py-30' header={cardHeader} radius>
    <Row className={classNames(styles.row, 'px-10')}>
      <div className='donut pr-30 py-5'>{leftContent}</div>
      <div className={classNames(styles.bars, 'pr-10')}>{rightContent}</div>
    </Row>
    <div>{bottomContent}</div>
  </Card>
)

export default DashboardCard
