import classNames from 'classnames'
import {KeyboardEventHandler, useEffect, useRef, useState} from 'react'
import {FormControl} from 'react-bootstrap'
import {useHistory} from 'react-router-dom'

import {BSTextFieldEvent, Checkbox, Col, Icon, Row} from '../../DS/components'
import {appFetcher} from '../../config/axios'
import {useSearch} from '../../context/search'
import useLabels from '../../hooks/data/useLabels'
import {FA_ICONS, ICON_TYPES, KEYS, ROUTES} from '../../utils/constants'
import {getEndpointURL} from '../../utils/helpers'
import {buildQueryString} from '../../utils/helpers/api'

import styles from './Search.module.scss'
import {
  ALL_FIELD_NAME,
  ASSET_SECTION,
  DB_SOURCE,
  defaultParams,
  getOptions,
  notPersonnelField,
  optionsInfo,
  PROJECT_SECTION,
  searchToField,
  SEARCH_SOURCE,
  sections,
} from './SearchComponent.controller'
import {OptionParam, SearchDropdown} from './SearchDropdown'
import {SearchInput} from './SearchInput'

const SearchComponent = () => {
  const resultsAreaRef = useRef<HTMLDivElement>(null)

  const history = useHistory()
  const search = useSearch()
  const {labels: labelRecords} = useLabels()
  const [searchText, setSearchText] = useState<
    string | number | readonly string[] | undefined
  >()
  const [exactMatch, setExactMatch] = useState<boolean>(false)

  const [paramsValues, setParamsValues] = useState<OptionParam>(
    defaultParams(true),
  )
  const [fieldName, setFieldName] = useState<string>(ALL_FIELD_NAME)
  const [sectionName, setSectionName] = useState<string>(PROJECT_SECTION)
  const [searchObtained, setSearchObtained] = useState<boolean>(false)
  const [searchResult, setSearchResult] = useState<string[]>([])
  const [loading, setLoading] = useState<boolean>(false)

  const labels = Array.isArray(labelRecords) ? labelRecords : []
  const labelsIds = labels.map((label) => label.id.toString())
  const params = {
    exact: exactMatch.toString(),
    phrase: searchText?.toString(),
    admin: paramsValues.admin.toString(),
    trackTitle: paramsValues.trackTitle.toString(),
    projectTitle: paramsValues.projectTitle.toString(),
    artist: paramsValues.artist.toString(),
    labels: labelsIds,
  }
  const isAssetSection = sectionName === ASSET_SECTION
  const options = getOptions(isAssetSection)
  const showAll = notPersonnelField(fieldName)

  useEffect(() => {
    const body = document.querySelector('body')
    const handleClickOutside = (event: MouseEvent) => {
      const element = event.target as HTMLElement
      setSearchObtained(!!resultsAreaRef.current?.contains(element))
    }
    body?.addEventListener('click', handleClickOutside)
    return () => body?.removeEventListener('click', handleClickOutside)
  }, [])

  useEffect(() => {
    const isOpen = resultsAreaRef.current?.classList.contains('open')
    if ((searchObtained && !isOpen) || loading) {
      resultsAreaRef.current?.classList.add('open')
    } else if (!searchObtained && isOpen) {
      resultsAreaRef.current?.classList.remove('open')
    }
  }, [searchObtained, loading])

  const searchValues = async () => {
    setLoading(true)
    const route = sections[sectionName].apiRoute
    const URL = getEndpointURL(route, params)
    try {
      const response = await appFetcher(URL)
      setSearchResult(searchToField(isAssetSection, fieldName, response))
      setSearchObtained(true)
      setLoading(false)
    } catch (err) {
      setSearchObtained(true)
      setLoading(false)
    }
  }

  const getValuesFromSearch: KeyboardEventHandler<FormControl> &
    KeyboardEventHandler<HTMLInputElement> = async (event) => {
    if (event.key === KEYS.enter) {
      await searchValues()
    }
  }

  const handleInput = ({currentTarget: {value}}: BSTextFieldEvent) =>
    setSearchText(value)

  const setFieldInfo = (section: string, field: string) => {
    setSectionName(sections[section].name)
    setFieldName(field)
  }

  const rowOnClick = (
    paramsValuesUpdated: OptionParam,
    sectionNameParam: string,
    placeholder: string,
  ) => {
    setParamsValues(paramsValuesUpdated)
    setSectionName(sectionNameParam)
    setFieldInfo(sectionNameParam, placeholder)
  }

  const getAssetsObject = (dataSource: string, item?: string) => ({
    pathname: ROUTES.ASSET_REPORT,
    ...(dataSource === SEARCH_SOURCE
      ? {
          search: buildQueryString({dataSource}),
          state: {result: item ? [item] : searchResult},
        }
      : {
          search: buildQueryString({[fieldName]: item ?? searchResult}),
        }),
  })

  const getProjectsObject = (dataSource: string, item?: string) => ({
    pathname: ROUTES.RESULTS,
    ...(dataSource === SEARCH_SOURCE
      ? {
          search: buildQueryString({...params, dataSource}),
          state: {result: item ? [item] : searchResult},
        }
      : {
          search: buildQueryString({
            ...params,
            phrase: item ?? params.phrase,
            exact: item ? true : params.exact,
            dataSource,
          }),
        }),
  })

  const handleRedirect = (item?: string) => {
    const dataSource = notPersonnelField(fieldName) ? SEARCH_SOURCE : DB_SOURCE
    const searchObject = isAssetSection
      ? getAssetsObject(dataSource, item)
      : getProjectsObject(dataSource, item)
    history.push(searchObject)
  }

  const handleSelection = (item: string) => handleRedirect(item)

  const viewAll = () => handleRedirect()

  return (
    <Row
      className={classNames(`py-18 ${styles.search}`, {
        'd-none': !search.showSearch,
      })}
    >
      <div>
        <SearchDropdown
          setSearchResult={setSearchResult}
          title={`${sectionName} by ${optionsInfo[fieldName].title}`}
          sections={Object.values(sections)}
          rowOnClicK={rowOnClick}
          sectionToHide={ASSET_SECTION}
          fieldToHide={optionsInfo.adminName.title}
          options={Object.values(optionsInfo)}
        />

        <Col xs={6}>
          <SearchInput
            value={searchText}
            appendIcon={{
              name: FA_ICONS.search,
              addon: true,
              addonClass: 'bg-primary',
              type: ICON_TYPES.fas,
            }}
            placeholder={`Search by ${optionsInfo[fieldName].title}`}
            onChange={handleInput}
            onKeyPress={getValuesFromSearch}
            iconOnClick={searchValues}
            resultsAreaRef={resultsAreaRef}
            searchResult={searchResult}
            handleSelection={handleSelection}
            options={options}
            showAll={showAll}
            loading={loading}
            viewAllFunction={viewAll}
            inputRef={(ref) => {
              search.ref.current = ref
            }}
          />
        </Col>
        <Checkbox
          className='d-inline-block'
          checked={exactMatch}
          onChange={() => {
            setExactMatch((value) => !value)
          }}
        >
          Exact Match
        </Checkbox>
      </div>
      <Icon
        className='text-gray mr-40'
        size='xl'
        type={ICON_TYPES.far}
        iconName={FA_ICONS.close}
        onClick={() => search.updateSearch(false)}
      />
    </Row>
  )
}
export default SearchComponent
