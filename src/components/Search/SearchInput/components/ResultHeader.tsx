import {Col, Row} from '../../../../DS/components'
import {tableWidths} from '../../SearchComponent.controller'

const headerTitle = (value: string, colWidth: number) => (
  <Col sm={colWidth} className='font-weight-sbold'>
    {value}
  </Col>
)
const ResultHeader = () => (
  <Row>
    {headerTitle('Type', tableWidths.type)}
    {headerTitle('Title & Artist', tableWidths.titles)}
    {headerTitle('Label', tableWidths.label)}
    {headerTitle('Year', tableWidths.year)}
  </Row>
)

export default ResultHeader
