import {Row} from 'react-bootstrap'

import {uniquely} from '../../../../utils/helpers'

import {ResultInfo} from './index'

export interface ResultOptions {
  placeholder: string
  title: string
  colWidth: number
}

interface ResultListProps {
  handleSelection: (item: string) => void
  options: ResultOptions[]
  item: string
  index: number
  showAll: boolean
}
const ResultList = ({
  handleSelection,
  options,
  item,
  index,
  showAll,
}: ResultListProps) => (
  <Row
    key={uniquely()}
    className='py-2 mt-10'
    role='button'
    tabIndex={index + 1}
    onClick={() => handleSelection(item)}
  >
    {showAll ? (
      options.map(({placeholder, colWidth}) => {
        const description = JSON.parse(item)[placeholder]
        return (
          <ResultInfo description={description ?? 'N/A'} colWidth={colWidth} />
        )
      })
    ) : (
      <>{item}</>
    )}
  </Row>
)

export default ResultList
