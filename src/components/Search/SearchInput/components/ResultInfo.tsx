import classNames from 'classnames'

import {Col} from '../../../../DS/components'

interface ResultInformationProps {
  description: string[] | string
  colWidth: number
}
const descriptionTag = (value: string, className?: string) => (
  <span className={classNames('w-100', className)}>{value}</span>
)

const ResultInformation = ({description, colWidth}: ResultInformationProps) => (
  <Col className='d-flex' sm={colWidth}>
    {Array.isArray(description) ? (
      <Col className='d-flex flex-column w-100'>
        {description.map((descriptionPart, index) =>
          descriptionTag(descriptionPart, !index ? 'text-primary' : ''),
        )}
      </Col>
    ) : (
      descriptionTag(description)
    )}
  </Col>
)

export default ResultInformation
