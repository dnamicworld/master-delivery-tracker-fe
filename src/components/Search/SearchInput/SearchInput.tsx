import {FocusEventHandler, KeyboardEventHandler, RefObject} from 'react'

import {Icon, TextField} from '../../../DS/components'
import {
  Button,
  FormControl,
  TextFieldProps,
} from '../../../DS/components/bootstrap'
import {FA_ICONS, ICON_TYPES, LEGATO_ICONS} from '../../../utils/constants'

import {ResultHeader, ResultList} from './components'
import {ResultOptions} from './components/ResultList'

interface SearchInputProps extends TextFieldProps {
  placeHolder?: string
  onKeyPress?: KeyboardEventHandler<FormControl> &
    KeyboardEventHandler<HTMLInputElement>
  resultsAreaRef: RefObject<HTMLDivElement>
  searchResult: string[]
  handleSelection: (item: string) => void
  options: ResultOptions[]
  showAll: boolean
  loading?: boolean
  viewAllFunction: () => void
}

const SearchInput = ({
  value,
  autoFocus,
  placeHolder,
  onChange,
  onKeyPress,
  onFocus,
  iconOnClick,
  appendIcon,
  resultsAreaRef,
  searchResult,
  handleSelection,
  options,
  showAll,
  loading,
  viewAllFunction,
  inputRef,
}: SearchInputProps) => {
  const loadingComponent = (
    <div className='p-2'>
      <Icon
        className='mr-2'
        type={ICON_TYPES.far}
        iconName={FA_ICONS.spinnerThird}
      />{' '}
      Searching
    </div>
  )
  const resultComponent = searchResult.length ? (
    <>
      {showAll ? <ResultHeader /> : null}
      {searchResult.map((item, index) => (
        <ResultList
          handleSelection={handleSelection}
          options={options}
          item={item}
          index={index}
          showAll={showAll}
        />
      ))}
    </>
  ) : (
    <div className='p-2'>
      <Icon
        className='mr-2'
        type={ICON_TYPES.far}
        iconName={FA_ICONS.disappointed}
      />{' '}
      No results found
    </div>
  )
  return (
    <>
      <TextField
        bsSize='lg'
        className='mb-0'
        autoFocus={autoFocus}
        value={value}
        appendIcon={appendIcon}
        placeholder={placeHolder}
        onChange={onChange}
        onFocus={
          onFocus as FocusEventHandler<FormControl> &
            FocusEventHandler<HTMLInputElement>
        }
        onKeyPress={onKeyPress}
        iconOnClick={iconOnClick}
        inputRef={inputRef}
      />
      <div ref={resultsAreaRef} className='dropdown'>
        <div className='dropdown-menu m-0'>
          <div className='list checkboxes px-40 py-10'>
            {loading ? loadingComponent : <>{resultComponent} </>}
          </div>
          <Button
            containerStyle='link'
            icon={LEGATO_ICONS.magnifyingGlass}
            onClick={viewAllFunction}
          >
            View all Results {searchResult.length}
          </Button>
        </div>
      </div>
    </>
  )
}

export default SearchInput
