import {AssetTrack, ProductSearch} from 'src/models'
import {API_ROUTES, ROUTES} from 'src/utils/constants'
import {capitalize, uniq} from 'src/utils/helpers'

export const ALL_FIELD_NAME = 'all'
export const PROJECT_SECTION = 'projects'
export const ASSET_SECTION = 'assets'
export const SEARCH_SOURCE = 'fromSearch'
export const DB_SOURCE = 'fromDB'

export const defaultParams = (value: boolean) => ({
  admin: value,
  trackTitle: value,
  projectTitle: value,
  artist: value,
})

export const optionsInfo = {
  [ALL_FIELD_NAME]: {
    title: capitalize(ALL_FIELD_NAME),
    params: defaultParams(true),
    placeholder: ALL_FIELD_NAME,
  },
  adminName: {
    title: 'A & R Admin',
    params: {
      ...defaultParams(false),
      admin: true,
    },
    placeholder: 'adminName',
  },
  artist: {
    title: 'Artist',
    params: {
      ...defaultParams(false),
      artist: true,
    },
    placeholder: 'artist',
  },
  trackTitle: {
    title: 'Track Title',
    params: {
      ...defaultParams(false),
      trackTitle: true,
    },
    placeholder: 'trackTitle',
  },
  title: {
    title: 'Project Title',
    params: {
      ...defaultParams(false),
      projectTitle: true,
    },
    placeholder: 'title',
  },
}

export const sections = {
  [ASSET_SECTION]: {
    name: ASSET_SECTION,
    apiRoute: API_ROUTES.ASSET_TRACKS_SEARCH,
    title: capitalize(ASSET_SECTION),
    accordionId: 'assetsSelection',
    resultsRoute: ROUTES.ASSET_REPORT,
  },
  [PROJECT_SECTION]: {
    name: PROJECT_SECTION,
    apiRoute: API_ROUTES.PRODUCTS_SEARCH,
    title: capitalize(PROJECT_SECTION),
    accordionId: 'projectSelection',
    resultsRoute: ROUTES.RESULTS,
  },
}

export const tableWidths = {type: 2, titles: 6, label: 3, year: 1}

const productOptions = [
  {title: 'Type', placeholder: 'productType', colWidth: tableWidths.type},
  {title: 'Artist', placeholder: 'titleArtist', colWidth: tableWidths.titles},
  {title: 'Label', placeholder: 'label', colWidth: tableWidths.label},
  {title: 'Year', placeholder: 'year', colWidth: tableWidths.year},
]

const assetOptions = [
  {title: 'Type', placeholder: 'assetType', colWidth: tableWidths.type},
  {title: 'Artist', placeholder: 'titleArtist', colWidth: tableWidths.titles},
  {title: 'Label', placeholder: 'name', colWidth: tableWidths.label},
  {title: 'Year', placeholder: 'year', colWidth: tableWidths.year},
]

export const getOptions = (isAssetSection: boolean) =>
  isAssetSection ? assetOptions : productOptions

const assetFields = {
  title: 'project',
  trackTitle: 'title',
}

const getField = (field: string, isAsset: boolean): string =>
  isAsset ? assetFields[field] ?? field : field

const checkField = (key: string, field: string): boolean =>
  field === optionsInfo[key].placeholder

export const notPersonnelField = (field: string) =>
  !checkField('artist', field) && !checkField('adminName', field)

export const searchToField = (
  isAsset: boolean,
  fieldName: string,
  response: ProductSearch[] | AssetTrack[],
) =>
  notPersonnelField(fieldName)
    ? response.map((item: ProductSearch | AssetTrack) =>
        JSON.stringify({
          ...item,
          titleArtist: [
            item[getField('title', isAsset)],
            item[getField('trackTitle', isAsset)],
            item.artist,
          ],
        }),
      )
    : uniq(
        response.map(
          (item: ProductSearch | AssetTrack) =>
            item[getField(fieldName, isAsset)],
        ),
      )
