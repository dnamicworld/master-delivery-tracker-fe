import classNames from 'classnames'
import {MouseEventHandler, useState} from 'react'

import {Dropdown, Icon, Row} from '../../../DS/components'
import {FA_ICONS, ICON_TYPES} from '../../../utils/constants'
import {SearchAccordion} from '../SearchAccordion'

import {SearchDropdownProps} from './SearchDropdown.model'

import styles from './SearchDropdown.module.scss'

const SearchDropdown = ({
  setSearchResult,
  title,
  sections,
  options,
  sectionToHide,
  fieldToHide,
  rowOnClicK,
}: SearchDropdownProps) => {
  const [dropdownSections, setDropdownSections] = useState<{
    [key: string]: boolean
  }>({
    dropdown: false,
    projects: true,
    assets: true,
  })
  const handleAccordions = (value: boolean, section: string) => {
    setDropdownSections((previous) => ({
      ...previous,
      [section]: value,
    }))
    setSearchResult([])
  }
  const RowElement = (field: string, onClick: MouseEventHandler<Row>) => (
    <Row role='button' onClick={onClick} className='py-2 px-4' key={field}>
      {field}
    </Row>
  )
  const accordionElements = (sectionNameParam: string) =>
    options.map((value) =>
      sectionNameParam === sectionToHide && value.title === fieldToHide
        ? null
        : RowElement(value.title, () => {
            setSearchResult([])
            rowOnClicK(value.params, sectionNameParam, value.placeholder)
          }),
    )
  const iconName = dropdownSections.dropdown
    ? FA_ICONS.chevronUp
    : FA_ICONS.chevronDown
  return (
    <Dropdown
      className='mr-10 d-flex'
      manualToggle
      trigger={
        <span
          role='button'
          tabIndex={-1}
          className='text-decoration-none text-capitalize font-weight-bold pr-2'
          onClick={() =>
            handleAccordions(!dropdownSections.dropdown, 'dropdown')
          }
        >
          {title}
          <Icon
            className='ml-1 pl-3'
            type={ICON_TYPES.far}
            iconName={iconName}
          />
        </span>
      }
      classNameMenu={classNames('p-15', styles.box)}
    >
      {sections.map((section) => (
        <SearchAccordion
          key={section.accordionId}
          onChange={(value: boolean) => handleAccordions(value, section.name)}
          title={<strong>{section.title}</strong>}
          accordionId={section.accordionId}
          expanded={dropdownSections[section.name]}
        >
          {accordionElements(section.name)}
        </SearchAccordion>
      ))}
    </Dropdown>
  )
}
export default SearchDropdown
