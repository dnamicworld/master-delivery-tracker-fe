export interface Sections {
  name: string
  apiRoute: string
  title: string
  accordionId: string
}
export interface OptionParam {
  admin: boolean
  trackTitle: boolean
  projectTitle: boolean
  artist: boolean
}
export interface Options {
  title: string
  params: OptionParam
  placeholder: string
}
export interface SearchDropdownProps {
  setSearchResult: (value: string[]) => void
  title: string
  sections: Sections[]
  options: Options[]
  sectionToHide: string
  fieldToHide: string
  rowOnClicK: (
    params: OptionParam,
    sectionName: string,
    placeHolder: string,
  ) => void
}
