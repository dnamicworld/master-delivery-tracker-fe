import {ReactNode} from 'react'

import {AccordionToggleProps} from '../../../DS/components'

export interface SearchAccordionProps extends AccordionToggleProps {
  title: ReactNode
}
