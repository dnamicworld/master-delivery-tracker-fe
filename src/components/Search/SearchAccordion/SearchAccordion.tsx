import {AccordionBody, AccordionToggle} from '../../../DS/components'

import {SearchAccordionProps} from './SearchAccordion.model'

const SearchAccordion = ({
  onChange,
  title,
  accordionId,
  expanded,
  children,
}: SearchAccordionProps) => (
  <>
    <AccordionToggle
      accordionId={accordionId}
      expanded={expanded}
      onChange={onChange}
    >
      {title}
    </AccordionToggle>
    <AccordionBody className='pl-40' accordionId={accordionId}>
      {children}
    </AccordionBody>
  </>
)
export default SearchAccordion
