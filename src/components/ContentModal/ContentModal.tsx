import {Modal} from '../../DS/components'
import {ModalOptions} from '../../context/modal'
import {MESSAGES} from '../../utils/constants'

interface ContentModalProps extends ModalOptions {
  children: JSX.Element
  title?: string
  show: boolean
  disabledSuccessButton?: boolean
  backdropClassName?: string
}

const contentModal = ({
  children,
  actionName,
  cancelName,
  onActionClick,
  onCancelAction,
  show,
  vCenter,
  title,
  disabledSuccessButton,
  backdropClassName,
  cancelButtonStyle,
}: ContentModalProps) => {
  const onCancelCB = onCancelAction || (() => {})
  return (
    <Modal
      vCenter={vCenter}
      show={show}
      title={title ?? ''}
      onHide={onCancelCB}
      cancelAction={{
        name: cancelName ?? MESSAGES.CANCEL,
        action: onCancelCB,
        containerStyle: cancelButtonStyle?.containerStyle,
        colorType: cancelButtonStyle?.colorType,
      }}
      successAction={{
        name: actionName ?? MESSAGES.OK,
        action: onActionClick || (() => {}),
      }}
      disabledSuccessButton={disabledSuccessButton}
      backdropClassName={backdropClassName}
    >
      {children}
    </Modal>
  )
}

export default contentModal
