import {Prompt} from 'react-router-dom'
import {MODALS} from 'src/utils/constants'

interface UnsavedPromptProps {
  block: boolean
  mesage?: string
}

const UnsavedPrompt = ({block, mesage}: UnsavedPromptProps) => (
  <Prompt when={block} message={() => mesage ?? MODALS.UNSAVED_MODAL} />
)

export default UnsavedPrompt
