import classNames from 'classnames'

import {CheckboxBtn, RenderItemFunction, Select} from '../../DS/components'
import {GLYPHICONS, ICON_TYPES} from '../../utils/constants'
import {
  getFilterChecked,
  getMultipleSelectStyles,
  isEmpty,
} from '../../utils/helpers'

import {FilterProps} from '.'

export interface ResolutionFilterProps extends FilterProps {}

const ResolutionFilter = ({
  value,
  name,
  onChange,
  handleCheckOnEnter,
  options,
  resetFilters,
  fullWidth,
}: ResolutionFilterProps) => {
  const resolutionOptions = options?.map((selection) => {
    const renderItem: RenderItemFunction = (
      option,
      {value: selectValue, onChange: onSelectChange},
    ) => {
      const {value: optionValue} = option
      const asset = selection
      return (
        <CheckboxBtn
          key={optionValue}
          multiple
          name={name}
          checked={getFilterChecked(selectValue, optionValue)}
          label={!isEmpty(asset) ? asset : ''}
          value={optionValue}
          onChange={(e) => {
            onSelectChange?.(e, optionValue, {
              name,
              action: 'select-option',
              option,
            })
          }}
        />
      )
    }
    return {
      name: selection,
      value: selection,
      renderItem,
    }
  })
  return (
    <Select
      onChange={onChange}
      handleCheckOnEnter={handleCheckOnEnter}
      label='All Resolutions'
      multiple
      value={value}
      allFeature
      noPlaceholder
      className='mr-20'
      name={name}
      optionsClassNames={classNames(
        'inline btn-group',
        getMultipleSelectStyles(true),
      )}
      searchable={{
        appendIcon: {
          name: GLYPHICONS.filter,
          type: ICON_TYPES.glyphicon,
        },
        placeholder: 'Resolution Finder',
      }}
      options={resolutionOptions}
      clearIcon
      resetFilters={resetFilters}
      fullWidth={fullWidth}
    />
  )
}

export default ResolutionFilter
