import classNames from 'classnames'

import {Select} from '..'
import {CheckboxBtn, RenderItemFunction} from '../../DS/components'
import {ALL_ARTISTS, GLYPHICONS, ICON_TYPES} from '../../utils/constants'
import {getFilterChecked, getMultipleSelectStyles} from '../../utils/helpers'

import {FilterProps} from '.'

export interface ArtistProps extends FilterProps {}

const Artists = ({
  value,
  name,
  onChange,
  handleCheckOnEnter,
  options,
  fullWidth,
  resetFilters,
}: ArtistProps) => {
  const artistOptions = options?.map((artist) => {
    const renderItem: RenderItemFunction = (
      option,
      {value: selectValue, onChange: onSelectChange},
    ) => {
      const {value: optionValue} = option
      return (
        <CheckboxBtn
          key={optionValue}
          multiple
          name={name}
          checked={getFilterChecked(selectValue, optionValue)}
          label={artist}
          value={optionValue}
          onChange={(e) => {
            onSelectChange?.(e, optionValue, {
              name,
              action: 'select-option',
              option,
            })
          }}
        />
      )
    }
    return {
      name: artist,
      value: artist,
      renderItem,
    }
  })
  return (
    <Select
      onChange={onChange}
      handleCheckOnEnter={handleCheckOnEnter}
      label={ALL_ARTISTS}
      multiple
      value={value}
      allFeature
      noPlaceholder
      className='mr-20'
      name={name}
      optionsClassNames={classNames(
        'inline btn-group',
        getMultipleSelectStyles(true),
      )}
      searchable={{
        appendIcon: {
          name: GLYPHICONS.filter,
          type: ICON_TYPES.glyphicon,
        },
        placeholder: 'Artist Finder',
      }}
      options={artistOptions}
      fullWidth={fullWidth}
      clearIcon
      resetFilters={resetFilters}
    />
  )
}

export default Artists
