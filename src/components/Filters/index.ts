import {SelectProps} from '../../DS/components'
import {FilterObject} from '../../context/Filters/pageFilters'

export interface FilterProps
  extends Pick<SelectProps, 'onChange' | 'handleCheckOnEnter'> {
  name: string
  multiple?: boolean
  value: string | string[]
  options?: string[]
  fullWidth?: boolean
  resetFilters?: number
  filters?: FilterObject
  setFiltersState?: <T>(filters: T, initial?: T) => void
}

export {default as ArtistFilter} from './Artist'
export {default as AssetTypesFilter} from './AssetTypes'
export {default as ReleaseDateFilter} from './ReleaseDate'
export {default as ReleaseTypeFilter} from './ReleaseType'
export {default as LabelsFilter} from './Labels'
export {default as SubLabelsFilter} from './SubLabels'
export {default as TotalDeliveredFilter} from './TotalDelivered'
export {default as YearFilter} from './Years'
export {default as ResolutionFilter} from './ResolutionFilter'
export type {ReleaseTypeProps} from './ReleaseType'
