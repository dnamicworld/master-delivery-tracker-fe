import {MultipleCheckbox, Select} from '../../DS/components'
import {GLYPHICONS, ICON_TYPES} from '../../utils/constants'
import {cloneDeep, handleInputChange, isEmpty} from '../../utils/helpers'

import {FilterProps} from '.'

interface SubLabelsProps extends Omit<FilterProps, 'onChange'> {
  label: string
  subLabels: string[]
}

const SubLabels = ({
  filters,
  value,
  name,
  label,
  subLabels,
  fullWidth,
  resetFilters,
  handleCheckOnEnter,
  setFiltersState,
}: SubLabelsProps) => {
  const arrayValue = Array.isArray(value) ? value : []
  const handleParentChange = (checkName: string, checkValue: string) => {
    const filtersCopy = cloneDeep(filters ?? {})
    const filterValue = filtersCopy[name]
    let newValue: string[] = cloneDeep(filterValue)
    if (checkName === name) {
      newValue = checkValue ? subLabels : []
    } else if (filterValue.includes(checkName)) {
      newValue = filterValue.filter((element: string) => element !== checkName)
    } else {
      newValue.push(checkValue)
    }

    filtersCopy[name] = newValue
    setFiltersState?.(filtersCopy)
  }
  const changeCheckboxHandler = handleInputChange(handleParentChange)

  const allLabel = subLabels.length === arrayValue.length

  const selectDisplay =
    !isEmpty(value) && !allLabel ? arrayValue.join(',') : 'All Sub Labels'

  const options = subLabels.map((item) => ({
    value: item,
    name: item,
  }))

  return (
    <Select
      label={selectDisplay}
      noPlaceholder
      value={null}
      manualToggle
      name={name}
      className='mr-20'
      handleCheckOnEnter={handleCheckOnEnter}
      searchable={{
        placeholder: 'Sub Labels Finder',
        appendIcon: {
          name: GLYPHICONS.filter,
          type: ICON_TYPES.glyphicon,
        },
      }}
      options={options}
      fullWidth={fullWidth}
      clearIcon
      resetFilters={resetFilters}
    >
      <MultipleCheckbox
        name='subLabel'
        label={label}
        onChange={changeCheckboxHandler}
        value={arrayValue}
        options={options}
      />
    </Select>
  )
}

export default SubLabels
