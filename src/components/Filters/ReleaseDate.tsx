import {isArray} from 'lodash'
import {useState, useEffect} from 'react'
import {useHistory} from 'react-router-dom'

import {Select} from '..'
import {
  RadioGroup,
  RadioBtn,
  DatePicker,
  RangeDatePicker,
} from '../../DS/components'
import useYearRange from '../../hooks/data/useYearRange'
import {JOIN_DATE, JOIN_STRING} from '../../utils/constants'
import {
  cloneDeep,
  handleInputChange,
  isDate,
  getDateString,
  getDate,
  stringifyQuery,
} from '../../utils/helpers'

import {FilterProps} from '.'

export interface ReleaseDateProps
  extends Pick<
    FilterProps,
    'name' | 'fullWidth' | 'resetFilters' | 'filters' | 'setFiltersState'
  > {
  reportId?: number
  defaultYear?: number
}

interface DatePickersState {
  selected: string
  singleDate?: Date
  rangeStartDate?: Date
  rangeEndDate?: Date
}

const displayDate = (date?: Date) =>
  getDateString({
    date,
    dateSeparator: JOIN_DATE,
  })

const releaseFilterInitialState = {
  selected: '',
  singleDate: undefined,
  rangeStartDate: undefined,
  rangeEndDate: undefined,
}

const setDateFilters = (releaseDate: string) => {
  const getRangeDate = (index: number) => releaseDate.split(JOIN_STRING)[index]

  const getDateParams = (date: string) =>
    getDate({
      dateString: date,
      withCustomJoin: true,
    })

  return releaseDate?.includes(JOIN_STRING)
    ? {
        selected: 'range',
        singleDate: undefined,
        rangeStartDate: getDateParams(getRangeDate(0)),
        rangeEndDate: getDateParams(getRangeDate(1)),
      }
    : {
        selected: 'single',
        singleDate: getDateParams(releaseDate),
        rangeStartDate: undefined,
        rangeEndDate: undefined,
      }
}

const ReleaseDate = ({
  name,
  reportId,
  defaultYear,
  fullWidth,
  filters,
  setFiltersState,
}: ReleaseDateProps) => {
  const [datePickers, setDatePickers] = useState<DatePickersState>(
    releaseFilterInitialState,
  )
  const history = useHistory()
  const {min, max} = useYearRange({id: reportId}, () => !!reportId)
  const minYear = min ?? defaultYear
  const maxYear = max ?? defaultYear

  useEffect(() => {
    const filter = setDateFilters(filters?.releaseDate)
    setDatePickers(filter)
  }, [filters?.releaseDate])

  const FIELDS = {
    NAMES: {
      START_DATE: 'rangeStartDate',
      END_DATE: 'rangeEndDate',
    },
  }

  const {NAMES} = FIELDS

  const handleChange = (
    inputName: string,
    value: Date | Date[] | null | string,
  ) => {
    const datePickersCopy = {...datePickers}
    datePickersCopy[inputName] = value
    setDatePickers(datePickersCopy)
    let newFilter = ''
    if (inputName === 'singleDate' && isDate(datePickersCopy.singleDate)) {
      newFilter = displayDate(datePickersCopy.singleDate)
    }
    if (newFilter) {
      const filtersCopy = {
        ...cloneDeep(filters),
        [name]: newFilter,
      }
      setFiltersState?.(filtersCopy)
      history.push({
        search: stringifyQuery(filtersCopy),
      })
    }
  }

  const rangeHandleChange = (
    _inputName: string,
    dates: Date | Date[] | null | string,
  ) => {
    if (isArray(dates)) {
      const [start, end] = dates
      const datePickersCopy = {...datePickers}
      datePickersCopy.rangeStartDate = start
      datePickersCopy.rangeEndDate = end
      setDatePickers(datePickersCopy)
      let newFilter = ''
      const startDate = datePickersCopy[NAMES.START_DATE]
      const endDate = datePickersCopy[NAMES.END_DATE]
      if (isDate(startDate) && isDate(endDate)) {
        newFilter = `${displayDate(startDate)}.${displayDate(endDate)}`
      }

      if (newFilter) {
        const filtersCopy = {
          ...cloneDeep(filters),
          [name]: newFilter,
        }
        setFiltersState?.(filtersCopy)
        history.push({
          search: stringifyQuery(filtersCopy),
        })
      }
    }
  }
  const selectDisplay = filters?.releaseDate
    ? filters.releaseDate.replace(/-/g, '/').replace(JOIN_STRING, ' - ')
    : 'Release Date'

  const fieldSelected = (value: string) => datePickers.selected === value
  const rangeSelected = fieldSelected('range')
  const singleSelected = fieldSelected('single')
  const {rangeStartDate, rangeEndDate, singleDate} = datePickers
  return (
    <Select
      label={selectDisplay}
      noPlaceholder
      value={null}
      manualToggle
      className='releaseDateFilter mr-20'
      name={name}
      fullWidth={fullWidth}
    >
      <RadioGroup inline>
        <RadioBtn
          key='radioRange'
          multiple
          label='Range'
          name='selected'
          value='range'
          checked={rangeSelected}
          onChange={handleInputChange(handleChange)}
        />
        <RangeDatePicker
          small
          name='rangeDate'
          className='ml-15 font-italic'
          fromDate={{
            name: NAMES.START_DATE,
            value: rangeStartDate,
          }}
          toDate={{name: NAMES.END_DATE, value: rangeEndDate}}
          onChange={rangeHandleChange}
          disabled={!rangeSelected}
          showYearMonths
          minYear={minYear}
          maxYear={maxYear}
          fromFilter
        />
        <RadioBtn
          key='radioSingle'
          multiple
          label='Single Date'
          name='selected'
          value='single'
          checked={singleSelected}
          onChange={handleInputChange(handleChange)}
        />
        <DatePicker
          small
          name='singleDate'
          className='ml-15 font-italic'
          value={singleDate}
          onChange={handleChange}
          disabled={!singleSelected}
          showYearMonths
          minYear={minYear}
          maxYear={maxYear}
          fromFilter
        />
      </RadioGroup>
    </Select>
  )
}

export default ReleaseDate
