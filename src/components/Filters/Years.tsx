import classNames from 'classnames'

import useAllYears from 'src/hooks/data/useAllYears'

import {Select} from '..'
import {CheckboxBtn, RadioBtn, RenderItemFunction} from '../../DS/components'
import useYearRange from '../../hooks/data/useYearRange'
import {GLYPHICONS, ICON_TYPES} from '../../utils/constants'
import {
  createRange,
  getFilterChecked,
  getMultipleSelectStyles,
  sortFilterDatesDescending,
} from '../../utils/helpers'

import {FilterProps} from '.'

export interface YearsProps extends FilterProps {
  plain?: boolean
  labelId?: string
}

const Years = ({
  value,
  plain,
  name,
  labelId,
  onChange,
  handleCheckOnEnter,
  multiple,
  fullWidth,
  resetFilters,
}: YearsProps) => {
  const {min, max} = useYearRange()
  const {allYears} = useAllYears({labelId}, () => labelId !== undefined)
  const yearRange =
    allYears?.map((year) => String(year)) ?? createRange(min, max)
  const yearRangeSort = yearRange.sort(sortFilterDatesDescending)
  const OptionBtn = multiple ? CheckboxBtn : RadioBtn
  const yearOptions = yearRangeSort?.map((year) => {
    const renderItem: RenderItemFunction = (
      option,
      {value: selectValue, onChange: onSelectChange},
    ) => {
      const {value: optionValue} = option
      return (
        <OptionBtn
          key={optionValue}
          multiple={multiple}
          name={name}
          checked={getFilterChecked(selectValue, optionValue)}
          label={year}
          value={optionValue}
          onChange={(e) => {
            onSelectChange?.(e, optionValue, {
              name,
              action: 'select-option',
              option,
            })
          }}
        />
      )
    }
    return {
      name: year,
      value: year,
      multiple,
      renderItem,
    }
  })
  return (
    <Select
      plain={plain}
      onChange={onChange}
      handleCheckOnEnter={handleCheckOnEnter}
      label={multiple ? 'All Years' : 'Year'}
      multiple={multiple}
      value={value}
      allFeature
      noPlaceholder
      className={classNames({
        'mr-20': !plain,
      })}
      name={name}
      optionsClassNames={classNames(
        'inline btn-group',
        getMultipleSelectStyles(multiple),
      )}
      searchable={{
        appendIcon: {
          name: GLYPHICONS.filter,
          type: ICON_TYPES.glyphicon,
        },
        placeholder: 'Year Finder',
      }}
      options={yearOptions}
      fullWidth={fullWidth}
      clearIcon
      resetFilters={resetFilters}
    />
  )
}

export default Years
