import classNames from 'classnames'

import {Select} from '..'
import {CheckboxBtn, RadioBtn, RenderItemFunction} from '../../DS/components'
import {RELEASE_TYPES} from '../../utils/constants'
import {getFilterChecked, getMultipleSelectStyles} from '../../utils/helpers'

import {FilterProps} from '.'

export interface ReleaseTypeProps extends FilterProps {
  plain?: boolean
}

const ReleaseType = ({
  value,
  plain,
  name,
  onChange,
  multiple,
  fullWidth,
}: ReleaseTypeProps) => {
  const OptionBtn = multiple ? CheckboxBtn : RadioBtn
  const assetOptions = RELEASE_TYPES?.map((releaseType) => {
    const renderItem: RenderItemFunction = (
      option,
      {value: selectValue, onChange: onSelectChange},
    ) => {
      const {value: optionValue} = option
      return (
        <OptionBtn
          key={optionValue}
          multiple={multiple}
          name={name}
          checked={getFilterChecked(selectValue, optionValue)}
          label={releaseType}
          value={optionValue}
          onChange={(e) => {
            onSelectChange?.(e, optionValue, {
              name,
              action: 'select-option',
              option,
            })
          }}
        />
      )
    }
    return {
      name: releaseType,
      value: releaseType,
      multiple,
      renderItem,
    }
  })

  return (
    <Select
      plain={plain}
      onChange={onChange}
      label='All Release Types'
      multiple={multiple}
      allFeature
      noPlaceholder
      value={value}
      className={classNames({
        'mr-20': !plain,
      })}
      optionsClassNames={classNames(
        'inline btn-group',
        getMultipleSelectStyles(multiple),
      )}
      name={name}
      options={assetOptions}
      fullWidth={fullWidth}
    />
  )
}

export default ReleaseType
