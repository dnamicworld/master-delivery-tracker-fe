import {CheckboxBtn, RenderItemFunction} from 'src/DS/components'
import {getFilterChecked} from 'src/utils/helpers'

import {Select} from '..'

import {TOTAL_DELIVERED} from '../../utils/constants'

import {FilterProps} from '.'

export interface TotalDeliveredProps extends FilterProps {}

const TotalDelivered = ({
  value,
  name,
  onChange,
  fullWidth,
}: TotalDeliveredProps) => {
  const deliveredOptions = TOTAL_DELIVERED?.map((delivered) => {
    const renderItem: RenderItemFunction = (
      option,
      {value: selectValue, onChange: onSelectChange},
    ) => {
      const {value: optionValue} = option
      return (
        <CheckboxBtn
          key={delivered}
          multiple
          name={name}
          checked={getFilterChecked(selectValue, optionValue)}
          label={delivered}
          onChange={(e) => {
            onSelectChange?.(e, optionValue, {
              name,
              action: 'select-option',
              option,
            })
          }}
          value={optionValue}
        />
      )
    }
    return {
      name: delivered,
      value: delivered,
      multiple: true,
      renderItem,
    }
  })

  return (
    <Select
      onChange={onChange}
      label='All Total % Delivered'
      multiple
      value={value}
      allFeature
      noPlaceholder
      name={name}
      className='mr-20'
      options={deliveredOptions}
      fullWidth={fullWidth}
    />
  )
}

export default TotalDelivered
