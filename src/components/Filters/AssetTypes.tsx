import classNames from 'classnames'

import {Select} from '..'
import {CheckboxBtn, RadioBtn, RenderItemFunction} from '../../DS/components'
import useAssetTypes from '../../hooks/data/useAssetTypes'
import {GLYPHICONS, ICON_TYPES} from '../../utils/constants'
import {getFilterChecked, getMultipleSelectStyles} from '../../utils/helpers'

import {FilterProps} from '.'

export interface AssetTypesProps extends FilterProps {
  includeAllAssets?: boolean
}

const AssetTypes = ({
  value,
  name,
  onChange,
  handleCheckOnEnter,
  multiple,
  includeAllAssets,
  fullWidth,
  resetFilters,
}: AssetTypesProps) => {
  const {assetTypes: assetTypesResponse} = useAssetTypes({
    filterDefaultAssets: !includeAllAssets,
  })
  const assetTypes = Array.isArray(assetTypesResponse) ? assetTypesResponse : []
  const OptionBtn = multiple ? CheckboxBtn : RadioBtn

  const assetTypesOptions = assetTypes.map((assetType) => {
    const renderItem: RenderItemFunction = (
      option,
      {value: selectValue, onChange: onSelectChange},
    ) => {
      const {value: optionValue} = option
      return (
        <OptionBtn
          key={optionValue}
          multiple={multiple}
          name={name}
          label={option.name}
          checked={getFilterChecked(selectValue, optionValue)}
          value={optionValue}
          onChange={(e) => {
            onSelectChange?.(e, optionValue, {
              name,
              action: 'select-option',
              option,
            })
          }}
        />
      )
    }
    return {
      name: assetType.longName,
      value: String(assetType.id),
      multiple,
      renderItem,
    }
  })
  return (
    <Select
      onChange={onChange}
      handleCheckOnEnter={handleCheckOnEnter}
      label='All Asset Types'
      multiple={multiple}
      allFeature
      noPlaceholder
      value={value}
      className='mr-20'
      name={name}
      optionsClassNames={classNames(
        'inline btn-group',
        getMultipleSelectStyles(multiple),
      )}
      options={assetTypesOptions}
      fullWidth={fullWidth}
      searchable={{
        appendIcon: {
          name: GLYPHICONS.filter,
          type: ICON_TYPES.glyphicon,
        },
        placeholder: 'Asset Types Finder',
      }}
      clearIcon
      resetFilters={resetFilters}
    />
  )
}

export default AssetTypes
