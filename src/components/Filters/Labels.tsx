import {Select} from '..'
import {CheckboxBtn, RenderItemFunction} from '../../DS/components'
import useLabels from '../../hooks/data/useLabels'
import {GLYPHICONS, ICON_TYPES} from '../../utils/constants'

import {FilterProps} from '.'

interface LabelsProps extends FilterProps {}

const Labels = ({
  value,
  name,
  onChange,
  fullWidth,
  resetFilters,
  handleCheckOnEnter,
}: LabelsProps) => {
  const {labels: labelRecords} = useLabels()
  const labels = Array.isArray(labelRecords) ? labelRecords : []

  const labelsOptions = labels?.map((year) => {
    const renderItem: RenderItemFunction = (
      option,
      {value: selectValue, onChange: onSelectChange},
    ) => {
      const {name: yearName, value: optionValue} = option
      return (
        <CheckboxBtn
          name={name}
          multiple
          checked={selectValue?.includes(optionValue)}
          key={optionValue}
          label={yearName}
          value={optionValue}
          onChange={(e) => {
            onSelectChange?.(e, optionValue, {
              name,
              action: 'select-option',
              option,
            })
          }}
        />
      )
    }
    return {
      name: year.name,
      value: String(year.id),
      multiple: true,
      renderItem,
    }
  })
  return (
    <Select
      onChange={onChange}
      handleCheckOnEnter={handleCheckOnEnter}
      allFeature
      noPlaceholder
      multiple
      label='All Labels'
      value={value}
      name={name}
      className='mr-20'
      options={labelsOptions}
      fullWidth={fullWidth}
      searchable={{
        appendIcon: {
          name: GLYPHICONS.filter,
          type: ICON_TYPES.glyphicon,
        },
        placeholder: 'Label Finder',
      }}
      clearIcon
      resetFilters={resetFilters}
    />
  )
}

export default Labels
