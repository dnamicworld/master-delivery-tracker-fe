import classnames from 'classnames'
import {useEffect, useRef, useState} from 'react'

import {
  Button,
  Icon,
  SelectOptions,
  SelectOptionsProps,
} from 'src/DS/components'

import {DOM, FA_ICONS, ICON_TYPES, KEYS} from '../../../../utils/constants'

import styles from './kabob.module.scss'

export interface KabobProps
  extends Pick<SelectOptionsProps, 'options' | 'multiple'> {}

const Kabob = ({options}: KabobProps) => {
  const [show, setShow] = useState<boolean>(false)
  const kabobRef = useRef<HTMLDivElement>(null)
  const menuRef = useRef<HTMLDivElement>(null)

  const handleClickOutside = (event: Event) => {
    const target = event.target as Node
    if (
      !menuRef.current?.contains(target) &&
      !kabobRef.current?.contains(target)
    ) {
      setShow(false)
    }
  }

  const handleKeyupEscape = (event: Event) => {
    const {key} = event as KeyboardEvent
    if (key === KEYS.escape) setShow(false)
  }

  useEffect(() => {
    if (kabobRef.current) {
      const agTable = kabobRef.current.closest(`.${DOM.AG_TABLE}`)
      agTable?.addEventListener('click', handleClickOutside)
      agTable?.addEventListener('keyup', handleKeyupEscape)

      return () => {
        agTable?.removeEventListener('click', handleClickOutside)
        agTable?.removeEventListener('keyup', handleKeyupEscape)
      }
    }
    return () => {}
  }, [])

  return (
    <div
      ref={kabobRef}
      className={classnames('kabob', styles.kabob)}
      onClick={() => setShow(!show)}
      aria-hidden='true'
    >
      <Button
        containerStyle='link'
        onClick={() => setShow(!show)}
        className='px-15 py-10'
      >
        <Icon type={ICON_TYPES.fas} iconName={FA_ICONS.ellipsisV} />
      </Button>
      {show ? (
        <div
          ref={menuRef}
          className={classnames(
            'dropdown-menu m-0 p-0',
            styles.kabobMenu,
            styles.displayToLeft,
          )}
        >
          <SelectOptions value={null} options={options} />
        </div>
      ) : null}
    </div>
  )
}
export default Kabob
