import classNames from 'classnames'
import {RefObject, forwardRef} from 'react'

import {SelectOption, SelectOptions, SelectOptionsProps} from '../..'

export interface AgSelectProps extends Omit<SelectOptionsProps, 'options'> {
  ref?: RefObject<HTMLDivElement>
  alignLeft?: boolean
  options: SelectOption[]
}

const AgSelect = forwardRef(
  ({onChange, options, value, alignLeft}: AgSelectProps, ref) => (
    <div
      ref={ref as RefObject<HTMLDivElement>}
      className={classNames('select_cell_renderer_container', {
        center_content: !alignLeft,
      })}
    >
      <SelectOptions value={value} options={options} onChange={onChange} />
    </div>
  ),
)

export default AgSelect
