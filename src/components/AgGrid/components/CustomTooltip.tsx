import {ITooltipParams} from 'ag-grid-community'
import {ForwardedRef, forwardRef, useImperativeHandle} from 'react'

import styles from './CustomTooltip.module.scss'

interface CustomTooltipProps extends ITooltipParams {}

const CustomTooltip = (
  tooltipProps: CustomTooltipProps,
  // Ref comes from ag-grid, valid any usage
  ref: ForwardedRef<any>,
) => {
  useImperativeHandle(ref, () => ({
    getReactContainerClasses() {
      return [styles.customTooltip]
    },
  }))

  return <div className={styles.customTooltip}>Tooltip top</div>
}

export default forwardRef(CustomTooltip)
