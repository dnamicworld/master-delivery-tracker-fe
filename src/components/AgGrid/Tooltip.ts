import {ColDef} from 'ag-grid-community'
import {ReactElement} from 'react'

import {TooltipProps} from 'src/DS/components'

export interface AgTooltipProps extends TooltipProps {
  // Data comes from ag-grid, valid any usage
  textBuilder: (data: any, colDef?: ColDef) => ReactElement
}
