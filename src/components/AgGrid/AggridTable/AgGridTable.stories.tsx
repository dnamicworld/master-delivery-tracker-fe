import {Story} from '@storybook/react'

import {useEffect, useState} from 'react'

import {AgGridTable} from '../../index'

import {AgGridTableProps} from '.'

interface DataInterface {
  header: string
  field: string
}

interface ExtendedAgGridTableProps
  extends Omit<AgGridTableProps<DataInterface>, 'columnDefs' | 'rowData'> {
  columnDefs: string
  rowData: string
}

const defaultColumnDefs = [
  {
    headerName: 'A&R Admin',
    field: 'admin',
  },
  {
    headerName: 'Comments',
    field: 'comments',
  },
  {
    headerName: 'Title',
    field: 'title',
  },
  {
    headerName: 'Total %',
    field: 'total',
  },
  {
    headerName: 'Product Type',
    field: 'type',
  },
  {
    headerName: 'Release Date',
    field: 'date',
  },
]

const defaultRowData = [
  {
    admin: 'Aretha Kofax1',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
  },
  {
    admin: 'Aretha Kofax2',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
  },
  {
    admin: 'Aretha Kofax3',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
  },
  {
    admin: 'Aretha Kofax4',
    comments: 'Engineers in the process of sending remaining assets',
    title: 'Don’t Feed The Pop Monster',
    total: '55%',
    type: 'Album',
    date: '25 Apr 2020',
  },
]

export default {
  title: 'AgGridTable',
  component: AgGridTable,
  args: {
    columnDefs: [],
    rowData: [],
  },
  argTypes: {
    fullWidth: {
      control: {type: 'boolean'},
    },
  },
}

const Template: Story<ExtendedAgGridTableProps> = ({
  columnDefs,
  rowData,
  ...args
}) => {
  const [rowDataSate, setRowDataSate] = useState<string>(rowData)
  useEffect(() => {
    setRowDataSate(rowData)
  }, [rowData])
  return (
    <AgGridTable
      columnDefs={JSON.parse(columnDefs)}
      rowData={JSON.parse(rowDataSate)}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  )
}

export const Default = Template.bind({})

Default.args = {
  columnDefs: JSON.stringify(defaultColumnDefs),
  rowData: JSON.stringify(defaultRowData),
}
