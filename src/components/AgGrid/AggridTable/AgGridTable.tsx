import {
  ColDef,
  GridApi,
  GridReadyEvent,
  ICellRendererParams,
  RowNode,
} from 'ag-grid-community'
import {AgGridReact, AgGridColumn} from 'ag-grid-react'
import classnames from 'classnames'
import {useCallback, useEffect, useMemo, useRef, useState} from 'react'

import {DOM, productPage} from '../../../utils/constants'
import {uniquely, isTableDeleteAction} from '../../../utils/helpers'

import {SelectCellEditor, PopupTextCellEditor} from '../CellEditors'
import {CustomCell, WithEditButton, TextFieldCell} from '../CellRenderers'
import {CustomHeader, HeaderWithIcon} from '../HeaderComponents'
import {Kabob} from '../components'

import {AgGridTableProps} from './AgGridTable.model'

const getElementScrollPosition = (
  element: HTMLElement,
  offsetFromTop: number,
) => {
  const bodyRect = document.body.getBoundingClientRect()
  const elemRect = element.getBoundingClientRect()
  return elemRect.top - bodyRect.top - offsetFromTop
}

const getRowHeight = ({data}: RowNode) => (isTableDeleteAction(data) ? 0 : 60)

const getRowStyle = ({data}: RowNode) =>
  isTableDeleteAction(data) ? {display: 'none'} : {display: 'block'}

const getRowId = <T extends {id?: string}>(data: T) => data.id || uniquely()

export const updateCellHandler = (editableParams: ICellRendererParams) => {
  const {api, column, rowIndex} = editableParams
  api?.startEditingCell({
    rowIndex,
    colKey: column?.getColId() ?? '',
  })
}

const isMacOS = () => navigator.userAgent.includes('Mac OS')

const AgGridTable = <T extends {}>({
  gridRef,
  children,
  gridOptions,
  rowData,
  columnDefs,
  fullWidth,
  onTableReady,
  kabob,
  frameworkComponents,
  pagination,
  paginationPageSize,
  postSort,
  ...remainingTableProps
}: AgGridTableProps<T>) => {
  const [columns, setColumns] = useState(columnDefs)
  const [gridApi, setGridApi] = useState<GridApi | null>(null)
  const tableRef = useRef<HTMLDivElement>(null)

  const onGridReady = useCallback(
    (params: GridReadyEvent) => {
      setGridApi(params.api)
      if (fullWidth) params.api.sizeColumnsToFit()
      onTableReady?.(params)
    },
    [fullWidth, onTableReady, setGridApi],
  )
  useEffect(() => {
    const handleScroll = () => {
      if (tableRef.current) {
        const element = tableRef.current
        const headersShouldBeSticky = element
          ? window.pageYOffset > getElementScrollPosition(element, 60)
          : false
        const STICKY_HEADER = 'sticky-header'
        const operation = headersShouldBeSticky ? 'add' : 'remove'
        element?.classList[operation](STICKY_HEADER)
      }
    }
    document.addEventListener('scroll', handleScroll)
    return () => document.removeEventListener('scroll', handleScroll)
  }, [tableRef])

  useEffect(() => {
    gridApi?.setColumnDefs(columns ?? [])
    gridApi?.refreshCells({force: true})
    gridApi?.resetRowHeights()
    gridApi?.setGetRowStyle(getRowStyle)
    setColumns(columns)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gridApi, columns])

  useEffect(() => {
    setColumns(columnDefs)
  }, [columnDefs])

  useEffect(() => {
    gridApi?.setRowData(rowData ?? [])
  }, [gridApi, rowData])

  const KabobDisplay = kabob ? (
    <Kabob options={kabob.options} multiple={kabob.multiple} />
  ) : null

  const agComponents = useMemo(
    () => ({
      SelectCellEditor,
      PopupTextCellEditor,
      HeaderWithIcon,
      CustomHeader,
      CustomCell,
      WithEditButton,
      TextFieldCell,
      ...frameworkComponents,
    }),
    [frameworkComponents],
  )

  let paginationSize = paginationPageSize

  if (paginationPageSize === undefined && pagination) {
    paginationSize = productPage
  }

  const agOptions = useMemo(
    () => ({
      immutableData: true,
      suppressColumnMoveAnimation: true,
      getRowHeight,
      getRowStyle,
      suppressMovableColumns: true,
      rowBuffer: 5,
      debounceVerticalScrollbar: true,
      ...(postSort ? {postSort} : {}),
      ...gridOptions,
    }),
    [gridOptions, postSort],
  )

  return (
    <div
      className={classnames(
        'ag-theme-bootstrap ag-theme-custom',
        DOM.AG_TABLE,
        {
          isMacOS: isMacOS(),
        },
      )}
      ref={tableRef}
    >
      {KabobDisplay}
      <AgGridReact
        ref={gridRef}
        pagination={pagination}
        paginationPageSize={paginationSize}
        suppressPaginationPanel={pagination}
        suppressScrollOnNewData={pagination}
        applyColumnDefOrder
        suppressClickEdit
        enableCellTextSelection
        getRowNodeId={getRowId}
        rowDragManaged
        headerHeight={51}
        onGridReady={onGridReady}
        domLayout='autoHeight'
        popupParent={document.querySelector('body') ?? undefined}
        defaultColDef={{
          wrapText: true,
        }}
        rowData={rowData}
        frameworkComponents={agComponents}
        gridOptions={agOptions}
        // This prop usage is fine, because it extend AgGridReactProps
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...remainingTableProps}
      >
        {columns?.map((column: ColDef) => (
          <AgGridColumn
            // eslint-disable-next-line react/jsx-props-no-spreading
            {...column}
            key={column?.field ?? uniquely()}
          />
        ))}
        {children}
      </AgGridReact>
    </div>
  )
}

export default AgGridTable
