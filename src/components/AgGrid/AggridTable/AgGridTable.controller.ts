import {GridOptions, ICellRendererParams, RowNode} from 'ag-grid-community'

import {SelectOption} from '../../../DS/components'
import {get, isTableDeleteAction, uniquely} from '../../../utils/helpers'

import {SelectCellEditorProps} from '../CellEditors'

export const getOptions = (
  options: SelectCellEditorProps['options'],
  valueKey?: SelectCellEditorProps['valueKey'],
  displayKey?: SelectCellEditorProps['displayKey'],
): SelectOption[] =>
  options.map((option) => {
    const valueProp = valueKey ?? 'value'
    const displayProp = displayKey ?? 'name'
    const value = get(option, valueProp)
    const display = get(option, displayProp)

    const formattedOption = {
      value,
      name: display,
    }

    return formattedOption as SelectOption
  })

export const getElementScrollPosition = (
  element: HTMLElement,
  offsetFromTop: number,
) => {
  const bodyRect = document.body.getBoundingClientRect()
  const elemRect = element.getBoundingClientRect()
  return elemRect.top - bodyRect.top - offsetFromTop
}

export const handleScroll = (table: HTMLDivElement | null) => () => {
  if (table) {
    const headersShouldBeSticky = table
      ? window.pageYOffset > getElementScrollPosition(table, 60)
      : false
    const STICKY_HEADER = 'sticky-header'
    const operation = headersShouldBeSticky ? 'add' : 'remove'
    table?.classList[operation](STICKY_HEADER)
  }
}

const getRowHeight = ({data}: RowNode) => (isTableDeleteAction(data) ? 0 : 60)

const getRowStyle = ({data}: RowNode) =>
  isTableDeleteAction(data) ? {display: 'none'} : {display: 'block'}

const getRowNodeId = <T extends {id?: string}>(data: T) => data.id || uniquely()

export const agGeneralOptions: GridOptions = {
  applyColumnDefOrder: true,
  enableCellTextSelection: true,
  debounceVerticalScrollbar: true,
  defaultColDef: {
    wrapText: true,
  },
  domLayout: 'autoHeight',
  getRowNodeId,
  getRowHeight,
  getRowStyle,
  headerHeight: 51,
  immutableData: true,
  rowBuffer: 5,
  rowDragManaged: true,
  suppressClickEdit: true,
  suppressColumnMoveAnimation: true,
  suppressMovableColumns: true,
}

export const updateCellHandler = (editableParams: ICellRendererParams) => {
  const {api, column, rowIndex} = editableParams
  api?.startEditingCell({
    rowIndex,
    colKey: column?.getColId() ?? '',
  })
}
