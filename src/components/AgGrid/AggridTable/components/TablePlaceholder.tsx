import {ColDef} from 'ag-grid-community'

import {useMemo} from 'react'

import {Placeholder} from '../../..'
import {cloneDeep, uniquely} from '../../../../utils/helpers'
import {AgGridTableProps} from '../AgGridTable.model'

interface TablePlaceholderProps<T> {
  columnDefs: AgGridTableProps<T>['columnDefs']
  paginationSize?: number
}
const TablePlaceholder = <T extends object>({
  columnDefs,
  paginationSize = 30,
}: TablePlaceholderProps<T>) => {
  const values = useMemo(
    () => Array(paginationSize).fill(null),
    [paginationSize],
  )

  const columnSizes = useMemo(() => {
    const filteredColumns = columnDefs?.filter((column: ColDef) => !column.hide)
    return (
      filteredColumns?.reduce((prev: number[], column: ColDef) => {
        const prevCopy = cloneDeep(prev)
        const columnElement = document.querySelector(
          `.ag-header-cell[col-id="${column.field}"]`,
        )
        if (columnElement?.clientWidth) prevCopy.push(columnElement.clientWidth)
        return prevCopy
      }, []) ?? []
    )
  }, [columnDefs])

  return (
    <div className='w-100'>
      {values.map(() =>
        columnSizes.map((size) => {
          const width = size ? `${size}px` : undefined
          return (
            <Placeholder
              key={uniquely()}
              className='d-inline-block'
              width={width}
            />
          )
        }),
      )}
    </div>
  )
}

export default TablePlaceholder
