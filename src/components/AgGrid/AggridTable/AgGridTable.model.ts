import {GridReadyEvent, RowNode} from 'ag-grid-community'
import {AgGridReactProps} from 'ag-grid-react'
import {MutableRefObject, ReactNode} from 'react'

import {KabobProps} from '../components'

export interface AgGridTableProps<T> extends AgGridReactProps<T> {
  gridRef?: MutableRefObject<undefined>
  children?: ReactNode
  fullWidth?: boolean
  onTableReady?: (event: GridReadyEvent) => void
  kabob?: KabobProps
  paginationPageSize?: number
  pagination?: boolean
  postSort?: (nodes: RowNode[]) => void
}
