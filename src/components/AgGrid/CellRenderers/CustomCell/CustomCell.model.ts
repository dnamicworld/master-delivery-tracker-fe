import {ICellRendererParams, RowNode} from 'ag-grid-community'

import {IconProps} from 'src/DS/components'

import {AppColors} from '../../../../utils/constants'
import {AgTooltipProps} from '../../Tooltip'

export interface AgExtendedIcon extends Omit<IconProps, 'onClick'> {
  color?: 'gray'
  hoverColor?: string
  getIconName?: (data?: RowNode) => string
  getIconColor?: <T>(value: T, data: RowNode) => keyof typeof AppColors
  onClick?: (rowData: ICellRendererParams) => void
  tooltip?: AgTooltipProps
  cellParams: CustomCellParams
}

export interface CustomCellParams extends ICellRendererParams {
  className?: String
  contentClassName?: String
  tooltip?: AgTooltipProps
  showValueOnTooltip?: Boolean
  rightIcon?: AgExtendedIcon
  leftIcon?: AgExtendedIcon
  editable?: boolean
  wrapWithEllipsis?: boolean
  getClassNames?: (params: CustomCellParams) => string
  hideText?: boolean
}
