import classNames from 'classnames'

import {Button, Icon, Tooltip} from '../../../../../../DS/components'
import {isEmpty} from '../../../../../../utils/helpers'
import {AgExtendedIcon} from '../../CustomCell.model'

const CustomCellIcon = ({
  cellParams,
  className,
  color,
  disabled,
  getIconColor,
  getIconName,
  iconName,
  onClick,
  tooltip,
  type,
  hoverColor,
}: AgExtendedIcon) => {
  const {data, colDef, value} = cellParams
  const icon = getIconName ? getIconName(data) : iconName
  const iconColor = getIconColor ? getIconColor(value, data) : color

  let content = (
    <Button
      className={classNames(className, 'p-0')}
      disabled={disabled}
      containerStyle='link'
      onClick={() => onClick?.(cellParams)}
    >
      <Icon
        color={iconColor}
        hoverColor={hoverColor}
        type={type}
        iconName={icon}
      />
    </Button>
  )

  if (!isEmpty(tooltip)) {
    const tooltipContent =
      tooltip?.tooltipChildren ?? tooltip?.textBuilder(data, colDef)

    content = (
      <Tooltip
        id='tooltip'
        type={tooltip?.type}
        tooltipChildren={tooltipContent}
      >
        {content}
      </Tooltip>
    )
  }

  return content
}

export default CustomCellIcon
