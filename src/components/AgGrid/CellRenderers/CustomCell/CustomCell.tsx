import classnames from 'classnames'
import {PropsWithChildren, ReactNode, useMemo} from 'react'

import {Tooltip} from 'src/DS/components'

import {isEmpty} from '../../../../utils/helpers'

import {AgExtendedIcon, CustomCellParams} from './CustomCell.model'
import styles from './CustomCell.module.scss'
import {CustomCellIcon} from './components'

const getIconButton = (
  className: string,
  cellParams: CustomCellParams,
  icon?: AgExtendedIcon,
): ReactNode | null => {
  if (icon && !isEmpty(icon)) {
    return (
      <CustomCellIcon
        cellParams={cellParams}
        className={classnames(className, icon.className)}
        color={icon.color}
        disabled={icon.disabled}
        getIconColor={icon.getIconColor}
        getIconName={icon.getIconName}
        iconName={icon.iconName}
        onClick={icon.onClick}
        tooltip={icon.tooltip}
        type={icon.type}
        hoverColor={icon.hoverColor}
      />
    )
  }
  return null
}

const CustomCell = (params: PropsWithChildren<CustomCellParams>) => {
  const {
    children,
    value,
    valueFormatted,
    tooltip,
    data,
    colDef,
    showValueOnTooltip,
    rightIcon,
    leftIcon,
    className,
    getClassNames,
    hideText,
    contentClassName,
  } = params

  const contentValue: ReactNode = children ?? valueFormatted ?? value

  const leftIconComponent = useMemo(
    () => getIconButton('mr-2', params, leftIcon),
    [params, leftIcon],
  )

  const rightIconComponent = useMemo(
    () => getIconButton('mr-2', params, rightIcon),
    [params, rightIcon],
  )

  let content = (
    <div
      className={classnames(
        styles.customCell,
        className,
        getClassNames?.(params),
      )}
    >
      {leftIconComponent}
      <span className={classnames(styles.content, contentClassName)}>
        {hideText ? null : contentValue}
      </span>
      {rightIconComponent}
    </div>
  )

  if ((!isEmpty(tooltip) || showValueOnTooltip) && contentValue) {
    let tooltipContent

    if (showValueOnTooltip) {
      tooltipContent = contentValue
    } else {
      tooltipContent =
        tooltip?.tooltipChildren ?? tooltip?.textBuilder(data, colDef)
    }

    content = (
      <Tooltip
        id='tooltip'
        type={tooltip?.type}
        tooltipChildren={tooltipContent}
      >
        {content}
      </Tooltip>
    )
  }

  return content
}
export default CustomCell
