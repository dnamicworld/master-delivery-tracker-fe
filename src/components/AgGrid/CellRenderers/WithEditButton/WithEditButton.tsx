import classnames from 'classnames'
import {PropsWithChildren} from 'react'

import {updateCellHandler} from '../../..'
import {FA_ICONS, ICON_TYPES} from '../../../../utils/constants'
import {AgExtendedIcon, CustomCell, CustomCellParams} from '../CustomCell'

import styles from './WithEditButton.module.scss'

interface WithEditButtonProps
  extends CustomCellParams,
    Pick<AgExtendedIcon, 'onClick'> {}

const WithEditButton = (params: PropsWithChildren<WithEditButtonProps>) => {
  const {contentClassName, colDef, rightIcon} = params

  const handleOnClick = rightIcon?.onClick || updateCellHandler

  const editIcon: AgExtendedIcon = {
    disabled: !colDef?.editable,
    type: ICON_TYPES.far,
    iconName: FA_ICONS.pencil,
    onClick: () => handleOnClick(params),
    ...rightIcon,
    cellParams: params,
    className: classnames(styles.editButton, rightIcon?.className),
  }

  return (
    <CustomCell
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...params}
      contentClassName={classnames('pr-15', contentClassName)}
      rightIcon={editIcon}
    />
  )
}

export default WithEditButton
