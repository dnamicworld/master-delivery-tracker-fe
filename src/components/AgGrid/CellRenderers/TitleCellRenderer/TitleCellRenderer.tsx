import {ICellRendererParams} from 'ag-grid-community'

import {getDetailPathname} from '../../../../pages/shared/IndexPages'

import {LinkCellRenderer} from '../LinkCellRenderer'

import styles from './TitleCellRenderer.module.scss'

interface TitleCellRendererParams extends ICellRendererParams {
  detailRoute: (data: any) => string
}

const TitleCellRenderer = (params: TitleCellRendererParams) => {
  const {detailRoute, data, value, valueFormatted} = params
  return (
    <div className={styles.titleRenderer}>
      <LinkCellRenderer
        value={value}
        valueFormatted={valueFormatted}
        path={getDetailPathname(detailRoute(data), data.id)}
        data
      />
      <span className='small'>{data.artist}</span>
    </div>
  )
}

export default TitleCellRenderer
