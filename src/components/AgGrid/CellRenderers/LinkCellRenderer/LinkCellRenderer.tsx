import {ICellRendererParams} from 'ag-grid-community'
import {Link} from 'react-router-dom'

import {Tooltip} from '../../../../DS/components'

import {AgTooltipProps} from '../../Tooltip'

interface LinkCellRendererParams
  extends Pick<ICellRendererParams, 'value' | 'valueFormatted' | 'data'> {
  path: string | ((params: ICellRendererParams) => string)
  tooltip?: AgTooltipProps
}

const LinkCellRenderer = ({
  valueFormatted,
  value,
  path,
  tooltip,
  data,
}: LinkCellRendererParams) => {
  const cellText = valueFormatted ?? value
  const linkComponent = (
    <Link
      to={{
        pathname: typeof path === 'string' ? path : path(data),
      }}
    >
      {cellText}
    </Link>
  )
  const tooltipComponent = (
    <Tooltip id='tooltip' type={tooltip?.type} tooltipChildren={cellText}>
      {linkComponent}
    </Tooltip>
  )

  return tooltipComponent
}

export default LinkCellRenderer
