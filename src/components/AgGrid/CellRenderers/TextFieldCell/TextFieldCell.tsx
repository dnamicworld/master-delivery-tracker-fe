import React, {FC, useRef} from 'react'

import {updateCellHandler} from '../../AggridTable'
import {CustomCell} from '../CustomCell'

import * as CellRenderers from '../index'

import {TextFieldCellParams, FieldProp} from './TextFieldCell.model'
import * as Fields from './fields'
import {AgGridInputProps} from './fields/AgGridInput/AgGridInput'

interface FieldProps {
  placeholder?: string
  invalid?: boolean
}

const TextFieldCell = (params: TextFieldCellParams) => {
  const {cellRenderer, editing, field, fieldProps, ...customCellProps} = params
  const {value} = customCellProps
  const validFieldProps = fieldProps as FieldProps

  const inputRef = useRef<HTMLInputElement>()

  const FieldComponent: React.FC<
    {ref: typeof inputRef} & FieldProp & AgGridInputProps
  > = Fields[field ?? 'AgGridInput']

  let content
  if (cellRenderer) {
    const CellRendererComponent: FC<TextFieldCellParams> =
      CellRenderers[cellRenderer]
    // eslint-disable-next-line react/jsx-props-no-spreading
    content = <CellRendererComponent {...customCellProps} />
  } else {
    // eslint-disable-next-line react/jsx-props-no-spreading
    content = <CustomCell {...customCellProps} />
  }

  const handleFocus = () => {
    updateCellHandler(customCellProps)
  }
  return !editing ? (
    content
  ) : (
    <FieldComponent
      readOnly
      invalid={!!validFieldProps?.invalid}
      key={value}
      ref={inputRef}
      value={value}
      onFocus={handleFocus}
      placeholder={validFieldProps?.placeholder}
    />
  )
}

export default TextFieldCell
