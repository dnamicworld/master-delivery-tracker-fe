import {AllHTMLAttributes} from 'react'

import {CustomCellParams} from '../CustomCell'

export type FieldProp = AllHTMLAttributes<HTMLInputElement>

export interface TextFieldCellParams extends CustomCellParams {
  cellRenderer?: string
  editing?: boolean
  field?: string
  fieldProps?: FieldProp
}
