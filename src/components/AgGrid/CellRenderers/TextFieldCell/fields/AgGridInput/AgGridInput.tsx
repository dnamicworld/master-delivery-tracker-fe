import classnames from 'classnames'
import {AllHTMLAttributes, ForwardedRef, forwardRef} from 'react'

import styles from './AgGridInput.module.scss'

export interface AgGridInputProps extends AllHTMLAttributes<HTMLInputElement> {
  invalid: boolean
}

const AgGridInput = forwardRef(
  (
    {
      className,
      placeholder,
      invalid,
      defaultValue,
      onFocus,
      onBlur,
      onChange,
      value,
      readOnly,
    }: AgGridInputProps,
    ref: ForwardedRef<HTMLInputElement>,
  ) => (
    <div className='ag-wrapper ag-input-wrapper ag-text-field-input-wrapper'>
      <input
        placeholder={placeholder}
        ref={ref}
        value={value}
        defaultValue={defaultValue}
        onFocus={onFocus}
        onBlur={onBlur}
        onChange={onChange}
        readOnly={readOnly}
        className={classnames(
          styles.customAgInputFieldInput,
          {[styles.withValidationError]: invalid && !defaultValue},
          'ag-input-field-input ag-text-field-input focus-visible',
          className,
        )}
      />
    </div>
  ),
)

export default AgGridInput
