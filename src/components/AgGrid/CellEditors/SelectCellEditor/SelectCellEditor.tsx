import {ICellEditorParams} from 'ag-grid-community'

import {
  forwardRef,
  ReactNode,
  useEffect,
  useImperativeHandle,
  useRef,
  useState,
} from 'react'

import {SelectOption, SelectProps} from '../../..'
import {getOptions} from '../../AggridTable/AgGridTable.controller'
import {AgSelect} from '../../components'

import styles from './SelectCellEditor.module.scss'

export interface SelectCellEditorProps extends ICellEditorParams {
  cellRenderer?: ReactNode
  options: SelectOption[]
  displayKey?: string
  valueKey?: string
  onSelect: <T>(value: T, cellEditorParams: ICellEditorParams) => void
  field: string
}

const selectCellEditorDefaultProps: Partial<SelectCellEditorProps> = {
  displayKey: 'name',
  valueKey: 'value',
}

const SelectCellEditor = forwardRef(
  (editorParams: SelectCellEditorProps, ref) => {
    const {options, displayKey, valueKey, value, onSelect, api} = editorParams
    const refContainer = useRef(null)
    const [editing, setEditing] = useState(true)

    useEffect(() => {
      if (!editing) {
        api?.stopEditing()
      }
    }, [api, editing])

    useImperativeHandle(ref, () => ({
      getValue() {
        return value
      },
      isPopup() {
        return true
      },
    }))

    const handleClick: SelectProps['onChange'] = (
      event,
      optionValue: SelectProps['value'],
    ) => {
      onSelect(optionValue, editorParams)
      setEditing(false)
    }
    const selectOptions = getOptions(options, valueKey, displayKey)

    return (
      <AgSelect
        className={styles.select_cell_renderer_container}
        ref={refContainer}
        value={value}
        onChange={handleClick}
        options={selectOptions}
      />
    )
  },
)

SelectCellEditor.defaultProps = selectCellEditorDefaultProps

export default SelectCellEditor
