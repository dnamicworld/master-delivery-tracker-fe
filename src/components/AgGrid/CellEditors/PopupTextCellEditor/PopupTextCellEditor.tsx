import {ICellEditorParams} from 'ag-grid-community'
import {ChangeEvent, forwardRef, useImperativeHandle, useState} from 'react'

import {TextField, TooltipDisplay} from '../../../../DS/components'

export interface PopupTextCellEditorProps extends ICellEditorParams {
  name: string
  placeholder: string
  withinTooltip?: boolean
}

const PopupTextCellEditor = forwardRef(
  (
    {value, name, placeholder, withinTooltip}: PopupTextCellEditorProps,
    ref,
  ) => {
    const [innerValue, setInnerValue] = useState<string>(value)

    useImperativeHandle(ref, () => ({
      getValue() {
        return innerValue
      },
      isPopup() {
        return true
      },
    }))

    const handleChange = ({
      target,
      currentTarget,
    }: ChangeEvent<HTMLInputElement>) => {
      const newValue = target.value ?? currentTarget.value
      setInnerValue(newValue)
    }

    const inputField = (
      <TextField
        name={name}
        placeholder={placeholder}
        value={innerValue}
        onChange={handleChange}
      />
    )

    return withinTooltip ? (
      <TooltipDisplay
        id='tooltip'
        className='in'
        placement='top'
        tooltipChildren={inputField}
      />
    ) : (
      inputField
    )
  },
)

export default PopupTextCellEditor
