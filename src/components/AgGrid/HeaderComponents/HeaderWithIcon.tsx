import {IHeaderParams} from 'ag-grid-community'
import classNames from 'classnames'
import {CSSProperties, useEffect, useRef, useState} from 'react'
import {Modal} from 'react-overlays'

import {Icon, Button, SelectAction, SelectProps} from '../../../DS/components'
import {DOM, ICON_TYPES} from '../../../utils/constants'
import {getOptions} from '../AggridTable/AgGridTable.controller'
import {AgSelect, AgSelectProps} from '../components'

export interface HeaderSelectOptions extends Pick<SelectProps, 'value'> {
  action?: SelectAction
  headerParams?: HeaderWithIconProps
}

interface HeaderWithIconProps extends IHeaderParams {
  title?: string
  colId: string
  icon: string
  options: AgSelectProps['options']
  displayKey?: string
  valueKey?: string
  alignRight?: boolean
  leftContent?: boolean
  iconShowOnHover?: boolean
  disabled?: boolean
  onChange?: (params: HeaderSelectOptions) => void
}

const getElementStyles = (
  element: Element | null,
  alignRight?: boolean,
): CSSProperties => {
  const elementPosition = element?.getBoundingClientRect()
  const width = elementPosition?.width ?? 0
  const top = `${elementPosition?.top ?? 0}px`
  // Using position.right doesn't work to align right, only position.left behaves as expected
  const left = `${(elementPosition?.left ?? 0) - (alignRight ? width : 0)}px`
  return {position: 'fixed', top, left}
}

const HeaderWithIcon = (headerParams: HeaderWithIconProps) => {
  const {
    eGridHeader,
    title,
    colId,
    icon,
    options,
    alignRight,
    leftContent,
    iconShowOnHover,
    disabled,
    onChange,
    valueKey,
    displayKey,
  } = headerParams

  const [show, setShow] = useState<boolean>(false)
  const modal = useRef<HTMLDivElement>(null)
  const element = document.querySelector(`[col-id="${colId}"]`)
  const overlayStyles = getElementStyles(element, alignRight)

  useEffect(() => {
    const agTable = eGridHeader.closest(`.${DOM.AG_TABLE}`)
    const closeModal = (event: Event) => {
      if (!modal.current?.contains(event.target as Node)) {
        setShow(false)
      }
    }
    agTable?.addEventListener('mousedown', closeModal)
    return () => {
      agTable?.removeEventListener('mousedown', closeModal)
    }
  }, [eGridHeader, modal])

  const titleSection = title ? (
    <>
      {title}
      &nbsp;
    </>
  ) : null

  const handleClick: SelectProps['onChange'] = (
    event,
    value: SelectProps['value'],
    action: SelectAction,
  ) => {
    setShow(false)
    onChange?.({value, action, headerParams})
  }

  return (
    <>
      {titleSection}
      <Button
        disabled={disabled}
        className={classNames({
          editable_column_button: iconShowOnHover,
        })}
        onClick={() => setShow(true)}
        containerStyle='link'
      >
        <Icon iconName={icon} type={ICON_TYPES.fas} />
      </Button>
      <Modal show={show} style={overlayStyles}>
        <div ref={modal} className='ag-popup-editor ag-ltr ag-popup-child'>
          <div className='ag-react-container'>
            <AgSelect
              name='headerSelect'
              value={null}
              alignLeft={leftContent}
              onChange={handleClick}
              options={getOptions(options, valueKey, displayKey)}
            />
          </div>
        </div>
      </Modal>
    </>
  )
}

export default HeaderWithIcon
