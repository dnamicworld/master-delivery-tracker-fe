import {IHeaderParams} from 'ag-grid-community'
import classnames from 'classnames'
import {cloneElement, ReactElement} from 'react'

import {omit} from '../../../../utils/helpers'

import styles from './CustomHeader.module.scss'

export interface CustomHeaderParams extends IHeaderParams {
  className?: string
  children?: ReactElement
}

const CustomHeader = (params: CustomHeaderParams) => {
  const {children, className, displayName} = params

  const childrenElement = children
    ? cloneElement(children, omit(params, ['className', 'children']))
    : null

  return (
    <div className={classnames(styles.custom_header, className)}>
      {displayName ? (
        <div className='custom_header__display_name'>{displayName}</div>
      ) : null}
      {children ? (
        <div className='custom_header__children'>{childrenElement}</div>
      ) : null}
    </div>
  )
}

export default CustomHeader
