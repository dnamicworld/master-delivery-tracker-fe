import classNames from 'classnames'
import {useHistory, Link} from 'react-router-dom'

import {Button, Icon, Navbar, Popover, Tooltip} from 'src/DS/components'
import {AsperaPopover} from 'src/components'

import {useCreateReport} from '../../context/createReport'
import {useSearch} from '../../context/search'

import useUser from '../../hooks/user'
import {
  BREADCRUMBS,
  FA_ICONS,
  HelpPageContent,
  ICON_TYPES,
  NAVBAR_PDF,
  ROUTES,
} from '../../utils/constants'

import styles from './AppNavbar.module.scss'

const AppNavbar = () => {
  const history = useHistory()
  const {user} = useUser()
  const search = useSearch()
  const navigationLinks = [
    {
      name: BREADCRUMBS.LABELS.label,
      to: BREADCRUMBS.LABELS.link,
    },
    {
      name: 'Assets Report',
      to: ROUTES.ASSET_REPORT,
    },
  ]
  const {
    location: {pathname},
  } = history
  const routesToInclude = [
    ROUTES.LABEL,
    ROUTES.HOME,
    ROUTES.ALBUM_INDEX,
    ROUTES.SINGLE_INDEX,
  ]

  const shouldShowCreateReport =
    routesToInclude.some((routeToValidate) =>
      pathname.includes(routeToValidate, 0),
    ) && user.roles.hasFullWriteAccess()

  const {setShowReportModal} = useCreateReport()
  return (
    <Navbar title='Master Delivery Report' fixedTop navItems={[]}>
      <div
        className={classNames(
          styles.appNavbar,
          'd-flex justify-content-end align-items-center h-100',
        )}
      >
        {navigationLinks.map((link) => (
          <Link
            key={link.name}
            className={classNames(
              styles.navbarItems,
              'font-weight-sbold ml-40',
            )}
            to={link.to}
          >
            <h4>{link.name}</h4>
          </Link>
        ))}
        <span className='navbarIcons'>
          <Tooltip
            id='tooltip'
            placement='bottom'
            tooltipChildren={HelpPageContent.icons.search}
          >
            <span className='ml-40'>
              <Icon
                type={ICON_TYPES.fal}
                iconName={FA_ICONS.search}
                className={classNames({
                  [styles.active]: search.showSearch,
                })}
                onClick={() => {
                  search.updateSearch(!search.showSearch)

                  search.handleClick()
                }}
              />
            </span>
          </Tooltip>
          <Tooltip
            id='tooltip'
            placement='bottom'
            tooltipChildren={HelpPageContent.icons.PDF}
          >
            <a href={NAVBAR_PDF} download className='ml-40'>
              <Icon
                type={ICON_TYPES.fal}
                iconName={FA_ICONS.invoice}
                className='text-gray'
              />
            </a>
          </Tooltip>
          <Tooltip
            id='tooltip'
            placement='bottom'
            tooltipChildren={HelpPageContent.icons.help}
          >
            <span className='ml-40 mr-20'>
              <Icon
                type={ICON_TYPES.fal}
                className='text-gray'
                iconName={FA_ICONS.circleQuestion}
                onClick={() => {
                  history.push(ROUTES.HELP)
                }}
              />
            </span>
          </Tooltip>
          <Popover
            id='popover'
            placement='bottom'
            trigger={['click']}
            popoverChildren={<AsperaPopover />}
          >
            <span className='ml-20 mr-40'>
              <Icon
                type={ICON_TYPES.fal}
                className='text-gray'
                iconName={FA_ICONS.cloudArrowUp}
              />
            </span>
          </Popover>
        </span>
        <Button
          bsStyle='primary'
          className={classNames('mr-20', {
            'd-none': !shouldShowCreateReport,
          })}
          onClick={() => {
            setShowReportModal(true)
          }}
        >
          <Icon
            className='mr-2'
            type={ICON_TYPES.far}
            iconName={FA_ICONS.circlePlus}
          />
          New Report
        </Button>
      </div>
    </Navbar>
  )
}

export default AppNavbar
