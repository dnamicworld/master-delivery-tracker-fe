import classNames from 'classnames'
import {useEffect, useMemo, useState} from 'react'

import {useHistory, useParams} from 'react-router-dom'

import {
  Col,
  HeaderTitle,
  Row,
  SelectProps,
  Select,
} from '../../../DS/components'
import {useAlertDisplay} from '../../../context/alert'
import useAllYears from '../../../hooks/data/useAllYears'
import useLabels from '../../../hooks/data/useLabels'
import {Report} from '../../../models'
import {ProductIndexRouteParams} from '../../../pages/shared/IndexPages'
import {ALBUM_TYPE, CREATE_REPORT, ROUTES} from '../../../utils/constants'
import {
  createReport,
  generateReportData,
  getCurrentYear,
  getLabelOptions,
  newReportInitialState,
  NewReportState,
  stringifyQuery,
} from '../../../utils/helpers'
import {getYearsFromArray} from '../../../utils/helpers/reports'
import {ContentModal} from '../../ContentModal'

import styles from './CreateReportModal.module.scss'

export interface CreateReportModalProps {
  shown: boolean
  onClose: () => void
  onSuccess?: (report?: Report) => void
}

const CreateReportModal = ({
  shown,
  onClose,
  onSuccess,
}: CreateReportModalProps) => {
  const history = useHistory()
  const {display} = useAlertDisplay()
  const {labelId} = useParams<ProductIndexRouteParams>()
  const currentYear = getCurrentYear()
  const [newReportState, setNewReportState] = useState<NewReportState>(
    newReportInitialState,
  )
  const labelParams = labelId
    ? {
        id: Number(labelId),
      }
    : {}

  const {labels} = useLabels(labelParams)

  const {allYears} = useAllYears(
    {labelId: newReportState.label},
    () => newReportState.label !== undefined,
  )

  const yearOptions = allYears ? getYearsFromArray(allYears) : []

  const labelsArray = useMemo(() => {
    if (labels) {
      return Array.isArray(labels) ? labels : [labels]
    }
    return []
  }, [labels])

  const labelOptions = getLabelOptions(labelsArray)

  const handleSetNewReport = (newValue: {[key: string]: string}) => {
    setNewReportState((oldReport) => ({
      ...oldReport,
      ...newValue,
    }))
  }
  const handleSelectChange: SelectProps['onChange'] = (
    event,
    value,
    selectAction,
  ) => {
    handleSetNewReport({[selectAction.name as string]: String(value)})
  }

  useEffect(() => {
    if (newReportState.label) {
      const valueReduce = yearOptions.find(
        (yearOption) => yearOption.value >= String(currentYear),
      )
      handleSetNewReport({
        year: valueReduce?.value,
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [JSON.stringify(yearOptions), newReportState.label])

  const createReportCB = async () => {
    const currentLabel = labelsArray.find(
      (label) => label && String(label?.id) === newReportState.label,
    )
    if (currentLabel && newReportState.year !== undefined) {
      try {
        const reportData = generateReportData(
          currentLabel,
          ALBUM_TYPE,
          newReportState.year,
        )
        const report = await createReport(reportData)
        setNewReportState(newReportInitialState)
        if (onSuccess) {
          onSuccess(report)
          onClose()
          display('Report Successfully Created!')
        } else {
          history.push({
            pathname: ROUTES.HOME,
            search: stringifyQuery({
              year: String(report?.year),
              label: String(report?.label.id),
              newReport: 'true',
            }),
          })
        }
      } catch (err) {
        // eslint-disable-next-line no-console
        console.log(err)
      }
    }
  }

  return (
    <ContentModal
      show={shown}
      title={CREATE_REPORT.title}
      actionName={CREATE_REPORT.action}
      onActionClick={createReportCB}
      onCancelAction={() => {
        setNewReportState(newReportInitialState)
        onClose()
      }}
      disabledSuccessButton={!newReportState.label && !newReportState.year}
      cancelButtonStyle={{
        containerStyle: 'outline',
        colorType: 'secondary-black',
      }}
    >
      <Row className='pb-10 px-10'>
        <Col sm={6}>
          <HeaderTitle className='pb-10'>Select a Label</HeaderTitle>
          <Select
            fullWidth
            onChange={handleSelectChange}
            noPlaceholder
            value={newReportState.label}
            name='label'
            options={labelOptions}
            className={classNames({
              [styles.borderBox]: newReportState.label,
            })}
          />
        </Col>
        <Col sm={6}>
          <HeaderTitle className='pb-10'>Select a Year</HeaderTitle>
          <Select
            fullWidth
            disabled={!newReportState.label}
            onChange={handleSelectChange}
            noPlaceholder
            value={newReportState.year}
            name='year'
            className={classNames('mr-20', {
              [styles.borderBox]: newReportState.year,
            })}
            options={yearOptions}
          />
        </Col>
      </Row>
    </ContentModal>
  )
}

export default CreateReportModal
