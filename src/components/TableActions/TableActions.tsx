import {ReactNode} from 'react'

import {Row, Col} from '../../DS/components'

interface TableActionsProps {
  title: ReactNode
  actions?: ReactNode
}

const TableActions = ({title, actions}: TableActionsProps) => (
  <Row className='pb-20'>
    <Col sm={actions ? 6 : 12}>{title}</Col>
    {actions ? <Col sm={6}>{actions}</Col> : null}
  </Row>
)

export default TableActions
