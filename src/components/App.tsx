import {BrowserRouter as Router} from 'react-router-dom'

import '../config/bootstrap'
import AppRouter from './AppRouter'
import '../assets/theme/styles.scss'

const App = () => (
  <Router>
    <AppRouter />
  </Router>
)
export default App
