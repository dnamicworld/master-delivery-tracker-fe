import classnames from 'classnames'
import {CSSProperties} from 'react'

import styles from './Placeholder.module.scss'

interface PlaceholderProps {
  className?: string
  borderCircle?: boolean
  height?: string
  width?: string
  noRadius?: boolean
}

const Placeholder = ({
  className,
  borderCircle,
  height,
  width,
  noRadius,
}: PlaceholderProps) => {
  const placeHolderImageProperties: CSSProperties = {
    height,
    width,
  }
  return (
    <div
      style={placeHolderImageProperties}
      className={classnames(styles.placeholder, className, {
        [styles.borderCircle]: borderCircle,
        [styles.noRadius]: noRadius,
      })}
    >
      &nbsp;
    </div>
  )
}
export default Placeholder
