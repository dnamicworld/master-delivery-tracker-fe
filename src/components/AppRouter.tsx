import {toRelativeUrl} from '@okta/okta-auth-js'
import {Security, SecureRoute} from '@okta/okta-react'

import {RestoreOriginalUriFunction} from '@okta/okta-react/bundles/types/OktaContext'
import {ComponentType, useEffect} from 'react'
import {Route, useHistory} from 'react-router-dom'

import oktaConfig from '../config/okta'
import {PageFilterContextProvider} from '../context/Filters/pageFilters'
import {ProductFilterContextProvider} from '../context/Filters/productsFilters'
import {UserContextProvider} from '../context/user'
import {ProductDetailsContextProvider} from '../hooks/productDetails'
import {TableContextProvider} from '../hooks/table'
import {
  AlbumsIndex,
  AlbumDetails,
  AssetReport,
  ProductPdf,
  Components,
  Login,
  Help,
  Home,
  Label,
  UnAuthorized,
  Report,
  ResultsPage,
  SinglesIndex,
  SingleDetails,
} from '../pages'
import {FEATURE_FLAGS, ROUTES} from '../utils/constants'

import {isFeatureFlagEnabled} from '../utils/helpers'

import {WithMainLayout} from './Layout'
import {WithMainLayoutProps} from './Layout/WithMainLayout'
import {LoginCallback} from './Okta'
import {SWRConfig} from './SWR'

export const withMainFilters = (
  Component: ComponentType<WithMainLayoutProps>,
) =>
  WithMainLayout((props) => (
    <PageFilterContextProvider>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <Component {...props} />
    </PageFilterContextProvider>
  ))

export const withProductFilters = (Component: ComponentType) =>
  withMainFilters((props) => (
    <ProductFilterContextProvider>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <Component {...props} />
    </ProductFilterContextProvider>
  ))

const AppRouter = () => {
  const history = useHistory()

  useEffect(() => {
    history.listen(() => {
      if (history.action === 'POP') {
        history.go(-1)
      }
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const restoreOriginalUri: RestoreOriginalUriFunction = async (
    _,
    originalUri,
  ) => {
    history.replace(toRelativeUrl(originalUri || '/', window.location.origin))
  }

  const customAuthHandler = () => {
    history.push(ROUTES.LOGIN)
  }

  return (
    <Security
      oktaAuth={oktaConfig}
      restoreOriginalUri={restoreOriginalUri}
      onAuthRequired={customAuthHandler}
    >
      <Route
        path={ROUTES.LOGIN}
        exact
        component={() => (
          <SWRConfig>
            <UserContextProvider>
              <Login />
            </UserContextProvider>
          </SWRConfig>
        )}
      />
      {isFeatureFlagEnabled(FEATURE_FLAGS.componentsPage) ? (
        <Route
          path={ROUTES.COMPONENTS}
          exact
          component={WithMainLayout(Components)}
        />
      ) : null}
      <SecureRoute path={ROUTES.HELP} exact component={withMainFilters(Help)} />
      <SecureRoute
        path={ROUTES.ASSET_REPORT}
        exact
        component={withMainFilters(AssetReport)}
      />
      <SecureRoute path={ROUTES.HOME} exact component={withMainFilters(Home)} />
      <SecureRoute
        path={`${ROUTES.SINGLE_INDEX}/:labelId`}
        exact
        component={withProductFilters(SinglesIndex)}
      />
      <SecureRoute
        path={`${ROUTES.LABEL}/:labelId`}
        exact
        component={withMainFilters(Label)}
      />
      <SecureRoute
        path={`${ROUTES.ALBUM_INDEX}/:labelId`}
        exact
        component={withProductFilters(AlbumsIndex)}
      />
      <SecureRoute
        path={`${ROUTES.ALBUM_DETAIL}/:productId?`}
        exact
        component={WithMainLayout(() => (
          <TableContextProvider>
            <ProductDetailsContextProvider>
              <AlbumDetails />
            </ProductDetailsContextProvider>
          </TableContextProvider>
        ))}
      />
      <SecureRoute
        path={`${ROUTES.SINGLE_DETAIL}/:productId?`}
        exact
        component={WithMainLayout(() => (
          <TableContextProvider>
            <ProductDetailsContextProvider>
              <SingleDetails />
            </ProductDetailsContextProvider>
          </TableContextProvider>
        ))}
      />
      <SecureRoute
        path={ROUTES.PRODUCT_PDF}
        exact
        component={WithMainLayout(() => (
          <TableContextProvider>
            <ProductDetailsContextProvider>
              <ProductPdf />
            </ProductDetailsContextProvider>
          </TableContextProvider>
        ))}
      />
      <SecureRoute
        path={ROUTES.RESULTS}
        exact
        component={WithMainLayout(() => (
          <ResultsPage />
        ))}
      />
      <SecureRoute
        path={ROUTES.REPORT}
        exact
        component={WithMainLayout(() => (
          <Report />
        ))}
      />
      <Route path={ROUTES.LOGIN_CB} component={() => <LoginCallback />} />
      <Route path={ROUTES.UNAUTHORIZED} component={UnAuthorized} />
    </Security>
  )
}
export default AppRouter
