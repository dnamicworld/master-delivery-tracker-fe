interface CardHeaderProps {
  img?: string
  title: string
}

const CardHeader = ({title, img}: CardHeaderProps) => (
  <div>
    <div className='wmg-label'>
      {img ? <img src={img} alt='card logo' /> : null}
      <h4 className='wmg-label__title m-0 pl-10 font-weight-sbold'>{title}</h4>
    </div>
  </div>
)

export default CardHeader
