import {ReactElement} from 'react'
import {SWRConfig} from 'swr'

import {appFetcher} from '../../config/axios'

interface SWRProps {
  children: ReactElement
}

const SWR = ({children}: SWRProps) => (
  <SWRConfig
    value={{
      fetcher: appFetcher,
    }}
  >
    {children}
  </SWRConfig>
)

export default SWR
