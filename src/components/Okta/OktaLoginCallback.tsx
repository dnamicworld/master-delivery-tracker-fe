import {useOktaAuth} from '@okta/okta-react'
import {ComponentType, FC, useEffect, useState} from 'react'

import {isEmpty} from '../../utils/helpers'

import OktaError from './OktaError'

export interface LoginCallbackProps {
  errorComponent?: ComponentType<{error: Error}>
}

interface LoginCallbackState {
  hasError: boolean
  error: Error
}

const loginErrorInitial = {
  hasError: false,
  error: {
    name: '',
    message: '',
  },
}

const LoginCallback: FC<LoginCallbackProps> = ({
  errorComponent,
}: LoginCallbackProps) => {
  const [loginError, setLoginError] =
    useState<LoginCallbackState>(loginErrorInitial)
  const {oktaAuth, authState} = useOktaAuth()

  const ErrorReporter = errorComponent || OktaError

  useEffect(() => {
    async function handleLogin() {
      try {
        const tokens = await oktaAuth.tokenManager.getTokens()
        await oktaAuth.handleLoginRedirect(
          !isEmpty(tokens) ? tokens : undefined,
        )
      } catch (err) {
        const error = err as Error
        setLoginError({
          hasError: true,
          error: {
            name: error?.name ?? '',
            message: error?.message ?? error.toString(),
          },
        })
      }
    }
    handleLogin()
  }, [oktaAuth])

  if (authState?.error || loginError.hasError) {
    return <ErrorReporter error={authState?.error || loginError.error} />
  }

  return null
}

export default LoginCallback
