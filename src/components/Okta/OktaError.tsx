import {Redirect} from 'react-router-dom'

import {ERRORS, ROUTES} from '../../utils/constants'

interface OktaErrorProps {
  error: Error
}

const displayError = (error: Error) =>
  error.name && error.message
    ? `${error.name}: {error.message}`
    : error.toString()

const OktaError = ({error}: OktaErrorProps) => {
  if (error.name === 'OAuthError' && error.message === ERRORS.NO_ACCESS) {
    return <Redirect push to={ROUTES.UNAUTHORIZED} />
  }
  return <p>Error: {displayError(error)}</p>
}

export default OktaError
