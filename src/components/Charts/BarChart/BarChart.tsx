import classNames from 'classnames'
import {ReactElement, ReactNode, useMemo} from 'react'

import {
  BarChart as ReBarChart,
  Bar,
  XAxis,
  YAxis,
  Cell,
  ResponsiveContainer,
  Text,
  TextProps,
  LabelList,
} from 'recharts'
import {LayoutType, Margin} from 'recharts/types/util/types'

import {CHART_COLORS, CHART_LABELS_COLOR} from '../../../utils/constants'

// Property in chart interface has any type
interface CustomizedLabelProps extends TextProps {
  fill?: string
  value?: number | string
  payload?: {
    [key: string]: any
    value: number | string
  }
}

export interface BarChartProps {
  layout?: LayoutType
  data: {[key: string]: string | number}[]
  yKey: string
  xKey: string
  leyendKey?: string
  barSize?: number
  withNumericAxis?: boolean
  colorIndex?: number
}

let ctx: CanvasRenderingContext2D | null

export const measureText14HelveticaNeue = (text: string) => {
  if (!ctx) {
    ctx = document.createElement('canvas').getContext('2d')
    if (ctx) {
      ctx.font = "14px 'Helvetica Neue"
    }
  }
  return ctx?.measureText(text).width
}

const BAR_AXIS_SPACE = 45

const BarChart = ({
  data,
  layout = 'horizontal',
  xKey,
  yKey,
  leyendKey = 'leyend',
  barSize = 8,
  withNumericAxis,
  colorIndex,
}: BarChartProps) => {
  const multiBars = data.length > 1
  const isVertical = layout === 'vertical'
  const maxTextWidth = useMemo(
    () =>
      data.reduce((acc, cur) => {
        const value = cur[yKey]
        const width = measureText14HelveticaNeue(value.toLocaleString())
        return width !== undefined && width > acc ? width : acc
      }, 0),
    [data, yKey],
  )
  const maxLabelWidth = useMemo(
    () =>
      data.reduce((acc, cur) => {
        const value = cur[xKey]
        const width = measureText14HelveticaNeue(value.toLocaleString())
        return width !== undefined && width > acc ? width : acc
      }, 0),
    [data, xKey],
  )

  let margin: Margin = {left: 60, right: maxTextWidth + (BAR_AXIS_SPACE - 8)} // vertical

  const getColor = (index: number) =>
    CHART_COLORS[colorIndex !== undefined ? colorIndex : index]

  const CustomizedLabel = ({
    x,
    y,
    dy,
    value,
    payload,
    type,
    orientation,
    height,
    className,
    stroke,
    fill,
  }: CustomizedLabelProps): ReactElement<SVGElement> => {
    const labelValue = payload ? payload.value : value
    return (
      <Text
        type={type}
        orientation={orientation}
        height={height}
        className={className}
        stroke={stroke}
        fill={fill}
        x={isVertical ? 90 : x}
        y={y}
        dy={isVertical ? dy : -4}
        verticalAnchor={isVertical ? 'middle' : 'end'}
        textAnchor='end'
        color={CHART_LABELS_COLOR}
        maxLines={1}
        width={maxLabelWidth * 3}
      >
        {labelValue}
      </Text>
    )
  }

  // tick property definition: (props: any) => ReactElement<SVGElement>
  const CustomizedYLabel = ({
    index,
    payload,
    value,
    textAnchor,
    verticalAnchor,
    orientation,
    type,
    width,
    height,
    x,
    y,
    className,
  }: any): ReactElement<SVGElement> => {
    const labelValue = payload ? payload.value : value
    return (
      <Text
        textAnchor={textAnchor}
        verticalAnchor={verticalAnchor}
        orientation={orientation}
        type={type}
        width={width}
        height={height}
        x={x}
        y={y}
        className={className}
        transform={`translate(${maxTextWidth + BAR_AXIS_SPACE}, 0)`}
        fill={getColor(index)}
        fontWeight={600}
      >
        {labelValue !== ''
          ? `${labelValue.toLocaleString()}%`
          : data[index].display ?? '-'}
      </Text>
    )
  }

  const getChartType = (): ReactNode => {
    if (isVertical) {
      return [
        <XAxis key={0} hide axisLine={false} type='number' domain={[0, 100]} />,
        <YAxis
          key={1}
          yAxisId={0}
          dataKey={xKey}
          type='category'
          axisLine={false}
          tickLine={false}
          tick={CustomizedLabel}
          interval={0}
        />,
        <YAxis
          key={2}
          orientation='right'
          yAxisId={1}
          dataKey={yKey}
          type='category'
          axisLine={false}
          tickLine={false}
          tick={CustomizedYLabel}
          interval={multiBars ? 0 : 'preserveStart'}
          mirror
        />,
      ]
    }
    margin = {top: 25, right: 0, left: 0, bottom: 25}
    return [<XAxis dataKey={xKey} dy='25' />, <YAxis hide />]
  }

  const buildChartType = getChartType()

  return (
    <ResponsiveContainer
      width='100%'
      height={barSize * data.length + 14}
      debounce={50}
    >
      <ReBarChart
        data={data}
        layout={layout}
        margin={margin}
        className={classNames({'font-number': withNumericAxis})}
      >
        {buildChartType}
        <Bar
          dataKey={yKey}
          minPointSize={2}
          barSize={barSize}
          label={isVertical ? undefined : CustomizedLabel}
        >
          {data.map((entry, idx) => (
            <Cell key={entry[xKey]} fill={getColor(idx)} />
          ))}
          <LabelList
            position={isVertical ? 'right' : 'top'}
            dataKey={leyendKey}
            width={200}
          />
        </Bar>
      </ReBarChart>
    </ResponsiveContainer>
  )
}

export default BarChart
