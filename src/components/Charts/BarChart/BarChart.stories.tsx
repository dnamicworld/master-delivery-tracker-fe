import {object} from '@storybook/addon-knobs'
import {Story} from '@storybook/react'

import {BarChart, BarChartProps} from '..'

interface ExtendedBarchartProps extends Omit<BarChartProps, 'data'> {}

const defaultData: BarChartProps['data'] = [
  {
    name: 'Albums delivered',
    pv: 65,
  },
]

export default {
  title: 'BarChart',
  component: BarChart,
  args: {
    barSize: 8,
    xKey: 'name',
    yKey: 'pv',
  },
  argTypes: {
    layout: {
      options: ['horizontal', 'vertical', 'centric', 'radial'],
      control: {type: 'select'},
    },
    withNumericAxis: {
      control: {type: 'boolean'},
    },
    colorIndex: {
      options: [0, 1],
      control: {type: 'select'},
    },
  },
}

const Template: Story<ExtendedBarchartProps> = ({...args}) => {
  const chartData = object('Data list', defaultData)
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <BarChart data={chartData} {...args} />
  )
}

export const Default = Template.bind({})

Default.args = {
  layout: 'vertical',
  withNumericAxis: true,
  colorIndex: 0,
}
