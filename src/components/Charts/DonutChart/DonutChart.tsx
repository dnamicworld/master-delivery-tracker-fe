import {Pie, PieChart} from 'recharts'

import {CHART_COLORS, CHART_BLANK, GRAY_COLOR} from '../../../utils/constants'

import styles from './DonutChart.module.scss'

export interface DonutChartProps {
  data: {[key: string]: string | number}
  text?: string
  size?: number
  thickness?: number
  yKey?: string
  xKey?: string
  index?: number
  withLegend?: boolean
  bigText?: boolean
}

const defaultTextProps = {
  numberTextSize: 62,
  percentageTextSize: 23,
  xTextSpace: 0,
  yTextSpace: 0,
  YLabelSpace: 0,
}

const bigTextProps = {
  numberTextSize: 74,
  percentageTextSize: 30,
  xTextSpace: 15,
  yTextSpace: 10,
  YLabelSpace: 10,
}

const DonutChart = ({
  data,
  text = '',
  size = 150,
  thickness = 1,
  yKey = 'name',
  xKey = 'value',
  index = 0,
  bigText,
  withLegend,
}: DonutChartProps) => {
  const center = (size - 10) / 2
  const maxRadius = size / 2 - 1
  const chartLabel = data[yKey]
  const chartValue = data[xKey]
  const chartColor = CHART_COLORS[index]
  let percentagePosition
  if (chartValue === 100) {
    percentagePosition = 55
  } else {
    percentagePosition = chartValue > 9 ? 45 : 28
  }
  const customTextProps = bigText ? bigTextProps : defaultTextProps
  const donutData = [
    {name: chartLabel, value: chartValue, fill: chartColor},
    {name: 'Blank', value: 100 - Number(chartValue), fill: CHART_BLANK},
  ]
  const textProps = {
    display: withLegend ? 'initial' : 'none',
    textAnchor: 'middle',
    dx: 5,
    dy: 10 + customTextProps.yTextSpace,
    x: center - 2,
    y: center,
    fontSize: bigText ? 16 : 11,
  }

  return (
    <PieChart width={size} height={size} className={styles.donutChart}>
      <text
        className='font-weight-light percentage-number'
        display={textProps.display}
        textAnchor={textProps.textAnchor}
        dx={textProps.dx}
        dy={textProps.dy}
        x={textProps.x}
        fill={chartColor}
        fontSize={customTextProps.numberTextSize}
        y={center + 4}
      >
        {chartValue}
      </text>
      <text
        display={textProps.display}
        textAnchor={textProps.textAnchor}
        dx={textProps.dx}
        dy={textProps.dy}
        fill={chartColor}
        fontSize={customTextProps.percentageTextSize}
        x={center + percentagePosition + customTextProps.xTextSpace - 2}
        y={center - 27 - customTextProps.yTextSpace + 4}
      >
        %
      </text>
      <text
        display={textProps.display}
        textAnchor={textProps.textAnchor}
        dx={textProps.dx}
        x={textProps.x}
        y={textProps.y}
        fontSize={textProps.fontSize}
        fill={GRAY_COLOR}
        className='font-weight-sbold'
        dy={30 + customTextProps.yTextSpace + customTextProps.YLabelSpace}
      >
        {chartLabel}
      </text>
      <text
        display={textProps.display}
        textAnchor={textProps.textAnchor}
        dx={textProps.dx}
        x={textProps.x}
        y={textProps.y}
        fontSize={textProps.fontSize}
        fill={GRAY_COLOR}
        className='font-weight-sbold'
        dy={42 + customTextProps.yTextSpace + customTextProps.YLabelSpace}
      >
        {text}
      </text>
      <Pie
        data={donutData}
        dataKey='value'
        cx={center}
        cy={center}
        outerRadius={maxRadius}
        innerRadius={maxRadius - thickness}
        startAngle={450}
        endAngle={90}
        blendStroke
      />
    </PieChart>
  )
}

export default DonutChart
