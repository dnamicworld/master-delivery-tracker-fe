import {object} from '@storybook/addon-knobs'
import {Story} from '@storybook/react'

import {DonutChart, DonutChartProps} from '..'

interface ExtendedDonutChartProps extends Omit<DonutChartProps, 'data'> {}

const defaultData: DonutChartProps['data'] = {
  name: 'Albums',
  value: 65,
}

export default {
  title: 'DonutChart',
  component: DonutChart,
  args: {
    text: 'Total Delivery',
    size: 150,
    thickness: 1,
  },
  argTypes: {
    bigText: {
      control: {type: 'boolean'},
    },
    withLegend: {
      control: {type: 'boolean'},
    },
    index: {
      options: [0, 1],
      control: {type: 'select'},
    },
  },
}

const Template: Story<ExtendedDonutChartProps> = ({...args}) => {
  const chartData = object('Data list', defaultData)
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <DonutChart data={chartData} {...args} />
  )
}

export const Default = Template.bind({})

Default.args = {
  bigText: false,
  withLegend: true,
  index: 0,
}
