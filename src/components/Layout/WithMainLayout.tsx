import {ComponentType, ReactNode} from 'react'
import {Row} from 'react-bootstrap'
import {RouteComponentProps} from 'react-router-dom'

import {AppNavbar, SWRConfig} from '..'
import {Grid, Footer} from '../../DS/components'
import ContextWrapper from '../../context/contextWrapper'
import {AppContextProvider} from '../../context/coreProvider'
import {CreateReportProvider} from '../../context/createReport'
import {UserContextProvider} from '../../context/user'
import SearchComponent from '../Search/SearchComponent'

export interface WithMainLayoutProps extends RouteComponentProps {
  children: ReactNode
}

function WithMainLayout<T extends WithMainLayoutProps = WithMainLayoutProps>(
  WrappedComponent: ComponentType<T>,
) {
  // Try to create a nice displayName for React Dev Tools.
  const displayName =
    WrappedComponent.displayName || WrappedComponent.name || 'Component'

  const ComponentWithTheme = (props: Omit<T, keyof WithMainLayoutProps>) => (
    <SWRConfig>
      <UserContextProvider>
        <Grid>
          <ContextWrapper>
            <CreateReportProvider>
              <AppNavbar />
              <Row className='layoutWrapper mb-2'>
                <SearchComponent />
                <Grid>
                  <AppContextProvider>
                    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                    <WrappedComponent {...(props as T)} />
                  </AppContextProvider>
                </Grid>
              </Row>
              <Footer />
            </CreateReportProvider>
          </ContextWrapper>
        </Grid>
      </UserContextProvider>
    </SWRConfig>
  )

  ComponentWithTheme.displayName = `withTheme(${displayName})`

  return ComponentWithTheme
}

export default WithMainLayout
