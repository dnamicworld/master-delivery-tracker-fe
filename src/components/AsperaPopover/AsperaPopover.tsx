import {CSSProperties, useState} from 'react'
import {CopyToClipboard} from 'react-copy-to-clipboard'
import {Button, Icon, Tooltip} from 'src/DS/components'

import {
  ASPERA_URL,
  ASPERA_BUTTON_LABEL,
  FA_ICONS,
  ICON_TYPES,
} from '../../utils/constants'

const popOverStyle: CSSProperties = {width: 210}

const AsperaPopover = () => {
  const [copied, setCopied] = useState(false)
  return (
    <>
      <div className='m-3 text-center' style={popOverStyle}>
        <div className='text-center h4'>Upload to Aspera</div>
        <br />
        <CopyToClipboard text={ASPERA_URL} onCopy={() => setCopied(true)}>
          <Button
            className='mr-10'
            colorType='secondary-white'
            containerStyle='outline'
            bsStyle='secondary-black'
          >
            {copied ? (
              <>
                <Icon
                  type={ICON_TYPES.fas}
                  iconName={FA_ICONS.badgeCheck}
                  className='mr-5 text-success'
                />
                <span>{ASPERA_BUTTON_LABEL.POST_COPY}</span>
              </>
            ) : (
              ASPERA_BUTTON_LABEL.PRE_COPY
            )}
          </Button>
        </CopyToClipboard>
        <Tooltip
          id='tooltip'
          placement='bottom'
          tooltipChildren={ASPERA_BUTTON_LABEL.UPLOAD_TOOLTIP}
        >
          <a
            href={ASPERA_URL}
            target='_blank'
            className='ml-5'
            rel='noreferrer'
          >
            <Button colorType='primary' label={ASPERA_BUTTON_LABEL.UPLOAD} />
          </a>
        </Tooltip>
      </div>
    </>
  )
}

export default AsperaPopover
