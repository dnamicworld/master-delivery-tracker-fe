import classnames from 'classnames'
import {PRODUCT_INDEXES} from 'src/utils/constants'

import {ListViewType} from '../../models'
import {isUndefined} from '../../utils/helpers'
import {DonutChart, DonutChartProps} from '../Charts'

interface AssetChartProps extends Pick<DonutChartProps, 'text' | 'data'> {
  big?: boolean
  index?: number
  listViewType?: keyof typeof ListViewType
}

const AssetChart = ({
  big,
  data,
  index = 0,
  listViewType,
  text,
}: AssetChartProps) => (
  <div
    className={classnames({
      'py-8': !big,
    })}
  >
    <DonutChart
      size={big ? 210 : undefined}
      index={!isUndefined(listViewType) ? PRODUCT_INDEXES[listViewType] : index}
      data={data}
      text={text}
      bigText={big}
      withLegend
    />
  </div>
)

export default AssetChart
