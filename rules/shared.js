module.exports = {
  ignorePatterns: ['lib/*', 'dist/*', 'documentation/*'],
  rules: {
    strict: 'error',
    'react/react-in-jsx-scope': 'off',
    'import/no-extraneous-dependencies': ['error', {devDependencies: true}],
    'prettier/prettier': 'error',
    'import/order': [
      'error',
      {
        groups: [
          'builtin',
          'external',
          'internal',
          'parent',
          'sibling',
          'index',
        ],
        alphabetize: {
          order: 'asc',
        },
        'newlines-between': 'always-and-inside-groups',
      },
    ],
    'import/prefer-default-export': 'off',
    'no-continue': 'off',
    'no-restricted-syntax': 'off',
    'import/no-cycle': 'off',
    'prefer-destructuring': [
      'error',
      {
        VariableDeclarator: {
          array: false,
          object: true,
        },
        AssignmentExpression: {
          array: false,
          object: false,
        },
      },
      {
        enforceForRenamedProperties: false,
      },
    ],
  },
  plugins: ['prettier'],
  env: {
    browser: true,
    node: true,
  },
}
